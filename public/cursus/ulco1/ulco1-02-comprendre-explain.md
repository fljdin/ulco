---
revision: 24.12
date: "Année académique 2024-2025"

licence : "Creative Commons BY-NC-SA"
include_licence : CC-BY-NC-SA-2.0-FR

author: © 2005-2025 DALIBO SARL SCOP

trademarks: |
  Le logo éléphant de PostgreSQL (« Slonik ») est une création sous copyright et
  le nom « PostgreSQL » est une marque déposée par PostgreSQL Community Association
  of Canada.

##
## PDF Options
##

## Limiter la profondeur de la table des matières
toc-depth: 2

## Mettre les lien http en pieds de page
links-as-notes: false

## Police plus petite dans un bloc de code

code-blocks-fontsize: small

## Filtre : pandoc-latex-admonition
## les catégories `important` et `warning` sont synonymes
## même chose pour `tip` et `note`
pandoc-latex-admonition:
  - color: Red
    classes: [warning]
    linewidth: 4
  - color: Red
    classes: [important]
    linewidth: 4
  - color: DarkSeaGreen
    classes: [tip]
    linewidth: 4
  - color: DarkSeaGreen
    classes: [note]
    linewidth: 4
  - color: DodgerBlue
    classes: [slide-content]
    linewidth: 4

##
## Reveal Options
##

## Taille affichage
width: 960
height: 700

## beige/blood/moon/simple/solarized/black/league/night/serif/sky/white
theme: white

## None - Fade - Slide - Convex - Concave - Zoom
transition: None
transitionSpeed: fast

## Barre de progression
progress: true

## Affiche N° de slide
slideNumber: true

## Le numero de slide apparait dans la barre d'adresse
history: true

## Defilement des slides avec la roulette
mouseWheel: true

## Annule la transformation uppercase de certains thèmes
title-transform : none

## Cache l'auteur sur la première slide
## Mettre en commentaire pour désactiver
hide_author_in_slide: true

title : "Savoir lire un plan d'exécution"
subtitle : 'FCU Calais - M1 I2L'
---

# Introduction aux plans d'exécution

![PostgreSQL](medias/logos/slonik.png){ height=500 }
\

<div class="notes">
</div>

---

## Introduction

<div class="slide-content">

  * Qu'est-ce qu'un plan d'exécution ?
  * Quels outils peuvent aider

</div>

<div class="notes">

Ce module a pour but de faire une présentation très rapide de l'optimiseur
et des plans d'exécution. Il contient surtout une introduction sur la commande
`EXPLAIN` et sur différents outils en relation.

</div>

---

### Au menu

<div class="slide-content">
  * Exécution globale d'une requête
  * Optimiseur
  * `EXPLAIN`
  * Nœuds d'un plan
  * Outils
</div>

<div class="notes">

</div>

---

### Niveau SGBD

![Traitement d'une requête SQL](medias/common/traitement-requete.png){ height=660 }
\

<div class="notes">

<!-- Paragraphe repris dans J0 J2 -->

<!--
 Ceci est en doublon dans J0 (l'intro très rapide de DEVPG)
 et dans J2 (DEVPG aussi mais aussi PERF2)
 
 dans les notes du schéma de traitement des requêtes
 
-->

Lorsque le serveur récupère la requête, un ensemble de traitements est
réalisé.

Tout d'abord, le _parser_ va réaliser une analyse syntaxique de la requête.

Puis le _rewriter_ va réécrire, si nécessaire, la requête. Pour cela, il prend
en compte les règles, les vues non matérialisées et les fonctions SQL.

Si une règle demande de changer la requête, la requête envoyée est remplacée
par la nouvelle.

Si une vue non matérialisée est utilisée, la requête qu'elle contient est
intégrée dans la requête envoyée. Il en est de même pour une fonction SQL
intégrable.

Ensuite, le _planner_ va générer l'ensemble des plans d'exécutions. Il calcule
le coût de chaque plan, puis il choisit le plan le moins coûteux, donc le plus
intéressant.

Enfin, l'_executer_ exécute la requête.

Pour cela, il doit commencer par récupérer les verrous nécessaires sur les
objets ciblés. Une fois les verrous récupérés, il exécute la requête.

Une fois la requête exécutée, il envoie les résultats à l'utilisateur.

Plusieurs goulets d'étranglement sont visibles ici. Les plus importants sont :

  * la planification (à tel point qu'il est parfois préférable de ne générer
    qu'un sous-ensemble de plans, pour passer plus rapidement à la phase
    d'exécution) ;
  * la récupération des verrous (une requête peut attendre plusieurs secondes,
    minutes, voire heures, avant de récupérer les verrous et exécuter
    réellement la requête) ;
  * l'exécution de la requête ;
  * l'envoi des résultats à l'utilisateur.

<!-- Fin doublon avec J2 -->

<!-- résumé du slide suivant dans J2 -->
En général, le principal souci pour les performances sur ce type d'instructions est donc
l'obtention des verrous et l'exécution réelle de la requête.
Il existe quelques ordres (comme `TRUNCATE` ou `COPY`) exécutés beaucoup
plus directement.

</div>

---

## Optimiseur

<div class="slide-content">

  * SQL est un langage déclaratif
  * Une requête décrit le résultat à obtenir
    + mais pas la façon pour l'obtenir
  * C'est à l'optimiseur de déduire le moyen de parvenir au résultat demandé : comment ?

</div>

<div class="notes">

Les moteurs de base de données utilisent un langage SQL
qui permet à l'utilisateur de décrire le résultat qu'il souhaite obtenir,
mais pas la manière. C'est à la base de données de se débrouiller pour obtenir ce
résultat le plus rapidement possible.

</div>

---

### Principe de l'optimiseur

<div class="slide-content">

Le modèle vise à minimiser un coût :

  * Énumérer tous les plans d'exécution
    + ou presque tous…
  * Statistiques + configuration + règles → coût calculé
  * Coût le plus bas = meilleur plan

</div>

<div class="notes">

<!-- développé un peu plus dans J2 -->

Le but de l'optimiseur est assez simple. Pour une requête, il existe de
nombreux plans d'exécution possibles. Il va donc énumérer tous les plans
d'exécution possibles (sauf si cela représente vraiment trop de plans auquel
cas, il ne prendra en compte qu'une partie des plans possibles).

Pour calculer le « coût » d'un plan, PostgreSQL
dispose d'informations sur les données (des statistiques), d'une configuration
(réalisée par l'administrateur de bases de données) et d'un ensemble de règles
inscrites en dur.

À la fin de l'énumération et du calcul de coût, il ne lui reste plus qu'à
sélectionner le plan qui a le plus petit coût.

<div class="box tip">
Le coût d'un plan est une valeur calculée sans unité ni signification physique.
</div>

</div>

---

### Exemple de requête et son résultat

<div class="slide-content">

```sql
SELECT nom, prenom, num_service
FROM employes
WHERE nom LIKE 'B%'
ORDER BY num_service;
```
```default
    nom    |  prenom  | num_service
-----------+----------+-------------
 Berlicot  | Jules    |           2
 Brisebard | Sylvie   |           3
 Barnier   | Germaine |           4
```

</div>

<div class="notes">

La requête en exemple permet de récupérer des informations sur tous les
employés dont le nom commence par la lettre B en triant les employés par leur
service.

Un moteur de bases de données peut récupérer les données de plusieurs façons :

  * faire un parcours séquentiel de la table `employes` en filtrant les
    enregistrements d'après leur nom, puis trier les données grâce à un
    algorithme ;
  * faire un parcours d'index (s'il y en a un) sur la colonne `nom` pour
    trouver plus rapidement les enregistrements de la table `employes`
    satisfaisant le filtre `'B%'`, puis trier les données grâce à un
    algorithme ;
  * faire un parcours d'index sur la colonne `num_service` pour récupérer les
    enregistrements déjà triés par service, et ne retourner que ceux vérifiant
    le prédicat `nom like 'B%'`.

Et ce ne sont que quelques exemples, car il serait possible d'avoir un index
utilisable à la fois pour le tri et le filtre par exemple.

Donc la requête décrit le résultat à obtenir, et le planificateur va chercher
le meilleur moyen pour parvenir à ce résultat.
Pour ce travail, il dispose d'un certain nombre d'opérations de base. Ces
opérations travaillent sur des ensembles de lignes, généralement un ou deux.
Chaque opération renvoie un seul ensemble de lignes. Le planificateur peut
combiner ces opérations suivant certaines règles. Une opération peut renvoyer
l'ensemble de résultats de deux façons : d'un coup (par exemple le tri) ou
petit à petit (par exemple un parcours séquentiel). Le premier cas utilise
plus de mémoire, et peut nécessiter d'écrire des données temporaires sur
disque. Le deuxième cas aide à accélérer des opérations comme les curseurs,
les sous-requêtes `IN` et `EXISTS`, la clause `LIMIT`, etc.

</div>

---

### Décisions de l'optimiseur

<!-- Version raccourcie d'un slide de J2 -->

<div class="slide-content">

  * Comment accéder aux lignes ?
    + parcours de table, d'index, de fonction, etc.
  * Comment joindre les tables ?
    + ordre
    + type
  * Comment agréger ?
    + brut, tri, hachage…

</div>

<div class="notes">

<!-- Ne pas détailler ici -->

Pour exécuter une requête, le planificateur va utiliser des opérations. Pour
lire des lignes, il peut utiliser un parcours de table (une lecture complète du
fichier), un parcours d'index ou
encore d'autres types de parcours. Ce sont généralement les premières
opérations utilisées.

Pour joindre les tables, l'ordre dans lequel ce sera fait est très
important. Pour la jointure elle-même, il existe plusieurs méthodes différentes.
Il existe aussi plusieurs algorithmes d'agrégation de lignes.
Un tri peut être nécessaire pour une jointure, une agrégation, ou
pour un `ORDER BY`, et là encore il y a plusieurs algorithmes
possibles, ou des techniques pour éviter de le faire.

</div>

---

## Mécanisme de calcul de coûts

<div class="slide-content">

  * Chaque opération a un coût :
    + lire un bloc selon sa position sur le disque
    + manipuler une ligne
    + appliquer un opérateur
    + …
  * et généralement un paramètre associé

</div>

<div class="notes">

L'optimiseur statistique de PostgreSQL utilise un modèle de calcul de coût.
Les coûts calculés sont des indications arbitraires de la charge nécessaire
pour répondre à une requête. Chaque facteur de coût représente une unité de
travail : lecture d'un bloc, manipulation d'une ligne en mémoire, application
d'un opérateur sur un champ.

</div>

---

### Statistiques

<!-- C'est répété dans J2 -->

<div class="slide-content">

  * Connaître le coût de traitement d'une ligne est bien
    + mais combien de lignes à traiter ?
  * Statistiques sur les données
    + mises à jour : `ANALYZE`
  * Sans bonnes statistiques, pas de bons plans !

</div>

<div class="notes">

Connaître le coût unitaire de traitement d'une ligne est une bonne
chose, mais si on ne sait pas le nombre de lignes à traiter, on ne peut pas
calculer le coût total.
L'optimiseur a donc besoin de statistiques sur les données, comme par exemple
le nombre de blocs et de lignes d'une table, les valeurs les plus fréquentes
et leur fréquence pour chaque colonne de chaque table.
Les statistiques sur les données sont calculées lors de l'exécution de la
commande SQL `ANALYZE`.

Généralement c'est l'autovacuum qui rafraîchit régulièrement les statistiques.

Sinon c'est au propriétaire de la table ou au superutilisateur de le faire manuellement.
À partir de PostgreSQL 17, un utilisateur de maintenance dédié,
sans droit de lecture sur les tables, peut lancer ces commandes,
soit sur toutes les tables s'il a le rôle `pg_maintain`,
soit sur les tables où on lui aura octroyé un `GRANT MAINTAIN`.

<div class="box warning">
Des statistiques périmées ou pas assez fines sont une source fréquente de plans non optimaux !
</div>

</div>

---

### Exemple - parcours d'index

<div class="slide-content">

```sql
CREATE TABLE t1 (c1 integer, c2 integer);
INSERT INTO t1 SELECT i, i FROM generate_series(1, 1000) i;
CREATE INDEX ON t1(c1);
ANALYZE t1;
```
```sql
EXPLAIN SELECT * FROM t1 WHERE c1=1 ;
```
```default
                         QUERY PLAN
---------------------------------------------------------------
Index Scan using t1_c1_idx on t1 (cost=0.28..8.29 rows=1 width=8)
  Index Cond: (c1 = 1)
```

</div>

<div class="notes">

L'exemple crée une table et lui ajoute 1000 lignes. Chaque ligne a une valeur
différente dans les colonnes `c1` et `c2` (de 1 à 1000).
```sql
SELECT * FROM t1 ;
```
```default
  c1  |  c2  
------+------
    1 |    1
    2 |    2
    3 |    3
    4 |    4
    5 |    5
    6 |    6
…
  996 |  996
  997 |  997
  998 |  998
  999 |  999
 1000 | 1000
(1000 lignes)
```

Dans cette requête :

```sql
EXPLAIN SELECT * FROM t1 WHERE c1=1 ;
```

nous savons qu'un `SELECT`
filtrant sur la valeur 1 pour la colonne `c1` ne ramènera qu'une ligne. Grâce
aux statistiques relevées par la commande `ANALYZE` exécutée juste avant,
l'optimiseur estime lui aussi qu'une seule ligne sera récupérée. Une ligne sur
1000, c'est un bon ratio pour faire un parcours d'index. C'est donc ce que
recommande l'optimiseur.

</div>

---

### Exemple - parcours de table

<div class="slide-content">

```sql
UPDATE t1 SET c1=1 ;   /* 1000 lignes identiques */

ANALYZE t1 ;           /* ne pas oublier ! */
```
```sql
EXPLAIN SELECT * FROM t1 WHERE c1=1;
```
```default
                    QUERY PLAN
------------------------------------------------------
 Seq Scan on t1  (cost=0.00..21.50 rows=1000 width=8)
   Filter: (c1 = 1)
```

</div>

<div class="notes">

La même table, mais avec 1000 lignes ne contenant plus que la valeur 1. Un `SELECT`
filtrant sur cette valeur 1 ramènera dans ce cas toutes les lignes.
L'optimiseur s'en rend compte et décide qu'un parcours séquentiel de la table
est préférable à un parcours d'index. C'est donc ce que
recommande l'optimiseur.

Dans cet exemple, l'ordre `ANALYZE` garantit que les statistiques sont à jour
(le démon autovacuum n'est pas forcément assez rapide).


</div>

---

### Exemple - parcours d'index forcé

<div class="slide-content">

```sql
SET enable_seqscan TO off ;

EXPLAIN SELECT * FROM t1 WHERE c1=1;
```
```default
                         QUERY PLAN
---------------------------------------------------------------
Index Scan using t1_c1_idx on t1 (cost=0.28..57.77 rows=1000 width=8)
  Index Cond: (c1 = 1)
```
```sql
RESET enable_seqscan ;
```

</div>

<div class="notes">

Le coût du parcours de table était de 21,5 pour la récupération des 1000
lignes, donc un coût bien supérieur au coût du parcours d'index, qui lui était
de 8,29, mais pour une seule ligne. On pourrait se demander le coût du
parcours d'index pour 1000 lignes. À titre expérimental, on peut désactiver (ou plus
exactement désavantager) le parcours de table en configurant le paramètre
`enable_seqscan` à `off`.

En faisant cela, on s'aperçoit que le plan passe finalement par un parcours
d'index, tout comme le premier. Par contre, le coût n'est plus de 8,29, mais
de 57,77, donc supérieur au coût du parcours de table.
<!-- NB: l index a grossi à cause de l'UPDATE : avec un REINDEX, coût descned à 37.65 -->
C'est pourquoi
l'optimiseur avait d'emblée choisi un parcours de table. Un index n'est pas
forcément le chemin le plus court.

</div>

---

## Qu'est-ce qu'un plan d'exécution ?

<div class="slide-content">

  * Représente les différentes opérations pour répondre à la requête
  * Sous forme arborescente
  * Composé des nœuds d'exécution
  * Plusieurs opérations simples mises bout à bout

</div>

<div class="notes">

L'optimiseur transforme une grosse action (exécuter une requête) en plein de petites actions unitaires (trier un ensemble de données, lire une table, parcourir un index, joindre deux ensembles de données, etc). Ces petites actions sont liées les unes aux autres. Par exemple, pour exécuter cette requête :

```sql
SELECT * FROM une_table ORDER BY une_colonne;
```

peut se faire en deux actions :

  * récupérer les enregistrements de la table ;
  * trier les enregistrements provenant de la lecture de la table.

Mais ce n'est qu'une des possibilités.

</div>

---

### Nœud d'exécution

<div class="slide-content">

  * Nœud
    + opération simple : lectures, jointures, tris, etc.
    + unité de traitement
    + produit et consomme des données
  * Enchaînement des opérations
    + chaque nœud produit les données consommées par le nœud parent
    + le nœud final retourne les données à l'utilisateur

</div>

<div class="notes">

Les nœuds correspondent à des unités de traitement qui réalisent des
opérations simples sur un ou deux ensembles de données : lecture d'une table,
jointures entre deux tables, tri d'un ensemble, etc. Si le plan d'exécution
était une recette, chaque nœud serait une étape de la recette.

Les nœuds peuvent produire et consommer des données.

</div>

---

### Récupérer un plan d'exécution

<div class="slide-content">

  * Commande `EXPLAIN`
    + suivi de la requête complète
  * Uniquement le plan finalement retenu

</div>

<div class="notes">

Pour récupérer le plan d'exécution d'une requête, il suffit d'utiliser la
commande `EXPLAIN`. Cette commande est suivie de la requête pour laquelle on
souhaite le plan d'exécution.

Seul le plan sélectionné est affichable. Les plans ignorés du fait de leur
coût trop important ne sont pas récupérables. Ceci est dû au fait que les
plans en question peuvent être abandonnés avant d'avoir été totalement
développés si leur coût partiel est déjà supérieur à celui de plans déjà
considérés.

</div>

---

### Exemple de requête

<div class="slide-content">

```sql
EXPLAIN SELECT * FROM t1  WHERE c2<10 ORDER BY c1;
```

</div>

<div class="notes">

Cette requête va récupérer tous les enregistrements de t1 pour lesquels la
valeur de la colonne c2 est inférieure à 10. Les enregistrements sont triés
par rapport à la colonne c1.

</div>

---

### Plan pour cette requête

<div class="slide-content">

```default
                       QUERY PLAN
---------------------------------------------------------
 Sort  (cost=21.64..21.67 rows=9 width=8)
   Sort Key: c1
   ->  Seq Scan on t1  (cost=0.00..21.50 rows=9 width=8)
         Filter: (c2 < 10)
```
</div>

<div class="notes">

L'optimiseur envoie ce plan à l'exécuteur. Ce dernier voit qu'il a une
opération de tri à effectuer (nœud `Sort`). Pour cela, il a besoin de données
que le nœud suivant va lui donner. Il commence donc l'opération de lecture
(nœud `SeqScan`). Il envoie chaque enregistrement valide au nœud `Sort` pour que
ce dernier les trie.

Chaque nœud dispose d'un certain nombre d'informations placées soit sur la
même ligne entre des parenthèses, soit sur la ou les lignes du dessous. La
différence entre une ligne de nœud et une ligne d'informations est que la
ligne de nœud contient une flèche au début (`->`). Par exemple, le nœud `Sort`
contient des informations entre des parenthèses et une information
supplémentaire sur la ligne suivante indiquant la clé de tri (la colonne
`c1`). Par contre, la troisième ligne n'est pas une ligne d'informations du
nœud `Sort` mais un nouveau nœud (`SeqScan`).

</div>

---

### Informations sur la ligne nœud

<div class="slide-content">

```default
->  Seq Scan on t1  (cost=0.00..21.50 rows=9 width=8)
      Filter: (c2 < 10)
```

  * `cost` : coûts de récupération
    + de la **première** ligne
    + de **toutes** les lignes
  * `rows`
    + nombre de lignes en sortie du nœud
  * `width`
    + largeur moyenne d'un enregistrement (octets)

</div>

<div class="notes">
<!-- même paragraphe dans J2 -->
Chaque nœud montre les coûts estimés dans le premier groupe de parenthèses.
`cost` est un couple de deux coûts :
la première valeur correspond au coût pour récupérer la première ligne
(souvent nul dans le cas d'un parcours séquentiel) ;
la deuxième valeur correspond au coût pour récupérer toutes les lignes
(elle dépend essentiellement de la taille de la table lue, mais
aussi d'opération de filtrage).
`rows` correspond au nombre de lignes que le planificateur pense récupérer
à la sortie de ce nœud. Dans le cas d'une nouvelle table traitée par
`ANALYZE`, les versions antérieures à la version 14 calculaient une valeur
probable du nombre de lignes en se basant sur la taille moyenne d'une ligne
et sur une table faisant 10 blocs. La version 14 corrige cela en ayant une
meilleure idée du nombre de lignes d'une nouvelle table.
`width` est la largeur en octets de la ligne.

<!--
Redefine pg_class.reltuples to be -1 before the first VACUUM or ANALYZE.
https://git.postgresql.org/gitweb/?p=postgresql.git;a=commit;h=3d351d916b20534f973eda760cde17d96545d4c4
-->

</div>

---

### Informations sur les lignes suivantes

<div class="slide-content">

```default
Sort  (cost=21.64..21.67 rows=9 width=8)
  Sort Key: c1
Seq Scan on t1  (cost=0.00..21.50 rows=9 width=8)
  Filter: (c2 < 10)
```

  * `Sort`
    + `Sort Key` : clé de tri
  * `Seq Scan`
    + `Filter` : filtre (si besoin)
  * Dépend
    + du type de nœud
    + des options de `EXPLAIN`
    + des paramètres de configuration
    + de la version de PostgreSQL

</div>

<div class="notes">

Les informations supplémentaires dépendent de beaucoup d'éléments. Elles
peuvent différer suivant le type de nœud, les options de la commande
`EXPLAIN`, et certains paramètres de configuration. De même la version de
PostgreSQL joue un rôle majeur : les nouvelles versions peuvent apporter des
informations supplémentaires pour que le plan soit plus lisible et que
l'utilisateur soit mieux informé.

</div>

---

<!--

  Ne parler ici que de ANALYZE et des options conseillées, le reste dans slide en vrac

-->

### EXPLAIN ( ANALYZE )

<div class="slide-content">

```sql
EXPLAIN (ANALYZE)  /* exécution !! */
SELECT   *   FROM t1   WHERE c2<10   ORDER BY c1;
```
```default
                        QUERY PLAN
---------------------------------------------------------------
 Sort  (cost=21.64..21.67 rows=9 width=8)
       (actual time=0.493..0.498 rows=9 loops=1)
   Sort Key: c1
   Sort Method: quicksort  Memory: 25kB
   ->  Seq Scan on t1  (cost=0.00..21.50 rows=9 width=8)
               (actual time=0.061..0.469 rows=9 loops=1)
         Filter: (c2 < 10)
         Rows Removed by Filter: 991
 Planning Time: 0.239 ms
 Execution Time: 0.606 ms
```

</div>

<div class="notes">

Le but de cette option est d'obtenir les informations sur l'exécution réelle
de la requête.

<div class="box warning">

Avec `ANALYZE`, la requête est réellement exécutée ! Attention
donc aux `INSERT`/`UPDATE`/`DELETE`. N'oubliez pas non plus qu'un `SELECT` peut
appeler des fonctions qui écrivent dans la base.
Dans le doute, pensez à englober l'appel dans une
transaction que vous annulerez après coup.

</div>

Quatre nouvelles informations apparaissent dans un nouveau bloc de
parenthèses. Elles sont toutes liées à l'exécution réelle de la requête :

  * `actual time`
  * la première valeur correspond à la durée en milliseconde pour récupérer la
    première ligne ;
  * la deuxième valeur est la durée en milliseconde pour récupérer toutes les
    lignes ;
  * `rows` est le nombre de lignes réellement récupérées ;
  * `loops` est le nombre d'exécutions de ce nœud, soit dans le cadre d'une
    jointure, soit dans le cadre d'une requête parallélisée.

<div class="box tip">

Multiplier la durée par le nombre de boucles pour obtenir la durée réelle
d'exécution du nœud !

</div>

L'intérêt de cette option est donc de trouver l'opération qui prend du temps
dans l'exécution de la requête, mais aussi de voir les différences entre les
estimations et la réalité (notamment au niveau du nombre de lignes).

</div>

---

### EXPLAIN (ANALYZE, BUFFERS)

<div class="slide-content">

```sql
EXPLAIN (ANALYZE, BUFFERS)
SELECT   *   FROM t1   WHERE c2<10   ORDER BY c1;
```
```default
                        QUERY PLAN
---------------------------------------------------------
 Sort  (cost=17.64..17.67 rows=9 width=8)
       (actual time=0.126..0.127 rows=9 loops=1)
   Sort Key: c1
   Sort Method: quicksort  Memory: 25kB
   Buffers: shared hit=3 read=5
   ->  Seq Scan on t1  (cost=0.00..17.50 rows=9 width=8)
                       (actual time=0.017..0.106 rows=9 loops=1)
         Filter: (c2 < 10)
         Rows Removed by Filter: 991
         Buffers: shared read=5
```
<!-- on ne veut pas parler ici de ce coût en planning lié en partie au fait que le psql a redémarré
   - v13.1
 Planning:
   Buffers: shared hit=43 read=7
 Planning Time: 0.261 ms
 Execution Time: 0.147 ms
-->
</div>

<div class="notes">

`BUFFERS` fait apparaître le nombre de blocs (_buffers_) impactés par chaque
nœud du plan d'exécution, en lecture comme en écriture.
<div class="box tip">
Il est conseillé de l'activer systématiquement. La quantité de blocs
manipulés par une requête est souvent surprenante, et souvent la cause
d'une mauvaise performance.
</div>

`shared read=5` en bas signifie que 5 blocs ont été trouvés et lus **hors** du cache
de PostgreSQL (_shared buffers_). 5 blocs est ici la taille de `t1` sur le disque.
Le cache de l'OS est peut-être intervenu, ce n'est pas visible ici.
Un peu plus haut, `shared hit=3 read=5` indique que 3 blocs ont été lus dans ce cache,
et 5 autres toujours hors du cache.
Les valeurs exactes dépendent donc de l'état du cache.
Si on relance la requête, pour une telle petite table, les relectures se feront
uniquement en `shared hit`.

`BUFFERS` compte aussi les blocs de fichiers ou tables temporaires
(`temp` ou `local`), ou les blocs écrits sur disque (`written`).
<!-- Il y a ceci dans J2 mais ici c'est trop
+----------------+---------------------------+---------------------------------+
| Informations   | Type d'objet concerné     | Explications                    |
+================+===========================+=================================+
| Shared hit     | Table ou index permanent  | Lecture d'un bloc dans le cache |
+----------------+---------------------------+---------------------------------+
| Shared read    | Table ou index permanent  | Lecture d'un bloc hors du cache |
+----------------+---------------------------+---------------------------------+
| Shared written | Table ou index permanent  | Écriture d'un bloc              |
+----------------+---------------------------+---------------------------------+
| Local hit      | Table ou index temporaire | Lecture d'un bloc dans le cache |
+----------------+---------------------------+---------------------------------+
| Local read     | Table ou index temporaire | Lecture d'un bloc hors du cache |
+----------------+---------------------------+---------------------------------+
| Local written  | Table ou index temporaire | Écriture d'un bloc              |
+----------------+---------------------------+---------------------------------+
| Temp read      | Tris et hachages          | Lecture d'un bloc               |
+----------------+---------------------------+---------------------------------+
| Temp written   | Tris et hachages          | Écriture d'un bloc              |
+----------------+---------------------------+---------------------------------+

-->

`EXPLAIN (ANALYZE, BUFFERS)` n'affiche que des données réelles, pas des estimations.
`EXPLAIN (BUFFERS)` sans `ANALYZE` peut être utilisé, mais
il ne montre que les blocs utilisés par la planification,
plutôt que accédés à l'exécution.
Ces blocs sont surtout des appels aux tables systèmes,
et sont moins nombreux, voire absents,
quand on appelle la requête une deuxième fois, ou plus,
dans la même session.

</div>

---

### EXPLAIN (ANALYZE, WAL)

<div class="slide-content">

```sql
EXPLAIN (ANALYZE, WAL)
INSERT INTO t1 SELECT i, i FROM generate_series(1,1000) i ;
```
```default
                      QUERY PLAN
----------------------------------------------------
 Insert on t1  (cost=0.00..10.00 rows=1000 width=8)
          (actual time=8.078..8.079 rows=0 loops=1)
   WAL: records=2017 fpi=3 bytes=162673
   ->  Function Scan on generate_series i
       (cost=0.00..10.00 rows=1000 width=8)
       (actual time=0.222..0.522 rows=1000 loops=1)
 Planning Time: 0.076 ms
 Execution Time: 8.141 ms
```

</div>

<div class="notes">

Désactivée par défaut et nécessitant l'option `ANALYZE`, l'option `WAL` permet
d'obtenir le nombre d'enregistrements et le nombre d'octets écrits dans les
journaux de transactions. (Rappelons que les écritures dans les fichiers de
données se font généralement plus tard, en arrière-plan.)
Il est conseillé de l'activer systématiquement, même pour les `SELECT`.

</div>

---

### EXPLAIN (ANALYZE, SERIALIZE)

<div class="slide-content">

<!-- exemple tiré de M4 -->
```sql
EXPLAIN (ANALYZE,VERBOSE,BUFFERS,SERIALIZE)
SELECT * FROM demotoast ;
```
```default
                            QUERY PLAN
-----------------------------------------------------------------------
 Seq Scan on public.demotoast  (cost=0.00..177.00 rows=1000 width=1196) (actual time=0.012..0.182 rows=1000 loops=1)
   Output: i, t, png, j
   Buffers: shared hit=167
 Query Identifier: 698565989149231362
 Planning Time: 0.055 ms
 Serialization: time=397.480 ms  output=307084kB  format=text
   Buffers: shared hit=14500
 Execution Time: 397.776 ms
```

  * PostgreSQL 17

</div>

<div class="notes">

Cette option, apparue avec PostgreSQL 17, force la « sérialisation » du résultat 
de la requête, et met ainsi en ainsi en évidence sa participation dansle temps 
d'exécution total de la requête et volumétrie de données a transférer.
Sans cette option, `EXPLAIN (ANALYZE)` peut afficher un résultat trop optimiste
par rapport aux requêtes réelles. L'exemple montre l'impact
de la lecture (pas forcément voulue) de la partie TOAST de la table,
où sont relégués les gros champs textes ou binaires, à cause
du `SELECT *`. Le coût réseau n'est toutefois pas inclus.

</div>

---

### EXPLAIN ( GENERIC_PLAN )

<div class="slide-content">

Quel plan générique pour les requêtes préparées ?

```sql
EXPLAIN (GENERIC_PLAN)
SELECT * FROM t1 WHERE c1 < $1 ;
```

  * PostgreSQL 16

</div>

<div class="notes">

L'option `GENERIC_PLAN` n'est malheureusement pas disponible avant PostgreSQL 16.
Elle est pourtant très pratique quand on cherche le plan d'une requête
préparée sans connaître ses paramètres, ou pour savoir quel est le plan
générique que prévoit PostgreSQL pour une requête préparée.

En effet, les plans des requêtes préparées ne sont pas forcément recalculés
à chaque appel avec les paramètres exacts (le système est assez complexe
et dépend du paramètre `plan_cache_mode`).
<!-- et on ne détaillera pas ici, et ça reste à faire dans J2  - FIXME -->
La requête ne peut être exécutée sans vraie valeur de paramètre,
donc l'option `ANALYZE` est inutilisable, mais
en activant `GENERIC_PLAN` on peut tout de même voir
le plan générique que PostgreSQL peut choisir
(`SUMMARY ON` affiche en plus le temps de planification) :

```sql
EXPLAIN (GENERIC_PLAN, SUMMARY ON)
SELECT * FROM t1 WHERE c1 < $1 ;
```
```default
                              QUERY PLAN                               
-----------------------------------------------------------------------
 Index Scan using t1_c1_idx on t1  (cost=0.15..14.98 rows=333 width=8)
   Index Cond: (c1 < $1)
 Planning Time: 0.195 ms
```
C'est effectivement le plan qui serait optimal pour `$1`=1.
Mais pour la valeur 1000, qui ramène toute la table, un _Seq Scan_ serait
plus pertinent.

</div>

---

### EXPLAIN (SETTINGS)

<div class="slide-content">

```sql
SET enable_seqscan TO off ;
SET work_mem TO '100MB';
```
```sql
EXPLAIN (SETTINGS)
SELECT   *   FROM t1   WHERE c2<10   ORDER BY c1;
```
```default
                         QUERY PLAN
---------------------------------------------------------------
Index Scan using t1_c1_idx on t1  (cost=0.28..57.77 rows=9 width=8)
  Filter: (c2 < 10)
Settings: enable_seqscan = 'off', work_mem = '100MB'
```
```sql
RESET ALL ;
```
<!--  57.77 car index bloaté à cause UPDATE ; sinon 0.15..33.65 -->

</div>

<div class="notes">

Désactivée par défaut, l'option `SETTINGS` permet d'obtenir la liste des paramètres
influant sur la planification et
qui ne sont pas à leur valeur par défaut pour la session ou la requête en cours.
Il est conseillé de l'activer systématiquement.
Elle est pratique quand il faut transmettre le plan à
un collègue ou un prestataire qui n'a pas forcément accès à la machine et à sa configuration.

</div>

---

### Autres options

<div class="slide-content">

  * `VERBOSE`
    + affichage verbeux : schémas, colonnes, workers : conseillé
  * `MEMORY`
    + mémoire utilisée pour le plan (v17)
  * `COSTS OFF`
    + masquer les coûts
  * `TIMING OFF`
    + désactiver le chronométrage & des informations vues/calculées par l'optimiseur
  * `SUMMARY`
    + affichage du temps de planification et exécution (si applicable)
  * `FORMAT`
    + sortie en texte, JSON, XML, YAML

</div>

<div class="notes">

Ces options sont moins utilisées, mais certaines restent intéressantes dans
des cas précis.

**Option VERBOSE**

N'hésitez pas à utiliser l'option `VERBOSE` pour afficher des informations
supplémentaires comme :

  * la liste des colonnes en sortie ;
  * le nom des objets qualifiés par le nom du schéma ;
  * des statistiques sur les workers (pour les requêtes parallélisées) ;
  * le code SQL envoyé à un serveur distant (pour les tables distantes avec `postgres_fdw` notamment).

Dans l'exemple suivant, le nom du schéma est ajouté au nom de la table.
La nouvelle ligne `Output` indique la liste des colonnes de l'ensemble de
données en sortie du nœud.

```sql
EXPLAIN (VERBOSE) SELECT * FROM t1 WHERE c2<10 ORDER BY c1 ;
```
```default
                           QUERY PLAN
----------------------------------------------------------------
 Sort  (cost=21.64..21.67 rows=9 width=8)
   Output: c1, c2
   Sort Key: t1.c1
   ->  Seq Scan on public.t1  (cost=0.00..21.50 rows=9 width=8)
         Output: c1, c2
         Filter: (t1.c2 < 10)
```

**Option MEMORY**

À partir de PostgreSQL 17, cette option affiche la mémoire consommée par le planificateur.
Ce n'est utile que pour les requêtes compliquées. Cet exemple montre
que même pour une requête très basique, un peu de mémoire est utilisée
uniquement pour la planification :

```sql
EXPLAIN (MEMORY) SELECT relname FROM pg_class ;
```
```default
                         QUERY PLAN                         
------------------------------------------------------------
 Seq Scan on pg_class  (cost=0.00..27.27 rows=527 width=64)
 Planning:
   Memory: used=9kB  allocated=16kB
```

**Option COSTS**

Cette option est activée par défaut. Il peut être intéressant de la désactiver pour
n'avoir que le plan.

```sql
EXPLAIN (COSTS OFF) SELECT * FROM t1 WHERE c2<10 ORDER BY c1 ;
```
```default
        QUERY PLAN
---------------------------
 Sort
   Sort Key: c1
   ->  Seq Scan on t1
         Filter: (c2 < 10)
```

**Option TIMING**

Cette option est activée par défaut. Il peut être intéressant de le désactiver
sur les systèmes où le chronométrage prend beaucoup de temps et allonge
inutilement la durée d'exécution de la requête. Mais de ce fait, le résultat
devient beaucoup moins intéressant.

<!-- FIXME  Tient pas compte du INSERT ci-dessus : est-ce si important ?-->
```sql
EXPLAIN (ANALYZE, TIMING OFF) SELECT * FROM t1 WHERE c2<10 ORDER BY c1 ;
```
```default
                                   QUERY PLAN
---------------------------------------------------------------------------------
 Sort  (cost=21.64..21.67 rows=9 width=8) (actual rows=9 loops=1)
   Sort Key: c1
   Sort Method: quicksort  Memory: 25kB
   ->  Seq Scan on t1  (cost=0.00..21.50 rows=9 width=8) (actual rows=9 loops=1)
         Filter: (c2 < 10)
         Rows Removed by Filter: 991
 Planning Time: 0.155 ms
 Execution Time: 0.381 ms
```

**Option SUMMARY**

Elle permet d'afficher ou non le résumé
final indiquant la durée de la planification et de l'exécution. Un `EXPLAIN`
simple n'affiche pas le résumé par défaut (la durée de planification
est pourtant parfois importante).
Par contre, un `EXPLAIN ANALYZE` l'affiche par défaut.

```sql
EXPLAIN (SUMMARY ON) SELECT * FROM t1 WHERE c2<10 ORDER BY c1;
```
```default
                       QUERY PLAN
---------------------------------------------------------
 Sort  (cost=21.64..21.67 rows=9 width=8)
   Sort Key: c1
   ->  Seq Scan on t1  (cost=0.00..21.50 rows=9 width=8)
         Filter: (c2 < 10)
 Planning Time: 0.185 ms
```
```sql
EXPLAIN (ANALYZE, SUMMARY OFF) SELECT * FROM t1 WHERE c2<10 ORDER BY c1;
```
```default
                    QUERY PLAN
---------------------------------------------------------
 Sort  (cost=21.64..21.67 rows=9 width=8)
       (actual time=0.343..0.346 rows=9 loops=1)
   Sort Key: c1
   Sort Method: quicksort  Memory: 25kB
   ->  Seq Scan on t1  (cost=0.00..21.50 rows=9 width=8)
               (actual time=0.031..0.331 rows=9 loops=1)
         Filter: (c2 < 10)
         Rows Removed by Filter: 991
```

**Option FORMAT**

L'option `FORMAT` permet de préciser le format du texte en sortie. Par défaut,
il s'agit du format texte habituel, mais il est possible de choisir un format
semi-structuré parmi JSON, XML et YAML.
Les formats semi-structurés sont utilisés principalement par des outils
d'analyse comme [explain.dalibo.com], car
le contenu est plus facile à analyser, et même un peu plus complet.
Voici ce que donne la commande `EXPLAIN` avec le format JSON :

```bash
psql -X -AtX \
-c 'EXPLAIN (FORMAT JSON) SELECT * FROM t1 WHERE c2<10 ORDER BY c1' | jq '.[]'
```
```json
{
  "Plan": {
    "Node Type": "Sort",
    "Parallel Aware": false,
    "Async Capable": false,
    "Startup Cost": 34.38,
    "Total Cost": 34.42,
    "Plan Rows": 18,
    "Plan Width": 8,
    "Sort Key": [
      "c1"
    ],
    "Plans": [
      {
        "Node Type": "Seq Scan",
        "Parent Relationship": "Outer",
        "Parallel Aware": false,
        "Async Capable": false,
        "Relation Name": "t1",
        "Alias": "t1",
        "Startup Cost": 0,
        "Total Cost": 34,
        "Plan Rows": 18,
        "Plan Width": 8,
        "Filter": "(c2 < 10)"
      }
    ]
  }
}
```
</div>

---

### Paramètre track_io_timing

<div class="slide-content">

```sql
SET track_io_timing TO on;
EXPLAIN (ANALYZE, BUFFERS)
SELECT   *   FROM t1   WHERE c2<10   ORDER BY c1 ;
```
<!-- il faut couper un peu pour avoir tous les I/O timing dans le slide... -->
```default
                            QUERY PLAN
---------------------------------------------------------
 Sort  (cost=52.14..52.21 rows=27 width=8) (actual time=1.359..1.366 rows=27 loops=1)
   …
   Buffers: shared hit=3 read=14
   I/O Timings: read=0.388
   ->  Seq Scan on t1  (cost=0.00..51.50 rows=27 width=8) (actual time=0.086..1.233 rows=27 loops=1)
         Filter: (c2 < 10)
         Rows Removed by Filter: 2973
         Buffers: shared read=14
         I/O Timings: read=0.388
 Planning:
   Buffers: shared hit=43 read=14
   I/O Timings: read=0.469
 Planning Time: 1.387 ms
 Execution Time: 1.470 ms
```

</div>

<div class="notes">

La configuration du paramètre `track_io_timing` permet de demander le
chronométrage des opérations d'entrée/sortie disque. Sur ce plan, nous pouvons
voir que 14 blocs ont été lus en dehors du cache de PostgreSQL et que cela a
pris 0,388 ms pour les lire (ils étaient certainement dans le cache du système
d'exploitation).

Cette information permet de voir si le temps d'exécution de la requête est
dépensé surtout dans la demande de blocs au système d'exploitation (donc
hors du cache de PostgreSQL) ou dans l'exécution même de la requête (donc interne à
PostgreSQL).

</div>

---

### Détecter les problèmes

<div class="slide-content">

  * Temps d'exécution de chaque opération
  * Différence entre l'estimation du nombre de lignes et la réalité
  * Boucles
    + appels, même rapides, nombreux
  * Opérations utilisant beaucoup de blocs (`BUFFERS`)
  * Opérations lentes de lecture/écriture (`track_io_timing`)

</div>

<div class="notes">

Lorsqu'une requête s'exécute lentement, cela peut être un problème dans le
plan. La sortie de `EXPLAIN` peut apporter quelques informations qu'il faut
savoir décoder.

Par exemple, une différence importante entre le nombre estimé de lignes et
le nombre réel de lignes laisse un doute sur les statistiques présentes. Soit
elles n'ont pas été réactualisées récemment, soit l'échantillon n'est pas
suffisamment important pour que les statistiques donnent une vue proche du
réel du contenu de la table.

Les boucles sont à surveiller. Par exemple, un accès à une ligne par un index
est généralement très rapide, mais répété des millions de fois à cause d'une
boucle, le total est parfois plus long qu'une lecture complète de la table indexée.
C'est notamment l'enjeu du réglage entre `seq_page_cost` et `random_page_cost`.

L'option `BUFFERS` d'`EXPLAIN` permet également de mettre en valeur les
opérations d'entrées/sorties lourdes. Cette option affiche notamment le nombre
de blocs lus en/hors du cache de PostgreSQL. Sachant qu'un bloc fait généralement
8 kilo-octets, il est aisé de déterminer le volume de données manipulé par une requête.

</div>

---

## Nœuds d'exécution les plus courants (introduction)

<div class="slide-content">

  * Parcours
  * Jointures
  * Agrégats
  * Tri

</div>

<div class="notes">

<!-- Développé dans J2 : service minimum -->

Nous n'allons pas détailler tous les nœuds existants, mais évoquer simplement
les plus importants. Une analyse plus poussée des nœuds et une référence
complète sont disponibles dans les modules [J2](https://dali.bo/j2_html)
et [J6](https://dali.bo/j6_html).

</div>

---

### Parcours

<div class="slide-content">

  * Table
    + _Seq Scan_, _Parallel Seq Scan_
  * Index
    + _Index Scan_, _Bitmap Scan_, _Index Only Scan_
    + et les variantes parallélisées
  * Autres
    + _Function Scan_, _Values Scan_

</div>

<div class="notes">

<!-- Intro TRES rapide ; les détails sont pour J0 mais surtout J2 ou (J6/KB) -->

Plusieurs types d'objets peuvent être parcourus. Pour chacun,
l'optimiseur peut choisir entre plusieurs
types de parcours.

Les tables passent par un _Seq Scan_ qui est une lecture simple de la table,
bloc par bloc, ligne par ligne. Ce parcours peut filtrer les données mais ne
les triera pas. Une variante parallélisée existe sous le nom de
_Parallel Seq Scan_.

Les index disposent de plusieurs parcours, principalement suivant la quantité
d'enregistrements à récupérer :

  * _Index Scan_ quand il y a très peu d'enregistrements à récupérer ;
  * _Bitmap Scan_ quand il y en a un peu plus ou quand on veut lire plusieurs
    index d'une même table pour satisfaire plusieurs conditions de filtre ;
  * _Index Only Scan_ quand les champs de la requête correspondent aux colonnes
    de l'index (ce qui permet d'éviter la lecture de tout ou partie de la
    table).

Ces différents parcours sont parallélisables. Ils ont dans ce cas le mot
_Parallel_ ajouté en début du nom du nœud.

Enfin, il existe des parcours moins fréquents, comme les parcours de fonction
(_Function Scan_) ou de valeurs (_Values Scan_).

</div>

---

### Jointures

<div class="slide-content">

  * Algorithmes
    + _Nested Loop_
    + _Hash Join_
    + _Merge Join_
  * Parallélisation possible
  * Pour `EXISTS`, `IN` et certaines jointures externes
    + _Semi Join_
    + _Anti Join_

</div>

<div class="notes">

<!-- Développé dans J2 : service minimum -->

Trois nœuds existent pour les jointures.

Le _Nested Loop_ est utilisé pour toutes les conditions de jointure
n'utilisant pas l'opérateur d'égalité. Il est aussi utilisé quand un des deux
ensembles de données renvoie très peu de données.

Le _Hash Join_ est certainement le nœud le plus commun. Il est utilisé un peu
dans tous les cas, sauf si les deux ensembles de données arrivent déjà triés.
Dans ce cas, il est préférable de passer par un _Merge Join_ qui réclame deux
ensembles de données déjà triés.

Les _Semi Join_ et _Anti Join_ sont utilisés dans des cas très particuliers et
peu fréquents.

</div>

---

### Agrégats

<div class="slide-content">

  * Un résultat au total
    + _Aggregate_
  * Un résultat par regroupement
    + _Hash Aggregate_
    + _Group Aggregate_
    + _Mixed Aggregate_
  * Parallélisation
    + _Partial Aggregate_
    + _Finalize Aggregate_

</div>

<div class="notes">

<!-- Développé dans J2 : service minimum -->

De même il existe plusieurs algorithmes d'agrégation qui s'occupent
des sommes, des moyennes, des regroupements divers, etc. Ils sont souvent parallélisables.

</div>

---

### Opérations unitaires

<div class="slide-content">

  * _Sort_
  * _Incremental Sort_
  * _Limit_
  * _Unique_ (`DISTINCT`)
  * _Append_ (`UNION ALL`), _Except_, _Intersect_
  * _Gather_ (parallélisme)
  * _Memoize_ (14+)

</div>

<div class="notes">

<!-- Développé dans J2 : service minimum -->

Un grand nombre de petites opérations ont leur propre nœud, comme le tri avec
_Sort_ et _Incremental Sort_, la limite de lignes (`LIMIT`) avec _Limit_, la clause
`DISTINCT` avec _Unique_), etc.
Elles prennent généralement un ensemble de données et renvoient un autre
ensemble de données issu du traitement du premier.

Le groupe des nœuds _Append_, _Except_ et _Intersect_ ne se comporte pas ainsi. Notamment,
_Append_ est le seul nœud à prendre potentiellement plus de deux ensembles de
données en entrée.

Apparu avec PostgreSQL 14, le nœud _Memoize_ est un cache de résultat qui
permet d'optimiser les performances d'autres nœuds en mémorisant des données
qui risquent d'être accédées plusieurs fois de suite. Pour le moment, ce nœud
n'est utilisable que pour les données de l'ensemble interne d'un _Nested Loop_.

</div>

---

## Outils graphiques

<div class="slide-content">

  * pgAdmin
  * explain.depesz.com
  * explain.dalibo.com

</div>

<div class="notes">

L'analyse de plans complexes devient très vite fastidieuse. Nous n'avons vu
ici que des plans d'une dizaine de lignes au maximum, mais les plans de
requêtes réellement problématiques peuvent faire plusieurs centaines, voire
milliers de lignes. L'analyse manuelle devient impossible. Des outils ont été
créés pour mieux visualiser les parties intéressantes des plans.

</div>

---

### pgAdmin

<div class="slide-content">

  * Vision graphique d'un `EXPLAIN`
  * Une icône par nœud
  * La taille des flèches dépend de la quantité de données
  * Le détail de chaque nœud est affiché en survolant les nœuds

</div>

<div class="notes">

pgAdmin propose depuis très longtemps un affichage graphique de l'`EXPLAIN`.
Cet affichage est intéressant car il montre simplement l'ordre dans lequel les
opérations sont effectuées. Chaque nœud est représenté par une icône. Les
flèches entre chaque nœud indiquent où sont envoyés les flux de données, la
taille de la flèche précisant la volumétrie des données.

Les statistiques ne sont affichées qu'en survolant les nœuds.

</div>

---

### pgAdmin - copie d'écran

![EXPLAIN par pgAdmin](medias/pgAdmin4-explain_graphique.png)
\

<div class="notes">

Voici un exemple d'un `EXPLAIN` graphique réalisé par pgAdmin 4. En cliquant
sur un nœud, un message affiche les informations statistiques sur le
nœud.

</div>

---

### explain.depesz.com

<div class="slide-content">

  * Site web avec affichage amélioré du `EXPLAIN ANALYZE`
  * Lignes colorées pour indiquer les problèmes
  * Installable en local

</div>

<div class="notes">

Hubert Lubaczewski est un contributeur très connu dans la communauté
PostgreSQL. Il publie notamment un grand nombre d'articles sur les nouveautés
des prochaines versions. Cependant, il est aussi connu pour avoir créé un site
web d'analyse des plans d'exécution, [explain.depesz.com].

Il suffit d'aller sur ce site, de coller le résultat d'un `EXPLAIN ANALYZE`,
et le site affichera le plan d'exécution avec des codes couleurs pour bien
distinguer les nœuds performants des autres.

Le code couleur est simple : blanc indique que tout va bien, jaune est
inquiétant, marron est plus inquiétant, et rouge très inquiétant.

Plutôt que d'utiliser le service web, il est possible d'installer ce site en
local :

  * [le module explain en Perl](https://gitlab.com/depesz/Pg--Explain)
  * [la partie site web](https://gitlab.com/depesz/explain.depesz.com)

</div>

---

### explain.depesz.com - exemple

![explain.depesz.com](medias/plan/explain-depesz.png)
\

<div class="notes">

Cet exemple montre l'affichage d'un plan sur le site
[explain.depesz.com].

Voici la signification des différentes colonnes :

  * _Exclusive_ : durée passée exclusivement sur un nœud ;
  * _Inclusive_ : durée passée sur un nœud et ses fils ;
  * _Rows x_ : facteur d'échelle de l'erreur d'estimation du nombre de lignes ;
  * _Rows_ : nombre de lignes renvoyées ;
  * _Loops_ : nombre de boucles.

Sur une exécution de 600 ms, un tiers est passé à lire la table avec un
parcours séquentiel.

</div>

---

### explain.dalibo.com

<div class="slide-content">

 <!--  Pub assumée
 on ne parle qu'à la marge du nom pev2 (qui est en minuscules)
 -->

  * Reprise de **pev** d'Alex Tatiyants, par Pierre Giraud (Dalibo)
  * Page web avec affichage graphique d'un `EXPLAIN [ANALYZE]`
  * Repérage des nœuds longs, lourds…
  * Affichage flexible
  * explain.dalibo.com
  * Installable en local

</div>

<div class="notes">

À l'origine, pev (_PostgreSQL Explain Visualizer_) est un outil
[libre](https://github.com/AlexTatiyants/pev) offrant un affichage graphique
du plan d'exécution et pointant le nœud le plus coûteux, le plus long, le plus
volumineux, etc. Utilisable [en ligne](https://tatiyants.com/pev/#/plans),
il n'est hélas plus maintenu depuis plusieurs années.

[explain.dalibo.com] en est un _fork_, très étendu
et activement maintenu par Pierre Giraud de Dalibo. Les plans au format texte
comme JSON sont acceptés. Les versions récentes de PostgreSQL sont supportées,
avec leurs spécificités : nouvelles options d'`EXPLAIN`, nouveaux types de
nœuds… Tout se passe en ligne. Les plans peuvent être partagés.
Si vous ne souhaitez pas qu'ils soient stockés chez Dalibo, utilisez
[la version strictement locale de pev2](https://www.github.com/dalibo/pev2/releases/latest/download/index.html).

[Le code](https://github.com/dalibo/pev2) est sous licence PostgreSQL.
Techniquement, c'est un composant VueJS qui peut être intégré à vos
propres outils.

</div>

---

### explain.dalibo.com - exemple

![EXPLAIN par pev](medias/pev2-demo-hashaggregate.png)
\

<!-- Plan de démo servant pour l exemple : https://explain.dalibo.com/plan/692dd6g5g0de31ag -->

<div class="notes">

explain.dalibo.com permet de repérer d'un coup d'œil les parties les plus
longues du plan, celles utilisant le plus de lignes, les écarts d'estimation,
les dérives du temps de planification… Les nœuds peuvent être repliés.
Plusieurs modes d'affichage sont disponibles.

Un grand nombre de plans d'exemple sont disponibles sur le site.

</div>

---

## Conclusion

<!-- FIXME   Parler d'autres outils proprio ? explain.tensor.ru ,  pgmustard
-->


<div class="slide-content">

  * Un optimiseur très avancé
  * Ne vous croyez pas plus malin que lui
  * Mais il est important de savoir comment il fonctionne

</div>

<div class="notes">

Cette introduction à l'optimiseur de PostgreSQL permet de comprendre
comment il fonctionne et sur quoi il se base. Cela permet de pointer
certains des problèmes. C'est aussi un prérequis indispensable pour voir
plus tard l'intérêt des différents index et nœuds d'exécution de PostgreSQL.

</div>

---

### Questions

<div class="slide-content">

N'hésitez pas, c'est le moment !

</div>

<div class="notes">

</div>

---

\newpage
## Travaux pratiques

<div class="notes">

### Statistiques sur les données

**But** : Comprendre le lien entre les statistiques sur les données et le choix
du plan d'exécution par l'optimiseur de requête.

> Créer une table `exo1` avec une seule colonne `c1` de type `integer`.

> Empêcher l'autovacuum d'analyser automatiquement la table.

> Insérer 1 million de lignes différentes dans `c1` avec `generate_series`.

> Rechercher une ligne précise dans la table.

> Exécuter la commande `ANALYZE`.

> Relancer la même requête que ci-dessus. Que constatez-vous ?

> Ajouter un index sur `c1`.

> Relancer la même requête que ci-dessus. Que constatez-vous ?

> Modifier la colonne `c1` avec la valeur 100000 pour toutes les lignes.

> Exécuter un `EXPLAIN (ANALYSE)`. Que constatez-vous ?

> Exécuter la commande `ANALYZE`.

> Relancer la même requête que ci-dessus.

</div>

\newpage
## Travaux pratiques (solutions)

<div class="notes">

### Statistiques sur les données

> Créer une table `exo1` avec une seule colonne `c1` de type `integer`.

```sql
postgres=# CREATE TABLE exo1 (c1 integer);
CREATE TABLE
```

> Empêcher autovacuum d'analyser automatiquement la table.

```sql
postgres=# ALTER TABLE exo1 SET (autovacuum_enabled=false);
ALTER TABLE
```

> Insérer 1 million de lignes différentes dans `c1` avec `generate_series`.

```sql
postgres=# INSERT INTO exo1 SELECT generate_series(1, 1000000);
INSERT 0 1000000
```

> Rechercher une ligne précise dans la table.

```sql
postgres=# EXPLAIN SELECT * FROM exo1 WHERE c1=100000;
                                QUERY PLAN
--------------------------------------------------------------------------
 Gather  (cost=1000.00..11866.15 rows=5642 width=4)
   Workers Planned: 2
   ->  Parallel Seq Scan on exo1  (cost=0.00..10301.95 rows=2351 width=4)
         Filter: (c1 = 100000)
(4 rows)
```

> Exécuter la commande `ANALYZE`.

```sql
postgres=# ANALYZE exo1;
ANALYZE
```

> Relancer la même requête que ci-dessus. Que constatez-vous ?

```sql
postgres=# EXPLAIN SELECT * FROM exo1 WHERE c1=100000;
                             QUERY PLAN                             
--------------------------------------------------------------------
 Gather  (cost=1000.00..10633.43 rows=1 width=4)
   Workers Planned: 2
   ->  Parallel Seq Scan on exo1  (cost=0.00..9633.33 rows=1 width=4)
         Filter: (c1 = 100000)
(4 rows)
```

Les statistiques sont beaucoup plus précises. PostgreSQL sait maintenant qu'il ne 
va récupérer qu'une seule ligne, sur le million de lignes dans la table. C'est 
le cas typique où un index est intéressant.

> Ajouter un index sur `c1`.

```sql
postgres=# CREATE INDEX ON exo1(c1);
CREATE INDEX
```

> Relancer la même requête que ci-dessus.

```sql
postgres=# EXPLAIN SELECT * FROM exo1 WHERE c1=100000;
                                 QUERY PLAN
-----------------------------------------------------------------------------
 Index Only Scan using exo1_c1_idx on exo1  (cost=0.42..8.44 rows=1 width=4)
   Index Cond: (c1 = 100000)
(2 rows)
```

Après création de l'index, on constate que PostgreSQL choisit un autre plan qui
permet d'utiliser cet index.

> Modifier la colonne c1 avec la valeur 100000 pour toutes les lignes.

```sql
postgres=# UPDATE exo1 SET c1=100000;
UPDATE 1000000
```

Toutes les lignes ont la même valeur.

> Exécuter un `EXPLAIN (ANALYSE)`. Que constatez-vous ?

```sql
postgres=# EXPLAIN (ANALYZE) SELECT * FROM exo1 WHERE c1=100000;
                  QUERY PLAN
---------------------------------------------------------
 Index Only Scan using exo1_c1_idx on exo1
       (cost=0.43..8.45 rows=1 width=4)
       (actual time=0.040..265.573 rows=1000000 loops=1)
   Index Cond: (c1 = 100000)
   Heap Fetches: 1000001
 Planning time: 0.066 ms
 Execution time: 303.026 ms
(5 rows)
```

Là, un parcours séquentiel serait plus performant. Mais comme PostgreSQL n'a plus 
de statistiques à jour, il se trompe de plan et utilise toujours l'index.

> Exécuter la commande `ANALYZE`.

```sql
postgres=# ANALYZE exo1;
ANALYZE
```

> Lire la table avec un filtre sur `c1`.
 

```sql
postgres=# EXPLAIN ANALYZE SELECT * FROM exo1 WHERE c1=100000;
                     QUERY PLAN 
----------------------------------------------------------
 Seq Scan on exo1
       (cost=0.00..21350.00 rows=1000000 width=4)
       (actual time=75.185..186.019 rows=1000000 loops=1)
   Filter: (c1 = 100000)
 Planning time: 0.122 ms
 Execution time: 223.357 ms
(4 rows)
```

Avec des statistiques à jour et malgré la présence de l'index, PostgreSQL va 
utiliser un parcours séquentiel qui, au final, sera plus performant.

Si l'autovacuum avait été activé, les modifications de la table auraient provoqué 
assez rapidement la mise à jour des statistiques.

</div>

\newpage

## Travaux pratiques

<div class="notes">
La version en ligne des solutions de ces TP est disponible sur <https://dali.bo/j0_solutions>. <!-- Texte généré par script, ne pas modifier -->
</div>

Tous les TP se basent sur la configuration par défaut de PostgreSQL, sauf précision contraire.


<!-- inclus dans tp-enonce.md --> 

### Manipuler explain

<div class="notes">

<div class="slide-content">
  **But** : Première manipulation d'EXPLAIN
</div>

<!--
  Le but est d'être très basique. Seq Scan, index scan, une jointure, peu d'ANALYZE
  Dernier test : 16β2 20230830
-->
<!-- Script création & schéma répété dans énoncé et solution
     SQL de création : tp/machines/machines_donnees-creation.sql
--> 
<!-- TP basique pour J0 -->

> Créer une base **machines** et y générer les données comme indiqué
> ci-dessous.
> L'exécution peut durer une minute ou deux suivant la machine.
> ```bash
> curl -kL https://dali.bo/tp_machines_donnees -o machines_donnees.sql
> createdb machines 
> psql machines < machines_donnees.sql
> ```
> 
> La base **machines** contiendra alors deux tables :
> 
>  * `machines` est une liste de machines ;
>  * `donnees` contient des données horodatées de quelques
> capteurs de ces machines, entre janvier et août 2023.

![Tables machines et donnees](medias/tp/machines-donnees-schema.png)

> Nettoyage et mise à jour des statistiques :
>```sql
>VACUUM ANALYZE machines,donnees;
>```

> Quelles sont les tailles des tables ?

> Pour simplifier certains plans, désactivons le parallélisme et la compilation à la volée :
> ```sql
> SET max_parallel_workers_per_gather TO 0 ;
> SET jit TO off ;
> ```

**Requêtes sur les périodes** :

> Quel est le plan prévu pour récupérer les données du 31 janvier dans la table `donnees` ?
>```sql
>EXPLAIN
>SELECT * FROM donnees
>WHERE horodatage = '2023-01-31'::date ;
>```

> Quel est le plan prévu pour récupérer les données du mois de janvier dans la table `donnees` ?
>```sql
>EXPLAIN
>SELECT * FROM donnees
>WHERE horodatage BETWEEN '2023-01-01'::date AND '2023-01-31'::date ;
>```

> Quel est le plan prévu pour cette variante de la requête sur janvier ?
>```sql
>EXPLAIN
>SELECT * FROM donnees
>WHERE to_char (horodatage, 'YYYYMM') = '202301';
>```
> 
>  Pourquoi est-il différent ? Comparer avec le précédant en utilisant `EXPLAIN ANALYZE`.

> Quel est le plan pour la même requête, cette fois sur deux mois ?
>```sql
>EXPLAIN
>SELECT * FROM donnees
>WHERE horodatage BETWEEN '2023-03-01'::date AND '2023-04-30'::date;
>```
> Relancer avec `EXPLAIN (ANALYZE)`.

**Jointure** :

> Quel est le plan prévu pour cette jointure sur toutes les données d'une machine ?
>
>```sql
>EXPLAIN
>SELECT *
>FROM donnees INNER JOIN machines USING (id_machine)
>WHERE machines.code = 'E4DA3B' AND type = 5;
>```

> Quel est le plan prévu pour la requête suivante, qui récupère toutes les données d'après juillet
> pour un type de machines donné ? Quelles en sont les 3 étapes ?
>
>```sql
>EXPLAIN
>SELECT description, horodatage, valeur1 
>FROM donnees INNER JOIN machines USING (id_machine)
>WHERE machines.type = 1
>AND donnees.horodatage > '2023-07-01' ;
>```

</div>


\newpage


<!-- inclus dans tp-enonce.md --> 

### Manipuler explain (base magasin)

<div class="notes">

<div class="slide-content">
  **But** : Manipuler explain.
</div>

> - Créer une base de données nommée `magasin`.

> - Importer le jeu de données d'exemple :

La base **magasin** (dump de 96 Mo, pour 667 Mo sur le disque au final)
peut être téléchargée et restaurée comme suit dans une nouvelle base **magasin** :
```bash
createdb magasin
curl -kL https://dali.bo/tp_magasin -o /tmp/magasin.dump
pg_restore -d magasin  /tmp/magasin.dump
# le message sur public préexistant est normal
rm -- /tmp/magasin.dump
```
Les données sont dans deux schémas, **magasin** et **facturation**.
Penser au `search_path`.

Pour ce TP, figer les paramètres suivants :

```sql
SET max_parallel_workers_per_gather to 0;
SET seq_page_cost TO 1 ;
SET random_page_cost TO 4 ;
```

> - Le schéma à utiliser se nomme également `magasin`.
> - Consulter les tables.

> - Lancer un `ANALYZE` sur la base.

> Le but est de chercher une personne nommée Moris Russel dans la table `contacts` par les champs `prenom` et `nom`.
>
> - Quel est le plan qu'utilisera PostgreSQL pour le trouver ?
> - À combien de résultats le planificateur s'attend-il ?

> - Afficher le résultat.

> - Quel est le plan réellement exécuté ?

> - Rechercher la même personne par son `contact_id`.
> - Quel est le plan ?

> - La requête suivante recherche tous les fournisseurs résidents d'Hollywood.
> ```sql
> SELECT c.nom, c.prenom FROM contacts c
> INNER JOIN fournisseurs f
> ON (f.contact_id = c.contact_id)
> WHERE c.ville = 'Hollywood' ;
> ```
> - Quel est le plan prévu ?
> - Que donne-t-il à l'exécution ?

</div>



\newpage

## Travaux pratiques (solutions)

<div class="notes">


<!-- inclus dans tp-solution.md --> 

### Manipuler explain 

<!--
  Le but est d'être très basique. Seq Scan, index scan, une jointure, peu d'ANALYZE
-->

<!-- Script création & schéma répété dans énoncé et solution
     SQL de création : tp/machines/machines_donnees-creation.sql
-->
<!-- TP basique pour J0 -->

> Créer une base **machines** et y générer les données comme indiqué
> ci-dessous.
> L'exécution peut durer une minute ou deux suivant la machine.
> ```bash
> curl -kL https://dali.bo/tp_machines_donnees -o machines_donnees.sql
> createdb machines 
> psql machines < machines_donnees.sql
> ```
> 
> La base **machines** contiendra alors deux tables :
> 
>  * `machines` est une liste de machines ;
>  * `donnees` contient des données horodatées de quelques
> capteurs de ces machines, entre janvier et août 2023.

![Tables machines et donnees](medias/tp/machines-donnees-schema.png)

> Nettoyage et mise à jour des statistiques :
>```sql
>VACUUM ANALYZE machines,donnees;
>```

Cette opération est à faire systématiquement sur des tables récentes,
ou au moindre doute. L'autovacuum n'est parfois pas assez rapide pour
effectuer ces opérations.

> Quelles sont les tailles des tables ?

Sous `psql` :

```default
=# \dt+
                                                 Liste des relations
 Schéma |       Nom       | Type  | Propriétaire |  … | Taille  | …
--------+-----------------+-------+--------------+----+---------+--
 public | donnees         | table | postgres     |    | 284 MB  | 
 public | machines        | table | postgres     |    | 112 kB  | 
```

Quant aux nombres de lignes :

```sql
SELECT count (*) FROM machines ;
```
```default
 count 
-------
  1000
```
```sql
SELECT count (*) FROM donnees ;
```
```default
  count  
---------
 4950225
```

<div class="box warning">
Tout plan d'exécution dépend de la configuration de PostgreSQL.
Sauf précision contraire, nous partons toujours
de la configuration par défaut.
</div>

> Pour simplifier certains plans, désactivons le parallélisme et la compilation à la volée :
> ```sql
> SET max_parallel_workers_per_gather TO 0 ;
> SET jit TO off ;
> ```

**Requêtes sur les périodes** :

> Quel est le plan prévu pour récupérer les données du 31 janvier dans la table `donnees` ?
>```sql
>EXPLAIN
>SELECT * FROM donnees
>WHERE horodatage = '2023-01-31'::date ;
>```

Le plan prévu est :

```default
                                  QUERY PLAN                                   
-------------------------------------------------------------------------------
Index Scan using donnees_horodatage_idx on donnees (cost=0.43..8.64 rows=12 width=30)
  Index Cond: (horodatage = '2023-01-31'::date)
```

Il existe un index sur le critère, il est naturel qu'il soit utilisé.

> Quel est le plan prévu pour récupérer les données du mois de janvier dans la table `donnees` ?
>```sql
>EXPLAIN
>SELECT * FROM donnees
>WHERE horodatage BETWEEN '2023-01-01'::date AND '2023-01-31'::date ;
>```

Le plan prévu est le même, au critère près :

```default
                                  QUERY PLAN                                   
-------------------------------------------------------------------------------
 Index Scan using donnees_horodatage_idx on donnees  (cost=0.43..933.87 rows=28722 width=30)
   Index Cond: ((horodatage >= '2023-01-01'::date) AND (horodatage <= '2023-01-31'::date))
```

Noter la réécriture du `BETWEEN` sous forme d'inégalités.

> Quel est le plan prévu pour cette variante de la requête sur janvier ?
>```sql
>EXPLAIN
>SELECT * FROM donnees
>WHERE to_char (horodatage, 'YYYYMM') = '202301';
>```
> 
>  Pourquoi est-il différent ? Comparer avec le précédant en utilisant `EXPLAIN ANALYZE`.

Le plan cette fois est un parcours de table. L'index est ignoré, toute la table est lue :

```default
                            QUERY PLAN                            
------------------------------------------------------------------
 Seq Scan on donnees  (cost=0.00..110652.25 rows=24751 width=30)
   Filter: (to_char(horodatage, 'YYYYMM'::text) = '202301'::text)
```

Si le parallélisme est activé, il existe une variante parallélisée de ce plan :

```default
                                  QUERY PLAN                                   
-------------------------------------------------------------------------------
 Gather  (cost=1000.00..70812.96 rows=24751 width=30)
   Workers Planned: 2
   ->  Parallel Seq Scan on donnees  (cost=0.00..67337.86 rows=10313 width=30)
         Filter: (to_char(horodatage, 'YYYYMM'::text) = '202301'::text)
```

La raison du changement de plan est le changement du critère. C'est évident pour un
humain, mais PostgreSQL ne fait pas l'équivalence entre les deux formulations du critère
sur le mois de janvier. Or il n'y a pas d'index sur la fonction
`to_char(horodatage, 'YYYYMM')` (il serait possible d'en créer un).

Si l'on compare les deux plans en les exécutant réellement, avec `EXPLAIN (ANALYZE)`, on obtient
pour la variante avec `BETWEEN` :

```sql
EXPLAIN (ANALYZE,BUFFERS)
SELECT * FROM donnees
WHERE horodatage BETWEEN '2023-01-01'::date AND '2023-01-31'::date ;
```
```default
                                  QUERY PLAN                                   
------------------------------------------------------------------------------- 
 Index Scan using donnees_horodatage_idx on donnees  (cost=0.43..933.75 rows=28716 width=30) (actual time=0.060..9.405 rows=19600 loops=1)
   Index Cond: ((horodatage >= '2023-01-01'::date) AND (horodatage <= '2023-01-31'::date))
   Buffers: shared hit=6 read=191
 Planning:
   Buffers: shared hit=8
 Planning Time: 0.072 ms
 Execution Time: 10.472 ms
```

et pour la variante avec `to_char` :

```sql
EXPLAIN (ANALYZE,BUFFERS)
SELECT * FROM donnees
WHERE to_char (horodatage, 'YYYYMM') = '202301';
```
```default
                                  QUERY PLAN                                   
------------------------------------------------------------------------------- 
 Seq Scan on donnees  (cost=0.00..110652.25 rows=24751 width=30) (actual time=0.013..1503.631 rows=19600 loops=1)
   Filter: (to_char(horodatage, 'YYYYMM'::text) = '202301'::text)
   Rows Removed by Filter: 4930625
   Buffers: shared hit=16063 read=20336
 Planning Time: 0.025 ms
 Execution Time: 1504.379 ms
```

La dernière ligne indique 10 ms pour la variante avec `BETWEEN`
contre 1,5 s pour la variante avec `to_char` :
l'utilisation de l'index est nettement plus intéressante que le parcours complet de la table.
Le plan indique aussi que beaucoup plus de blocs (_buffers_) ont été lus.
<!-- ne pas détailler encore ici -->

> Quel est le plan pour la même requête, cette fois sur deux mois ?
>```sql
>EXPLAIN
>SELECT * FROM donnees
>WHERE horodatage BETWEEN '2023-03-01'::date AND '2023-04-30'::date;
>```
> Relancer avec `EXPLAIN (ANALYZE)`.

On s'attend au même plan que pour la recherche sur janvier,
mais PostgreSQL prévoit cette fois un parcours complet :

```default
                                  QUERY PLAN                                   
------------------------------------------------------------------------------- 
 Seq Scan on donnees  (cost=0.00..110652.25 rows=4184350 width=30)
   Filter: ((horodatage >= '2023-03-01'::date) AND (horodatage <= '2023-04-30'::date))
```

En effet, il y a beaucoup plus de lignes à récupérer sur mars-avril qu'en janvier.
La mention `rows` indique l'estimation des lignes ramenées et indique 4,2 millions
de lignes sur les 4,9 de la table ! Le plus efficace est donc de lire directement la table.
Les statistiques permettent donc à PostgreSQL de changer de stratégie suivant les volumétries attendues.

Une exécution réelle indique que cette estimation est bonne, et dure logiquement à peu près aussi longtemps que le parcours complet ci-dessus :

```sql
EXPLAIN (ANALYZE)
SELECT * FROM donnees
WHERE horodatage BETWEEN '2023-03-01'::date AND '2023-04-30'::date ;
```
```default
                                  QUERY PLAN                                   
-------------------------------------------------------------------------------
 Seq Scan on donnees  (cost=0.00..110652.25 rows=4184350 width=30) (actual time=160.385..1255.020 rows=4182160 loops=1)
   Filter: ((horodatage >= '2023-03-01'::date) AND (horodatage <= '2023-04-30'::date))
   Rows Removed by Filter: 768065
 Planning Time: 0.470 ms
 Execution Time: 1378.383 ms
```

**Jointure** :

> Quel est le plan prévu pour cette jointure sur toutes les données d'une machine ?
>
>```sql
>EXPLAIN
>SELECT *
>FROM donnees INNER JOIN machines USING (id_machine)
>WHERE machines.code = 'E4DA3B' AND type = 5;
>```

Le plan est :

```default
                                  QUERY PLAN                                   
-------------------------------------------------------------------------------
 Nested Loop  (cost=0.71..5033.11 rows=4950 width=75)
   ->  Index Scan using machines_type_code_key on machines  (cost=0.28..8.29 rows=1 width=49)
         Index Cond: ((type = 5) AND ((code)::text = 'E4DA3B'::text))
   ->  Index Scan using donnees_id_machine_idx on donnees  (cost=0.43..4124.77 rows=90004 width=30)
         Index Cond: (id_machine = machines.id_machine)
```

Il s'agit : 

  * d'un accès à `machines` par l'index sur `machines (type, code)` (cet index marque l'unicité) ;
  * suivi d'un accès à `donnees`, toujours par l'index sur le champ indexé `id_machine`.

> Quel est le plan prévu pour la requête suivante, qui récupère toutes les données d'après juillet
> pour un type de machines donné ? Quelles en sont les 3 étapes ?
>
>```sql
>EXPLAIN
>SELECT description, horodatage, valeur1 
>FROM donnees INNER JOIN machines USING (id_machine)
>WHERE machines.type = 1
>AND donnees.horodatage > '2023-07-01' ;
>```

```default
                                  QUERY PLAN                                   
-------------------------------------------------------------------------------
Hash Join  (cost=30.67..8380.56 rows=138788 width=47)
   Hash Cond: (donnees.id_machine = machines.id_machine)
   ->  Index Scan using donnees_horodatage_idx on donnees  (cost=0.43..7671.54 rows=257492 width=16)
         Index Cond: (horodatage > '2023-07-01 00:00:00+02'::timestamp with time zone)
   ->  Hash  (cost=23.50..23.50 rows=539 width=39)
         ->  Seq Scan on machines  (cost=0.00..23.50 rows=539 width=39)
               Filter: (type = 1)
```

<!--  Explication délibérément basique --> 
Il s'agit ici d'une jointure en _hash join_, courante dans les jointures brassant beaucoup de lignes.

PostgreSQL commence par un parcours complet de `machines` (`type = 1` concerne la plupart des machines ).
Puis il crée une « table de hachage » à partir des `id_machine` des lignes résultantes.
Il parcoure `donnees` en se basant sur l'index sur la date.
Les lignes résultantes seront comparées au contenu de la table de hachage pour savoir s'il faut garder les valeurs.


\newpage


<!-- inclus dans tp-solution.md --> 

### Manipuler explain (base magasin)

<!-- Le but est d'être basique, mais un peu moins que le TP précédent

Quand des tables un peu intéressantes seront dispo, les remplacer ici.

-->

> - Créer une base de données nommée `magasin`.

Si l'on est connecté à la base, en tant que superutilisateur **postgres** :
```sql
CREATE DATABASE magasin;
```

Alternativement, depuis le shell, en tant qu'utilisateur système **postgres** :

```default
postgres$ createdb --echo  magasin
```
```sql
SELECT pg_catalog.set_config('search_path', '', false);
CREATE DATABASE magasin;
```

> - Importer le jeu de données d'exemple :

La base **magasin** (dump de 96 Mo, pour 667 Mo sur le disque au final)
peut être téléchargée et restaurée comme suit dans une nouvelle base **magasin** :
```bash
createdb magasin
curl -kL https://dali.bo/tp_magasin -o /tmp/magasin.dump
pg_restore -d magasin  /tmp/magasin.dump
# le message sur public préexistant est normal
rm -- /tmp/magasin.dump
```
Les données sont dans deux schémas, **magasin** et **facturation**.
Penser au `search_path`.

Pour ce TP, figer les paramètres suivants :

```sql
SET max_parallel_workers_per_gather to 0;
SET seq_page_cost TO 1 ;
SET random_page_cost TO 4 ;
```

> - Le schéma à utiliser se nomme également `magasin`.
> - Consulter les tables.

Le schéma par défaut `public` ne contient effectivement aucune table intéressante.

```default
\dn
     Liste des schémas
     Nom     | Propriétaire
-------------+--------------
 facturation | postgres
 magasin     | postgres
 public      | postgres
```
```sql
SET search_path to magasin ;
```
```default
 \dt+
                                     Liste des relations
 Schéma  |         Nom          | Type  | Propriétaire | Persistence |   Taille   | D…
---------+----------------------+-------+--------------+-------------+------------+---
 magasin | clients              | table | postgres     | permanent   | 8248 kB    |
 magasin | commandes            | table | postgres     | permanent   | 79 MB      |
 magasin | conditions_reglement | table | postgres     | permanent   | 16 kB      |
 magasin | contacts             | table | postgres     | permanent   | 24 MB      |
 magasin | etats_retour         | table | postgres     | permanent   | 16 kB      |
 magasin | fournisseurs         | table | postgres     | permanent   | 840 kB     |
 magasin | lignes_commandes     | table | postgres     | permanent   | 330 MB     |
 magasin | lots                 | table | postgres     | permanent   | 74 MB      |
 magasin | modes_expedition     | table | postgres     | permanent   | 16 kB      |
 magasin | modes_reglement      | table | postgres     | permanent   | 16 kB      |
 magasin | numeros_sequence     | table | postgres     | permanent   | 16 kB      |
 magasin | pays                 | table | postgres     | permanent   | 16 kB      |
 magasin | pays_transporteurs   | table | postgres     | permanent   | 8192 bytes |
 magasin | produit_fournisseurs | table | postgres     | permanent   | 216 kB     |
 magasin | produits             | table | postgres     | permanent   | 488 kB     |
 magasin | regions              | table | postgres     | permanent   | 16 kB      |
 magasin | transporteurs        | table | postgres     | permanent   | 16 kB      |
 magasin | types_clients        | table | postgres     | permanent   | 16 kB      |
```

Conseils pour la suite :

  * Préciser `\timing on` dans `psql` pour afficher les temps d'exécution de la recherche.

  * Pour rendre les plans plus lisibles, désactiver le JIT et le parallélisme :
```sql
SET jit TO off ;
SET max_parallel_workers_per_gather TO 0 ;
```

> - Lancer un `ANALYZE` sur la base.

```sql
ANALYZE ;
```

> Le but est de chercher une personne nommée Moris Russel dans la table `contacts` par les champs `prenom` et `nom`.
>
> - Quel est le plan qu'utilisera PostgreSQL pour le trouver ?
> - À combien de résultats le planificateur s'attend-il ?

```sql
 EXPLAIN SELECT * FROM contacts WHERE nom ='Russel'  AND prenom = 'Moris' ;
```
```default
                                   QUERY PLAN
---------------------------------------------------------------------------------
 Seq Scan on contacts  (cost=0.00..4693.07 rows=1 width=298)
   Filter: (((nom)::text = 'Russel'::text) AND ((prenom)::text = 'Moris'::text))
```

La table sera entièrement parcourue (_Seq Scan_). PostgreSQL pense qu'il trouvera une ligne.

> - Afficher le résultat.

```sql
 SELECT * FROM contacts WHERE nom ='Russel' AND prenom = 'Moris' ;
```
```default
-[ RECORD 1 ]----------------------------------------
contact_id  | 26452
login       | Russel_Moris
passwd      | 9f81a90c36dd3c60ff06f3c800ae4c1b
email       | ubaldo@hagenes-kulas-and-oberbrunner.mo
nom         | Russel
prenom      | Moris
adresse1    | 02868 Norris Greens
adresse2    | ¤
code_postal | 62151
ville       | Laguna Beach
code_pays   | CA
telephone   | {"+(05) 4.45.08.11.03"}

Temps : 34,091 ms
```

La requête envoie bien une ligne, et l'obtenir a pris 34 ms sur cette machine avec SSD.

> - Quel est le plan réellement exécuté ?

Il faut relancer la requête :

```sql
EXPLAIN (ANALYZE, BUFFERS) SELECT * FROM contacts
  WHERE nom ='Russel' AND prenom = 'Moris' ;
```
```default
                                   QUERY PLAN
---------------------------------------------------------------------------------
 Seq Scan on contacts  (cost=0.00..4693.07 rows=1 width=297)
                       (actual time=3.328..16.789 rows=1 loops=1)
   Filter: (((nom)::text = 'Russel'::text) AND ((prenom)::text = 'Moris'::text))
   Rows Removed by Filter: 110004
   Buffers: shared hit=3043
 Planning Time: 0.052 ms
 Execution Time: 16.848 ms
```

PostgreSQL a à nouveau récupéré une ligne. Ici, cela n'a pris que 17 ms.

La table a été parcourue entièrement, et 110 004 lignes ont été rejetées. La ligne _shared hit_ indique que 3043 blocs de 8 ko ont été lus dans le cache de PostgreSQL. La requête précédente a apparemment suffi à charger la table entière en cache (il n'y a pas de _shared read_).

> - Rechercher la même personne par son `contact_id`.
> - Quel est le plan ?

```sql
 EXPLAIN (ANALYZE, BUFFERS) SELECT * FROM contacts WHERE contact_id = 26452 ;
```
```default
                                   QUERY PLAN
---------------------------------------------------------------------------------
 Index Scan using contacts_pkey on contacts (cost=0.42..8.44 rows=1 width=297)
                                     (actual time=0.057..0.058 rows=1 loops=1)
   Index Cond: (contact_id = 26452)
   Buffers: shared hit=4 read=3
 Planning:
   Buffers: shared hit=6 read=3
 Planning Time: 0.137 ms
 Execution Time: 0.081 ms
```

PostgreSQL estime correctement trouver une ligne. Cette fois, il s'agit d'un _Index Scan_, en l'occurrence sur l'index de la clé primaire.
Le résultat est bien plus rapide : 137 µs pour planifier, 81 µs pour exécuter.

Les blocs lus se répartissent entre _read_ et _hit_ : une partie était en mémoire, notamment ceux liés à la table, puisque la table aussi a été interrogée (l'index ne contient que les données de `contact_id`) ; mais l'index n'était pas en mémoire.

> - La requête suivante recherche tous les fournisseurs résidents d'Hollywood.
> ```sql
> SELECT c.nom, c.prenom FROM contacts c
> INNER JOIN fournisseurs f
> ON (f.contact_id = c.contact_id)
> WHERE c.ville = 'Hollywood' ;
> ```
> - Quel est le plan prévu ?
> - Que donne-t-il à l'exécution ?

Le plan simplifié est :

```sql
 EXPLAIN (COSTS OFF)
 SELECT c.nom, c.prenom
 FROM contacts c INNER JOIN fournisseurs f ON (f.contact_id = c.contact_id)
 WHERE c.ville = 'Hollywood' ;
```
```default
                    QUERY PLAN
-----------------------------------------------------
 Merge Join
   Merge Cond: (c.contact_id = f.contact_id)
   ->  Index Scan using contacts_pkey on contacts c
         Filter: ((ville)::text = 'Hollywood'::text)
   ->  Sort
         Sort Key: f.contact_id
         ->  Seq Scan on fournisseurs f
```
Il consiste à parcourir intégralement la table `fournisseurs` (_Seq Scan_), à trier sa colonne `contact_id`,
et à effectuer une jointure de type _Merge Join_ avec la clé primaire de la table `contacts`.
En effet, un _Merge Join_ s'effectue entre deux ensembles triés : l'index l'est déjà, mais `fournisseurs.contact_id`
ne l'est pas.

Noter qu'aucune donnée n'est récupérée de `fournisseurs`. Il est pourtant nécessaire de la joindre
à `contacts` car de nombreux contacts ne sont _pas_ des fournisseurs.

Exécutée, cette requête renvoie le plan suivant :

```sql
EXPLAIN (ANALYZE,BUFFERS)
SELECT c.nom, c.prenom FROM contacts c
INNER JOIN fournisseurs f ON (f.contact_id = c.contact_id)
WHERE c.ville = 'Hollywood' ;
```
```default
                                   QUERY PLAN
---------------------------------------------------------------------------------
 Merge Join  (cost=864.82..1469.89 rows=31 width=14)
             (actual time=5.079..11.063 rows=32 loops=1)
   Merge Cond: (c.contact_id = f.contact_id)
   Buffers: shared hit=7 read=464
   ->  Index Scan using contacts_pkey on contacts c
                                    (cost=0.42..6191.54 rows=346 width=22)
                                    (actual time=0.029..4.842 rows=33 loops=1)
         Filter: ((ville)::text = 'Hollywood'::text)
         Rows Removed by Filter: 11971
         Buffers: shared hit=7 read=364
   ->  Sort  (cost=864.39..889.39 rows=10000 width=8)
             (actual time=5.044..5.559 rows=10000 loops=1)
         Sort Key: f.contact_id
         Sort Method: quicksort  Memory: 853kB
         Buffers: shared read=100
         ->  Seq Scan on fournisseurs f (cost=0.00..200.00 rows=10000 width=8)
                                        (actual time=0.490..2.960 rows=10000 loops=1)
               Buffers: shared read=100
 Planning:
   Buffers: shared hit=4
 Planning Time: 0.150 ms
 Execution Time: 11.174 ms
```

Ce plan est visible graphiquement sur <https://explain.dalibo.com/plan/dum> :

![Plan d'exécution](medias/tp/magasin-fournisseurs-hollywood.png)

Le _Seq Scan_ sur `fournisseurs` lit 10 000 lignes (100 blocs, hors du cache), ce qui était prévu. Cela prend 2,96 ms.
Le nœud _Sort_ trie les `contact_id` et  consomme 853 ko en mémoire. Il renvoie bien sûr aussi 10 000 lignes,
et il commence à le faire au bout de 5,04 ms.

La jointure peut commencer. Il s'agit de parcourir simultanément l'ensemble que l'on vient de trier d'une part,
et l'index `contacts_pkey` d'autre part. À cette occasion, le nœud _Index Scan_ va filtrer les lignes récupérées en comparant à
la valeur de `ville`, et en exclue 11 971.
<!-- plus que dans la table ... -->
Au final, le parcours de l'index sur `contacts` renvoie 33 lignes, et non les 346 estimées au départ
(valeur dérivée de l'estimation du nombre de lignes où la ville est « Hollywood »).
Si l'on regarde les coûts calculés, c'est cette étape qui est la plus lourde (6191).

En haut, on peut lire qu'au total 464 blocs ont été lus hors du cache, et 7 dedans.
Ces valeurs varient bien sûr en fonction de l'activité précédente sur la base. Au final, 32 lignes sont retournées,
ce qui était attendu.

Le temps écoulé est de 11,17 ms. La majorité de ce temps s'est déroulé pendant le _Merge Join_ (11,0-5,0 = 6 ms),
dont l'essentiel est constitué par le parcours de l'index.


</div>

