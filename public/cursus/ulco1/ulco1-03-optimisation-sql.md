---
revision: 24.12
date: "Année académique 2024-2025"

licence : "Creative Commons BY-NC-SA"
include_licence : CC-BY-NC-SA-2.0-FR

author: © 2005-2025 DALIBO SARL SCOP

trademarks: |
  Le logo éléphant de PostgreSQL (« Slonik ») est une création sous copyright et
  le nom « PostgreSQL » est une marque déposée par PostgreSQL Community Association
  of Canada.

##
## PDF Options
##

## Limiter la profondeur de la table des matières
toc-depth: 2

## Mettre les lien http en pieds de page
links-as-notes: false

## Police plus petite dans un bloc de code

code-blocks-fontsize: small

## Filtre : pandoc-latex-admonition
## les catégories `important` et `warning` sont synonymes
## même chose pour `tip` et `note`
pandoc-latex-admonition:
  - color: Red
    classes: [warning]
    linewidth: 4
  - color: Red
    classes: [important]
    linewidth: 4
  - color: DarkSeaGreen
    classes: [tip]
    linewidth: 4
  - color: DarkSeaGreen
    classes: [note]
    linewidth: 4
  - color: DodgerBlue
    classes: [slide-content]
    linewidth: 4

##
## Reveal Options
##

## Taille affichage
width: 960
height: 700

## beige/blood/moon/simple/solarized/black/league/night/serif/sky/white
theme: white

## None - Fade - Slide - Convex - Concave - Zoom
transition: None
transitionSpeed: fast

## Barre de progression
progress: true

## Affiche N° de slide
slideNumber: true

## Le numero de slide apparait dans la barre d'adresse
history: true

## Defilement des slides avec la roulette
mouseWheel: true

## Annule la transformation uppercase de certains thèmes
title-transform : none

## Cache l'auteur sur la première slide
## Mettre en commentaire pour désactiver
hide_author_in_slide: true

title : 'Optimiser une application'
subtitle : 'FCU Calais - M1 I2L'
---

# PostgreSQL : Optimisations SQL

![PostgreSQL](medias/logos/slonik.png){ height=500 }
\

<div class="notes">

</div>

---

### Introduction

<div class="slide-content">

L'optimisation doit porter sur :

 * Le matériel
     + serveur, distribution, kernel, stockage, réseau…
 * Le moteur de la base :
   + `postgresql.conf` & co
   + l'organisation des fichiers de PostgreSQL
 * L'application
   + schéma, requêtes, vues…

</div>

<div class="notes">

Les bases de données sont des systèmes très complexes. Afin d'en tirer toutes
les performances possibles, l'optimisation doit porter sur un très grand nombre
de composants différents : le serveur qui héberge la base de données,
et de manière générale tout l'environnement matériel, les
processus faisant fonctionner la base de données, les fichiers et disques
servant à son stockage, le partitionnement,
mais aussi, et surtout, l'application elle-même. C'est
sur ce dernier point que les gains sont habituellement les plus importants.
(Les autres sont traités entre autres dans le module de formation
[J1](https://dali.bo/j1_html).)

Ce module se focalise sur ce dernier point. Il n'aborde pas les plans
d'exécution à proprement parler, ceux-ci étant traités ailleurs.

</div>

---

## Axes d'optimisation

<div class="slide-content">

« 80% des effets sont produits par 20% des causes. »  (Principe de Pareto)

  * Il est illusoire d'optimiser une application sans connaître les sources de ralentissement
  * Cibler l'optimisation :
    + trouver ces « 20% »
    + ne pas micro-optimiser ce qui n'a pas d'influence

</div>

<div class="notes">

Le principe de Pareto et la loi de Pareto sont des constats empiriques. On peut
définir mathématiquement une distribution vérifiant le
[principe de Pareto](https://fr.wikipedia.org/wiki/Principe_de_Pareto).

> Le pourcentage de la population dont la richesse est
> supérieure à une valeur x est proportionnel à A/x^α »
>
> [Vilfredo Pareto](https://fr.wikipedia.org/wiki/Vilfredo_Pareto), économiste du XIXe siècle

De nombreux phénomènes suivent cette distribution.
Dans le cadre de PostgreSQL, cela se vérifie souvent. Une poignée de requêtes peut
être responsable de l'essentiel du ralentissement. Une table mal conçue peut être
responsable de la majorité de vos problèmes.
Le temps de développement n'était pas infini, il ne sert à rien de passer beaucoup de
temps à optimiser chaque paramètre sans avoir identifié les principaux consommateurs
de ressource.

</div>

---

### Quelles requêtes optimiser ?

<div class="slide-content">

Seul un certain nombre de requêtes sont critiques

  * Identification (outil de profiling)
    + à optimiser prioritairement
  * Différencier
    + longues en temps cumulé = coûteuses en ressources serveur
    + longues et interactives = mauvais ressenti des utilisateurs

</div>

<div class="notes">

Toutes les requêtes ne sont pas critiques, seul un certain nombre d'entre elles
méritent une attention particulière.
Il y a deux façon de déterminer les requêtes qui nécessitent d'être
travaillées.

La première dépend du ressenti utilisateur : un utilisateur
devant son écran est notoirement impatient. Il faudra donc en
priorité traiter les requêtes interactives. Certaines auront déjà
d'excellents temps de réponse, d'autres pourront être améliorées.

L'autre méthode pour déterminer les requêtes à optimiser consiste à
utiliser les outils de profiling habituels, dont nous allons voir quelques exemples.
Ces outils permettent de déterminer les requêtes les plus
fréquemment exécutées, et d'établir un classement des requêtes
qui ont nécessité le plus de temps cumulé à leur exécution (voir l'onglet
_Time consuming queries (N)_ d'un rapport pgBadger). Les requêtes les plus
fréquemment exécutées méritent également qu'on leur porte attention.
Même si leur temps d'exécution cumulé est acceptable, leur
optimisation peut permettre d'économiser quelques ressources du serveur,
et une dérive peut avoir vite de gros impacts.

</div>

---

### Recherche des axes d'optimisation

<div class="slide-content">

Quelques profilers :

  * pgBadger
    + <https://pgbadger.darold.net>
  * pg_stat_statements :
    + <https://dali.bo/x2_html#pg_stat_statements>
  * [PoWA]
    + <https://powa.readthedocs.io/en/latest/>

<!-- URL directes cause bug #838 dans les PDF... -->

</div>

<div class="notes">

Trois outils courants permettent d'identifier rapidement les requêtes les plus
consommatrices sur un serveur. Les outils pour PostgreSQL ont le fonctionnement
suivant :

**pgBadger** :

pgBadger est un analyseur de fichiers de traces PostgreSQL. Il nécessite de tracer dans les journaux
applicatifs de PostgreSQL toutes les requêtes et leur durée.
L'outil sait repérer des requêtes identiques avec des paramètres différents.
Il les analyse
et retourne les requêtes les plus fréquemment exécutées, les plus gourmandes
unitairement, les plus gourmandes en temps cumulé (somme des temps unitaires).

Pour plus de détails, voir <https://dali.bo/h1_html#pgbadger>.

**pg_stat_statements** :

L'extension `pg_stat_statements`[](https://dali.bo/x2_html#pg_stat_statements)
est livrée avec PostgreSQL. Elle trace pour chaque ordre (même SQL, paramètres différents)
exécuté sur l'instance son nombre d'exécutions, sa durée cumulée, et un
certain nombre d'autres statistiques très utiles.

<!-- Juste 2 exemples, le reste est dans H2 ou X2 -->

Si elle est présente, la requête suivante permet de
déterminer les requêtes dont les temps d'exécution cumulés sont les plus
importants :

```sql
SELECT r.rolname, d.datname, s.calls, s.total_exec_time,
       s.total_exec_time / s.calls AS avg_time, s.query
  FROM pg_stat_statements s
  JOIN pg_roles r     ON (s.userid=r.oid)
  JOIN pg_database d  ON (s.dbid = d.oid)
 ORDER BY s.total_exec_time DESC
 LIMIT 10 ;
```

Et la requête suivante permet de
déterminer les requêtes les plus fréquemment appelées :

```sql
SELECT r.rolname, d.datname, s.calls, s.total_exec_time,
       s.total_exec_time / s.calls  AS avg_time, s.query
  FROM pg_stat_statements s
  JOIN pg_roles r     ON (s.userid=r.oid)
  JOIN pg_database d  ON (s.dbid = d.oid)
 ORDER BY s.calls DESC
 LIMIT 10;
```

<!-- ça suffira pour une intro -->

Pour plus de détails sur les métriques relevées,
voir  <https://dali.bo/h2_html#pg_stat_statements>,
et pour l'installation et des exemples de requêtes,
voir <https://dali.bo/x2_html#pg_stat_statements>,
ou encore la
[documentation officielle](https://docs.postgresql.fr/current/pgstatstatements.html)
<!-- en attendant la KB ? -->

**PoWA** :

PoWA est une extension disponible dans les dépôts du PGDG,
qui s'appuie sur `pg_stat_statements`
pour historiser l'activité du serveur. Une interface web permet
ensuite de visualiser l'activité ainsi historisée et repérer les requêtes
problématiques avec les fonctionnalités de _drill-down_ de l'interface.

</div>

---

## SQL et requêtes

<div class="slide-content">

Le SQL :

  * est un langage déclaratif :
    + on décrit le résultat, pas la façon de l'obtenir
    + c'est le travail de la base de déterminer le traitement à effectuer
  * décrit un traitement ensembliste :
    + ≠ traitement procédural
    + « _on effectue des opérations sur des relations pour obtenir des relations_ »
  * est normalisé

</div>

<div class="notes">

Le langage [SQL](https://fr.wikipedia.org/wiki/Structured_Query_Language) a été normalisé par l'ANSI en 1986 et est devenu une norme ISO
internationale en 1987. Il s'agit de la norme ISO 9075. Elle a subi plusieurs
évolutions dans le but d'ajouter des fonctionnalités correspondant aux
attentes de l'industrie logicielle. Parmi ces améliorations, notons
l'intégration de quelques fonctionnalités objet pour le modèle
relationnel-objet. La dernière version stable de la norme est
[SQL:2023](https://en.wikipedia.org/wiki/SQL:2023) (juin 2023).

</div>

---

### Opérateurs relationnels

<div class="slide-content">
Les opérateurs purement relationnels :

  * Projection = `SELECT`
    + choix des colonnes
  * Sélection = `WHERE`
    + choix des enregistrements
  * Jointure = `FROM/JOIN`
    + choix des tables
  * Bref : tout ce qui détermine sur quelles données on travaille
</div>

<div class="notes">

Tous ces opérateurs sont optimisables : il y a 40 ans de théorie mathématique
développée afin de permettre l'optimisation de ces traitements. L'optimiseur
fera un excellent travail sur ces opérations, et les organisera de façon
efficace.

Par exemple : `a JOIN b JOIN c WHERE c.col=constante` sera très probablement
réordonné en `c JOIN b JOIN a WHERE c.col=constante` ou `c JOIN a JOIN b WHERE
c.col=constante`. Le moteur se débrouillera aussi pour choisir le meilleur
algorithme de jointure pour chacune, suivant les volumétries ramenées.

</div>

---

### Opérateurs non-relationnels

<div class="slide-content">
Les autres opérateurs sont non-relationnels :

  * `ORDER BY`
  * `GROUP BY/DISTINCT`
  * `HAVING`
  * sous-requête, vue
  * fonction (classique, d'agrégat, analytique)
  * jointure externe

</div>

<div class="notes">

Ceux-ci sont plus difficilement optimisables : ils introduisent par exemple des
contraintes d'ordre dans l'exécution :

```sql
SELECT * FROM table1
WHERE montant > (
 SELECT avg(montant) FROM table1 WHERE departement='44'
);
```

On doit exécuter la sous-requête avant la requête.

Les jointures externes sont relationnelles, mais posent tout de même des
problèmes et doivent être traitées prudemment.

```sql
SELECT * FROM t1
LEFT JOIN t2 on (t1.t2id=t2.id)
JOIN t3 on (t1.t3id=t3.id) ;
```

Il faut faire les jointures dans l'ordre indiqué : joindre `t1` à `t3` puis le
résultat à `t2` pourrait ne pas amener le même résultat (un `LEFT JOIN` peut
produire des `NULL`). Il est donc préférable de toujours mettre les jointures
externes en fin de requête, sauf besoin précis : on laisse bien plus de
liberté à l'optimiseur.

Le mot clé `DISTINCT` ne doit être utilisé qu'en dernière extrémité. On le
rencontre très fréquemment dans des requêtes mal écrites qui produisent des
doublons, afin de corriger le résultat — souvent en passant par un tri de
l'ensemble du résultat, ce qui est coûteux.

</div>

---

### Données utiles

<div class="slide-content">

Le volume de données récupéré a un impact sur les performances.

  * N'accéder qu'aux tables nécessaires
  * N'accéder qu'aux colonnes nécessaires
    + viser _Index Only Scan_
    + se méfier : stockage TOAST
  * Plus le volume de données à traiter est élevé, plus les opérations seront
lentes :
    + tris et Jointures
    + éventuellement stockage temporaire sur disque pour certains algorithmes

</div>

<div class="notes">

**Éviter les `SELECT *`** :

C'est une bonne pratique, car la
requête peut changer de résultat suite à un changement de schéma, ce qui risque
d'entraîner des conséquences sur le reste du code.

**Ne récupérer que les colonnes utilisées** :

<!-- FIXME :  exemple complet à refaire

Certains moteurs suppriment
d'eux-même les colonnes qui ne sont pas retournées à l'appelant, comme par
exemple dans le cas de :

```sql
SELECT col1, col2 FROM (SELECT * FROM t1 JOIN t2 USING (t2id) ) x ;
```

PostgreSQL ne le fait pas pour le moment.


En précisant uniquement les colonnes nécessaires, le moteur peut utiliser un parcours
appelé _Index Only Scan_ qui lui permet d'éviter d'avoir aussi à lire des morceaux de
la table.

-->

Dans beaucoup de cas, PostgreSQL sait repérer des colonnes qui figurent
dans la requête mais sont finalement inutiles. Mais il n'est pas parfait. 
Surtout, il ne pourra pas repérer que vous n'avez pas réellement besoin
d'une colonne issue d'une requête.
En précisant uniquement les colonnes nécessaires, le moteur peut parfois
utiliser des parcours plus simples, notamment des _Index Only Scans_.
Il peut aussi éviter de lire les colonnes à gros contenu qui sont généralement
déportés dans la partie [TOAST](https://dali.bo/m4_html#mécanisme-toast)
d'une table (des fichiers séparés de la table principale pour certains grands
champs, transparents à l'utilisation mais dont l'accès n'est pas gratuit).

**Éviter les jointures sur des tables inutiles** :

Il n'y a que peu de cas où
l'optimiseur peut supprimer de lui-même l'accès à une table inutile.
Notamment, PostgreSQL le fait dans le cas d'un `LEFT JOIN` sur une table inutilisée dans le
`SELECT`, au travers d'une clé étrangère, car on peut garantir que cette table
est effectivement inutile.

</div>

---

### Limiter le nombre de requêtes

<div class="slide-content">
SQL : langage ensembliste

  * Ne pas faire de traitement unitaire par enregistrement
  * Utiliser les jointures, ne pas accéder à chaque table une par une
  * Une seule requête, parcours de curseur
  * Fréquent avec les ORM

</div>

<div class="notes">

Les bases de données relationnelles sont conçues pour manipuler des relations,
pas des enregistrements unitaires.
Le langage SQL (et même les autres langages relationnels qui ont existé comme
QUEL, SEQUEL) est conçu pour permettre la manipulation d'un gros volume de
données, et la mise en correspondance (jointure) d'informations. Une base de
données relationnelle n'est pas une simple couche de persistance.

Le fait de récupérer en une seule opération l'ensemble des informations
pertinentes est aussi, indépendamment du langage, un gain de performance
énorme, car il permet de s'affranchir en grande partie des latences de
communication entre la base et l'application.

Préparons un jeu de test :
```sql
CREATE TABLE test (id int, valeur varchar);
INSERT INTO test SELECT i,chr(i%94+32) FROM generate_series (1,1000000) g(i);
ALTER TABLE test ADD PRIMARY KEY (id);
VACUUM ANALYZE test ;
```

Le script perl génère 1000 ordres pour récupérer des
enregistrements un par un,
avec une requête préparée pour être dans le cas le plus efficace :

<!-- FIXME remplacer par \o script.sql ou  \gexec -->

```perl
#!/usr/bin/perl -w
print "PREPARE ps (int) AS SELECT * FROM test WHERE id=\$1;\n";
for (my $i=0;$i<=1000;$i++)
{
        print "EXECUTE ps($i);\n";
}
```

Exécutons ce code :

```bash
time perl demo.pl  | psql > /dev/null

real    0m44,476s
user    0m0,137s
sys     0m0,040s
```

La durée totale de ces 1000 requêtes dépend fortement de la distance
au serveur de base de données. Si le serveur est sur le même sous-réseau,
on peut descendre à une seconde. Noter que l'établissement de la connexion
au serveur n'a lieu qu'une fois.

Voici maintenant la même chose, en un seul ordre SQL, avec la même volumétrie
en retour :

```bash
time psql -c "SELECT * FROM test WHERE id BETWEEN 0 AND 1000" > /dev/null

real    0m0,129s
user    0m0,063s
sys     0m0,018s
```

Les allers-retours au serveur sont donc très coûteux dès qu'ils se cumulent.

Le problème se rencontre assez fréquemment avec des ORM.
La plupart d'entre eux fournissent un moyen de traverser des liens entre objets.
Par exemple, si une commande est liée à plusieurs articles, un ORM permettra
d'écrire un code similaire à celui-ci (exemple en Java avec Hibernate) :

```java
List commandes = sess.createCriteria(Commande.class);

for(Commande cmd : commandes)
{
  // Un traitement utilisant les produits
  // Génère une requête par commande !!
  System.out.println(cmd.getProduits());
}
```

Tel quel, ce code générera *N+1* requête, N étant le nombre de commandes.
En effet, pour chaque accès à l'attribut "produits", l'ORM générera une
requête pour récupérer les produits correspondants à la commande.

Le SQL généré sera alors similaire à celui-ci :

```sql
SELECT * FROM commande;
SELECT * from produits where commande_id = 1;
SELECT * from produits where commande_id = 2;
SELECT * from produits where commande_id = 3;
SELECT * from produits where commande_id = 4;
SELECT * from produits where commande_id = 5;
SELECT * from produits where commande_id = 6;
...
```
<div class="box tip">
La plupart des ORM proposent des options pour personnaliser la stratégie d'accès
aux collections. Il est extrêmement important de connaître celles-ci afin de
permettre à l'ORM de générer des requêtes optimales.
</div>

Par exemple, dans le cas précédent, nous savons que tous les produits de
toutes les commandes seront utilisés. Nous pouvons donc informer l'ORM de ce
fait :

```java
List commandes = sess.createCriteria(Commande.class)
  .setFetchMode('produits', FetchMode.EAGER);

for(Commande cmd : commandes)
{
  // Un traitement utilisant les produits
  System.out.println(cmd.getProduits());
}
```

Ceci générera une seule et unique requête du type :

```sql
SELECT * FROM commandes
LEFT JOIN produits ON commandes.id = produits.commande_id;
```
</div>

---

### Sous-requêtes dans un IN

<div class="slide-content">

Un _Semi Join_ peut être très efficace (il ne lit pas tout)

```sql
SELECT * FROM t1
  WHERE val1  IN (  SELECT val2 …  )
```

  * Sinon attention s'il y a beaucoup de valeurs dans la sous-requête !
    + dédoublonner :
```sql
SELECT * FROM t1
  WHERE val1 IN (  SELECT  DISTINCT  val2 …  )
```
    + surtout : réécriture avec `EXISTS` (si index disponible)

</div>

<div class="notes">

De la même manière que pour la clause `EXISTS`,
un des intérêts du `IN` est de savoir quand
il n'y a pas besoin de lire toute la sous-requête, comme ici,
où seuls 5 blocs de la grande table `lots` sont lus,
qui contiennent déjà toutes les valeurs possibles de `transporteur_id` :
<!-- FIXME  : expliquer pourquoi le Nested L Anti Join
est si efficace dans cette version simplifiée.
https://www.dalibo.info/home/christophe/public/in_nested_loop_semi_join_astuce_effet_de_cache_distinct
-->
```sql
EXPLAIN (ANALYZE,BUFFERS)
SELECT nom
FROM transporteurs
WHERE transporteur_id IN (
        SELECT transporteur_id
        FROM lots
        WHERE date_depot IS NOT NULL
        ) ;
```
```default
                               QUERY PLAN
-------------------------------------------------------------------------------
 Nested Loop Semi Join  (cost=0.00..19478.49 rows=5 width=12) (actual time=0.032..0.042 rows=5 loops=1)
   Join Filter: (transporteurs.transporteur_id = lots.transporteur_id)
   Rows Removed by Join Filter: 13
   Buffers: shared hit=5 read=1
   ->  Seq Scan on transporteurs  (cost=0.00..1.05 rows=5 width=20) (actual time=0.004..0.005 rows=5 loops=1)
         Buffers: shared hit=1
   ->  Seq Scan on lots  (cost=0.00..19476.04 rows=1006704 width=8) (actual time=0.005..0.006 rows=4 loops=5)
         Filter: (date_depot IS NOT NULL)
         Buffers: shared hit=4 read=1
 Planning:
   Buffers: shared hit=173 read=1
 Planning Time: 0.664 ms
 Execution Time: 0.069 ms
```

Mais la requête dans le `IN` peut être arbitrairement complexe,
l'optimisation peut échouer et PostgreSQL peut basculer sur une forme de jointure plus lourde
avec un regroupement des valeurs de la sous-requête :

```sql
EXPLAIN (ANALYZE,COSTS OFF)
SELECT nom
FROM transporteurs
WHERE transporteur_id IN (
        SELECT transporteur_id + 0  /* modification du critère */
        FROM lots
        WHERE date_depot IS NOT NULL
        ) ;
```
```default
                               QUERY PLAN
-------------------------------------------------------------------------------
 Hash Join (actual time=139.280..139.283 rows=5 loops=1)
   Hash Cond: (transporteurs.transporteur_id = (lots.transporteur_id + 0))
   ->  Seq Scan on transporteurs (actual time=0.010..0.011 rows=5 loops=1)
   ->  Hash (actual time=139.265..139.265 rows=5 loops=1)
         Buckets: 1024  Batches: 1  Memory Usage: 9kB
         ->  HashAggregate (actual time=139.259..139.261 rows=5 loops=1)
               Group Key: (lots.transporteur_id + 0)
               Batches: 1  Memory Usage: 24kB
               ->  Seq Scan on lots (actual time=0.010..70.184 rows=1006704 loops=1)
                     Filter: (date_depot IS NOT NULL)
 Planning Time: 0.201 ms
 Execution Time: 139.325 ms
```
Le dédoublonnage explicite au sein même de la sous-requête est alors parfois une bonne idée,
même s'il vient forcément avec un certain coût :

```sql
EXPLAIN (ANALYZE,BUFFERS)
SELECT nom
FROM transporteurs
WHERE transporteur_id IN (
        SELECT   DISTINCT   transporteur_id + 0
        FROM lots
        WHERE date_depot IS NOT NULL
        ) ;
```
```default
                               QUERY PLAN
-------------------------------------------------------------------------------
 Hash Join (actual time=46.385..48.943 rows=5 loops=1)
   Hash Cond: (transporteurs.transporteur_id = ((lots.transporteur_id + 0)))
   ->  Seq Scan on transporteurs (actual time=0.005..0.006 rows=5 loops=1)
   ->  Hash (actual time=46.375..48.931 rows=5 loops=1)
         Buckets: 1024  Batches: 1  Memory Usage: 9kB
         ->  Unique (actual time=46.367..48.925 rows=5 loops=1)
               ->  Sort (actual time=46.366..48.922 rows=15 loops=1)
                     Sort Key: ((lots.transporteur_id + 0))
                     Sort Method: quicksort  Memory: 25kB
                     ->  Gather (actual time=46.306..48.914 rows=15 loops=1)
                           Workers Planned: 2
                           Workers Launched: 2
                           ->  HashAggregate (actual time=44.707..44.708 rows=5 loops=3)
                                 Group Key: (lots.transporteur_id + 0)
                                 Batches: 1  Memory Usage: 24kB
                                 Worker 0:  Batches: 1  Memory Usage: 24kB
                                 Worker 1:  Batches: 1  Memory Usage: 24kB
                                 ->  Parallel Seq Scan on lots (actual time=0.008..23.330 rows=335568 loops=3)
                                       Filter: (date_depot IS NOT NULL)
 Planning Time: 0.096 ms
 Execution Time: 48.979 ms
```

Mais on ne retrouve pas les performances de la première version.

Une autre possibilité est de réécrire la requête avec `EXISTS`
mais cela n'a d'intérêt ici que si on peut indexer le champ calculé ;
auquel cas les requêtes ci-dessus peuvent redevenir efficaces.

```sql
SELECT nom
FROM transporteurs t
WHERE EXISTS (
        SELECT 'ok'
        FROM lots l
        WHERE date_depot IS NOT NULL
        AND l.transporteur_id+0 /* à indexer */
                          = t.transporteur_id
        ) ;
```

</div>

---

### Sous-requêtes liées

<div class="slide-content">

À éviter :

```sql
SELECT a,b
  FROM t1
  WHERE val IN (  SELECT f(b)  )
```

  * un appel de fonction ou sous-requête par ligne !
  * est-ce voulu ?
  * transformer en clause `WHERE`
  * penser à la clause `LATERAL`

</div>

<div class="notes">

Dans l'exemple ci-dessus, le résultat de la sous-requête dépend de la valeur
`b` de chaque ligne de `t1`. On a donc autant d'appels, ce qui peut être une
catastrophe. L'expérience montre que ce n'est parfois pas vraiment voulu…

Selon la complexité de la sous-requête, elle peut être réécrite en une simple clause
`WHERE`. Il faut connaître aussi la clause
`LATERAL`[] (https://docs.postgresql.fr/current/queries-table-expressions.html#QUERIES-LATERAL)
dédiée à ce genre de chose et qui a au moins le mérite d'être lisible et explicite.

</div>

---

### Sous-requêtes : équivalences IN/EXISTS/LEFT JOIN

<div class="slide-content">

Ces sous-requêtes sont strictement équivalentes (_Semi-join_) :

```sql
SELECT * FROM t1
WHERE fk IN ( SELECT pk FROM t2 WHERE … )

SELECT * FROM t1
WHERE EXISTS  ( SELECT 1 FROM t2 WHERE t2.pk = t1.fk AND … )

SELECT t1.*
FROM   t1   LEFT JOIN t2   ON (t1.fk=t2.pk)
WHERE
     t2.id IS NULL
```

(Et _Anti-join_ pour les variantes avec `NOT`)

  * Attention à `NOT IN` : préférer `NOT EXISTS`

</div>

<div class="notes">

Les seules sous-requêtes sans danger sont celles qui retournent un ensemble
constant et ne sont exécutés qu'une fois, ou celles qui expriment un _Semi-Join_
(test d'existence) ou _Anti-Join_ (test de non-existence), qui sont presque des
jointures : la seule différence est qu'elles ne récupèrent pas
l'enregistrement de la table cible.

Attention toutefois à l'utilisation du prédicat `NOT IN`, ils peuvent générer
des plans d'exécution catastrophiques, avec une exécution de la sous-requête
par ligne.

```sql
EXPLAIN SELECT *
FROM    commandes c
WHERE   numero_commande NOT IN (SELECT l.numero_commande
                                FROM   lignes_commandes l );
```
```default
                               QUERY PLAN
-------------------------------------------------------------------------------
 Gather  (cost=1000.00..22803529388.17 rows=500000 width=51)
   Workers Planned: 2
   ->  Parallel Seq Scan on commandes c  (cost=0.00..22803478388.17 rows=208333 width=51)
         Filter: (NOT (SubPlan 1))
         SubPlan 1
           ->  Materialize  (cost=0.00..101602.11 rows=3141807 width=8)
                 ->  Seq Scan on lignes_commandes l  (cost=0.00..73620.07 rows=3141807 width=8)
```
La raison est la suivante : si un `NULL` est présent dans la liste du `NOT IN`,
`NOT IN` vaut systématiquement _false_.
Or, nous savons qu'il n'y aura pas de `numero_commandes` à `NULL`.
(Dans cette requête précise, PostgreSQL aurait pu le deviner car le champ
`lignes_commandes.numero_commande` est `NOT NULL`,
mais il n'en est pas encore capable.)
Une réécriture avec `EXISTS` est strictement
équivalente et produit un plan d'exécution largement plus intéressant
avec un _Hash Right Anti Join_ :

```sql
EXPLAIN SELECT *
FROM commandes
WHERE NOT EXISTS ( SELECT 1
                   FROM lignes_commandes l
                   WHERE l.numero_commande = commandes.numero_commande );
```
```default
                                 QUERY PLAN
------------------------------------------------------------------------------
 Gather  (cost=24604.00..148053.15 rows=419708 width=51)
   Workers Planned: 2
   ->  Parallel Hash Right Anti Join  (cost=23604.00..105082.35 rows=174878 width=51)
         Hash Cond: (l.numero_commande = commandes.numero_commande)
         ->  Parallel Seq Scan on lignes_commandes l  (cost=0.00..55292.86 rows=1309086 width=8)
         ->  Parallel Hash  (cost=14325.67..14325.67 rows=416667 width=51)
               ->  Parallel Seq Scan on commandes  (cost=0.00..14325.67 rows=416667 width=51)
```

</div>

---

### Les vues

<div class="slide-content">
Une vue est une requête pré-déclarée en base.

  * Équivalent relationnel d'une fonction
  * Si utilisée dans une autre requête, elle est traitée
comme une sous-requête
  * et _inlinée_
  * Pas de problème si elle est relationnelle…

</div>

<div class="notes">

Les vues sont des requêtes dont le code peut être inclus
dans une autre requête, comme s'il s'agissait d'une
sous-requête.
Les vues sont très pratiques en SQL et, en théorie, permettent de séparer le
modèle physique (les tables) de ce que voient les développeurs, et donc de
faire évoluer le modèle physique sans impact pour le développement.
Elles sont surtout très pratiques pour rendre les requêtes plus lisibles,
permettre la réutilisation de code SQL,
et masquer la complexité à des utilisateurs peu avertis.

Dans le cas idéal, une vue reste relationnelle et donc ne contient
que `SELECT`, `FROM` et `WHERE`, et elle peut être fusionnée avec
le reste de la requête ; y compris avec des vues basées sur des vues.
Ici les critères de deux vues imbriquées se retrouvent dans un même
nœud :

<!--  table test définie plus haut --> 
```sql
CREATE OR REPLACE VIEW v_test_az
AS SELECT * FROM test
WHERE valeur BETWEEN 'A' AND 'Z' ;

CREATE OR REPLACE VIEW v_test_1000_az
AS SELECT * FROM v_test_az
WHERE id < 100 ;

EXPLAIN SELECT * FROM v_test_1000_az ;
```
```default
                                 QUERY PLAN
------------------------------------------------------------------------------
 Index Scan using test_pkey on test  (cost=0.42..10.68 rows=54 width=6)
   Index Cond: (id < 100)
   Filter: (((valeur)::text >= 'A'::text) AND ((valeur)::text <= 'Z'::text))
```

</div>

---

### Éviter les vues non-relationnelles

<div class="slide-content">

  * Attention aux vues avec `DISTINCT`, `GROUP BY` etc.
    + tous les problèmes des sous-requêtes déjà vus
    + impossible de l'_inliner_
    + barrière d'optimisation
    + …et mauvaises performances
  * Les vues sont dangereuses en termes de performance
    + masquent la complexité
  * Penser aux vues matérialisées si la requête est lourde

</div>

<div class="notes">

En pratique, les vues sont souvent sources de ralentissement : elles masquent la
complexité, et l'utilisateur crée alors sans le savoir des
requêtes très complexes, mettant en jeu des dizaines de tables (voire des
dizaines de fois les **mêmes** tables !).
<div class="box waning">
Avant d'utiliser une vue, il faut s'intéresser un peu à son contenu
et ce qu'elle fait.
</div>

On retrouve aussi toute la complexité liée aux sous-requêtes,
puisqu'une vue en est l'équivalent.
En particulier, il faut se méfier des vues contenant des
opérations non-relationnelles, qui peuvent empêcher de nombreuses
optimisations. En voici un exemple simple :

<!--  table test définie plus haut -->
```sql
CREATE OR REPLACE VIEW v_test_did
AS SELECT DISTINCT ON (id) id,valeur FROM test ;

EXPLAIN (ANALYZE, COSTS OFF)
  SELECT id,valeur
  FROM v_test_did
  WHERE valeur='b' ;
```
```default
                                   QUERY PLAN
--------------------------------------------------------------------------------
 Subquery Scan on v_test_did (actual time=0.070..203.584 rows=10638 loops=1)
   Filter: ((v_test_did.valeur)::text = 'b'::text)
   Rows Removed by Filter: 989362
   ->  Unique (actual time=0.017..158.458 rows=1000000 loops=1)
         ->  Index Scan using test_pkey on test (actual time=0.015..98.200 rows=1000000 loops=1)
 Planning Time: 0.202 ms
 Execution Time: 203.872 ms
```
<!-- PG 16.0 -->

On constate que la condition de filtrage sur `b` n'est appliquée qu'à la fin.
C'est normal : à cause du `DISTINCT ON`, l'optimiseur ne peut pas savoir si
l'enregistrement qui sera retenu dans la sous-requête vérifiera `valeur='b'` ou
pas, et doit donc attendre l'étape suivante pour filtrer. Le coût en
performances, même avec un volume de données raisonnable, peut être astronomique.

</div>

---

## Accès aux données

<div class="slide-content">

L'accès aux données est coûteux.

  * Quelle que soit la base
  * Dialogue entre client et serveur
    + plusieurs aller/retours potentiellement
  * Analyse d'un langage complexe
    + SQL PostgreSQL : `gram.y` de 19000 lignes
  * Calcul de plan :
    + langage déclaratif => converti en impératif à chaque exécution

</div>

<div class="notes">

Dans les captures réseau ci-dessous, le serveur est sur le port 5932.

`SELECT * FROM test`, 0 enregistrement :

```default
10:57:15.087777 IP 127.0.0.1.39508 > 127.0.0.1.5932:
    Flags [P.], seq 109:134, ack 226, win 350,
    options [nop,nop,TS val 2270307 ecr 2269578], length 25
10:57:15.088130 IP 127.0.0.1.5932 > 127.0.0.1.39508:
    Flags [P.], seq 226:273, ack 134, win 342,
    options [nop,nop,TS val 2270307 ecr 2270307], length 47
10:57:15.088144 IP 127.0.0.1.39508 > 127.0.0.1.5932:
    Flags [.], ack 273, win 350,
    options [nop,nop,TS val 2270307 ecr 2270307], length 0
```

`SELECT * FROM test`, 1000 enregistrements :

```default
10:58:08.542660 IP 127.0.0.1.39508 > 127.0.0.1.5932:
    Flags [P.], seq 188:213, ack 298, win 350,
    options [nop,nop,TS val 2286344 ecr 2285513], length 25
10:58:08.543281 IP 127.0.0.1.5932 > 127.0.0.1.39508:
    Flags [P.], seq 298:8490, ack 213, win 342,
    options [nop,nop,TS val 2286344 ecr 2286344], length 8192
10:58:08.543299 IP 127.0.0.1.39508 > 127.0.0.1.5932:
    Flags [.], ack 8490, win 1002,
    options [nop,nop,TS val 2286344 ecr 2286344], length 0
10:58:08.543673 IP 127.0.0.1.5932 > 127.0.0.1.39508:
    Flags [P.], seq 8490:14241, ack 213, win 342,
    options [nop,nop,TS val 2286344 ecr 2286344], length 5751
10:58:08.543682 IP 127.0.0.1.39508 > 127.0.0.1.5932:
    Flags [.], ack 14241, win 1012,
    options [nop,nop,TS val 2286344 ecr 2286344], length 0
```

Un client JDBC va habituellement
utiliser un aller/retour de plus, en raison des requêtes préparées : un
dialogue pour envoyer la requête et la préparer, et un autre pour envoyer les
paramètres. Le problème est la latence du réseau, habituellement : de 50 à
300 μs. Cela limite à 3 000 à 20 000 le nombre d'opérations maximum par
seconde par socket. On peut bien sûr paralléliser sur plusieurs sessions, mais
cela complique le traitement.

En ce qui concerne le parser :
[comme indiqué dans ce message](https://www.postgresql.org/message-id/CA+TgmoaaYvJ7yDKJHrWN1BVk_7fcV16rvc93udSo59gfxG_t7A@mail.gmail.com) :
`gram.o`, le parser fait 1 Mo une fois compilé !

</div>

---

### Coût des connexions

<div class="slide-content">
Se connecter coûte cher :

  * Authentification, permissions
  * Latence réseau
  * Négociation SSL
  * Création de processus & contexte d'exécution
  * Acquisition de verrous

→ Maintenir les connexions côté applicatif, ou utiliser un pooler.

</div>

<div class="notes">

L'établissement d'une connexion client au serveur est coûteuse,
en temps comme ressource. Il faut plusieurs allers-retours réseau
pour établir la connexion. Il y a souvent une négociation pour le
chiffrement SSL. PostgreSQL doit authentifier l'utilisateur,
puis créer son processus, le contexte d'exécution, poser quelques
verrous, avant que les requêtes puissent arriver.
Pour des requêtes répétées, il est beaucoup plus efficace d'ouvrir
une connexion et de la réutiliser pour de nombreuses requêtes.

On peut tester l'impact d'une connexion/déconnexion avec `pgbench`,
dont l'option `-C` lui demande de se connecter à chaque requête :

```bash
$ pgbench pgbench -T 20 -c 10 -j5 -S -C
```
```default
starting vacuum...end.
transaction type: <builtin: select only>
scaling factor: 2
query mode: simple
number of clients: 10
number of threads: 5
duration: 20 s
number of transactions actually processed: 16972
latency average = 11.787 ms
tps = 848.383850 (including connections establishing)
tps = 1531.057609 (excluding connections establishing)
```

Sans se reconnecter à chaque requête :

```bash
$ pgbench pgbench -T 20 -c 10 -j5 -S
```
```default
starting vacuum...end.
transaction type: <builtin: select only>
scaling factor: 2
query mode: simple
number of clients: 10
number of threads: 5
duration: 20 s
number of transactions actually processed: 773963
latency average = 0.258 ms
tps = 38687.524110 (including connections establishing)
tps = 38703.239556 (excluding connections establishing)
```

On passe de 900 à 38 000 transactions par seconde.

Un pooler est souvent intégré d'office aux serveurs d'applications
(par exemple Tomcat). Sinon, [PgBouncer](https://www.pgbouncer.org/)
est l'outil généralement recommandé.
Pour les détails, voir notre module de formation [W6](https://dali.bo/w6_html).

</div>

---

### Penser relationnel

<div class="slide-content">

  * Les spécifications sont souvent procédurales, voire objet !
  * Prendre du recul
  * Réfléchir de façon ensembliste
    + on travaille sur des ensembles de données
    + penser aux CTE (`WITH`)

</div>

<div class="notes">

Si les spécifications disent (version simplifiée bien sûr) :

  * vérifier la présence du client ;
  * s'il est présent, mettre à jour son adresse ;
  * sinon, créer le client avec la bonne adresse,

on peut être tenter d'écrire (pseudo-code client) :

```sql
SELECT count(*) from clients where client_name = 'xxx'
INTO compte

IF compte > 0
  UPDATE clients set adresse='yyy' WHERE client_name='xxx'
ELSE
  INSERT client SET client_name='xxx', adresse='yyy'
END IF
```

D'où 3 requêtes, systématiquement 2 appels à la base.
On peut facilement économiser une requête :

```sql
UPDATE clients set adresse='yyy' WHERE client_name='xxx'
IF NOT FOUND
  INSERT client SET client_name='xxx', adresse='yyy'
END IF
```

Les versions modernes de PostgreSQL permettent de tout faire
en un seul ordre. L'exemple suivant utilise une
[fusion des enregistrements dans PostgreSQL avec des CTE](https://vibhorkumar.wordpress.com/2011/10/26/upsertmerge-using-writable-cte-in-postgresql-9-1/).

```sql
WITH
  enregistrements_a_traiter AS (
    SELECT * FROM (VALUES ('toto' , 'adresse1' ),('tata','adresse2'))
    AS val(nom_client,adresse)
  ),
  mise_a_jour AS (
    UPDATE client SET adresse=enregistrements_a_traiter.adresse
    FROM enregistrements_a_traiter
    WHERE enregistrements_a_traiter.nom_client=client.nom_client
    RETURNING client.nom_client
  )
INSERT INTO client (nom_client,adresse)
  SELECT nom_client,adresse from enregistrements_a_traiter
  WHERE NOT EXISTS (
    SELECT 1 FROM mise_a_jour
    WHERE mise_a_jour.nom_client=enregistrements_a_traiter.nom_client
  );
```

Dans beaucoup de cas on peut faire encore plus simple
grâce à la clause `ON CONFLICT … DO UPDATE`
(« [upsert](https://docs.postgresql.fr/current/sql-insert.html) ») :

```sql
INSERT INTO client (nom_client,adresse) VALUES ('toto' , 'adresse1' ), ('tata','adresse2')
    ON CONFLICT (nom_client) DO UPDATE
    SET adresse = EXCLUDED.adresse
    WHERE client.nom_client = EXCLUDED.nom_client;
```

PostgreSQL 15 apporte même une commande [MERGE](https://public.dalibo.com/exports/formation/workshops/fr/ws15/150-postgresql_15.handout.html#ajout-de-la-commande-sql-merge).
Par rapport à `INSERT ON CONFLICT`, `MERGE` permet aussi des suppressions,
et possède un mécanisme différent.

</div>

---

### Pas de DDL applicatif

<div class="slide-content">

Le schéma est la modélisation des données

   * Une application n'a pas à y toucher lors de son fonctionnement normal
    + exception : tables temporaires
  * SQL manipule les données en flux continu :
    + chaque étape d'un plan d'exécution n'attend pas la fin de la précédente
    + donc : une table temporaire est souvent une perte de temps
</div>

<div class="notes">

Il est fortement déconseillé qu'une application modifie le schéma de données
pendant son fonctionnement, notamment qu'elle crée des tables
ou ajoute des colonnes.
Une exception fréquente concerne les tables « temporaires »,
qui n'existent que le temps d'une session. Elles sont
inévitables dans certaines circonstances, assez courantes pendant
des batchs, mais dans le flux normal de l'applicatif l'utilisation
de tables temporaires ne sert généralement qu'à multiplier
les étapes et à poser des sortes de points d'arrêt artificiels
dans le maniement des données. C'est très net sur cette réécriture
de l'exemple précédent :

```sql
CREATE TEMP TABLE temp_a_inserer  (nom_client text, adresse text);

INSERT INTO temp_a_inserer
  SELECT * FROM (VALUES ('toto' , 'adresse1' ), ('tata','adresse2')) AS tmp;

UPDATE client SET adresse=temp_a_inserer.adresse
FROM temp_a_inserer
WHERE temp_a_inserer.nom_client=client.nom_client;

INSERT INTO client (nom_client,adresse)
SELECT nom_client,adresse from temp_a_inserer
WHERE NOT EXISTS (
          SELECT 1 FROM client
          WHERE client.nom_client=temp_a_inserer.nom_client
          );
DROP TABLE temp_a_inserer;
```

</div>

---

### Optimiser chaque accès

<div class="slide-content">

Les moteurs SQL sont très efficaces, et évoluent en permanence

  * Ils ont de nombreuses méthodes de tri, de jointure,
choisies en fonction du contexte
  * En SQL :
    + optimisation selon volume & configuration
    + évolution avec le moteur
  * Dans l'application cliente : vous devrez le maintenir et
l'améliorer
  * Faites le maximum côté SQL :
    + agrégats, fonctions analytiques, tris,
numérotations, `CASE`, etc.
    + Commentez avec `--`  et `/* */`

</div>

<div class="notes">

L'avantage du code SQL est, encore une fois, qu'il est déclaratif. Il aura donc
de nombreux avantages sur un code procédural côté applicatif, quel que
soit le langage.
L'exécution par le moteur évoluera pour prendre en compte les variations
de volumétrie des différentes tables.
Les optimiseurs sont la partie la plus importante d'un moteur SQL. Ils
progressent en permanence. Chaque nouvelle version va donc potentiellement
améliorer vos performances.

Si vous écrivez du procédural avec des appels unitaires à la base dans des
boucles, le moteur ne pourra rien optimiser.
Si vous faites vos tris ou regroupements côté client, vous êtes limités aux
algorithmes fournis par vos langages, voire à ceux que vous aurez écrit
manuellement à un moment donné.
Alors qu'une base de données bascule automatiquement entre une dizaine
d'algorithmes différents suivant le volume, le type de données à trier, ce pour
quoi le tri est ensuite utilisé, etc., voire évite de trier en utilisant des
tables de hachage ou des index disponibles. La migration à une nouvelle
version du moteur peut vous apporter d'autres techniques prises alors en
compte de manière transparente.

</div>

---

### Ne faire que le nécessaire

<div class="slide-content">

Prendre de la distance vis-à-vis des spécifications
fonctionnelles (bis) :

  * Ex : mise à jour ou insertion ?
    + tenter la mise à jour, et regarder combien d'enregistrements ont été mis à jour
    + surtout pas de `COUNT(*)`
    + éventuellement un test de l'existence d'un seul enregistrement
    + gérer les exceptions plutôt que de vérifier préalablement que les conditions
sont remplies (si l'exception est rare)
    + et se renseigner sur la syntaxe

<!-- Ceci reprend les exemples de mise à jour ci-dessus -->

</div>

<div class="notes">

Toujours coder les accès aux données pour que la base fasse le maximum de
traitement, mais uniquement les traitements nécessaires : l'accès aux données
est coûteux, il faut l'optimiser. Et le gros des pièges peut être évité
avec les quelques règles d'« hygiène » simples qui viennent d'être énoncées.

</div>

---

## Index

<!-- NB : ne doit figurer ici qu'une très courte intro aux index
 car J4 comprend l introduction et (dans DEVPG) il est à la suite
-->

<div class="slide-content">

  * Objets destinés à l'optimisation des accès
  * À poser par les développeurs :

```sql
CREATE INDEX ON ma_table (nom colonne) ;
```

</div>

<div class="notes">

Les index sont des objets uniquement destinés à accélérer les requêtes
(filtrage mais aussi jointures et tris, ou respect des contraintes d'unicité).
Ils ne modifient jamais le résultat d'une requête (tout au plus :
ils peuvent changer l'ordre des lignes résultantes si celui-ci
est indéfini.)
Il est capital pour un développeur d'en maîtriser les bases,
car il est celui qui sait quels sont les champs interrogés dans son application.
Les index sont un sujet en soi qui sera traité par ailleurs.

</div>

---

## Impact des transactions

<div class="slide-content">

  * Verrous : relâchés à la **fin** de la transaction
    + `COMMIT`
    + `ROLLBACK`
  * Validation des données sur le disque au `COMMIT`
    + écriture synchrone : coûteux
  * Contournements :
    + tables temporaires/unlogged ?
    + parfois : `synchronous_commit = off` (…si perte possible)
  * → Faire des transactions qui correspondent au fonctionnel
    + pas trop nombreuses
    + courtes, pas de travail inutile une fois des verrous posés

</div>

<div class="notes">

Réaliser des transactions permet de garantir l'atomicité des opérations :
toutes les modifications sont validées (`COMMIT`), ou tout est annulé
(`ROLLBACK`). Il n'y a pas d'état intermédiaire. Le `COMMIT` garantit aussi la
durabilité des opérations : une fois que le `COMMIT` a réussi, la base de
données garantit que les opérations ont bien été stockées, et ne seront pas
perdues… sauf perte du matériel (disque) sur lequel ont été écrites ces
opérations bien sûr.

L'opération `COMMIT` a bien sûr un coût : il faut garantir que les
données sont bien écrites sur le disque, il faut les écrire sur le disque
(évidemment), mais aussi attendre la confirmation du disque,
voire de serveurs répliqués distants parfois. Même avec des disques SSD,
plus performants que les disques classiques, cette opération reste
coûteuse. Un disque dur classique doit attendre la rotation du disque et
placer sa tête au bon endroit (dans le journal de transaction),
écrire la donnée, et confirmer au système que c'est fait.
Un disque SSD doit écrire réellement le bloc demandé, c'est-à-dire
l'effacer (relativement lent) puis le réécrire. Dans les deux cas,
il faut compter de l'ordre de la milliseconde.

<div class="box tip">
Il est parfois acceptable de perdre les dernières données en cas de
panne de courant (par défaut, celles committées pendant les
derniers 300 ms). On peut donc réduire la fréquence de la synchronisation
des journaux avec :
```sql
SET synchronous_commit TO off ;          /* pour une session */
SET LOCAL synchronous_commit TO off ;    /* pour une transaction */
```
La synchronisation n'aura plus lieu aussi fréquemment.
L'impact peut être énorme sur les petites transactions nombreuses.
</div>

De plus, les transactions devant garantir l'atomicité des opérations, il est nécessaire
qu'elles prennent des verrous : sur les enregistrements modifiés, sur les tables
accédées (pour éviter les changements de structure pendant leur manipulation),
sur des prédicats (dans certains cas compliqués comme le niveau d'isolation
_serializable_)… Tout ceci a un impact :

  * par le temps d'acquisition des verrous, bien sûr ;
  * par la contention entre sessions : certaines risquent d'en bloquer d'autres.

Les verrous étant posés lors des ordres d'écriture et relâchés en fin de transaction,
on fera en sorte que la transaction fasse le minimum de choses après les
premières écritures.

Il est donc très difficile de déterminer la bonne durée d'une transaction.
Trop courte : on génère beaucoup d'opérations synchrones. Trop longue : on
risque de bloquer d'autres sessions. Le mieux (et le plus important en fait)
est de coller au besoin fonctionnel.

</div>

---

### Verrouillage et contention

<div class="slide-content">

  * Chaque transaction prend des verrous :
    + sur les objets (tables, index, etc.) pour empêcher au moins leur
suppression ou modification de structure pendant leur travail
    + sur les enregistrements
    + libérés à la fin de la transaction : les transactions très longues peuvent
donc être problématiques
  * Sous PostgreSQL, on peut quand même lire un enregistrement en cours de
modification : on voit l'ancienne version (MVCC)

</div>

<div class="notes">

Afin de garantir une isolation correcte entre les différentes sessions, le SGBD
a besoin de protéger certaines opérations. On ne peut par exemple pas
autoriser une session à modifier le même enregistrement qu'une autre, tant
qu'on ne sait pas si cette dernière a validé ou annulé sa modification. On a
donc un verrouillage des enregistrements modifiés.

Certains SGBD verrouillent totalement l'enregistrement modifié. Celui-ci n'est
plus accessible même en lecture tant que la modification n'a pas été validée
ou annulée. Cela a l'avantage d'éviter aux sessions en attente de voir une
ancienne version de l'enregistrement, mais le défaut de les bloquer, et donc de
fortement dégrader les performances.

PostgreSQL, comme Oracle, utilise un modèle dit MVCC (Multi-Version Concurrency
Control), qui permet à chaque enregistrement de cohabiter en plusieurs versions
simultanées en base. Cela permet d'éviter que les écrivains ne bloquent les
lecteurs ou les lecteurs ne bloquent les écrivains. Cela permet aussi de
garantir un instantané de la base à une requête, sur toute sa durée, voire
sur toute la durée de sa transaction si la session le demande
(`BEGIN ISOLATION LEVEL REPEATABLE READ`).

Dans le cas où il est réellement nécessaire de verrouiller un enregistrement
sans le mettre à jour immédiatement (pour éviter une mise à jour concurrente),
il faut utiliser l'ordre SQL `SELECT FOR UPDATE`. Ce dernier possède une
très intéressante option
`SKIP LOCKED`[](https://docs.postgresql.fr/current/sql-select.html#SQL-FOR-UPDATE-SHARE)
pour ne pas être bloqué par une ligne déjà verrouillée.

</div>

---

### Deadlocks

<div class="slide-content">

« Verrous mortels » : comment les éviter ?

  * Théorie : prendre toujours les verrous dans le même ordre
  * Pratique, ça n'est pas toujours possible ou commode
  * Conséquence : une des transactions est tuée
    + erreurs, ralentissements

</div>

<div class="notes">

Les _deadlocks_ se produisent quand plusieurs sessions acquièrent simultanément
des verrous et s'interbloquent. Par exemple :

+---------------------------+----------------------------+-----------------------+
|    Session 1              |    Session 2               |                       |
+===========================+============================+=======================+
|  `BEGIN`                  |  `BEGIN`                   |                       |
+---------------------------+----------------------------+-----------------------+
|  `UPDATE demo`            |                            |                       |
|  `  SET a=10 WHERE a=1;`  |                            |                       |
+---------------------------+----------------------------+-----------------------+
|                           |  `UPDATE demo`             |                       |
|                           |  `  SET a=11 WHERE a=2;`   |                       |
+---------------------------+----------------------------+-----------------------+
|  `UPDATE demo`            |                            |  _Session 1 bloquée._ |
|  `  SET a=11 WHERE a=2;`  |                            |  _Attend session 2._  |
+---------------------------+----------------------------+-----------------------+
|                           |  `UPDATE demo`             |  _Session 2 bloquée._ |
|                           |  `   SET a=10 WHERE a=1;`  |  _Attend session 1._  |
+---------------------------+----------------------------+-----------------------+

Bien sûr, la situation ne reste pas en l'état. Une session qui attend un
verrou appelle au bout d'un temps court (une seconde par défaut sous PostgreSQL)
le gestionnaire de _deadlocks_, qui finira par tuer une des deux sessions. Dans
cet exemple, il sera appelé par la session 2, ce qui débloquera la situation.

Une application qui a beaucoup de _deadlocks_ a plusieurs problèmes :

  * les transactions, mêmes celles qui réussissent,
attendent beaucoup (ce qui bride l'utilisation de toutes les ressources machine) ;
  * certaines finissent annulées et doivent donc être rejouées (travail
supplémentaire).

Dans notre exemple, on aurait pu éviter le problème, en définissant une
règle simple : toujours verrouiller par valeurs de a croissante. Dans la
pratique, sur des cas complexes, c'est bien sûr bien plus difficile à faire.
Par ailleurs, un `deadlock` peut impliquer plus de deux transactions. Mais
simplement réduire le volume de `deadlocks` aura toujours un impact très positif
sur les performances.

On peut aussi déclencher plus rapidement le gestionnaire de `deadlock`. 1
seconde, c'est quelquefois une éternité dans la vie d'une application. Sous
PostgreSQL, il suffit de modifier le paramètre `deadlock_timeout`. Plus cette
variable sera basse, plus le traitement de détection de `deadlock` sera
déclenché souvent. Et celui-ci peut être assez gourmand si de nombreux verrous
sont présents, puisqu'il s'agit de détecter des cycles dans les dépendances de
verrous.

</div>

---

<!-- Slides sur les durées des ordres,sessions, transactions, commun avec M4 (DBA2) -->
<!--

Slides commun à M4 (MVCC, verrous dans DBA2)
            et J3 (optimisation SQL, dont les verrous)
   
Slides niveaux 3 ici

À insérer après verrous

C'est en fin de DBA2, donc des rappels à titre pédagogigue,
qui ne feront pas de mal en DEVPG

-->

### Quelle durée pour les sessions & transactions ?

<div class="slide-content">

Divers seuils possibles, jamais globalement.

```sql
SET …_timeout TO '5s' ;
ALTER ROLE … IN DATABASE … SET ..._timeout TO '…s'
```

</div>

| Paramètre                            | Cible du seuil        |
|:-------------------------------------+-----------------------| 
| `lock_timeout`                         | Attente de verrou     |
| `statement_timeout`                    | Ordre en cours        |
| `idle_session_timeout`                 | Session inactive      |
| `idle_in_transaction_session_timeout`  | Transaction en cours, inactive  |
| `transaction_timeout` (v17)            | Transaction en cours  |

<div class="notes">

<!-- Certains vus plus haut en DBA2 -->

PostgreSQL possède divers seuils pour éviter des ordres, transactions
ou sessions trop longues. Le dépassement d'un seuil provoque la fin
et l'annulation de l'ordre, transaction ou session.
Par défaut, aucun n'est activé.

<div class="caution">
Ne définissez jamais des paramètres globalement,
les ordres de maintenance
ou les batchs de nuit seraient aussi concernés,
tout comme les superutilisateurs.
Utilisez un ordre `SET` dans la session, ou 
`ALTER ROLE … IN DATABASE … SET …`.
</div>

<div class="warning">
Il est important que l'application sache gérer l'arrêt de connexion ou
l'annulation d'un ordre ou d'une transaction et réagir en conséquence
(nouvelle tentative, abandon avec erreur…).
Dans le cas contraire, l'application pourrait rester déconnectée, et suivant les cas des
données pourraient être perdues.
</div>

`lock_timeout`
<!-- déjà vu plus haut dans DBA2 -->
permet d'interrompre une requête qui mettrait trop de temps à
acquérir son verrou. Il faut l'activer par précaution avant
une opération lourde (un `VACUUM FULL` notamment) pour
éviter un « empilement des verrous ». En effet, si l'ordre
ne peut pas s'exécuter immédiatement, il bloque toutes les autres requêtes
sur la table.

`statement_timeout` est la durée maximale d'un ordre.
Défini au niveau d'un utilisateur ou d'une session, cet ordre
permet d'éviter que des requêtes aberrantes chargent la
base pendant des heures ou des jours ; ou encore que des requêtes
« s'empilent » sur une base totalement saturée ou avec trop de contention.
Autre exemple : les utilisateurs de supervision et autres utilisateurs
non critiques.

À noter : l'extrait suivant d'une sauvegarde par `pg_dump` montre que l'outil
inhibe ces paramètres par précaution, afin que la restauration n'échoue pas.
```sql
-- Dumped from database version 17.2 (Debian 17.2-1.pgdg120+1)
-- Dumped by pg_dump version 17.2 (Debian 17.2-1.pgdg120+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET transaction_timeout = 0;
…
```
L'idée peut être reprise pour des scripts batchs, par exemple.

</div>

---

### Quelle durée pour une session ?

<div class="slide-content">

* Courte
  + coût & temps des connexions
  + pooler ?
* Longue
  + risque de saturation du nombre de connexions
  + (rare) gaspillage mémoire par les backends

</div>

<div class="notes">

Il est généralement conseillé d'utiliser des sessions assez longues,
car la création d'une connexion à PostgreSQL est une opération
lourde.
Si l'applicatif ne cesse de se connecter/déconnecter, il faudra penser
à un pooler (pgBouncer, ou côté applicatif).

Des sessions très longues et inactives ne sont en général pas un souci.
Un pooler garde justement ses sessions longtemps.
Mais si les sessions ne se ferment jamais, le nombre maximal de sessions
(`max_connections`) peut être atteint, et de nouvelles connexions refusées.

De plus, pour les performances, il n'est pas très bon qu'il y ait des milliers
de sessions ouvertes, malgré les progrès des versions récentes de PostgreSQL.
Enfin, si la consommation mémoire du _backend_ associé à une session
est raisonnable, il arrive, exceptionnellement, qu'il
se mette à occuper durablement de la mémoire (par exemple en cas d'accès
à des milliers de tables et index durant son existence).

La durée maximale des sessions peut être réglée par `idle_session_timeout`,
ou au niveau du pooler s'il y en a un.

</div>

---

### Quelle durée pour une transaction ?

<div class="slide-content">

* Courte
  + synchronisation fréquente coûteuse
* Longue
  + verrous bloquants

</div>

<div class="notes">

<!-- quelques répétitions par rapport à J3-->
Les transactions ouvertes et figées durablement sans faire de `COMMIT`
ou `ROLLBACK` sont un problème sérieux, car l'autovacuum ne pourra pas
nettoyer de ligne que cette session pourrait encore voir.
Si l'applicatif ne peut être corrigé, ou si des utilisateurs
ouvrent un outil quelconque sans le refermer, une solution est
de définir `idle_in_transaction_session_timeout`,
par exemple à une ou plusieurs heures.

Pour les performances,
il faut éviter les sessions trop courtes car le coût de la synchronisation
sur disque des journaux lors d'un `COMMIT` est coûteux
(sauf table _unlogged_ ou `synchronous_commit`).
Typiquement, un traitement de batch ou de chargement regroupera de nombreuses
opérations en une seule transaction.

D'un autre côté, chaque transaction maintient ses verrous jusqu'à sa fin,
et peut donc bloquer d'autres transactions et ralentir toute l'application.
En utilisation transactionnelle, il vaut donc mieux que
les transactions soient courtes et n'en fassent pas plus que ce qui
est dicté par le besoin fonctionnel, et si possible le plus tard possible dans
la transaction.

`transaction_timeout` (à partir de PostgreSQL 17) peut alors servir
comme sécurité en cas de problème.
À noter que cette limite ne concerne pas les transactions préparées, liées au
[transactions en deux phases](https://docs.postgresql.fr/current/two-phase.html),
et dont la longueur est parfois un souci.

</div>

---

## Base distribuée

<div class="slide-content">

Écrire sur plusieurs nœuds ?

  * Complexité (applicatif/exploitation)
    + → risque d'erreur (programmation, fausse manipulation)
    + reprise d'incident complexe
  * Essayez avec un seul serveur plus gros
    + après avoir optimisé bien sûr
    + PostgreSQL peut vous étonner

</div>

<div class="notes">

Il y a plusieurs variantes de l'utilisation de plusieurs nœuds.
PostgreSQL permet nativement la lecture sur des réplicas d'une instance
(et la technologie est fiable), mais pas l'écriture.
Il est cependant possible de faire du _sharding_ en répartissant
les données sur plusieurs instances indépendantes (_sharding_).
L'extension [Citus](https://www.citusdata.com/product)
permet de faire cela de manière transparente.
Mais il ne faut surtout pas négliger tous les
coûts de cette solution : non seulement le coût du matériel, mais aussi les
coûts humains : procédures d'exploitation, de maintenance, complexité accrue
de développement, etc.

Performance et robustesse peuvent être des objectifs contradictoires.
<div class="box tip">
Avant de complexifier votre système, pensez à augmenter les ressources
du serveur (après avoir optimisé autant que possible et identifié
quelle ressource pose problème : RAM, CPU, disque… ?)
</div>

</div>

---

## Bibliographie

<div class="slide-content">

  * Quelques références :
    + _The Art of SQL_, **Stéphane Faroult**
    + _Refactoring SQL Applications_, **Stéphane Faroult**
    + _SQL Performance Explained_, **Markus Winand**
    + _Introduction aux bases de données_, **Chris Date**
    + _The Art of PostgreSQL_, **Dimitri Fontaine**
    + Vidéos de **Stéphane Faroult** sous Youtube
</div>

<div class="notes">

Il existe bien des livres sur le développement en SQL.
Voici quelques sources intéressantes parmi bien d'autres :

Livres pratiques non propres à PostgreSQL :

  * _The Art of SQL_, **Stéphane Faroult**, 2006 (ISBN-13: 978-0596008949)
  * _Refactoring SQL Applications_, **Stéphane Faroult**, 2008 (ISBN-13: 978-0596514976)
  * _SQL Performance Explained_, **Markus Winand**, 2012 : même si ce livre ne tient pas compte des dernières nouveautés des index de PostgreSQL, il contient l'essentiel de ce qu'un développeur doit savoir sur les index B-tree sur diverses bases de données courantes
    + [site internet (fr)](https://use-the-index-luke.com/fr)
    + ISBN-13 en français : 978-3950307832, en anglais : 978-3950307825

En vidéos :

  * **Stéphane Faroult** ([roughsealtd sur Youtube](https://www.youtube.com/channel/UCW6zsYGFckfczPKUUVdvYjg)) : _SQL Best Practices in less than 20 minutes_
   [partie 1](https://www.youtube.com/watch?v=40Lnoyv-sXg&list=PL767434BC92D459A7),
   [partie 2](https://www.youtube.com/watch?v=GbZgnAINjUw&list=PL767434BC92D459A7),
   [partie 3](https://www.youtube.com/watch?v=y70FmugnhPU&list=PL767434BC92D459A7).

Livres spécifiques à PostgreSQL :

   * _The Art of PostgreSQL_[](https://theartofpostgresql.com/) de **Dimitri Fontaine** (2020) ;
  l'ancienne édition de 2017 se nommait _Mastering PostgreSQL in Application Development_.

Sur la théorie des bases de données :

  * _An Introduction to Database Systems_, **Chris Date** (8è édition de 2003, ISBN-13 en français :‎ 978-2711748389 ; en anglais : 978-0321197849) ;
  * _The World and the Machine_, **Michael Jackson** ([version en ligne](https://dl.acm.org/doi/10.1145/225014.225041) <!-- KO : http://users.mct.open.ac.uk/mj665/icse17kn.pdf -->.

</div>

---

\newpage

## Travaux pratiques


<!-- inclus dans tp-enonce.md --> 

### auto_explain

<div class="notes">

<div class="slide-content">
  **But** : Capturer les plans d'exécutions automatiquement avec auto_explain
</div>

> Installer le module `auto_explain`
> (documentation : <https://docs.postgresql.fr/current/auto-explain.html>).

> Exécuter des requêtes sur n'importe quelle base de données, et inspecter les traces générées.

> Passer le niveau de messages de sa session (`client_min_messages`) à `log`.

</div>

<!-- inclus dans tp-enonce.md --> 

### pg_stat_statements

<div class="notes">

<div class="slide-content">
  **But** : Analyser les performances des requêtes avec pg_stat_statements
</div>

> - pg_stat_statements nécessite une bibliothèque préchargée. La positionner dans le fichier `postgresql.conf`, redémarrer PostgreSQL et créer l'extension.

> - Inspecter le contenu de l'extension `pg_stat_statements` (`\dx` et `\dx+`).

> - Vérifier que le serveur est capable d'activer la mesure de la durée des entrées-sorties avec `pg_test_timing`. Puis l'activer (`track_io_timing`), sans oublier de redémarrer PostgreSQL.
> - Depuis un autre terminal, créer une base **pgbench** (si pas déjà disponible),
> l'initialiser (même si elle existait), et lancer une activité dessus :
> ```bash
> # en tant qu'utilisateur postgres
> createdb -e pgbench
> /usr/pgsql-16/bin/pgbench -i -s135 pgbench
> /usr/pgsql-16/bin/pgbench -c5 -j1 pgbench -T 600 -P1
> ```

> - Dans la vue `pg_stat_statements`, récupérer les 5 requêtes
> les plus gourmandes en temps cumulé sur l'instance et leur nombre de lignes.

> Quelle est la requête générant le plus d'écritures directes sur disques
> (_written_) ? Et en temps d'écriture ?

> Quel est le _hit ratio_ des requêtes les plus fréquentes ?

</div>

\newpage

## Travaux pratiques (solutions)


<!-- inclus dans tp-solution.md --> 

### auto_explain

> Installer le module `auto_explain`
> (documentation : <https://docs.postgresql.fr/current/auto-explain.html>).

Dans le fichier `postgresql.conf`, chargement du module et activation globale
pour _toutes_ les requêtes (ce qu'on évitera de faire en production) :

```sql
shared_preload_libraries = 'auto_explain'
auto_explain.log_min_duration = 0
```

Redémarrer PostgreSQL.

> Exécuter des requêtes sur n'importe quelle base de données, et inspecter les traces générées.

Le plan de la moindre requête (même un `\d+`) doit apparaître dans la trace.

> Passer le niveau de messages de sa session (`client_min_messages`) à `log`.

Il est possible de recevoir les messages directement dans sa session. Tous
les messages de log sont marqués d'un niveau de priorité. Les messages produits
par `auto_explain` sont au niveau `log`. Il suffit donc de passer le
paramètre `client_min_messages` au niveau `log`.

Positionner le paramètre de session comme ci-dessous, ré-exécuter la requête.

```sql
SET client_min_messages TO log;
SELECT…
```

<!-- inclus dans tp-solution.md --> 

### pg_stat_statements

> - pg_stat_statements nécessite une bibliothèque préchargée. La positionner dans le fichier `postgresql.conf`, redémarrer PostgreSQL et créer l'extension.

<!-- auto_explain ajouté dans TP précédent -->
Si une autre extension (ici `auto_explain`) est également présente,
on peut les lister ainsi :

```sql
shared_preload_libraries = 'auto_explain,pg_stat_statements'
```

Redémarrer PostgreSQL.

Dans la base **postgres** (par exemple), créer l'extension :

```sql
CREATE EXTENSION IF NOT EXISTS pg_stat_statements ;
```

> - Inspecter le contenu de l'extension `pg_stat_statements` (`\dx` et `\dx+`).

```default
 \dx+ pg_stat_statements
```
```default
Objets dans l'extension « pg_stat_statements »
         Description d'objet
--------------------------------------
 function pg_stat_statements(boolean)
 function pg_stat_statements_reset()
 view pg_stat_statements
```


> - Vérifier que le serveur est capable d'activer la mesure de la durée des entrées-sorties avec `pg_test_timing`. Puis l'activer (`track_io_timing`), sans oublier de redémarrer PostgreSQL.

[pg_test_timing](https://docs.postgresql.fr/current/pgtesttiming.html)
est livré avec PostgreSQL :

```bash
/usr/pgsql-16/bin/pg_test_timing
```
```default
Testing timing overhead for 3 seconds.
Per loop time including overhead: 33.24 ns
Histogram of timing durations:
  < us   % of total      count
     1     97.25509   87770521
     2      2.72390    2458258
     4      0.00072        646
     8      0.00244       2200
    16      0.00984       8882
    32      0.00328       2958
    64      0.00298       2689
   128      0.00099        892
   256      0.00055        499
   512      0.00016        141
  1024      0.00006         53
  2048      0.00000          1
```

Si le temps de mesure n'est que de quelques dizaines de nanosecondes, c'est OK.
(C'est le cas sur presque toutes les machines et systèmes d'exploitation
actuels, mais il y a parfois des surprises.)
Sinon, éviter de faire ce qui suit sur un serveur de production.
Sur une machine de formation, ce n'est pas un problème.

Dans le fichier `postgresql.conf`, positionner :

```ini
track_io_timing = on
```

Changer ce paramètre nécessite de redémarrer PostgreSQL.

> - Depuis un autre terminal, créer une base **pgbench** (si pas déjà disponible),
> l'initialiser (même si elle existait), et lancer une activité dessus :
> ```bash
> # en tant qu'utilisateur postgres
> createdb -e pgbench
> /usr/pgsql-16/bin/pgbench -i -s135 pgbench
> /usr/pgsql-16/bin/pgbench -c5 -j1 pgbench -T 600 -P1
> ```

```default
createdb -e pgbench
```
```default
SELECT pg_catalog.set_config('search_path', '', false);
CREATE DATABASE pgbench;
```
```bash
/usr/pgsql-16/bin/pgbench -i -s135 pgbench
```
```default
…
creating tables...
generating data (client-side)...
13500000 of 13500000 tuples (100%) done (elapsed 10.99 s, remaining 0.00 s)
vacuuming...
creating primary keys...
done in 14.97 s (drop tables 0.00 s, create tables 0.02 s, client-side generate 11.04 s, vacuum 0.34 s, primary keys 3.57 s).
```
```bash
/usr/pgsql-16/bin/pgbench -c5 -j1 pgbench -T 600 -P1
```
```default
pgbench (16.2)
starting vacuum...end.
progress: 1.0 s, 2364.9 tps, lat 2.078 ms stddev 1.203, 0 failed
progress: 2.0 s, 2240.0 tps, lat 2.221 ms stddev 0.871, 0 failed
…
```

On a donc 5 clients qui vont mettre à jour la base à raison de 2000
transactions par seconde (valeur très dépendante des CPUs et des disques).

> - Dans la vue `pg_stat_statements`, récupérer les 5 requêtes
> les plus gourmandes en temps cumulé sur l'instance et leur nombre de lignes.

```sql
SELECT calls, query, rows,
total_exec_time*interval '1ms' AS tps_total
FROM pg_stat_statements
ORDER BY total_exec_time DESC LIMIT 5
\gx
```

Le résultat va dépendre de l'historique de votre instance,
et du temps déroulé depuis le lancement de `pgbench`,
mais c'est probablement proche de ceci : 

```default
-[ RECORD 1 ]------------------------------------------------------------------
calls     | 879669
query     | UPDATE pgbench_accounts SET abalance = abalance + $1 WHERE aid = $2
rows      | 879669
tps_total | 00:02:56.184131
-[ RECORD 2 ]------------------------------------------------------------------
calls     | 879664
query     | UPDATE pgbench_branches SET bbalance = bbalance + $1 WHERE bid = $2
rows      | 879664
tps_total | 00:00:44.803628
-[ RECORD 3 ]------------------------------------------------------------------
calls     | 879664
query     | UPDATE pgbench_tellers SET tbalance = tbalance + $1 WHERE tid = $2
rows      | 879664
tps_total | 00:00:12.196055
-[ RECORD 4 ]------------------------------------------------------------------
calls     | 1
query     | copy pgbench_accounts from stdin with (freeze on)
rows      | 13500000
tps_total | 00:00:10.698976
-[ RECORD 5 ]------------------------------------------------------------------
calls     | 879664
query     | SELECT abalance FROM pgbench_accounts WHERE aid = $1
rows      | 879664
tps_total | 00:00:06.530169
```

Noter que l'unique `COPY` pour créer la base dure plus que les centaines
de milliers d'occurences de la cinquième requête.

> Quelle est la requête générant le plus d'écritures directes sur disques
> (_written_) ? Et en temps d'écriture ?

Pour les _written_, il faut tenir compte des trois sources :
blocs du cache partagé, blocs des _backends_, fichiers temporaires.

```sql
SELECT calls,
       pg_size_pretty(8192::numeric
       * (shared_blks_written+local_blks_written+temp_blks_written)) AS written,
       pg_size_pretty(8192::numeric*shared_blks_written)  AS shared_written,
       pg_size_pretty(8192::numeric*temp_blks_written)    AS temp_written,
       blk_write_time * interval '1ms'      AS blk_write_time,
       temp_blk_write_time * interval '1ms' AS temp_blk_write_time,
      query
FROM pg_stat_statements
ORDER BY shared_blks_written+local_blks_written+temp_blks_written DESC LIMIT 3 ;
```
```default
-[ RECORD 1 ]-------+-------------------------------------------------------------
calls               | 2400667
written             | 15 GB
shared_written      | 15 GB
temp_written        | 0 bytes
blk_write_time      | 00:00:11.840499
temp_blk_write_time | 00:00:00
query               | UPDATE pgbench_accounts SET abalance = abalance + $1 WHERE a.
                    |.id = $2
-[ RECORD 2 ]-------+-------------------------------------------------------------
calls               | 1
written             | 3442 MB
shared_written      | 3442 MB
temp_written        | 0 bytes
blk_write_time      | 00:00:00
temp_blk_write_time | 00:00:00
query               | copy pgbench_accounts from stdin with (freeze on)
-[ RECORD 3 ]-------+-------------------------------------------------------------
calls               | 1
written             | 516 MB
shared_written      | 0 bytes
temp_written        | 516 MB
blk_write_time      | 00:00:00
temp_blk_write_time | 00:00:00
query               | alter table pgbench_accounts add primary key (aid)
```

Il y a donc beaucoup d'écritures directes. C'est le signe que le cache en
écriture de PostgreSQL est insuffisant (la base fait 2 Go, à peu près
intégralement balayée, et le `shared_buffers` par défaut ne fait que 128 Mo)
ou que le `background writer` doit être modifié pour nettoyer plus souvent
les blocs _dirty_.

On note que l'`UPDATE` et le `COPY` ont écrit des blocs qui auraient dû
passer uniquement par le cache, alors le `ALTER TABLE`, lui, a essentiellement
écrit un fichier temporaire (c'est logique lors d'une création d'index).

Avec des _shared buffers_ plus importants, les `shared_written` sont
quasiment absents. Ils proviennent essentiellement
d'ordres lourds comme `COPY`.

> Quel est le _hit ratio_ des requêtes les plus fréquentes ?

```sql
SELECT calls, total_exec_time,
       round(100.0*shared_blks_hit
       /nullif(shared_blks_hit+shared_blks_read, 0),2) AS "hit %",
       query
FROM  pg_stat_statements
ORDER BY total_exec_time DESC LIMIT 5 ;
```
```default
-[ RECORD 1 ]---+-----------------------------------------------------------------
calls           | 2400667
total_exec_time | 464702.3557850064
hit %           | 73.24
query           | UPDATE pgbench_accounts SET abalance = abalance + $1 WHERE aid =.
                |. $2
-[ RECORD 2 ]---+-----------------------------------------------------------------
calls           | 2400658
total_exec_time | 141310.02034101041
hit %           | 100.00
query           | UPDATE pgbench_branches SET bbalance = bbalance + $1 WHERE bid =.
                |. $2
-[ RECORD 3 ]---+-----------------------------------------------------------------
calls           | 2400659
total_exec_time | 34201.65339700031
hit %           | 100.00
query           | UPDATE pgbench_tellers SET tbalance = tbalance + $1 WHERE tid = .
                |.$2
-[ RECORD 4 ]---+-----------------------------------------------------------------
calls           | 2400661
total_exec_time | 16494.857696000774
hit %           | 100.00
query           | SELECT abalance FROM pgbench_accounts WHERE aid = $1
-[ RECORD 5 ]---+-----------------------------------------------------------------
calls           | 2400656
total_exec_time | 11685.776115000388
hit %           | 100.00
query           | INSERT INTO pgbench_history (tid, bid, aid, delta, mtime) VALUES.
                |. ($1, $2, $3, $4, CURRENT_TIMESTAMP)
```

On constate que le _hit ratio_ est parfait, sauf la première requête.
C'est logique, car la table `pgbench_accounts` ne tient pas dans le cache
par défaut et elle est balayée à peu près entièrement par les requêtes de pgbench.
