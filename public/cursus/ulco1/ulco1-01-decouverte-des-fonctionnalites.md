---
revision: 24.12
date: "Année académique 2024-2025"

licence : "Creative Commons BY-NC-SA"
include_licence : CC-BY-NC-SA-2.0-FR

author: © 2005-2025 DALIBO SARL SCOP

trademarks: |
  Le logo éléphant de PostgreSQL (« Slonik ») est une création sous copyright et
  le nom « PostgreSQL » est une marque déposée par PostgreSQL Community Association
  of Canada.

##
## PDF Options
##

## Limiter la profondeur de la table des matières
toc-depth: 2

## Mettre les lien http en pieds de page
links-as-notes: false

## Police plus petite dans un bloc de code

code-blocks-fontsize: small

## Filtre : pandoc-latex-admonition
## les catégories `important` et `warning` sont synonymes
## même chose pour `tip` et `note`
pandoc-latex-admonition:
  - color: Red
    classes: [warning]
    linewidth: 4
  - color: Red
    classes: [important]
    linewidth: 4
  - color: DarkSeaGreen
    classes: [tip]
    linewidth: 4
  - color: DarkSeaGreen
    classes: [note]
    linewidth: 4
  - color: DodgerBlue
    classes: [slide-content]
    linewidth: 4

##
## Reveal Options
##

## Taille affichage
width: 960
height: 700

## beige/blood/moon/simple/solarized/black/league/night/serif/sky/white
theme: white

## None - Fade - Slide - Convex - Concave - Zoom
transition: None
transitionSpeed: fast

## Barre de progression
progress: true

## Affiche N° de slide
slideNumber: true

## Le numero de slide apparait dans la barre d'adresse
history: true

## Defilement des slides avec la roulette
mouseWheel: true

## Annule la transformation uppercase de certains thèmes
title-transform : none

## Cache l'auteur sur la première slide
## Mettre en commentaire pour désactiver
hide_author_in_slide: true

title : 'Découvrir PostgreSQL'
subtitle : 'FCU Calais - M1 I2L'
---

# Découverte des fonctionnalités

![PostgreSQL](medias/logos/slonik.png){ height=500 }
\

<div class="notes">
</div>

---

## Au menu

<div class="slide-content">

  * Fonctionnalités du moteur
  * Objets SQL

  * Connaître les différentes fonctionnalités et possibilités
  * Découvrir des exemples concrets

</div>

<div class="notes">

Ce module propose un tour rapide des fonctionnalités principales du moteur :
ACID, MVCC, transactions, journaux de transactions… ainsi que des objets SQL
gérés (schémas, index, tablespaces, triggers…).
Ce rappel des concepts de base permet d'avancer plus
facilement lors des modules suivants.

</div>

---

## Fonctionnalités du moteur

<div class="slide-content">

  * Standard SQL
  * ACID : la gestion transactionnelle
  * Niveaux d'isolation
  * Journaux de transactions
  * Administration
  * Sauvegardes
  * Réplication
  * Supervision
  * Sécurité
  * Extensibilité

</div>

<div class="notes">

Cette partie couvre les différentes fonctionnalités d'un moteur de bases de
données. Il ne s'agit pas d'aller dans le détail de chacune, mais de donner
une idée de ce qui est disponible. Les modules suivants de cette formation et
des autres formations détaillent certaines de ces fonctionnalités.

</div>

---

### Respect du standard SQL

<div class="slide-content">

  * Excellent support du SQL ISO
  * Objets SQL
    + tables, vues, séquences, routines, triggers
  * Opérations
    + jointures, sous-requêtes, requêtes CTE, requêtes de fenêtrage, etc.

</div>

<div class="notes">

La dernière version du standard SQL est [SQL:2023](https://en.wikipedia.org/wiki/SQL:2023).
À ce jour, aucun SGBD ne la supporte complètement, _mais_ :

  * PostgreSQL progresse et s'en approche au maximum, au fil des versions ;
  * la majorité de la norme est supportée, parfois avec des syntaxes
différentes ;
  * PostgreSQL est le SGDB le plus respectueux du standard.

</div>

---

### ACID

<!-- slide commun A2 S1 au moins -->
<!-- Slide entier sans titre récupéré de S1 et surtout destiné à A2
 ou à des rappels d'autres modules
--> 

<div class="slide-content">

Gestion transactionnelle : la force des bases de données relationnelles :

  * **Atomicité** (_Atomic_)
  * **Cohérence** (_Consistent_)
  * **Isolation** (_Isolated_)
  * **Durabilité** (_Durable_)

</div>

<div class="notes">

Les propriétés ACID sont le fondement même de toute bonne base de données.
Il s'agit de l'acronyme des quatre règles
que toute transaction (c'est-à-dire une suite d'ordres
modifiant les données) doit respecter :

 * **A** : Une transaction est appliquée en « tout ou rien ».
 * **C** : Une transaction amène la base d'un état stable à un autre.
 * **I** : Les transactions n'agissent pas les unes sur les autres.
 * **D** : Une transaction validée sera conservée de manière permanente.

Les bases de données relationnelles les plus courantes
depuis des décennies
(PostgreSQL bien sûr, mais aussi Oracle, MySQL, SQL Server,
SQLite…) se basent sur ces principes, même si elles font chacune des
compromis différents suivant leurs cas d'usage,
les compromis acceptés à chaque époque avec la performance
et les versions.
<!-- les pires vacheries sur MySQL ne sont plus d'actualité -->

**Atomicité** :

Une transaction doit être exécutée entièrement ou pas du tout,
et surtout pas partiellement, même si elle est longue et complexe,
même en cas d'incident majeur sur la base de données.
L'exemple basique est une transaction bancaire : le montant d'un
virement doit être sur un compte ou un autre, et en cas de
problème ne pas disparaître ou apparaître en double.
Ce principe garantit que les données modifiées par des transactions
valides seront toujours visibles dans un état stable,
et évite nombre de problèmes fonctionnels comme techniques.

**Cohérence** :

Un état cohérent respecte les règles de validité définies dans le
modèle, c'est-à-dire les contraintes définies dans
le modèle : types, plages de valeurs admissibles, unicité,
liens entre tables (clés étrangères), etc.
Le non-respect de ces règles par l'applicatif entraîne une erreur
et un rejet de la transaction.

**Isolation** :

Des transactions simultanées doivent agir comme si elles étaient
seules sur la base.
Surtout, elles ne voient pas les données _non validées_ des
autres transactions. Ainsi une transaction peut travailler sur
un état stable et fixe, et durer assez longtemps sans risque de gêner
les autres transactions.

Il existe plusieurs « niveaux d'isolation » pour définir précisément
le comportement en cas de lectures ou écritures simultanées sur
les mêmes données et pour arbitrer avec les contraintes de performances ;
le niveau le plus contraignant exige que tout se passe
comme si toutes les transactions se déroulaient successivement.
<!-- développé dans A2 -->

**Durabilité** :

Une fois une transaction validée par le serveur
(typiquement : `COMMIT` ne retourne pas d'erreur, ce qui valide
la cohérence et l'enregistrement physique), l'utilisateur doit
avoir la garantie que la donnée ne sera pas perdue ;
du moins jusqu'à ce qu'il décide de la modifier à nouveau.
Cette garantie doit valoir même en cas d'événément catastrophique :
plantage de la base, perte d'un disque…
C'est donc au serveur de s'assurer autant que
possible que les différents éléments (disque, système d'exploitation…)
ont bien rempli leur office. C'est à l'humain d'arbitrer entre le
niveau de criticité requis et les contraintes de performances
et de ressources adéquates (et fiables) à fournir à la base de données.

**NoSQL** : <!-- pas dans slide -->

À l'inverse, les outils de la mouvance (« NoSQL », par exemple MongoDB
ou Cassandra), ne fournissent pas les garanties ACID. C'est le cas de la
plupart des bases non-relationnelles, qui reprennent le
modèle [BASE](https://en.wikipedia.org/wiki/Eventual_consistency)
<!--
https://phoenixnap.com/kb/acid-vs-base
https://neo4j.com/blog/acid-vs-base-consistency-models-explained/
-->
(_Basically Available_,
_Soft State_, _Eventually Consistent_, soit succintement :
disponibilité d'abord ; incohérence possible entre les réplicas ;
cohérence… à terme, après un délai). Un intérêt est de débarasser
le développeur de certaines lourdeurs apparentes liées à la modélisation
assez stricte d'une base de données relationnelle.
Cependant, la plupart des applications ont d'abord besoin des garanties
de sécurité et cohérence qu'offrent un moteur transactionnel classique,
et la décision d'utiliser un système ne les garantissant pas
ne doit pas être prise à la légère ; sans parler d'autres critères
comme la fragmentation du domaine par rapport au monde relationnel
et son SQL (à peu près) standardisé. 
Avec le temps, les moteurs transactionnels ont acquis des fonctionnalités
qui faisaient l'intérêt des bases NoSQL (en premier lieu la facilité de
réplication et le stockage de JSON), et ces dernières ont tenté
d'intégrer un peu plus de sécurité dans leur modèle.
<!--
PG vs mongo, Matthieu Cornillon Pgday 2023 :
MongoDB pas plus rapide que PG si intégrité garantie
https://pgday.fr/docs/2023/07_Matthieu_Cornillon_De_la-feuille_a_lelephant.pdf
--> 

</div>

---

### MVCC

<div class="slide-content">

  * MultiVersion Concurrency Control
  * Le « noyau » de PostgreSQL
  * Garantit les propriétés ACID
  * Permet les accès concurrents sur la même table
    + une lecture ne bloque pas une écriture
    + une écriture ne bloque pas une lecture
    + une écriture ne bloque pas les autres écritures...
    + …sauf pour la mise à jour de la **même ligne**

</div>

<div class="notes">

MVCC (Multi Version Concurrency Control) est le mécanisme interne de
PostgreSQL utilisé pour garantir la cohérence des données lorsque plusieurs
processus accèdent simultanément à la même table.

MVCC maintient toutes les versions nécessaires de chaque ligne, ainsi **chaque
transaction voit une image figée de la base** (appelée _snapshot_). Cette
image correspond à l'état de la base lors du démarrage de la requête ou de la
transaction, suivant le niveau d'_isolation_ demandé par l'utilisateur à
PostgreSQL pour la transaction.

MVCC fluidifie les mises à jour en évitant les blocages trop contraignants
(verrous sur `UPDATE`) entre sessions et par conséquent de meilleures
performances en contexte transactionnel.

C'est notamment MVCC qui permet d'exporter facilement une base _à chaud_ et
d'obtenir un export cohérent alors même que plusieurs utilisateurs sont
potentiellement en train de modifier des données dans la base.

C'est la qualité de l'implémentation de ce système qui fait de PostgreSQL un
des meilleurs SGBD au monde : chaque transaction travaille dans son image de
la base, cohérent du début à la fin de ses opérations. Par ailleurs, les
écrivains ne bloquent pas les lecteurs et les lecteurs ne bloquent pas les
écrivains, contrairement aux SGBD s'appuyant sur des verrous de lignes. Cela
assure de meilleures performances, moins de contention et un fonctionnement
plus fluide des outils s'appuyant sur PostgreSQL.

</div>

---

### Transactions

<div class="slide-content">

  * Une transaction = ensemble **atomique** d'opérations
  * « Tout ou rien »
  * `BEGIN` obligatoire pour grouper des modifications
  * `COMMIT` pour valider
    + y compris le DDL
  * Perte des modifications si :
    + `ROLLBACK` / perte de la connexion / arrêt (brutal ou non) du serveur
  * `SAVEPOINT` pour sauvegarde des modifications d'une transaction à un instant `t`
  * Pas de transactions imbriquées

</div>

<div class="notes">

<!--
(Attention, capitaines est utilisé plus loin)
-->

L'exemple habituel et très connu des transactions est celui du virement d'une
somme d'argent du compte de Bob vers le compte d'Alice.  Le total du compte de
Bob ne doit pas montrer qu'il a été débité de X euros tant que le compte d'Alice
n'a pas été crédité de X euros. Nous souhaitons en fait que les deux opérations
apparaissent aux yeux du reste du système comme une seule opération unitaire.
D'où l'emploi d'une transaction explicite. En voici un exemple :

```sql
BEGIN;
UPDATE comptes SET solde=solde-200 WHERE proprietaire='Bob';
UPDATE comptes SET solde=solde+200 WHERE proprietaire='Alice';
COMMIT;
```

Contrairement à d'autres moteurs de bases de données, PostgreSQL accepte aussi
les instructions DDL dans une transaction. En voici un exemple :

```sql
BEGIN;
CREATE TABLE capitaines (id serial, nom text, age integer);
INSERT INTO capitaines VALUES (1, 'Haddock', 35);
```
```sql
SELECT age FROM capitaines;
```
```default
 age
---
  35
```

```sql
ROLLBACK;
SELECT age FROM capitaines;
```
```default
ERROR:  relation "capitaines" does not exist
LINE 1: SELECT age FROM capitaines;
                        ^
```

Nous voyons que la table `capitaines` a existé **à l'intérieur** de la
transaction.  Mais puisque cette transaction a été annulée (`ROLLBACK`), la
table n'a pas été créée au final.

Cela montre aussi le support du DDL transactionnel au sein de PostgreSQL :
PostgreSQL n'effectue aucun `COMMIT` implicite sur des ordres DDL tels que
`CREATE TABLE`, `DROP TABLE` ou `TRUNCATE TABLE`. De ce fait, ces ordres peuvent
être annulés au sein d'une transaction.

Un point de sauvegarde est une marque spéciale à l'intérieur d'une transaction
qui autorise l'annulation de toutes les commandes exécutées après son
établissement, restaurant la transaction dans l'état où elle était au moment de
l'établissement du point de sauvegarde.

```sql
BEGIN;
CREATE TABLE capitaines (id serial, nom text, age integer);
INSERT INTO capitaines VALUES (1, 'Haddock', 35);
SAVEPOINT insert_sp;
UPDATE capitaines SET age = 45 WHERE nom = 'Haddock';
ROLLBACK TO SAVEPOINT insert_sp;
COMMIT;
```
```sql
SELECT age FROM capitaines WHERE nom = 'Haddock';
```

```default
 age
---
  35
```

Malgré le `COMMIT` après l'`UPDATE`, la mise à jour n'est pas prise en compte.
En effet, le `ROLLBACK TO SAVEPOINT` a permis d'annuler cet `UPDATE` mais pas
les opérations précédant le `SAVEPOINT`.

À partir de la version 12, il est possible de chaîner les transactions avec
`COMMIT AND CHAIN` ou `ROLLBACK AND CHAIN`. Cela veut dire terminer une
transaction et en démarrer une autre immédiatement après avec les mêmes
propriétés (par exemple, le niveau d'isolation).

</div>

---

### Niveaux d'isolation

<div class="slide-content">

  * Chaque transaction (et donc session) est isolée à un certain point
    + elle ne voit pas les opérations des autres
    + elle s'exécute indépendamment des autres
  * Nous pouvons spécifier le niveau d'isolation au démarrage d'une transaction
    + `BEGIN ISOLATION LEVEL xxx;`
  * Niveaux d'isolation supportés
    + `read commited` (défaut)
    + `repeatable read`
    + `serializable`

</div>

<div class="notes">

Chaque transaction, en plus d'être atomique, s'exécute séparément des autres.
Le niveau de séparation demandé sera un compromis entre le besoin applicatif
(pouvoir ignorer sans risque ce que font les autres transactions) et les
contraintes imposées au niveau de PostgreSQL (performances, risque d'échec
d'une transaction).

Le standard SQL spécifie quatre niveaux, mais PostgreSQL n'en supporte que
trois (il n'y a pas de `read uncommitted` : les lignes non encore
committées par les autres transactions sont toujours invisibles).

</div>

---

### Fiabilité : journaux de transactions (1)

![Principe de la journalisation](medias/common/journalisation-bases.png)
\

<div class="notes">
<!-- noop nécessaire pour le slide -->
</div>

---

### Fiabilité : journaux de transactions (2)

<!-- include et slide de description rapide de la journalisation
commun à A2 (DBA1), J1 (PERF1)
-->

<div class="slide-content">

<!--
 Cet include est destiné au slide des rappels sur la journalisation
 Texte : voir include séparé
 Ajouter avant le schéma simplifié sur la journalisation dans medias/common/
 Utilisation : A2 (DBA1) J1 (PERF1) I2 (DBA3, sur PITR), mais pas M3 (DBA2)
-->

* _Write Ahead Logs_ (WAL)
* Chaque donnée est écrite **2 fois** sur le disque !
* Avantages :
  + sécurité infaillible (après `COMMIT`), intégrité, durabilité
  + écriture séquentielle rapide, et un seul _sync_ sur le WAL
  + fichiers de données écrits en asynchrone
  + sauvegarde PITR et réplication fiables

</div>

<div class="notes">

<!--
 Cet include est destiné au texte des rappels sur la journalisation
 Slide : voir include séparé
 Ajouter avant le schéma simplifié sur la journalisation dans medias/common/
 Utilisation : A2 (DBA1) J1 (PERF1) I2 (DBA3, sur PITR)
 M3 (journalisation) a une version plus développée de ce texte
-->

Les journaux de transactions (appelés souvent WAL) sont une garantie
contre les pertes de données.
Il s'agit d'une technique standard de journalisation appliquée à toutes les
transactions, pour garantir l'intégrité (la base reste cohérente quoiqu'il
arrive) et la durabilité (ce qui est validé ne sera pas perdu).

Ainsi lors d'une modification de donnée, l'écriture au niveau du
disque se fait généralement en deux temps :

 - écriture des modifications dans le journal de transactions,
avec écriture forcée sur disque (« synchronisation ») lors d'un `COMMIT` ;
 - écriture dans le fichier de données bien plus tard, lors d'un « checkpoint ».

Ainsi en cas de crash :

 - PostgreSQL redémarre ;
 - il vérifie s'il reste des données non intégrées aux fichiers de
   données dans les journaux (mode *recovery*) ;
 - si c'est le cas, ces données sont recopiées dans les fichiers de données
   afin de retrouver un état stable et cohérent ;
 - PostgreSQL peut alors s'ouvrir sans perte des transactions validées lors du crash
(une transaction interrompue, et donc jamais validée, est perdue).

Les écritures dans le journal se font de façon séquentielle, donc sans grand déplacement de la
tête d'écriture (sur un disque dur classique, c'est
l'opération la plus coûteuse).
De plus, comme nous n'écrivons que dans un seul fichier de transactions, la
synchronisation sur disque, lors d'un `COMMIT`, peut se faire sur ce seul fichier, si
le système de fichiers le supporte. Concrètement, ces journaux sont des fichiers
de 16 Mo par défaut, avec des noms comme `0000000100000026000000AF`,
dans le répertoire `pg_wal/` de l'instance PostgreSQL
(répertoire souvent sur une partition dédiée).

L'écriture définitive dans les fichiers de données est asynchrone, et généralement
lissée, ce qui est meilleur pour les performances qu'une écriture immédiate.
Cette opération est appelée « _checkpoint_ » et périodique (5 minutes par défaut, ou plus).

Divers paramètres et fonctionnalités peuvent altérer ce comportement par défaut,
par exemple pour des raisons de performances.
<!-- background writer, synchronous_commit, tables unlogged etc. -->

À côté de la sécurité et des performances, le mécanisme des journaux de transactions
est aussi utilisé pour des
fonctionnalités très intéressantes, comme le PITR et la réplication physique,
basés sur le rejeu des informations stockées dans ces journaux.

Pour plus d'informations :

* _[PostgreSQL et ses journaux de transactions](https://public.dalibo.com/archives/publications/glmf108_postgresql_et_ses_journaux_de_transactions.pdf)_,
> Guillaume Lelarge, GNU/Linux Magazine n°108 (septembre 2008), encore d'actualité pour l'essentiel.


</div>

---

### Sauvegardes

<div class="slide-content">

  * Sauvegarde des fichiers à froid
    + outils système
  * Import/Export logique
    + `pg_dump`, `pg_dumpall`, `pg_restore`
  * Sauvegarde physique à chaud
    + `pg_basebackup`
    + sauvegarde PITR

</div>

<div class="notes">

PostgreSQL supporte différentes solutions pour la sauvegarde.

La plus simple revient à sauvegarder à froid tous les fichiers des différents
répertoires de données mais cela nécessite d'arrêter le serveur, ce qui
occasionne une mise hors production plus ou moins longue, suivant la
volumétrie à sauvegarder.

L'export logique se fait avec le serveur démarré.
Plusieurs outils sont proposés : `pg_dump` pour
sauvegarder une base, `pg_dumpall` pour sauvegarder toutes les bases.
Suivant le format de l'export, l'import se fera avec les outils `psql` ou
`pg_restore`. Les sauvegardes se font à chaud et sont cohérentes sans
blocage de l'activité (seuls la suppression des tables et le changement de leur
définition sont interdits).

Enfin, il est possible de sauvegarder les fichiers à chaud. Cela nécessite de
mettre en place l'archivage des journaux de transactions. L'outil
`pg_basebackup` est conseillé pour ce type de sauvegarde.

Il est à noter qu'il existe un grand nombre d'outils développés par la
communauté pour faciliter encore plus la gestion des sauvegardes avec des
fonctionnalités avancées comme le PITR (_Point In Time Recovery_)
ou la gestion de la rétention, notamment pg_back (sauvegarde logique),
pgBackRest ou barman (sauvegarde physique).

</div>

---

### Réplication

<div class="slide-content">
  * Réplication physique
    + instance complète
    + même architecture
  * Réplication logique (PG 10+)
    + table par table / colonne par colonne avec ou sans filtre (PG 15)
    + voire opération par opération
  * Asynchrones ou synchrone
  * Asymétriques

</div>

<div class="notes">

PostgreSQL dispose de la réplication depuis de nombreuses années.

Le premier type de réplication intégrée est la réplication physique. Il n'y a
pas de granularité, c'est forcément l'instance complète (toutes les bases de données),
et au niveau des fichiers de données. Cette réplication est
asymétrique : un seul serveur primaire effectue lectures comme écritures, et les
serveurs secondaires n'acceptent que des lectures.

Le deuxième type de réplication est bien plus récent vu qu'il a été ajouté en
version 10. Il s'agit d'une réplication logique, où les données elles-mêmes
sont répliquées. Cette réplication est elle aussi asymétrique. Cependant, ceci
se configure table par table (et non pas au niveau de l'instance comme pour la
réplication physique). Avec la version 15, il devient possible de choisir
quelles colonnes sont publiées et de filtre les lignes à publier.

La réplication logique n'est pas intéressante quand nous voulons un serveur sur
lequel basculer en cas de problème sur le primaire. Dans ce cas, il vaut mieux
utiliser la réplication physique. Par contre, c'est le bon type de
réplication pour une réplication partielle ou pour une mise à jour de version
majeure.

Dans les deux cas, les modifications sont transmises en asynchrone
(avec un délai possible). Il est
cependant possible de la configurer en synchrone pour tous les serveurs ou
seulement certains.

</div>

---

### Extensibilité

<!-- Doublons avec X1 mais c'est inévitable et pas grave -->

<div class="slide-content">

  * Extensions
    + `CREATE EXTENSION monextension ;`
    + nombreuses : contrib, packagées… selon provenance
    + notion de confiance (v13+)
    + dont langages de procédures stockées !
  * Système des _hooks_
  * _Background workers_

</div>

<div class="notes">

Faute de pouvoir intégrer toutes les fonctionnalités demandées dans
PostgreSQL, ses développeurs se sont attachés à permettre à l'utilisateur
d'étendre lui-même les fonctionnalités sans avoir à modifier le
code principal.

Ils ont donc ajouté la possibilité de créer des extensions. Une extension contient un
ensemble de types de données, de fonctions, d'opérateurs, etc. en un seul objet
logique. Il suffit de créer ou de supprimer cet objet logique pour intégrer ou
supprimer tous les objets qu'il contient.
Cela facilite grandement l'installation et la désinstallation de nombreux objets.
Les extensions peuvent être codées en
différents langages, généralement en C ou en PL/SQL.
Elles ont eu un grand succès.

La possibilité de développer des routines dans différents langages en est un
exemple : perl, python, PHP, Ruby ou JavaScript sont disponibles.
PL/pgSQL est lui-même une extension à proprement parler, toujours présente.

Autre exemple : la possibilité d'ajouter des types de données, des routines et des opérateurs
a permis l'émergence de la couche spatiale de PostgreSQL (appelée PostGIS).

Les provenances, rôle et niveau de finition des extensions sont très variables.
Certaines sont des utilitaires éprouvés fournis avec PostgreSQL
(parmi les « contrib »). D'autres sont des utilitaires aussi complexes que
PostGIS ou un langage de procédures stockées. Des éditeurs diffusent leur produit
comme une extension plutôt que _forker_ PostgreSQL (Citus, timescaledb…).
Beaucoup d'extensions peuvent être installées très simplement depuis
des paquets disponibles dans les dépôts habituels (de la distribution ou du PGDG),
ou le site du concepteur. Certaines sont diffusées comme code source à compiler.
Comme tout logiciel, il faut faire attention à en vérifier la source, la qualité, la
réputation et la pérennité.

Une fois les binaires de l'extension en place sur le serveur,
l'ordre `CREATE EXTENSION` suffit généralement dans la base cible, et les
fonctionnalités sont immédiatement exploitables.

Les extensions sont habituellement installées par un administrateur (un
utilisateur doté de l'attribut `SUPERUSER`). À partir de la version 13,
certaines extensions sont déclarées de confiance `trusted`). Ces extensions
peuvent être installées par un utilisateur standard (à condition qu'il dispose
des droits de création dans la base et le ou les schémas concernés).

Les développeurs de PostgreSQL ont aussi ajouté des _hooks_ pour accrocher du
code à exécuter sur certains cas. Cela a permis entre autres de créer l'extension
`pg_stat_statements` qui s'accroche au code de l'exécuteur de requêtes pour
savoir quelles sont les requêtes exécutées et pour récupérer des statistiques sur
ces requêtes.

Enfin, les _background workers_ ont vu le jour. Ce sont des processus
spécifiques lancés par le serveur PostgreSQL lors de son démarrage et stoppés
lors de son arrêt. Cela a permis la création de PoWA (outil qui historise les
statistiques sur les requêtes) et une amélioration très intéressante de
`pg_prewarm` (sauvegarde du contenu du cache disque à l'arrêt de PostgreSQL,
restauration du contenu au démarrage).

Des exemples d'extensions sont décrites dans nos modules
[Extensions PostgreSQL pour l'utilisateur](https://dali.bo/x1_html),
[Extensions PostgreSQL pour la performance](https://dali.bo/x2_html),
[Extensions PostgreSQL pour les DBA](https://dali.bo/x3_html).

</div>

---

### Sécurité

<div class="slide-content">

  * Fichier `pg_hba.conf`
  * Filtrage IP
  * Authentification interne (MD5, SCRAM-SHA-256)
  * Authentification externe (identd, LDAP, Kerberos...)
  * Support natif de SSL

</div>

<div class="notes">

Le filtrage des connexions se paramètre dans le fichier de configuration
`pg_hba.conf`. Nous pouvons y définir quels utilisateurs (déclarés auprès de
PostgreSQL) peuvent se connecter à quelles bases, et depuis quelles adresses
IP.

L'authentification peut se baser sur des mots de passe chiffrés propres à
PostgreSQL (`md5` ou le plus récent et plus sécurisé `scram-sha-256` en version 10), ou se
baser sur une méthode externe (auprès de l'OS, ou notamment LDAP ou Kerberos
qui couvre aussi Active Directory).

Si PostgreSQL interroge un service de mots de passe centralisé,
vous devez toujours créer les rôle dans PostgreSQL.
Seule l'option `WITH PASSWORD` est inutile.
Pour créer, configurer mais aussi supprimer les rôles depuis un annuaire,
l'outil [ldap2pg](https://labs.dalibo.com/ldap2pg) existe.

L'authentification et le chiffrement de la connexion par SSL sont couverts.

</div>

---

## Objets SQL

<div class="slide-content">

  * Instances
  * Objets globaux :
    + Bases
    + Rôles
    + Tablespaces
  * Objets locaux :
    + Schémas
    + Tables
    + Vues
    + Index
    + Routines
    + ...

</div>

<div class="notes">

Le but de cette partie est de passer en revue les différents objets logiques
maniés par un moteur de bases de données PostgreSQL.

Nous allons donc aborder la notion d'instance, les différents objets globaux
et les objets locaux. Tous ne seront pas vus, mais le but est de donner une
idée globale des objets et des fonctionnalités de PostgreSQL.

</div>

---

### Organisation logique

![Organisation logique d'une instance](medias/common/organisation.png){ height=660 }
\

<div class="notes">

Il est déjà important de bien comprendre une distinction entre les objets. Une
instance est un ensemble de bases de données, de rôles et de tablespaces. Ces
objets sont appelés des objets globaux parce qu'ils sont disponibles quelque
soit la base de données de connexion. Chaque base de données contient ensuite
des objets qui lui sont propres. Ils sont spécifiques à cette base de données
et accessibles uniquement lorsque l'utilisateur est connecté à la base qui les
contient. Il est donc possible de voir les bases comme des conteneurs hermétiques en
dehors des objets globaux.

</div>

---

### Instances

<div class="slide-content">

  * Une instance
    + un répertoire de données
    + un port TCP
    + une configuration
    + plusieurs bases de données
  * Plusieurs instances possibles sur un serveur

</div>

<div class="notes">

Une instance est un ensemble de bases de données. Après avoir installé
PostgreSQL, il est nécessaire de créer un répertoire de données contenant un
certain nombre de répertoires et de fichiers qui permettront à PostgreSQL de
fonctionner de façon fiable. Le contenu de ce répertoire est créé initialement
par la commande `initdb`. Ce répertoire stocke ensuite tous les objets des
bases de données de l'instance, ainsi que leur contenu.

Chaque instance a sa propre configuration. Il n'est possible de lancer qu'un seul
`postmaster` par instance, et ce dernier acceptera les connexions à partir
d'un port TCP spécifique.

Il est possible d'avoir plusieurs instances sur le même serveur, physique ou
virtuel. Dans ce cas, chaque instance aura son répertoire de données dédié et
son port TCP dédié. Ceci est particulièrement utile quand l'on souhaite disposer
de plusieurs versions de PostgreSQL sur le même serveur (par exemple pour
tester une application sur ces différentes versions).

</div>

---

### Rôles

<div class="slide-content">

  * Utilisateurs / Groupes
    + Utilisateur : Permet de se connecter
  * Différents attributs et droits

</div>

<div class="notes">

Une instance contient un ensemble de rôles. Certains sont prédéfinis et
permettent de disposer de droits particuliers (lecture de fichier avec
`pg_read_server_files`, annulation d'une requête avec `pg_signal_backend`,
etc). Cependant, la majorité est composée de rôles créés pour permettre la
connexion des utilisateurs.

Chaque rôle créé peut être utilisé pour se connecter à n'importe quelle base de
l'instance, à condition que ce rôle en ait le droit. Ceci se gère directement
 avec l'attribution du droit `LOGIN` au rôle, et avec la configuration du fichier d'accès
`pg_hba.conf`.

Chaque rôle peut être propriétaire d'objets, auquel cas il a tous les droits
sur ces objets. Pour les objets dont il n'est pas propriétaire, il peut se voir
donner des droits, en lecture, écriture, exécution, etc par le propriétaire.

Nous parlons aussi d'utilisateurs et de groupes. Un utilisateur est un rôle qui a
la possibilité de se connecter aux bases alors qu'un groupe ne le peut pas. Un
groupe sert principalement à gérer plus simplement les droits d'accès aux
objets.

</div>

---

### Tablespaces

<div class="slide-content">

  * Répertoire physique contenant les fichiers de données de l'instance
  * Une base peut
    + se trouver sur un seul tablespace
    + être répartie sur plusieurs tablespaces
  * Permet de gérer l'espace disque et les performances
  * Pas de quota

</div>

<div class="notes">

Toutes les données des tables, vues matérialisées et index sont stockées dans
le répertoire de données principal. Cependant, il est possible de stocker des
données ailleurs que dans ce répertoire. Il faut pour cela créer un
tablespace. Un tablespace est tout simplement la déclaration d'un autre
répertoire de données utilisable par PostgreSQL pour y stocker des données :

```sql
CREATE TABLESPACE chaud LOCATION '/SSD/tbl/chaud';
```

Il est possible d'avoir un tablespace par défaut pour une base de données,
auquel cas tous les objets logiques créés dans cette base seront enregistrés
physiquement dans le répertoire lié à ce tablespace. Il est aussi possible de
créer des objets en indiquant spécifiquement un tablespace, ou de les déplacer
d'un tablespace à un autre. Un objet spécifique ne peut
appartenir qu'à un seul tablespace (autrement dit, un index ne pourra pas être
enregistré sur deux tablespaces). Cependant, pour les objets partitionnés,
le choix du tablespace peut se faire partition par partition.

Le but des tablespaces est de fournir une solution à des problèmes d'espace
disque ou de performances. Si la partition où est stocké le répertoire des
données principal se remplit fortement, il est possible de créer un tablespace
dans une autre partition et donc d'utiliser l'espace disque de cette
partition. Si de nouveaux disques plus rapides sont à disposition, il est possible de
placer les objets fréquemment utilisés sur le tablespace contenant les disques
rapides. Si des disques SSD sont à disposition, il est très intéressant d'y placer les
index, les fichiers de tri temporaires, des tables de travail…

Par contre, contrairement à d'autres moteurs de bases de données, PostgreSQL
n'a pas de notion de quotas. Les tablespaces ne peuvent donc pas être utilisés
pour contraindre l'espace disque utilisé par certaines applications ou
certains rôles.

</div>

---

### Bases

<div class="slide-content">

  * Conteneur hermétique
  * Un rôle ne se connecte pas à une instance
    + il se connecte forcément à une base
  * Une fois connecté, il ne voit que les objets de cette base
    + contournement : foreign data wrappers, dblink

</div>

<div class="notes">

Une base de données est un conteneur hermétique. En dehors des objets globaux,
le rôle connecté à une base de données ne voit et ne peut interagir qu'avec
les objets contenus dans cette base. De même, il ne voit pas les objets locaux
des autres bases. Néanmoins, il est possible de lui donner le droit d'accéder à certains
objets d'une autre base (de la même instance ou d'une autre instance) en
utilisant les _Foreign Data Wrappers_ (`postgres_fdw`) ou l'extension `dblink`.

Un rôle ne se connecte pas à l'instance. Il se connecte forcément à une base
spécifique.

</div>

---

### Schémas

<div class="slide-content">

  * Espace de noms
  * Sous-ensemble de la base
  * Non lié à un utilisateur
  * Résolution des objets : `search_path`
  * `pg_catalog`, `information_schema`
    + pour catalogues système (lecture seule !)

</div>

<div class="notes">

Les schémas sont des espaces de noms à l'intérieur d'une base de données
permettant :

  * de grouper logiquement les objets d'une base de données ;
  * de séparer les utilisateurs entre eux ;
  * de contrôler plus efficacement les accès aux données ;
  * d'éviter les conflits de noms dans les grosses bases de données.

Un schéma n'a à priori aucun lien avec un utilisateur donné.

Un schéma est un espace logique sans lien avec les emplacements physiques des
données (ne pas confondre avec les _tablespaces_).

Un utilisateur peut avoir accès à tous les schémas ou à un sous-ensemble, tout
dépend des droits dont il dispose. Depuis la version 15, un nouvel utilisateur
n'a le droit de créer d'objet nulle part.  Dans les versions précédentes, il
avait accès au schéma `public` de chaque base et pouvait y créer des objets.

Lorsque le schéma n'est pas indiqué explicitement pour les objets d'une requête,
PostgreSQL recherche les objets dans les schémas listés par le paramètre
`search_path` valable pour la session en cours .

Voici un exemple d'utilisation des schémas :

```sql
-- Création de deux schémas
CREATE SCHEMA s1;
CREATE SCHEMA s2;

-- Création d'une table sans spécification du schéma
CREATE TABLE t1 (id integer);

-- Comme le montre la méta-commande \d, la table est créée dans le schéma public
```
```default
postgres=# \d
                List of relations
 Schema |       Name        |   Type   |  Owner
--------+-------------------+----------+----------
 public | capitaines        | table    | postgres
 public | capitaines_id_seq | sequence | postgres
 public | t1                | table    | postgres
```

```sql
-- Ceci est dû à la configuration par défaut du paramètre search_path
-- modification du search_path
SET search_path TO s1;

-- création d'une nouvelle table sans spécification du schéma
CREATE TABLE t2 (id integer);

-- Cette fois, le schéma de la nouvelle table est s1
-- car la configuration du search_path est à s1
-- Nous pouvons aussi remarquer que les tables capitaines et s1
-- ne sont plus affichées
-- Ceci est dû au fait que le search_path ne contient que le schéma s1 et
-- n'affiche donc que les objets de ce schéma.
```
```default
postgres=# \d
        List of relations
 Schema | Name | Type  |  Owner
--------+------+-------+----------
 s1     | t2   | table | postgres
```
```sql
-- Nouvelle modification du search_path
SET search_path TO s1, public;

-- Cette fois, les deux tables apparaissent
```
```default
postgres=# \d
                List of relations
 Schema |       Name        |   Type   |  Owner
--------+-------------------+----------+----------
 public | capitaines        | table    | postgres
 public | capitaines_id_seq | sequence | postgres
 public | t1                | table    | postgres
 s1     | t2                | table    | postgres
```
```sql
-- Création d'une nouvelle table en spécifiant cette fois le schéma
CREATE TABLE s2.t3 (id integer);

-- changement du search_path pour voir la table
SET search_path TO s1, s2, public;

-- La table apparaît bien, et le schéma d'appartenance est bien s2
```
```default
postgres=# \d
                List of relations
 Schema |       Name        |   Type   |  Owner
--------+-------------------+----------+----------
 public | capitaines        | table    | postgres
 public | capitaines_id_seq | sequence | postgres
 public | t1                | table    | postgres
 s1     | t2                | table    | postgres
 s2     | t3                | table    | postgres
```
```sql
-- Création d'une nouvelle table en spécifiant cette fois le schéma
-- attention, cette table a un nom déjà utilisé par une autre table
CREATE TABLE s2.t2 (id integer);

-- La création se passe bien car, même si le nom de la table est identique,
-- le schéma est différent
-- Par contre, \d ne montre que la première occurence de la table
-- ici, nous ne voyons t2 que dans s1
```
```default
postgres=# \d
                List of relations
 Schema |       Name        |   Type   |  Owner
--------+-------------------+----------+----------
 public | capitaines        | table    | postgres
 public | capitaines_id_seq | sequence | postgres
 public | t1                | table    | postgres
 s1     | t2                | table    | postgres
 s2     | t3                | table    | postgres
```
```sql
-- Changeons le search_path pour placer s2 avant s1
SET search_path TO s2, s1, public;

-- Maintenant, la seule table t2 affichée est celle du schéma s2
```
```default
postgres=# \d
                List of relations
 Schema |       Name        |   Type   |  Owner
--------+-------------------+----------+----------
 public | capitaines        | table    | postgres
 public | capitaines_id_seq | sequence | postgres
 public | t1                | table    | postgres
 s2     | t2                | table    | postgres
 s2     | t3                | table    | postgres
```

Tous ces exemples se basent sur des ordres de création de table. Cependant, le
comportement serait identique sur d'autres types de commande (`SELECT`,
`INSERT`, etc) et sur d'autres types d'objets locaux.

Pour des raisons de sécurité, il est très fortement conseillé de laisser le
schéma `public` en toute fin du `search_path`. En effet, avant la version 15,
s'il est placé au début, comme tout le monde avait le droit de créer des objets
dans `public`, quelqu'un de mal intentionné pouvait placer un objet dans le
schéma `public` pour servir de proxy à un autre objet d'un schéma situé après
`public`. Même si la version 15 élimine ce risque, il reste la
bonne pratique d'adapter le `search_path` pour placer les schémas applicatifs
en premier.

Les schémas `pg_catalog` et `information_schema` contiennent des
tables utilitaires (« catalogues système ») et des vues.
Les catalogues système représentent l'endroit où une base de données
relationnelle stocke les métadonnées des schémas, telles que les
informations sur les tables, et les colonnes, et des données de suivi
interne. Dans PostgreSQL, ce sont de simples tables.
Un simple utilisateur lit fréquemment ces tables, plus ou moins
directement, mais n'a aucune raison d'y modifier des données.
Toutes les opérations habituelles pour un utilisateur ou administrateur
sont disponibles sous la forme de commandes SQL.
<div class="box warning">
Ne modifiez jamais directement les tables et vues système dans les schémas
`pg_catalog` et `information_schema` ; n'y ajoutez ni n'y effacez jamais rien !
</div>
Même si cela est techniquement possible, seules des
exceptions particulièrement ésotériques peuvent justifier une modification
directe des tables systèmes (par exemple, une correction de vue système,
suite à un bug corrigé dans une version mineure). <!-- 9.6.4 par ex -->
Ces tables n'apparaissent d'ailleurs pas dans une sauvegarde logique
(`pg_dump`).

</div>

---

### Tables

<div class="slide-content">

Par défaut, une table est :

  * Permanente
    + si temporaire, vivra le temps de la session (ou de la transaction)
  * Journalisée
    + si _unlogged_, perdue en cas de crash, pas de réplication
  * Non partitionnée
    + partitionnement possible par intervalle, valeur ou hachage

</div>

<div class="notes">

Par défaut, les tables sont permanentes, journalisées et non partitionnées.

Il est possible de créer des tables temporaires (`CREATE TEMPORARY TABLE`).
Celles-ci ne sont visibles
que par la session qui les a créées et seront supprimées par défaut à la fin de
cette session. Il est aussi possible de les supprimer automatiquement à la fin
de la transaction qui les a créées. Il n'existe pas dans PostgreSQL de notion de
table temporaire globale. Cependant, une
[extension](https://github.com/darold/pgtt) existe pour combler leur absence.

Pour des raisons de performance, il est possible de créer une table non
journalisée (`CREATE UNLOGGED TABLE`).
La définition de la table est journalisée mais pas
son contenu. De ce fait, en cas de crash, il est impossible de dire si la
table est corrompue ou non, et donc, au redémarrage du serveur, PostgreSQL
vide la table de tout contenu. De plus, n'étant pas journalisée, la table n'est
pas présente dans les sauvegardes PITR, ni repliquée vers d'éventuels serveurs
secondaires.

Enfin, depuis la version 10, il est possible de partitionner les tables suivant
un certain type de partitionnement : par intervalle, par valeur ou par hachage.

</div>

---

### Vues

<div class="slide-content">

  * Masquer la complexité
    + structure : interface cohérente vers les données, même si les tables évoluent
    + sécurité : contrôler l'accès aux données de manière sélective
  * Vues matérialisées
    + à rafraîchir à une certaine fréquence

</div>

<div class="notes">

Le but des vues est de masquer une complexité, qu'elle soit du côté de la
structure de la base ou de l'organisation des accès. Dans le premier cas,
elles permettent de fournir un accès qui ne change pas même si les structures
des tables évoluent. Dans le second cas, elles permettent l'accès à seulement
certaines colonnes ou certaines lignes. De plus, les vues étant exécutées avec
les mêmes droits que l'utilisateur qui les a créées, cela permet un changement temporaire
des droits d'accès très appréciable dans certains cas.

Voici un exemple d'utilisation :

```sql
SET search_path TO public;

-- création de l'utilisateur guillaume
-- il n'aura pas accès à la table capitaines
-- par contre, il aura accès à la vue capitaines_anon
CREATE ROLE guillaume LOGIN;

-- ajoutons une colonne à la table capitaines
-- et ajoutons-y des données
ALTER TABLE capitaines ADD COLUMN num_cartecredit text;
INSERT INTO capitaines (nom, age, num_cartecredit)
  VALUES ('Robert Surcouf', 20, '1234567890123456');

-- création de la vue
CREATE VIEW capitaines_anon AS
  SELECT nom, age, substring(num_cartecredit, 0, 10) || '******' AS num_cc_anon
  FROM capitaines;

-- ajout du droit de lecture à l'utilisateur guillaume
GRANT SELECT ON TABLE capitaines_anon TO guillaume;

-- connexion en tant qu'utilisateur guillaume
SET ROLE TO guillaume;

-- vérification qu'on lit bien la vue mais pas la table
SELECT * FROM capitaines_anon WHERE nom LIKE '%Surcouf';
```
```default
      nom       | age |   num_cc_anon
----------------+-----+-----------------
 Robert Surcouf |  20 | 123456789******
```
```sql
-- tentative de lecture directe de la table
SELECT * FROM capitaines;
ERROR:  permission denied for relation capitaines
```

Il est possible de modifier une vue en lui ajoutant des colonnes à la fin, au
lieu de devoir les détruire et recréer (ainsi que toutes les vues qui en
dépendent, ce qui peut être fastidieux).

Par exemple :

```sql
SET ROLE postgres;

CREATE OR REPLACE VIEW capitaines_anon AS SELECT
  nom,age,substring(num_cartecredit,0,10)||'******' AS num_cc_anon,
  md5(substring(num_cartecredit,0,10)) AS num_md5_cc
  FROM capitaines;
```
```sql
SELECT * FROM capitaines_anon WHERE nom LIKE '%Surcouf';
```
```default
      nom       | age |   num_cc_anon   |            num_md5_cc
----------------+-----+-----------------+----------------------------------
 Robert Surcouf |  20 | 123456789****** | 25f9e794323b453885f5181f1b624d0b
```

Nous pouvons aussi modifier les données au travers des vues simples, sans ajout de
code et de trigger :

```sql
UPDATE capitaines_anon SET nom = 'Nicolas Surcouf' WHERE nom = 'Robert Surcouf';
```
```sql
SELECT * from capitaines_anon WHERE nom LIKE '%Surcouf';
```
```default
       nom       | age |   num_cc_anon   |            num_md5_cc
-----------------+-----+-----------------+----------------------------------
 Nicolas Surcouf |  20 | 123456789****** | 25f9e794323b453885f5181f1b624d0b
```
```sql
UPDATE capitaines_anon SET num_cc_anon = '123456789xxxxxx'
  WHERE nom = 'Nicolas Surcouf';
```
```default
ERROR:  cannot update column "num_cc_anon" of view "capitaines_anon"
DETAIL:  View columns that are not columns of their base relation
         are not updatable.
```

PostgreSQL gère le support natif des vues matérialisées
(`CREATE MATERIALIZED VIEW nom_vue_mat AS SELECT …`). Les vues
matérialisées sont des vues dont le contenu est figé sur disque, permettant de
ne pas recalculer leur contenu à chaque appel. De plus, il est possible de les indexer
pour accélérer leur consultation. Il faut cependant faire attention à ce que
leur contenu reste synchrone avec le reste des données.

Les vues matérialisées ne sont pas mises à jour automatiquement, il faut demander explicitement
le rafraîchissement (`REFRESH MATERIALIZED VIEW`).
Avec la clause `CONCURRENTLY`, s'il y a un index d'unicité,
le rafraîchissement ne bloque pas les sessions lisant en même temps
les données d'une vue matérialisée.

```sql
-- Suppression de la vue
DROP VIEW capitaines_anon;

-- Création de la vue matérialisée
CREATE MATERIALIZED VIEW capitaines_anon AS
  SELECT nom,
    age,
    substring(num_cartecredit, 0, 10) || '******' AS num_cc_anon
  FROM capitaines;
```
```sql
-- Les données sont bien dans la vue matérialisée
SELECT * FROM capitaines_anon WHERE nom LIKE '%Surcouf';
```
```default
       nom       | age |   num_cc_anon
-----------------+-----+-----------------
 Nicolas Surcouf |  20 | 123456789******
```
```sql
-- Mise à jour d'une ligne de la table
-- Cette mise à jour est bien effectuée, mais la vue matérialisée
-- n'est pas impactée
UPDATE capitaines SET nom = 'Robert Surcouf' WHERE nom = 'Nicolas Surcouf';
```
```sql
SELECT * FROM capitaines WHERE nom LIKE '%Surcouf';
```
```default
 id |      nom       | age | num_cartecredit
----+----------------+-----+------------------
  1 | Robert Surcouf |  20 | 1234567890123456
```
```sql
SELECT * FROM capitaines_anon WHERE nom LIKE '%Surcouf';
```
```default
       nom       | age |   num_cc_anon
-----------------+-----+-----------------
 Nicolas Surcouf |  20 | 123456789******
```
```sql
-- Le résultat est le même mais le plan montre bien que PostgreSQL ne passe
-- plus par la table mais par la vue matérialisée :
EXPLAIN SELECT * FROM capitaines_anon WHERE nom LIKE '%Surcouf';
```
```default
                         QUERY PLAN
-----------------------------------------------------------------
 Seq Scan on capitaines_anon  (cost=0.00..20.62 rows=1 width=68)
   Filter: (nom ~~ '%Surcouf'::text)
```
```sql
-- Après un rafraîchissement explicite de la vue matérialisée,
-- cette dernière contient bien les bonnes données
REFRESH MATERIALIZED VIEW capitaines_anon;

SELECT * FROM capitaines_anon WHERE nom LIKE '%Surcouf';
```
```default
      nom       | age |   num_cc_anon
----------------+-----+-----------------
 Robert Surcouf |  20 | 123456789******
```
```sql
-- Pour rafraîchir la vue matérialisée sans bloquer les autres sessions :

REFRESH MATERIALIZED VIEW CONCURRENTLY capitaines_anon;
```
```default
ERROR:  cannot refresh materialized view "public.capitaines_anon" concurrently
HINT:  Create a unique index with no WHERE clause on one or more columns
       of the materialized view.
```
```sql
-- En effet, il faut un index d'unicité pour faire un rafraîchissement
-- sans bloquer les autres sessions.
CREATE UNIQUE INDEX ON capitaines_anon(nom);
```
```default
REFRESH MATERIALIZED VIEW CONCURRENTLY capitaines_anon;
```

</div>

---

### Index

<div class="slide-content">

  * Algorithmes supportés
    + B-tree (par défaut)
    + Hash
    + GiST / SP-GiST
    + GIN
    + BRIN
    + Bloom
  * Type
    + Mono ou multicolonne
    + Partiel
    + Fonctionnel
    + Couvrant

</div>

<div class="notes">

PostgreSQL propose plusieurs algorithmes d'index.

Pour une indexation standard, nous utilisons en général un index B-tree, de par ses
nombreuses possibilités et ses très bonnes performances.

Les index hash sont peu utilisés, essentiellement dans la comparaison d'égalité
de grandes chaînes de caractères.

Moins simples d'abord, les index plus spécifiques (GIN, GIST) sont spécialisés pour les grands
volumes de données complexes et multidimensionnelles : indexation textuelle,
géométrique, géographique, ou de tableaux de données par exemple.

Les index BRIN sont des index très compacts destinés aux grandes tables où les données sont
fortement corrélées par rapport à leur emplacement physique sur les disques.

Les index bloom sont des index probabilistes visant à indexer de nombreuses
colonnes interrogées simultanément. Ils nécessitent l'ajout d'une extension
(nommée `bloom`). Contrairement aux index btree, les index bloom ne dépendant
pas de l'ordre des colonnes.

Le module `pg_trgm` permet l'utilisation d'index dans des cas habituellement
impossibles, comme les expressions rationnelles et les `LIKE '%...%' `.

Généralement, l'indexation porte sur la valeur d'une ou plusieurs colonnes. Il
est néanmoins possible de n'indexer qu'une partie des lignes (index partiel)
ou le résultat d'une fonction sur une ou plusieurs colonnes en paramètre.
Enfin, il est aussi possible de modifier les index de certaines contraintes
(unicité et clé primaire) pour inclure des colonnes supplémentaires.

**Plus d'informations** :

  * [Article Wikipédia sur les arbres B](https://fr.wikipedia.org/wiki/Arbre_B) ;
  * [Article Wikipédia sur les tables de hachage](https://fr.wikipedia.org/wiki/Table_de_hachage) ;
  * [Documentation officielle française](https://docs.postgresql.fr/current/textsearch-indexes.html).

</div>

---

### Types de données

<div class="slide-content">

  * Types de base
    + natif : `int`, `float`
    + standard SQL : `numeric`, `char`, `varchar`, `date`, `time`, `timestamp`, `bool`
  * Type complexe
    + tableau
    + JSON (`jsonb`), XML
    + vecteur (données LLM, FTS)
  * Types métier
    + réseau, géométrique, etc.
  * Types créés par les utilisateurs
    + structure SQL, C, Domaine, Enum

</div>

<div class="notes">

PostgreSQL dispose d'un grand nombre de types de base, certains natifs (comme
la famille des `integer` et celle des `float`), et certains issus de la
norme SQL (`numeric`, `char`, `varchar`, `date`, `time`,
`timestamp`, `bool`).

Il dispose aussi de types plus complexes. Les tableaux (`array`) permettent
de lister un ensemble de valeurs discontinues. Les intervalles (`range`)
permettent d'indiquer toutes les valeurs comprises entre une valeur de début
et une valeur de fin. Ces deux types dépendent évidemment d'un type de base :
tableau d'entiers, intervalle de dates, etc. Existent aussi les
types complexes les données XML et JSON (préférer le type optimisé `jsonb`).

PostgreSQL sait travailler avec des vecteurs pour des calculs avancé. De base,
le type `tsvector` permet la recherche plein texte, avec calcul de proximité de
mots dans un texte, pondération des résultats, etc. L'extension `pgvector`
permet de stocker et d'indexer des vecteurs utilisé par les algorithmes LLM
implémentés dans les IA génératives.

Enfin, il existe des types métiers ayant trait principalement au réseau
(adresse IP, masque réseau), à la géométrie (point, ligne, boite).
Certains sont apportés par des extensions.

Tout ce qui vient d'être décrit est natif. Il est cependant possible de créer
ses propres types de données, soit en SQL soit en C. Les possibilités et les
performances ne sont évidemment pas les mêmes.

Voici comment créer un type en SQL :

```sql
CREATE TYPE serveur AS (
  nom             text,
  adresse_ip      inet,
  administrateur  text
);
```

Ce type de données va pouvoir être utilisé dans tous les objets SQL
habituels : table, routine, opérateur (pour redéfinir l'opérateur `+` par
exemple), fonction d'agrégat, contrainte, etc.

Voici un exemple de création d'un opérateur :

```sql
CREATE OPERATOR + (
    leftarg = stock,
    rightarg = stock,
    procedure = stock_fusion,
    commutator = +
);
```

(Il faut au préalable avoir défini le type `stock` et la fonction
`stock_fusion`.)

Il est aussi possible de définir des domaines. Ce sont des types créés par les
utilisateurs à partir d'un type de base et en lui ajoutant des contraintes
supplémentaires.

En voici un exemple :

```sql
CREATE DOMAIN code_postal_francais AS text CHECK (value ~ '^\d{5}$');
ALTER TABLE capitaines ADD COLUMN cp code_postal_francais;
UPDATE capitaines SET cp = '35400' WHERE nom LIKE '%Surcouf';
UPDATE capitaines SET cp = '1420' WHERE nom = 'Haddock';
```
```default
ERROR:  value for domain code_postal_francais violates check constraint
        "code_postal_francais_check"
```
```sql
UPDATE capitaines SET cp = '01420' WHERE nom = 'Haddock';
SELECT * FROM capitaines;
```
```default
 id |      nom       | age | num_cartecredit  |  cp
----+----------------+-----+------------------+-------
  1 | Robert Surcouf |  20 | 1234567890123456 | 35400
  1 | Haddock        |  35 |                  | 01420
```

Les domaines permettent d'intégrer la déclaration des contraintes à la
déclaration d'un type, et donc de simplifier la maintenance de l'application
si ce type peut être utilisé dans plusieurs tables : si la définition du code
postal est insuffisante pour une évolution de l'application, il est possible de la
modifier par un `ALTER DOMAIN`, et définir de nouvelles contraintes sur le
domaine. Ces contraintes seront vérifiées sur l'ensemble des champs ayant le
domaine comme type avant que la nouvelle version du type ne soit considérée
comme valide.

Le défaut par rapport à des contraintes `CHECK` classiques sur une table est
que l'information ne se trouvant pas dans la table, les contraintes sont plus
difficiles à lister sur une table.

Enfin, il existe aussi les enums. Ce sont des types créés par les utilisateurs
composés d'une liste ordonnée de chaînes de caractères.

En voici un exemple :

```sql
CREATE TYPE jour_semaine
  AS ENUM ('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi',
  'Samedi', 'Dimanche');

ALTER TABLE capitaines ADD COLUMN jour_sortie jour_semaine;

UPDATE capitaines SET jour_sortie = 'Mardi' WHERE nom LIKE '%Surcouf';
UPDATE capitaines SET jour_sortie = 'Samedi' WHERE nom LIKE 'Haddock';

SELECT * FROM capitaines WHERE jour_sortie >= 'Jeudi';
```
```default
 id |   nom   | age | num_cartecredit | cp | jour_sortie
----+---------+-----+-----------------+----+-------------
  1 | Haddock |  35 |                 |    | Samedi
```

Les _enums_ permettent de déclarer une liste de valeurs statiques dans le
dictionnaire de données plutôt que dans une table externe sur laquelle il
faudrait rajouter des jointures : dans l'exemple, nous aurions pu créer une table
`jour_de_la_semaine`, et stocker la clé associée dans `planning`. Nous aurions pu
tout aussi bien positionner une contrainte `CHECK`, mais nous n'aurions plus eu
une liste ordonnée.

**Exemple d'utilisation** :

* [Conférence de Heikki Linakangas sur la création d'un type color](https://wiki.postgresql.org/images/1/11/FOSDEM2011-Writing_a_User_defined_type.pdf).

</div>

---

### Contraintes

<div class="slide-content">

<!-- garder synchro avec description dans le texte -->

  * `CHECK`
    + `prix > 0`
  * `NOT NULL`
    + `id_client NOT NULL`
  * Unicité
    + `id_client UNIQUE`
  * Clés primaires
    + `UNIQUE NOT NULL` ==> `PRIMARY KEY (id_client)`
  * Clés étrangères
    + `produit_id REFERENCES produits(id_produit)`
  * `EXCLUDE`
    + `EXCLUDE USING gist (room WITH =, during WITH &&)`

</div>

<div class="notes">

Les contraintes sont la garantie de conserver des données de qualité ! Elles
permettent une vérification qualitative des données, beaucoup plus fine
qu'en définissant uniquement un type de données.

Les exemples ci-dessus reprennent :

  * un prix qui doit être strictement positif ;
  * un identifiant qui ne doit pas être vide (sinon des
jointures filtreraient des lignes) ;
  * une valeur qui doit être unique (comme des numéros de clients
ou de facture) ;
  * une clé primaire (unique non nulle), qui permet d'identifier
précisément une ligne ;
  * une clé étrangère vers la clé primaire d'une autre table
(là encore pour garantir l'intégrité des jointures) ;
  * une contrainte d'exclusion interdisant que deux plages
temporelles se recouvrent dans la réservation de la même salle
de réunion.

Les contraintes d'exclusion permettent un test sur plusieurs colonnes avec
différents opérateurs (et non uniquement l'égalité, comme dans le cas d'une contrainte
unique, qui n'est qu'une contrainte d'exclusion très spécialisée). Si le test
se révèle positif, la ligne est refusée.

Une contrainte peut porter sur plusieurs champs et un champ peut être impliqué
dans plusieurs contraintes :

```sql
CREATE TABLE commandes (
    no_commande     varchar(16) CHECK (no_commande ~ '^[A-Z0-9]*$'),
    id_entite_commerciale int REFERENCES entites_commerciales,
    id_client       int       REFERENCES clients,
    date_commande   date      NOT NULL,
    date_livraison  date      CHECK (date_livraison >= date_commande),
    PRIMARY KEY (no_commande, id_entite_commerciale)
);
```
```default
\d commandes
                                Table « public.commandes »
        Colonne        |         Type          | … | NULL-able | Par défaut 
-----------------------+-----------------------+---+-----------+------------
 no_commande           | character varying(16) |   | not null  | 
 id_entite_commerciale | integer               |   | not null  | 
 id_client             | integer               |   |           | 
 date_commande         | date                  |   | not null  | 
 date_livraison        | date                  |   |           | 
Index :
    "commandes_pkey" PRIMARY KEY, btree (no_commande, id_entite_commerciale)
Contraintes de vérification :
    "commandes_check" CHECK (date_livraison >= date_commande)
    "commandes_no_commande_check" CHECK (no_commande::text ~ '^[A-Z0-9]*$'::text)
Contraintes de clés étrangères :
    "commandes_id_client_fkey" FOREIGN KEY (id_client) REFERENCES clients(id_client)
    "commandes_id_entite_commerciale_fkey" FOREIGN KEY (id_entite_commerciale) REFERENCES entites_commerciales(id_entite_commerciale)
```

Les contraintes doivent être vues comme la dernière ligne de défense de votre
application face aux bugs.
En effet, le code d'une application change beaucoup plus souvent
que le schéma, et les données survivent souvent à l'application,
qui peut être réécrite entretemps.
Quoi qu'il se passe, des contraintes judicieuses garantissent
qu'il n'y aura pas d'incohérence logique dans la base.

Si elles sont gênantes pour le développeur
(car elles imposent un ordre d'insertion ou de mise à jour),
il faut se rappeler que les contraintes peuvent être « débrayées »
le temps d'une transaction :
```sql
BEGIN;
SET CONSTRAINTS ALL DEFERRED ;
…
COMMIT ;
```
<!-- exemple complet dans S2 -->
Les contraintes ne seront validées qu'au `COMMIT`.

Sur le sujet, voir par exemple
_[Constraints: a Developer's Secret Weapon](https://www.postgresql.eu/events/pgconfeu2018/sessions/session/2192-constraints-a-developers-secret-weapon/)_
de Will Leinweber (pgDay Paris 2018)
([slides](https://www.postgresql.eu/events/pgdayparis2018/sessions/session/1835/slides/70/2018-03-15%20constraints%20a%20developers%20secret%20weapon%20pgday%20paris.pdf),
[vidéo](https://youtu.be/hWh8QoV8z8k)).

Du point de vue des performances, les contraintes
permettent au planificateur d'optimiser
les requêtes. Par exemple, le planificateur sait ne pas prendre en compte
certaines jointures, notamment grâce à l'existence d'une contrainte
d'unicité. (Sur ce point,
la version 15 améliore les contraintes d'unicité en permettant de
choisir si la valeur NULL est considérée comme unique ou pas. Par défaut et
historiquement, une valeur NULL n'étant pas égal à une valeur NULL, les valeurs
NULL sont considérées distinctes, et donc on peut avoir plusieurs valeurs NULL
dans une colonne ayant une contrainte d'unicité.)

</div>

---

### Colonnes à valeur générée

<div class="slide-content">

  * Valeur calculée à l'insertion
  * `DEFAULT`
  * Identité
    + `GENERATED { ALWAYS | BY DEFAULT } AS IDENTITY`
  * Expression
    + `GENERATED ALWAYS AS ( generation_expr ) STORED`

</div>

<div class="notes">

Une colonne a par défaut la valeur `NULL` si aucune valeur n'est fournie lors de
l'insertion de la ligne.
Il existe néanmoins trois cas où le moteur peut substituer une autre valeur.

Le plus connu correspond à la clause `DEFAULT`. Dans ce cas, la valeur insérée
correspond à la valeur indiquée avec cette clause si aucune valeur n'est
indiquée pour la colonne. Si une valeur est précisée, cette valeur surcharge
la valeur par défaut. L'exemple suivant montre cela :

```sql
CREATE TABLE t2 (c1 integer, c2 integer, c3 integer DEFAULT 10);
INSERT INTO t2 (c1, c2, c3) VALUES (1, 2, 3);
INSERT INTO t2 (c1) VALUES (2);
SELECT * FROM t2;
```
```default
 c1 | c2 | c3
----+----+----
  1 |  2 |  3
  2 |    | 10
```

La clause `DEFAULT` ne peut pas être utilisée avec des clauses complexes,
notamment des clauses comprenant des requêtes.

<!-- FIXME : lors de la refonte des exemples de ce module,
reprendre l exemple de S2/T1 et mutualiser 
-->

Pour aller un peu plus loin, à partir de PostgreSQL 12, il est possible d'utiliser
`GENERATED ALWAYS AS ( expression ) STORED`.
Cela permet d'avoir une valeur calculée pour la
colonne, valeur qui ne peut pas être surchargée, ni à l'insertion, ni à la mise
à jour (mais qui est bien stockée sur le disque).

Comme exemple, nous allons reprendre la table `capitaines` et lui ajouter
une colonne ayant comme valeur la version modifiée du numéro de carte de crédit :

```sql
ALTER TABLE capitaines
  ADD COLUMN num_cc_anon text
  GENERATED ALWAYS AS (substring(num_cartecredit, 0, 10) || '******') STORED;

SELECT nom, num_cartecredit, num_cc_anon FROM capitaines;
```
```default
      nom       | num_cartecredit  |   num_cc_anon
----------------+------------------+-----------------
 Robert Surcouf | 1234567890123456 | 123456789******
 Haddock        |                  |
```
```sql
INSERT INTO capitaines VALUES
  (2, 'Joseph Pradere-Niquet', 40, '9876543210987654', '44000', 'Lundi', 'test');
```
```default
ERROR:  cannot insert into column "num_cc_anon"
DETAIL:  Column "num_cc_anon" is a generated column.
```
```sql
INSERT INTO capitaines VALUES
  (2, 'Joseph Pradere-Niquet', 40, '9876543210987654', '44000', 'Lundi');
```
```sql
SELECT nom, num_cartecredit, num_cc_anon FROM capitaines;
```
```default
          nom          | num_cartecredit  |   num_cc_anon
-----------------------+------------------+-----------------
 Robert Surcouf        | 1234567890123456 | 123456789******
 Haddock               |                  |
 Joseph Pradere-Niquet | 9876543210987654 | 987654321******
```

Enfin, `GENERATED { ALWAYS | BY DEFAULT } AS IDENTITY` permet d'obtenir une
colonne d'identité, bien meilleure que ce que le pseudo-type `serial`
propose. Si `ALWAYS` est indiqué, la valeur n'est pas modifiable.

```sql
ALTER TABLE capitaines
  ADD COLUMN id2 integer GENERATED ALWAYS AS IDENTITY;
```
```sql
SELECT nom, id2 FROM capitaines;
```
```default
          nom          | id2
-----------------------+-----
 Robert Surcouf        |   1
 Haddock               |   2
 Joseph Pradere-Niquet |   3
```
```sql
INSERT INTO capitaines (nom) VALUES ('Tom Souville');
```
```sql
SELECT nom, id2 FROM capitaines;
```
```default
          nom          | id2
-----------------------+-----
 Robert Surcouf        |   1
 Haddock               |   2
 Joseph Pradere-Niquet |   3
 Tom Souville          |   4
```

Le type `serial` est remplacé par le type `integer` et une séquence comme le
montre l'exemple suivant. C'est un problème dans la mesure ou la déclaration
qui est faite à la création de la table produit un résultat différent en base
et donc dans les exports de données.

```sql
CREATE TABLE tserial(s serial);
```
```console
                            Table "public.tserial"
 Column |  Type   | Collation | Nullable |              Default
--------+---------+-----------+----------+------------------------------------
 s      | integer |           | not null | nextval('tserial_s_seq'::regclass)
```

</div>

---

### Langages

<!-- Slide brève intro partagé avec X2 (extensions pour perf), qui creuse plus -->

<!--

## Langages de procédures stockés

Titre à la charge du module appelant
-->

<!-- Ceci est une très brève intro destinée à introduire le concept

Utilisation : A1 (présentation), X2 (extensions pour la perf)

-->

<div class="slide-content">

  * Procédures & fonctions en différents langages
  * Par défaut : SQL, C et PL/pgSQL
  * Extensions officielles : Perl, Python
  * Mais aussi Java, Ruby, Javascript…
  * Intérêts : fonctionnalités, performances

</div>

<div class="notes">

Les langages officiellement supportés par le projet sont :

  * PL/pgSQL ;
  * [PL/Perl](https://docs.postgresql.fr/current/plperl.html) ;
  * [PL/Python](https://docs.postgresql.fr/current/plpython.html) ;
  * PL/Tcl.

Voici une liste non exhaustive des langages procéduraux disponibles, à différents degrés de maturité :

  * [PL/sh](https://github.com/petere/plsh) ;
  * [PL/R](https://github.com/postgres-plr/plr) ;
  * [PL/Java](https://tada.github.io/pljava/) ;
  * PL/lolcode ;
  * PL/Scheme ;
  * PL/PHP ;
  * PL/Ruby ;
  * [PL/Lua](https://github.com/pllua/pllua) ;
  * PL/pgPSM ;
  * [PL/v8](https://github.com/plv8/plv8) (Javascript).

Le wiki PostgreSQL contient un
[tableau des langages supportés](https://wiki.postgresql.org/wiki/PL_Matrix).

Pour qu'un langage soit utilisable, il doit être activé au niveau de la base
où il sera utilisé. Les trois langages activés par défaut sont le C, le SQL et
le PL/pgSQL. Les autres doivent être ajoutés à partir des paquets
de la distribution ou du PGDG, ou compilés à la main, puis l'extension
installée dans la base :

```sql
CREATE EXTENSION plperl ;
CREATE EXTENSION plpython3u ;
-- etc.
```

Ces fonctions peuvent être utilisées dans des index fonctionnels et des triggers
comme toute fonction SQL ou PL/pgSQL.

Chaque langage a ses avantages et inconvénients. Par exemple, PL/pgSQL est
très simple à apprendre mais n'est pas performant quand il s'agit de traiter
des chaînes de caractères. Pour ce traitement, il est souvent préférable d'utiliser
PL/Perl, voire PL/Python. Évidemment, une routine en C aura les meilleures
performances mais sera beaucoup moins facile à coder et à maintenir, et ses
bugs seront susceptibles de provoquer un plantage du serveur.

Par ailleurs, les procédures peuvent s'appeler les unes les autres quel que soit
le langage.
S'ajoute l'intérêt de ne pas avoir à réécrire en PL/pgSQL des fonctions existantes
dans d'autres langages ou d'accéder à des modules bien établis de ces langages.

</div>

---

### Fonctions & procédures

<div class="slide-content">

  * Fonction
    + renvoie une ou plusieurs valeurs
    + `SETOF` ou `TABLE` pour plusieurs lignes
  * Procédure (v11+)
    + ne renvoie rien
    + peut gérer le transactionnel dans certains cas

</div>

<div class="notes">

Historiquement, PostgreSQL ne proposait que l'écriture de fonctions. Depuis la
version 11, il est aussi possible de créer des procédures. Le terme « routine »
est utilisé pour signifier procédure ou fonction.

Une fonction renvoie une donnée. Cette donnée peut comporter une ou plusieurs
colonnes. Elle peut aussi avoir plusieurs lignes dans le cas d'une fonction
`SETOF` ou `TABLE`.

Une procédure ne renvoie rien. Elle a cependant un gros avantage par rapport
aux fonctions dans le fait qu'elle peut gérer le transactionnel. Elle peut
valider ou annuler la transaction en cours. Dans ce cas, une nouvelle
transaction est ouverte immédiatement après la fin de la transaction
précédente.

</div>

---

### Opérateurs

<div class="slide-content">

  * Dépend d'un ou deux types de données
  * Utilise une fonction prédéfinie :
  ```sql
  CREATE OPERATOR //
    (FUNCTION=division0,
    LEFTARG=integer,
    RIGHTARG=integer);
   ```

</div>

<div class="notes">

Il est possible de créer de nouveaux opérateurs sur un type de base ou sur un
type utilisateur. Un opérateur exécute une fonction, soit à un argument pour
un opérateur unitaire, soit à deux arguments pour un opérateur binaire.

Voici un exemple d'opérateur acceptant une division par zéro sans erreur :

```sql
-- définissons une fonction de division en PL/pgSQL
CREATE FUNCTION division0 (p1 integer, p2 integer) RETURNS integer
LANGUAGE plpgsql
AS $$
BEGIN
  IF p2 = 0 THEN
      RETURN NULL;
  END IF;

  RETURN p1 / p2;
END
$$;

-- créons l'opérateur
CREATE OPERATOR // (FUNCTION = division0, LEFTARG = integer, RIGHTARG = integer);

-- une division normale se passe bien

SELECT 10/5;
```
```default
 ?column?
----------
        2
```
```sql
SELECT 10//5;
```
```default
 ?column?
----------
        2
```
```sql
-- une division par 0 ramène une erreur avec l'opérateur natif
SELECT 10/0;
```
```default
ERROR:  division by zero
```
```sql
-- une division par 0 renvoie NULL avec notre opérateur
SELECT 10//0;
```
```default
 ?column?
----------

(1 row)
```

</div>

---

### Triggers

<div class="slide-content">

  * Opérations : `INSERT`, `UPDATE`, `DELETE`, `TRUNCATE`
  * Trigger sur :
    + une colonne, et/ou avec condition
    + une vue
    + DDL
  * Tables de transition
  * Effet sur :
    + l'ensemble de la requête (`FOR STATEMENT`)
    + chaque ligne impactée (`FOR EACH ROW`)
  * N'importe quel langage supporté

</div>

<div class="notes">

Les triggers peuvent être exécutés avant (`BEFORE`) ou après (`AFTER`) une
opération.

Il est possible de les déclencher pour chaque ligne impactée (`FOR EACH ROW`)
ou une seule fois pour l'ensemble de la requête (`FOR STATEMENT`). Dans le
premier cas, il est possible d'accéder à la ligne impactée (ancienne et
nouvelle version). Dans le deuxième cas, il a fallu attendre la version 10
pour disposer des tables de transition qui donnent à l'utilisateur une vision
des lignes avant et après modification.

Par ailleurs, les triggers peuvent être écrits dans n'importe lequel des
langages de routine supportés par PostgreSQL (C, PL/pgSQL, PL/Perl, etc. )

Exemple :

```sql
ALTER TABLE capitaines ADD COLUMN salaire integer;

CREATE FUNCTION verif_salaire()
RETURNS trigger AS $verif_salaire$
BEGIN
  -- Nous verifions que les variables ne sont pas vides
  IF NEW.nom IS NULL THEN
    RAISE EXCEPTION 'Le nom ne doit pas être null.';
  END IF;

  IF NEW.salaire IS NULL THEN
    RAISE EXCEPTION 'Le salaire ne doit pas être null.';
  END IF;

  -- pas de baisse de salaires !
  IF NEW.salaire < OLD.salaire THEN
    RAISE EXCEPTION 'Pas de baisse de salaire !';
  END IF;

  RETURN NEW;
END;
$verif_salaire$ LANGUAGE plpgsql;

CREATE TRIGGER verif_salaire BEFORE INSERT OR UPDATE ON capitaines
  FOR EACH ROW EXECUTE PROCEDURE verif_salaire();

UPDATE capitaines SET salaire = 2000 WHERE nom = 'Robert Surcouf';
UPDATE capitaines SET salaire = 3000 WHERE nom = 'Robert Surcouf';
UPDATE capitaines SET salaire = 2000 WHERE nom = 'Robert Surcouf';
```
```default
ERROR:  pas de baisse de salaire !
CONTEXTE : PL/pgSQL function verif_salaire() line 13 at RAISE
```

</div>

---

### Questions

<div class="slide-content">

N'hésitez pas, c'est le moment !

</div>

<div class="notes">

</div>

---


\newpage

## Travaux pratiques

<div class="notes">

### Installation depuis les paquets binaires du PGDG (Rocky Linux)

<div class="notes">

<div class="slide-content">

**But** : Installer PostgreSQL à partir des paquets communautaires.

Cette instance servira aux TP suivants.

</div>

**Pré-installation**

> Quelle commande permet d'installer les paquets binaires de PostgreSQL ?

> Quelle version est packagée ?

> Quels paquets devront également être installés ?

**Installation**

> Installer le dépôt.

> Désactiver le module d'installation pour la version PostgreSQL de la distribution.

> Installer les paquets de PostgreSQL16 : serveur, client, contribs.

> Quel est le chemin des binaires ?

**Création de la première instance**

> Créer une première instance avec les outils de la famille Red Hat
> en activant les sommes de contrôle (_checksums_).

> Vérifier ce qui a été fait dans le journal `initdb.log`.

**Démarrage**

> Démarrer l'instance.

> Activer le démarrage de l'instance au démarrage de la machine.

> Où sont les fichiers de données (`PGDATA`), et les traces de l'instance ?

**Configuration**

> Vérifier la configuration par défaut de PostgreSQL. Est-ce que le serveur
> écoute sur le réseau ?

> Quel est l'utilisateur sous lequel tourne l'instance ?

**Connexion**

> En tant que **root**, tenter une connexion avec `psql`.

> En tant que **postgres**, tenter une connexion avec `psql`. Quitter.

> À quelle base se connecte-t-on par défaut ?

> Créer une première base de données et y créer des tables.

</div>
</div>

\newpage

## Travaux pratiques (solutions)

<div class="notes">

### Installation depuis les paquets binaires du PGDG (Rocky Linux)

**Pré-installation**

> Quelle commande permet d'installer les paquets binaires de PostgreSQL ?

Le présent TP utilise Rocky Linux en version 8 ou 9.
C'est une distribution communautaire qui se
veut succéder au projet CentOS, [clone de Red Hat, interrompu en 2021][ins-00].

(Une version
plus complète, ainsi que l'utilisation de paquets Debian, sont traités dans
l'annexe « Installation de PostgreSQL depuis les paquets communautaires ».)

[ins-00]: https://blog.centos.org/2020/12/future-is-centos-stream

> Quelle version est packagée ?

La dernière version stable de PostgreSQL disponible au moment de la rédaction
de ces lignes est la 17.0. Par contre, la dernière version disponible dans
les dépôts dépend de votre distribution. C'est la raison pour laquelle **les
dépôts du PGDG sont à privilégier**.

> Quels paquets devront également être installés ?

Le paquet `libpq` doit également être installé. Il est
aussi nécessaire d'installer les paquets `llvmjit` (pour la compilation à la volée),
qui réclame elle-même la présence du dépôt EPEL, mais c'est une fonctionnalité
optionnelle qui ne sera pas traitée ici.

**Installation**

> Installer le dépôt en vous inspirant des consignes sur :  
> <https://www.postgresql.org/download/linux/redhat>  
> mais en ajoutant les contribs et les sommes de contrôle.

Préciser :

  * PostgreSQL 17
  * Red Hat Enterprise, Rocky or Oracle version 8 (ou 9 selon le cas)
  * x86_64

Nous allons reprendre ligne à ligne ce script et le compléter.

Se connecter avec l'utilisateur **root** sur la machine de formation, et recopier le
script proposé par le guide. Dans la commande ci-dessous, les deux lignes **doivent
être copiées et collées ensemble**.

```bash
# Rocky Linux 8
dnf install -y https://download.postgresql.org\
/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
```
```bash
# Rocky Linux 9
dnf install -y https://download.postgresql.org\
/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
```

> Désactiver le module d'installation pour la version PostgreSQL de la distribution.

Cette opération est nécessaire pour Rocky Linux 8 ou 9.
```bash
dnf -qy module disable postgresql
```

> Installer les paquets de PostgreSQL17 : serveur, client, contribs.

```bash
dnf install -y postgresql17-server postgresql17-contrib
# optionnel
dnf install -y postgresql17-llvmjit
```

Il s'agit respectivement 

* des binaires du serveur ;
* des « contribs » et extensions optionnelles (mais chaudement conseillées) ;
* et du paquet nécessaire à la compilation à la volée (JIT).

Le paquet `postgresql17` (outils client) fait partie des dépendances
et est installé automatiquement.

> Quel est le chemin des binaires ?

Ils se trouvent dans `/usr/pgsql-17/bin/` (chemin propre à ces paquets) :

```bash
ls -1 /usr/pgsql-17/bin/
```
```default
clusterdb
createdb
;
postgres
postgresql-17-check-db-dir
postgresql-17-setup
psql
reindexdb
vacuumdb
vacuumlo
```

Noter qu'il existe des liens dans `/usr/bin` pointant vers la
version la plus récente des outils en cas d'installation de plusieurs
versions :

<!-- /usr/bin/psql pour user, /bin/psql pour root, mais /bin pointe vers /usr/bin
-->

```bash
which psql
```
```default
/usr/bin/psql
```
```bash
file /usr/bin/psql
```
```default
/usr/bin/psql: symbolic link to /etc/alternatives/pgsql-psql
```
```bash
file /etc/alternatives/pgsql-psql
```
```default
/etc/alternatives/pgsql-psql: symbolic link to /usr/pgsql-17/bin/psql
```

**Création de la première instance**

> Créer une première instance avec les outils de la famille Red Hat
> en activant les sommes de contrôle (_checksums_).

La création d'une instance passe par un outil spécifique à ces paquets.
<!-- au nom différent dans les AppStreams -->
<div class="box tip">
Cet outil doit être appelé en tant que **root** (et non **postgres**).
</div>
Optionnellement, on peut ajouter des paramètres d'initialisation à cette étape.
La mise en place des sommes de contrôle est généralement conseillée pour être
averti de toute corruption des fichiers.

Toujours en temps que **root** :  <!-- important à cause du export dans même session -->

```bash
export PGSETUP_INITDB_OPTIONS="--data-checksums"
/usr/pgsql-17/bin/postgresql-17-setup initdb
```
```default
Initializing database ... OK
```

L'`export` est nécessaire pour activer les sommes de contrôle.
<!-- s'il y a un moyen plus propre, le dire
-->

> Vérifier ce qui a été fait dans le journal `initdb.log`.

La sortie de la commande précédente est redirigée vers le fichier
`initdb.log` situé dans le répertoire qui contient celui de la base (`PGDATA`).
Il est possible d'y vérifier l'ensemble des étapes réalisées,
notamment l'activation des sommes de contrôle.

```default
$ cat /var/lib/pgsql/17/initdb.log
```
```default
The files belonging to this database system will be owned by user "postgres".
This user must also own the server process.

The database cluster will be initialized with locale "en_US.UTF-8".
The default database encoding has accordingly been set to "UTF8".
The default text search configuration will be set to "english".

Data page checksums are enabled.

fixing permissions on existing directory /var/lib/pgsql/17/data ... ok
creating subdirectories ... ok
selecting dynamic shared memory implementation ... posix
selecting default "max_connections" ... 100
selecting default "shared_buffers" ... 128MB
selecting default time zone ... UTC
creating configuration files ... ok
running bootstrap script ... ok
performing post-bootstrap initialization ... ok
syncing data to disk ... ok

Success. You can now start the database server using:

    /usr/pgsql-17/bin/pg_ctl -D /var/lib/pgsql/17/data/ -l logfile start
```

Ne pas tenir compte de la dernière ligne, qui est une suggestion qui
ne tient pas compte des outils prévus pour cet OS.

**Démarrage**

> Démarrer l'instance.

<div class="box important">
Attention, si vous avez créé une instance, par exemple à partir des sources,
elle doit impérativement être arrêtée pour pouvoir démarrer la
nouvelle instance !
Elles ne peuvent pas être démarrées en même temps, sauf à modifier le
port dans la configuration de l'une d'entre elles.
</div>

En tant que **root** :

```bash
systemctl start postgresql-17
```

Si aucune erreur ne s'affiche, tout va bien à priori.

Pour connaître l'état de l'instance :

```bash
systemctl status postgresql-17
```
```default
● postgresql-17.service - PostgreSQL 17 database server
     Loaded: loaded (/usr/lib/systemd/system/postgresql-17.service; disabled; preset: disabled)
     Active: active (running) since Tue 2024-10-15 18:44:08 UTC; 6s ago
       Docs: https://www.postgresql.org/docs/17/static/
    Process: 72901 ExecStartPre=/usr/pgsql-17/bin/postgresql-17-check-db-dir ${PGDATA} (code=exited, status=0/SUCCESS)
   Main PID: 72906 (postgres)
      Tasks: 7 (limit: 2676)
     Memory: 17.7M
        CPU: 37ms
     CGroup: /system.slice/postgresql-17.service
             ├─72906 /usr/pgsql-17/bin/postgres -D /var/lib/pgsql/17/data/
             ├─72907 "postgres: logger "
             ├─72908 "postgres: checkpointer "
             ├─72909 "postgres: background writer "
             ├─72911 "postgres: walwriter "
             ├─72912 "postgres: autovacuum launcher "
             └─72913 "postgres: logical replication launcher "

Oct 15 18:44:08 rocky9 systemd[1]: Starting PostgreSQL 17 database server...
Oct 15 18:44:08 rocky9 postgres[72906]: 2024-10-15 18:44:08.144 UTC [72906] LOG:  redirecting log output to logging collecto>
Oct 15 18:44:08 rocky9 postgres[72906]: 2024-10-15 18:44:08.144 UTC [72906] HINT:  Future log output will appear in director>
Oct 15 18:44:08 rocky9 systemd[1]: Started PostgreSQL 17 database server.
```

> Activer le démarrage de l'instance au démarrage de la machine.

Le packaging Red Hat ne prévoie pas l'activation du service au boot, il faut le
demander explicitement :

```bash
systemctl enable postgresql-17
```
```default
Created symlink /etc/systemd/system/multi-user.target.wants/postgresql-17.service → /usr/lib/systemd/system/postgresql-17.service.
```

> Où sont les fichiers de données (`PGDATA`), et les traces de l'instance ?

Les données et fichiers de configuration sont dans `/var/lib/pgsql/17/data/`.

```bash
ls -1 /var/lib/pgsql/17/data/
```
```default
base
current_logfiles
global
log
pg_commit_ts
pg_dynshmem
pg_hba.conf
pg_ident.conf
pg_logical
pg_multixact
pg_notify
pg_replslot
pg_serial
pg_snapshots
pg_stat
pg_stat_tmp
pg_subtrans
pg_tblspc
pg_twophase
PG_VERSION
pg_wal
pg_xact
postgresql.auto.conf
postgresql.conf
postmaster.opts
postmaster.pid
```

Les traces sont par défaut dans le sous-répertoire `log/` du PGDATA.

```bash
ls -l /var/lib/pgsql/17/data/log/
```
```default
total 4
-rw-------. 1 postgres postgres 1092 Oct 15 18:49 postgresql-Tue.log
```

NB : Dans les paquets RPM, le nom exact du fichier dépend du jour de la semaine.

**Configuration**

> Vérifier la configuration par défaut de PostgreSQL. Est-ce que le serveur
> écoute sur le réseau ?

Il est possible de vérifier dans le fichier `postgresql.conf` que par défaut,
le serveur écoute uniquement l'interface réseau `localhost`
(la valeur est commentée car c'est celle par défaut) :

```bash
grep listen_addresses /var/lib/pgsql/17/data/postgresql.conf
```
```ini
#listen_addresses = 'localhost'         # what IP address(es) to listen on;
```

Il faudra donc modifier ainsi et rédémarrer pour que des utilisateurs puissent se connecter
depuis d'autres machines :
```ini
listen_addresses = '*'         # what IP address(es) to listen on;
```
```bash
systemctl restart postgresql-17
```

Il est aussi possible de vérifier au niveau système en utilisant la commande
`netstat` (qui nécessite l'installation du paquet `net-tools`) :

```bash
netstat -anp|grep -E '(Active|Proto|postgres)'
```
```default
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address   Foreign Address State   PID/Program name    
tcp        0      0 0.0.0.0:5432    0.0.0.0:*       LISTEN  73049/postgres      
tcp6       0      0 :::5432         :::*            LISTEN  73049/postgres      
Active UNIX domain sockets (servers and established)
Proto RefCnt Flags    Type    State      I-Node PID/Program name     Path
unix  2      [ ACC ]  STREAM  LISTENING  87790  73049/postgres       /run/postgresql/.s.PGSQL.5432
unix  2      [ ACC ]  STREAM  LISTENING  87791  73049/postgres       /tmp/.s.PGSQL.5432
unix  3      [ ]      STREAM  CONNECTED  88816  73050/postgres: log  
…
```
(La présence de lignes `tcp6` dépend de la configuration de la machine.)

On notera que la _socket_ écoute à deux endroits, dans `/run/postgresql/`
et dans `/tmp`. <!-- paramètre unix_socket_directories -->
C'est un paramétrage par défaut lié aux paquets RPM.

> Quel est l'utilisateur sous lequel tourne l'instance ?

C'est l'utilisateur nommé **postgres** :

```bash
ps -U postgres -f -o pid,user,cmd
```
```default
    PID USER     CMD
    PID USER     CMD
  73049 postgres /usr/pgsql-17/bin/postgres -D /var/lib/pgsql/17/data/
  73050 postgres  \_ postgres: logger 
  73051 postgres  \_ postgres: checkpointer 
  73052 postgres  \_ postgres: background writer 
  73054 postgres  \_ postgres: walwriter 
  73055 postgres  \_ postgres: autovacuum launcher 
  73056 postgres  \_ postgres: logical replication launcher 
```

Il possède aussi le PGDATA :

```bash
ls -l /var/lib/pgsql/17/
```
```default
total 8
drwx------.  2 postgres postgres    6 Sep 26 20:10 backups
drwx------. 20 postgres postgres 4096 Oct 15 19:29 data
-rw-------.  1 postgres postgres  914 Oct 15 18:41 initdb.log
```

**postgres** est le nom traditionnel sur la plupart des distributions, mais il
n'est pas obligatoire (par exemple, le TP par compilation utilise un autre
utilisateur).

**Connexion**

> En tant que **root**, tenter une connexion avec `psql`.

```bash
 # psql
```
```default
psql: error: connection to server on socket "/run/postgresql/.s.PGSQL.5432" failed: FATAL:  role "root" does not exist
```

Cela échoue car `psql` tente de se connecter avec l'utilisateur système en cours,
soit **root**. Ça ne marchera pas mieux cependant en essayant de se connecter
avec l'utilisateur **postgres** :

```bash
psql -U postgres
```
```default
psql: error: connection to server on socket "/run/postgresql/.s.PGSQL.5432" failed: FATAL:  Peer authentication failed for user "postgres"
```

En effet, le `pg_hba.conf` est configuré de telle manière que l'utilisateur
de PostgreSQL et celui du système doivent porter le même nom (connexion `peer` ici).

> En tant que **postgres**, tenter une connexion avec `psql`.
> Quitter.

```bash
sudo -iu postgres psql
```
```default
psql (17.0)
Type "help" for help.

postgres=# exit
```

La connexion fonctionne donc indirectement depuis tout utilisateur pouvant
effectuer un `sudo`.

> À quelle base se connecte-t-on par défaut ?

```bash
sudo -iu postgres psql
```
```default
psql (17.0)
Type "help" for help.

postgres=# \conninfo 
You are connected to database "postgres" as user "postgres" via socket in "/run/postgresql" at port "5432".
postgres=# 
```

Là encore, la présence d'une base nommée `postgres` est une tradition et non une
obligation.

**Première base**

> Créer une première base de données et y créer des tables.

```bash
sudo -iu postgres psql
```
```sql
postgres=# CREATE DATABASE test ;
CREATE DATABASE
```

Alternativement :
```bash
sudo -iu postgres createdb test
```

Se connecter explicitement à la bonne base :

```bash
sudo -iu postgres psql -d test
```
```sql
test=# CREATE TABLE mapremieretable (x int);
CREATE TABLE

test=# \d+
```
```default
                           Liste des relations
 Schéma |       Nom       | Type  | Propriétaire | Taille  | Description
--------+-----------------+-------+--------------+---------+-------------
 public | mapremieretable | table | postgres     | 0 bytes |
```
</div>
