---
revision: 24.12
date: "Année académique 2024-2025"

licence : "Creative Commons BY-NC-SA"
include_licence : CC-BY-NC-SA-2.0-FR

author: © 2005-2025 DALIBO SARL SCOP

trademarks: |
  Le logo éléphant de PostgreSQL (« Slonik ») est une création sous copyright et
  le nom « PostgreSQL » est une marque déposée par PostgreSQL Community Association
  of Canada.

##
## PDF Options
##

## Limiter la profondeur de la table des matières
toc-depth: 2

## Mettre les lien http en pieds de page
links-as-notes: false

## Police plus petite dans un bloc de code

code-blocks-fontsize: small

## Filtre : pandoc-latex-admonition
## les catégories `important` et `warning` sont synonymes
## même chose pour `tip` et `note`
pandoc-latex-admonition:
  - color: Red
    classes: [warning]
    linewidth: 4
  - color: Red
    classes: [important]
    linewidth: 4
  - color: DarkSeaGreen
    classes: [tip]
    linewidth: 4
  - color: DarkSeaGreen
    classes: [note]
    linewidth: 4
  - color: DodgerBlue
    classes: [slide-content]
    linewidth: 4

##
## Reveal Options
##

## Taille affichage
width: 960
height: 700

## beige/blood/moon/simple/solarized/black/league/night/serif/sky/white
theme: white

## None - Fade - Slide - Convex - Concave - Zoom
transition: None
transitionSpeed: fast

## Barre de progression
progress: true

## Affiche N° de slide
slideNumber: true

## Le numero de slide apparait dans la barre d'adresse
history: true

## Defilement des slides avec la roulette
mouseWheel: true

## Annule la transformation uppercase de certains thèmes
title-transform : none

## Cache l'auteur sur la première slide
## Mettre en commentaire pour désactiver
hide_author_in_slide: true

title : 'Réplication physique'
subtitle : 'FCU Calais - M2 I2L'
---

# Réplication physique : fondamentaux

![PostgreSQL](medias/logos/slonik.png){ height=500 }
\

<div class="notes">

</div>

---

## Introduction

<div class="slide-content">

  * Principes
  * Mise en place
  * Administration

</div>

<div class="notes">

<!-- historique inutile ici, fait plus loin -->
PostgreSQL dispose d'une mécanique de
réplication en flux très complète. Ce module permet de comprendre les
principes derrière ce type de réplication, sa mise en place et son
administration.

</div>

---

### Objectifs

<div class="slide-content">

  * Connaître les avantages et limites de la réplication physique
  * Savoir la mettre en place
  * Savoir administrer et superviser une solution de réplication physique
</div>

<div class="notes">

</div>

---

## Concepts / principes

---

### Principe de la journalisation

<div class="slide-content">

  * Les journaux de transactions contiennent toutes les modifications
    + utilisation du contenu des journaux
  * Le serveur secondaire doit posséder une image des fichiers à un instant t
  * La réplication modifiera les fichiers
    + d'après le contenu des journaux suivants
</div>

<div class="notes">

Chaque transaction, implicite ou explicite, réalisant des modifications sur la
structure ou les données d'une base est tracée dans les journaux de
transactions. Ces derniers contiennent des informations d'assez bas niveau,
comme les blocs modifiés sur un fichier suite, par exemple, à un `UPDATE`. La
requête elle-même n'apparaît jamais. Les journaux de transactions sont
valables pour toutes les bases de données de l'instance.

Les journaux de transactions sont déjà utilisés en cas de crash du serveur.
Lors du redémarrage, PostgreSQL rejoue les transactions qui n'auraient pas
été synchronisées sur les fichiers de données.

Comme toutes les modifications sont disponibles dans les journaux de
transactions et que PostgreSQL sait rejouer les transactions à partir des
journaux, il suffit d'archiver les journaux sur une certaine période de temps
pour pouvoir les rejouer.

</div>

---

### Principales évolutions de la réplication physique

<div class="slide-content">

  * 8.2 : Réplication par journaux (_log shipping_), _Warm Standby_
  * 9.0 : Réplication en _streaming_, _Hot Standby_
  * 9.1 à 9.3 : Réplication synchrone, cascade, `pg_basebackup`
  * 9.4 : Slots de réplication, délai de réplication, décodage logique
  * 9.5 : `pg_rewind`, archivage depuis un serveur secondaire
  * 9.6 : Rejeu synchrone
  * 10 : Réplication synchrone sur base d'un quorum, slots temporaires
  * 12 : Déplacement de la configuration du `recovery.conf` vers le `postgresql.conf`
  * 13 : Sécurisation des slots
  * 15 : rejeu accéléré
  <!-- 14 et 15 rien qui vaille un point de slide -->

</div>

<div class="notes">

<!-- Cet historique vise à expliquer la multiplicité des différents outils et technos encore présents
 Ne pas détailler sur les versions non supportées
 C'est aussi un peu un rappel de W1
-->

La mise en place de la réplication a été très progressive au fil des versions.
Elle pouvait être simpliste au départ, mais elle est à présent au point,
et beaucoup plus complète.
L'historique permet d'expliquer certaines particularités et complexités.

La version 8.0, en 2005, contenait déjà tout le code qui permet aujourd'hui, après un crash du
serveur, de relire les journaux pour rendre à nouveau cohérents les fichiers de
données. Pour répliquer une instance, il a suffit d'automatiser l'envoi des journaux
vers un serveur secondaire, qui passe son temps à les rejouer, journal après journal (_log shipping_),
pour obtenir un serveur prêt à prendre le relai du primaire (_Warm Standby_).

Le serveur secondaire a ensuite été rendu utilisable pour des requêtes en lecture seule (_Hot Standby_).

La réplication a été ensuite améliorée : elle peut se faire en continu (_streaming replication_)
et non plus journal par journal, pour réduire le retard du secondaire.
Elle peut être synchrone, avec différents niveaux d'arbitrage entre performance et sécurité,
et même s'effectuer en cascade, ou avec un délai, et cela en _log shipping_ comme en _streaming_.

Puis, l'ajout des slots de réplication a permis au serveur primaire de connaître la position
de ses serveurs secondaires, pour savoir quels journaux de transactions sont encore nécessaires.

En parallèle, différents éléments ont été apportés, permettant l'apparition de la réplication
logique (qui n'a pas grand-chose à voir avec la réplication physique) en version 10.

L'outil `pg_rewind` a été ajouté pour faciliter la
reconstruction d'un ancien serveur primaire devenu secondaire.
Il est plus flexible depuis PostgreSQL 13
et peut utiliser un secondaire comme référence depuis la version 14.

La version 10 ajoute la possibilité d'appliquer arbitrairement une réplication synchrone
à un sous-ensemble d'un groupe d'instances (_quorum), et non plus juste
par ordre de priorité, avec ce paramétrage :
```ini
synchronous_standby_names = [FIRST]|[ANY] num_sync (node1, node2,...)
```

À partir de PostgreSQL 10, les slots de réplication peuvent être temporaires,
et ne durer que le temps de la connexion qui l'a créé.

La version 12 ne change rien sur le fond, mais opère une modification technique lourde :
le fichier de paramétrage traditionnel sur le secondaire,
`recovery.conf`, disparaît, et ses paramètres sont déplacés
dans `postgresql.conf` (ou `postgresql.auto.conf`),
ce qui facilite la centralisation de la configuration,
et évite d'avoir à redémarrer systématiquement après modification des paramètres concernés.
Nous ne parlerons pas ici du paramétrage d'avant la version 12.

La version 13 supprime
le plus gros inconvénient des slots de réplication en posant un maximum
à la volumétrie qu'ils peuvent conserver (`max_slot_wal_keep_size`).

La version 15 accélère le rejeu du _log shipping_.

Parallèlement à tout cela, les différents outils externes ont également beaucoup progressé,
notamment `pg_basebackup`. Par exemple, celui-ci permet une sauvegarde incrémentale
à partir de PostgreSQL 17.

</div>

---

### Avantages

<div class="slide-content">

  * Système de rejeu éprouvé
  * Mise en place simple
  * Pas d'arrêt ou de blocage des utilisateurs
  * Réplique tout

</div>

<div class="notes">

Le gros avantage de la réplication par enregistrements de journaux de
transactions est sa fiabilité : le système de rejeu qui a permis sa création
est un système éprouvé. La mise en place du système
complet est simple car son fonctionnement est facile à comprendre.
Elle n'implique pas d'arrêt du système, ni de blocage des utilisateurs.

L'autre gros avantage est qu'il réplique tout : modification des données comme
évolutions de la structure de la base (DDL), séquences, _large objects_,
fonctions…
C'est une fonctionnalité que tous les systèmes de
réplication logique (notamment par trigger) aimeraient avoir.

</div>

---

### Inconvénients

<div class="slide-content">

  * Réplication de l'instance complète
  * Serveur secondaire uniquement en lecture
  * Impossible de changer d'architecture
  * Même version majeure de PostgreSQL pour tous les serveurs
</div>

<div class="notes">

De manière assez étonnante, l'avantage de tout répliquer est aussi un
inconvénient : avec la réplication interne physique de PostgreSQL, il n'est pas possible de ne
répliquer qu'une seule base ou que quelques tables.

De même, il n'est pas possible de créer des objets supplémentaires sur
le serveur secondaire, comme des index ou des tables de travail, ce qui serait pourtant bien
pratique pour de la création de rapports ou pour stocker des résultats
intermédiaires de calculs statistiques. Le serveur secondaire est vraiment
réservé aux opérations de lecture seule (sauvegardes, répartition de la charge en lecture…)
Ces limites ont motivé le développement de la réplication logique pour certains cas d'usage
qui ne relèvent pas de la haute disponibilité.

La réplication se passe au niveau du contenu des fichiers et des journaux
de transactions. En conséquence, il n'est pas possible d'avoir deux nœuds
du système de réplication avec une architecture différente. Par exemple, ils
doivent être tous les deux 32 bits ou 64 bits, mais pas un mélange. De même,
les deux nœuds doivent être _big endian_ ou _little endian_, et doivent aussi être à la
même version majeure (pas forcément mineure, ce qui facilite les mises à jours mineures).
Pour éviter tout problème de librairie, il est même conseillé d'utiliser des systèmes
les plus proches possibles (même distribution de même niveau de mise à jour).

</div>

<!--
ATTENTION : Ceci est une partie commune à W2A (DBA3) comme R56 (HAPAT)
-->


---

## Réplication par streaming

---

### Mise en place de la réplication par streaming

<div class="slide-content">

  * Réplication en flux
  * Un processus du serveur primaire discute avec un processus du serveur
secondaire
    + d'où un _lag_ moins important
  * Asynchrone ou synchrone
  * En cascade

</div>

<div class="notes">

Le serveur PostgreSQL secondaire lance un processus appelé `walreceiver`, dont le but est
de se connecter au serveur primaire et d'attendre les modifications de la
réplication.

Le `walreceiver` a donc besoin de se connecter sur le serveur PostgreSQL primaire.
Ce dernier doit être configuré pour accepter cette connexion.
Quand elle est acceptée par le serveur primaire, le serveur
PostgreSQL du serveur primaire lance un nouveau processus, appelé `walsender`.
Ce dernier a pour but d'envoyer les données de réplication au serveur secondaire.
Les données de réplication sont envoyées suivant l'activité et certains
paramètres de configuration.

Cette méthode permet une réplication plus proche du serveur
primaire que le _log shipping_. On peut même configurer un mode synchrone :
un client du serveur primaire ne récupère pas la main tant que ses modifications ne sont
pas enregistrées sur le serveur primaire **et** sur le serveur secondaire
synchrone. Cela s'effectue à la validation de la transaction, implicite
ou lors d'un `COMMIT`.

Enfin, la réplication en cascade permet à un secondaire de fournir les
informations de réplication à un autre secondaire, déchargeant ainsi le serveur
primaire d'un certain travail et diminuant aussi la bande passante réseau
utilisée par le serveur primaire.

</div>

---

### Serveur primaire (1/2) - Configuration

<div class="slide-content">

Dans `postgresql.conf` :

  * `wal_level = replica`  (ou `logical`)
  * `max_wal_senders = X`
    + 1 par client par _streaming_
    + défaut : 10
  * `wal_sender_timeout = 60s`

</div>

<div class="notes">

Il faut tout d'abord s'assurer que PostgreSQL enregistre suffisamment
d'informations pour que le serveur secondaire puisse rejouer toutes les modifications
survenant sur le serveur primaire. Dans certains cas, PostgreSQL peut
économiser l'écriture de journaux quand cela ne
pose pas de problème pour l'intégrité des données en cas de crash. Par
exemple, sur une instance sans archivage ni réplication,
il est inutile de tracer la totalité d'une transaction
qui commence par créer une table, puis qui la remplit.
En cas de crash pendant l'opération, l'opération complète est annulée,
la table n'existera plus : PostgreSQL peut donc écrire directement
son contenu sur le disque sans journaliser.

Cependant, pour restaurer cette table ou la répliquer, il est
nécessaire d'avoir les étapes intermédiaires (le contenu de la table)
et il faut donc écrire ces informations supplémentaires dans les journaux.

Le paramètre `wal_level` fixe le comportement à adopter. Comme son nom
l'indique, il permet de préciser le niveau d'informations que l'on souhaite avoir
dans les journaux. Il connaît trois valeurs :

  * Le niveau `replica` est adapté à l'archivage ou la réplication,
en plus de la sécurisation contre les arrêts brutaux. C'est le niveau par défaut.
L'optimisation évoquée plus haut n'est pas possible.
  * Le niveau `minimal` n'offre que la protection contre les arrêts brutaux,
mais ne permet ni réplication ni sauvegarde PITR.
Ce niveau ne sert plus guère qu'aux environnements ni archivés,
ni répliqués, pour réduire la quantité de journaux générés,
comme dans l'optimisation ci-dessus.
  * Le niveau `logical` est le plus complet et doit être activé pour l'utilisation
du décodage logique, notamment pour utiliser la réplication logique.
Il n'est pas nécessaire pour la sauvegarde PITR ou la réplication physique,
ni incompatible.

Le serveur primaire accepte un nombre maximum de connexions de
réplication : il s'agit du paramètre `max_wal_senders`.
Il faut compter au moins une connexion pour chaque serveur secondaire
susceptible de se connecter,
ou les outils utilisant le _streaming_ comme `pg_basebackup`
ou `pg_receivewal`. Il est conseillé de prévoir « large » d'entrée :
l'impact mémoire est négligeable, et cela évite d'avoir à redémarrer
l'instance primaire à chaque modification.
La valeur par défaut de 10 devrait suffire dans la plupart des cas.

Le paramètre `wal_sender_timeout` permet de couper toute connexion inactive
après le délai indiqué par ce paramètre. Par défaut, le délai est d'une
minute. Cela permet au serveur primaire de ne pas conserver une connexion
coupée ou dont le client a disparu pour une raison ou une autre.
Le secondaire retentera par la suite une connexion complète.

</div>

---

### Serveur primaire (2/2) - Authentification

<div class="slide-content">

  * Le serveur secondaire doit pouvoir se connecter au serveur primaire
  * Pseudo-base `replication`
  * Utilisateur dédié conseillé avec attributs `LOGIN` et `REPLICATION`
  * Configurer `pg_hba.conf` :
<!-- ini pour coloration -->
```ini
host replication user_repli 10.2.3.4/32   scram-sha-256
```
  * Recharger la configuration

</div>

<div class="notes">

Il est nécessaire après cela de configurer le fichier `pg_hba.conf`. Dans ce fichier, une
ligne (par secondaire) doit indiquer les connexions de réplication. L'idée est
d'éviter que tout le monde puisse se connecter pour répliquer l'intégralité
des données.

Pour distinguer une ligne de connexion standard et une ligne de connexion de
réplication, la colonne indiquant la base de données doit contenir le mot
« replication ». Par exemple :

```ini
host   replication   user_repli   10.0.0.2/32   scram-sha-256
```

Dans ce cas, l'utilisateur `user_repli` pourra
entamer une connexion de réplication vers le serveur primaire à condition que
la demande de connexion provienne de l'adresse IP `10.0.0.2` et que cette demande
de connexion précise le bon mot de passe au format `scram-sha-256`.

Un utilisateur dédié à la réplication est conseillé pour des raisons de sécurité.
On le créera avec les droits suivants :
```sql
CREATE ROLE user_repli LOGIN REPLICATION ;
```
et bien sûr un mot de passe complexe.

Les connexions locales de réplication sont autorisées par défaut sans mot de passe.

Après modification du fichier `postgresql.conf` et du fichier `pg_hba.conf`, il est temps de demander à
PostgreSQL de recharger sa configuration. L'action `reload` suffit dans tous les cas,
sauf celui où `max_wal_senders` est modifié (auquel cas il faudra redémarrer PostgreSQL).

</div>

---

### Serveur secondaire (1/4) - Copie des données

<div class="slide-content">

Copie des données du serveur primaire (à chaud !) :

  * Copie généralement à chaud donc incohérente !
  * Le plus simple : `pg_basebackup`
    + simple mais a des limites
  * Idéal : outil PITR
  * Possible : `rsync`, `cp`…
    + ne pas oublier `pg_backup_start()`/`pg_backup_stop()` !
    + exclure certains répertoires et fichiers
    + garantir la disponibilité des journaux de transaction

</div>

<div class="notes">

La première action à réaliser ressemble beaucoup à ce que propose la
sauvegarde en ligne des fichiers. Il s'agit de copier le répertoire des
données de PostgreSQL ainsi que les tablespaces associés.

<div class="box warning">
Rappelons que
généralement cette copie aura lieu à chaud, donc une simple copie
directe sera incohérente.
</div>

**pg_basebackup** :

L'outil le plus simple est `pg_basebackup`. Ses avantages sont sa disponibilité et sa
facilité d'utilisation. Il sait ce qu'il n'y a pas besoin de copier et peut inclure
les journaux nécessaires pour ne pas avoir à paramétrer l'archivage.
<!-- NE PAS DETAILLER : ça a été fait dans I4 (outils PITR) ou sera fait dans W3 (outils répli) -->

Il peut utiliser la connexion de réplication déjà prévue pour le secondaire,
poser des slots temporaires ou le slot définitif.

Pour faciliter la mise en place d'un secondaire,
il peut générer les fichiers de configuration à partir des paramètres qui lui ont été fournis
(option `--write-recovery-conf`).

Malgré beaucoup d'améliorations dans les dernières versions,
la limite principale de `pg_basebackup` reste d'exiger un répertoire cible vide : on
doit toujours recopier l'intégralité de la base copiée. Cela peut être
pénible lors de tests répétés avec une grosse base, ou avec une liaison instable.
Toutefois, à partir de PostgreSQL 17, il permet une sauvegarde incrémentale.
<!-- FIXME  à détailler ici ou ailleurs ! -->

**Outils PITR** :

L'idéal est un outil de
restauration PITR permettant la restauration en mode delta, par exemple
pgBackRest avec l'option `--delta`. Ne sont restaurés que les fichiers ayant changé, et
le primaire n'est pas chargé par la copie.

<!-- exemple dans I4 -->

**rsync** :

<!-- Détaillé dans W3, donc ne pas détailler ici -->

Un script de copie reste une option possible.
Il est possible de le faire manuellement, tout comme pour une sauvegarde PITR.
<div class="box warning">
Une copie manuelle implique que les journaux sont archivés par ailleurs.
</div>
Rappelons les trois étapes essentielles :

  * le `pg_backup_start()` ;
  * la copie des fichiers : généralement avec `rsync --whole-file`, ou tout moyen permettant une copie fiable et rapide ;
  * le `pg_backup_stop()`.

<!-- maj en v17 , garder synchro avec I2 -->
On exclura les fichiers inutiles lors de la copie qui pourraient
gêner un redémarrage, notamment les fichiers
`postmaster.pid`, `postmaster.opts`, `pg_internal.init`, les
répertoires `pg_wal`, `pg_replslot`, `pg_dynshmem`, `pg_notify`, `pg_serial`, `pg_snapshots`,
`pg_stat_tmp`, `pg_subtrans`, `pgslq_tmp*`.
La liste complète figure dans la [documentation officielle](https://docs.postgresql.fr/current/continuous-archiving.html#BACKUP-LOWLEVEL-BASE-BACKUP).

</div>

---

### Serveur secondaire (2/4) - Fichiers de configuration

<div class="slide-content">

  * `postgresql.conf` & `postgresql.auto.conf`
    + paramètres
  * `standby.signal` (dans PGDATA)
    + vide

</div>

<div class="notes">

Au choix,
les paramètres sont à ajouter dans `postgresql.conf`,
dans un fichier appelé par ce dernier avec une clause d'inclusion,
ou dans `postgresql.auto.conf` (forcément dans le répertoire de
données pour ce dernier, et qui surcharge les fichiers précédents).
Cela dépend des habitudes, de la méthode d'industrialisation…

S'il y a des paramètres propres au primaire dans la configuration
d'un secondaire, ils seront ignorés, et vice-versa. Dans les cas simples,
le `postgresql.conf` peut donc être le même.

Puis il faut créer un fichier vide nommé `standby.signal` dans le répertoire `PGDATA`,
qui indique à PostgreSQL que le serveur doit rester en _recovery_ permanent.

<div class="box tip">
<!-- on met ici le minimum comme point de départ pour le versions <v11
qui traîne, pour ne plus en parler ailleurs
-->
Au cas où vous rencontreriez un vieux serveur en version
antérieure à la 12 : jusqu'en version 11, on activait le mode _standby_
non dans la configuration, mais en créant un fichier texte `recovery.conf`
dans le `PGDATA` de l'instance, et en y plaçant le paramètre `standby_mode` à `on`.
Les autres paramètres sont les mêmes. Toute modification
impliquait un redémarrage.
</div>
</div>

---

### Serveur secondaire (2/4) - Paramètres

<div class="slide-content">

  * `primary_conninfo` (_streaming_) :
<!-- bash pour coloration -->
```bash
primary_conninfo = 'user=user_repli host=prod port=5434
 application_name=standby '
```
  * Optionnel :
    + `primary_slot_name`
    + `restore_command`
    + `wal_receiver_timeout`
</div>

<div class="notes">

PostgreSQL doit aussi savoir comment se connecter au serveur
primaire. C'est le paramètre `primary_conninfo` qui le lui dit. Il s'agit d'un
DSN standard où il est possible de spécifier l'adresse IP de l'hôte ou son
alias, le numéro de port, le nom de l'utilisateur, etc. Il est aussi possible
de spécifier le mot de passe, mais c'est risqué en terme de sécurité.
En effet, PostgreSQL ne vérifie pas si ce fichier est lisible par quelqu'un
d'autre que lui. Il est donc préférable de placer le mot de passe dans le
fichier `.pgpass`, généralement dans `~postgres/` sur le secondaire, fichier
qui n'est utilisé que s'il n'est lisible que par son propriétaire. Par exemple :

```ini
primary_conninfo = 'user=postgres host=prod passfile=/var/lib/postgresql/.pgpass'
```

Toutes les options de la libpq sont accessibles. Par exemple,
cette chaîne de connexion a été générée pour un nouveau
secondaire par `pg_basebackup -R` :  <!-- mise à jour v16 -->
```ini
primary_conninfo = 'host=prod user=postgres passfile=''/var/lib/postgresql/.pgpass'' channel_binding=prefer port=5436 sslmode=prefer sslcompression=0 sslcertmode=allow sslsni=1 ssl_min_protocol_version=TLSv1.2 gssencmode=prefer krbsrvname=postgres gssdelegation=0 target_session_attrs=any load_balance_hosts=disable
```
S'y trouvent beaucoup de paramétrage par défaut dépendant
de méthodes d'authentification, ou pour le SSL.

Parmi les autres paramètres optionnels de `primary_conninfo`, il est conseillé
d'ajouter `application_name`, par exemple avec le nom du serveur.
Cela facilite la supervision. C'est même nécessaire pour paramétrer une réplication
synchrone.

```ini
primary_conninfo = 'user=postgres host=prod passfile=/var/lib/postgresql/.pgpass  application_name=secondaire2 '
```
Si `application_name` n'est pas fourni, le `cluster_name` du secondaire sera
<!-- paramètre implicite fallback_application_name visible dans pg_stat_wal_receiver -->
utilisé, mais il est rarement correctement configuré
(par défaut, il vaut `16/main` sur Debian/Ubuntu, et n'est pas configuré
sur Red Hat/Rocky Linux).

De manière optionnelle, nous verrons que l'on peut définir aussi deux paramètres :
<!-- détaillés dans W2B -->

  * `primary_slot_name`, pour sécuriser la réplication avec un slot de réplication ;
  * `restore_command`, pour sécuriser la réplication avec un accès à la sauvegarde PITR.

Le paramètre `wal_receiver_timeout` sur le secondaire
est le symétrique de `wal_sender_timeout` sur le primaire.
Il indique au bout de combien de temps couper une connexion inactive.
Le secondaire retentera la connexion plus tard.

</div>

---

### Serveur secondaire (3/4) - Démarrage

<div class="slide-content">

  * Démarrer PostgreSQL
  * Suivre dans les traces que tout va bien
</div>

<div class="notes">

Il ne reste plus qu'à démarrer le serveur secondaire.

En cas de problème, le premier endroit où aller chercher est bien entendu
le fichier de trace `postgresql.log`.

</div>

---

### Processus

<div class="slide-content">

Sur le primaire :

  * `walsender ... streaming 0/3BD48728`

Sur le secondaire :

  * `walreceiver   streaming 0/3BD48728`

</div>

<div class="notes">

Sur le primaire, un processus `walsender` apparaît pour chaque secondaire
connecté. Son nom de processus est mis à jour en permanence avec l'emplacement
dans le flux de journaux de transactions :
```default
 postgres: 16/secondaire1: walsender postgres [local] streaming 15/6A6EF408
 postgres: 16/secondaire2: walsender postgres [local] streaming 15/6A6EF408
```

Symétriquement, sur chaque secondaire, un process `walreceiver` apparaît.
```default
 postgres: 16/secondaire2: walreceiver streaming 0/DD73C218
```

</div>

---

<!--
ATTENTION : Ceci est une partie commune à W2A (DBA3) et R56 (HAPAT)
-->

## Promotion

---

### Au menu

<div class="slide-content">

  * Attention au _split-brain_ !
  * Vérification avant promotion
  * Promotion : méthode et déroulement
  * Retour à l'état stable
</div>

<div class="notes">

</div>

---

### Attention au split-brain !

<div class="slide-content">

  * Si un serveur secondaire devient le nouveau primaire
    + s'assurer que l'ancien primaire ne reçoit plus d'écriture
  * Éviter que les deux instances soient ouvertes aux écritures
    + confusion et perte de données !
</div>

<div class="notes">

La pire chose qui puisse arriver lors d'une bascule est d'avoir les deux
serveurs, ancien primaire et nouveau primaire promu, ouverts tous les deux en
écriture. Les applications risquent alors d'écrire dans l'un ou l'autre…

Quelques histoires « d'horreur » à ce sujet :

  * de [nombreux exemples sur diverses technologies de réplication](https://github.blog/2018-10-30-oct21-post-incident-analysis) ;
  * _post mortem_ d'un [gros problème chez Github en 2018](https://aphyr.com/posts/288-the-network-is-reliable).

</div>

---

### Vérification avant promotion

<div class="slide-content">

<!-- pas de coloration, c'est trop hasardeux -->
  * Primaire :

```default
# systemctl stop postgresql-14
```
```default
$ pg_controldata -D /var/lib/pgsql/14/data/ \
| grep -E '(Database cluster state)|(REDO location)'
Database cluster state:               shut down
Latest checkpoint's REDO location:    0/3BD487D0
```

  * Secondaire :

```default
$ psql -c 'CHECKPOINT;'
$ pg_controldata -D /var/lib/pgsql/14/data/ \
| grep -E '(Database cluster state)|(REDO location)'
Database cluster state:               in archive recovery
Latest checkpoint's REDO location:    0/3BD487D0
```
</div>

<div class="notes">

Avant une bascule, il est capital de vérifier que toutes les modifications envoyées par le primaire sont arrivées sur le secondaire. Si le primaire a été arrêté proprement, ce sera le cas. Après un `CHECKPOINT` sur le secondaire, on y retrouvera le même emplacement dans les journaux de transaction.

Ce contrôle doit être systématique avant une bascule. Même si toutes les écritures applicatives sont stoppées sur le primaire, quelques opérations de maintenance peuvent en effet écrire dans les journaux et provoquer un écart entre les deux serveurs (divergence). Il n'y aura alors pas de perte de données mais cela pourrait gêner la transformation de l'ancien primaire en secondaire, par exemple.
En revanche, **même avec un arrêt propre du primaire**, il peut y avoir perte de données
s'il y a un _lag_ important entre primaire et secondaire :
même si le rejeu va toujours jusqu'au bout avant le changement de _timeline_, les WAL qui
n'ont pas pu être récupérés avant la déconnexion, ou après la récupération des archives
(si le _log shipping_ est en place) sont perdus pour le nouveau primaire.

Noter que `pg_controldata` n'est pas dans les chemins par défaut des distributions. La fonction SQL `pg_control_checkpoint()`
affiche les même informations, mais n'est bien sûr pas accessible sur un primaire arrêté.

</div>

---

### Promotion du standby : méthode

<div class="slide-content">

  * Shell :
    + `pg_ctl  promote`
  * SQL :
    + fonction `pg_promote()`

</div>

<div class="notes">

Il existe plusieurs méthodes pour promouvoir un serveur PostgreSQL en mode
_standby_. Les méthodes les plus appropriées sont :

  * l'action `promote` de l'outil `pg_ctl`, ou de son équivalent dans
les scripts des paquets d'installation, comme `pg_ctlcluster` sous Debian ; <!-- apparemment, rien sous RH -->
  * la fonction SQL `pg_promote`.

Ces deux méthodes remplacent le fichier de déclenchement historique
(_trigger file_), défini par le paramètre `promote_trigger_file`,
qui n'existe plus à partir de PostgreSQL 16.
Dans les versions précédentes, un serveur secondaire vérifie en permanence si ce fichier existe.
Dès qu'il apparaît, l'instance est promue. Par mesure de sécurité, il est
préconisé d'utiliser un emplacement accessible uniquement aux administrateurs.

</div>

---

### Promotion du standby : déroulement

<div class="slide-content">

Une promotion déclenche :

  * déconnexion de la _streaming replication_ (bascule programmée)
  * rejeu des dernières transactions en attente d'application
  * récupération et rejeu de toutes les archives disponibles
  * choix d'une nouvelle _timeline_ du journal de transaction
  * suppression du fichier `standby.signal`
  * nouvelle _timeline_ et fichier `.history`
  * ouverture aux écritures

</div>

<div class="notes">

La promotion se déroule en bonne partie comme un _recovery_
après restauration PITR.

Une fois l'instance promue, elle finit de rejouer les derniers journaux de transaction en
provenance du serveur principal en sa possession, puis se déconnecte de celui-ci
(si l'on est encore connecté en _streaming_). Après la déconnexion, si une `restore_command`
est configurée, toutes les archives disponibles sont récupérées et rejouées (en général,
il n'y a pas d'archive contenant des WAL
plus récents que le dernier récupéré en _streaming_ ;
mais des écritures lourdes et/ou un réseau trop lent peuvent entraîner un retard
du _streaming_).
<!-- notamment si le débit réseau a été fortement réduit pendant un temps, sans que cela
n'ait fait décroché le secondaire, ni n'ait eu d'impact sur la vitesse de l'archivage ;
exemple : les PRA parisiens de S..m avec des primaires à 15000 km ont souvent des gros retards ;
on peut reproduire facilement en ajoutant de la latence artificielle sur le réseau local
avec une commande de ce genre : `tc qdisc add dev lo root netem delay 500ms`     -->


Le dernier journal reçu de l'ancien primaire est souvent incomplet.
Il est renommé avec le suffixe `.partial` et archivé. Cela évite un conflit
de nom éventuel avec le même fichier issu de l'ancien serveur, qui a pu
aussi être archivé, à un point éventuellement postérieur à la divergence.

Ensuite, l'instance choisit une nouvelle _timeline_ pour son journal de transactions.
Rappelons que la _timeline_ est le premier numéro dans le nom du segment (fichier WAL) ; par
exemple une timeline 5 pour un fichier nommé `000000050000003200000031`).
Le nouveau primaire choisit généralement le numéro suivant celui du primaire
(à moins que les archives ne contiennent d'autres _timelines_ de numéro
supérieur, s'il y a eu plusieurs restaurations et retours en arrière,
et il choisit alors le numéro suivant la dernière).

Le choix d'une nouvelle _timeline_ permet à PostgreSQL de rendre
les journaux de transactions de ce nouveau serveur en écriture incompatibles
avec son ancien serveur principal. De plus, des journaux
de nom différent permet l'archivage depuis ce primaire
sans perturber l'ancien s'il existe encore. Il n'y a plus de fichier en commun
même si l'espace d'archivage est partagé.

<div class="box warning">
Les _timelines_ ne changent pas que lors des promotions, mais aussi lors des
restaurations PITR. En général, on désire que les secondaires (parfois en cascade)
suivent. Heureusement, ceci est le paramétrage par défaut depuis la version 12 :
```ini
recovery_target_timeline = latest
```
<!-- détails déjà donnés dans I2 -->
Un secondaire suit donc par défaut les évolutions de _timeline_
de son primaire, tant que celui-ci n'effectue pas de retour en arrière.
</div>

L'instance crée un fichier d'historique dans `pg_wal/`,
par exemple `00000006.history` pour la nouvelle _timeline_ 6.
C'est un petit fichier texte
qui contient les différentes _timelines_ ayant mené à la nouvelle.
Ce fichier est immédiatement archivé s'il y a archivage.
<!-- pas de détail, trop compliqué -->

Enfin, l'instance autorise les connexions en lecture et en écriture.

</div>

---

### Opérations après promotion du standby

<div class="slide-content">

  * `VACUUM ANALYZE` conseillé
    + calcul d'informations nécessaires pour autovacuum

</div>

<div class="notes">

Il n'y a aucune opération obligatoire après une promotion. Cependant, il
est conseillé d'exécuter un `VACUUM` ou un `ANALYZE` pour que PostgreSQL mette à
jour les estimations de nombre de lignes vivantes et mortes. Ces estimations
sont utilisées par l'autovacuum pour lutter contre la fragmentation des tables
et mettre à jour les statistiques sur les données. Or ces estimations faisant
partie des statistiques d'activité, elles ne sont pas répliquées vers les
secondaires. Il est donc intéressant de les mettre à jour après une promotion.
<!-- Apparemment  VACUUM comme ANALYZE met à jour n_live_tup/n_dead_tup
L'analyze est à priori inutile car pg_stats est répliqué, mais peut être moins lourd
que vacuum.
-->
</div>

---

### Retour à l'état stable

<div class="slide-content">

  * Si un _standby_ a été momentanément indisponible, reconnexion directe possible si :
    + journaux nécessaires encore présents sur primaire (slot, `wal_keep_size`/`wal_keep_segments`)
    + journaux nécessaires présents en archives (`restore_command`)
  * Sinon
    + « décrochage »
    + reconstruction nécessaire

</div>

<div class="notes">

Si un serveur secondaire est momentanément indisponible mais revient en
ligne sans perte de données (réseau coupé, problème OS…), alors il
a de bonnes chances de se « raccrocher » à son serveur primaire. Il
faut bien sûr que l'ensemble des journaux de transaction depuis son arrêt
soit accessible à ce serveur, sans exception.

En cas de réplication par _streaming_ : le primaire ne doit pas avoir
recyclé les journaux après ses _checkpoints_. Il les aura conservés
s'il y a un slot de réplication actif dédié à ce secondaire, ou
si on a monté `wal_keep_size` (ou `wal_keep_segments` jusque PostgreSQL 12 compris)
assez haut par rapport à l'activité en
écriture sur le primaire. Les journaux seront alors toujours disponibles sur le principal
et le secondaire rattrapera son retard
par _streaming_. Si le primaire n'a plus les journaux, il affichera une
erreur, et le secondaire tentera de se rabattre sur le _log shipping_,
s'il est aussi configuré.

En cas de réplication par _log shipping_, il faut que la `restore_command`
fonctionne, que le stock des journaux remonte assez loin dans le temps (jusqu'au
moment où le secondaire a perdu contact), et qu'aucun journal ne manque ou ne
soit corrompu. Sinon le secondaire se bloquera au dernier journal chargé. En cas
d'échec, ou si le dernier journal disponible vient d'être rejoué, le secondaire
basculera sur le _streaming_, s'il est configuré.

Si le secondaire ne peut rattraper le flux des journaux du primaire, il doit
être reconstruit par l'une des méthodes précédentes.

</div>

---

### Retour à l'état stable, suite

<div class="slide-content">

  * Synchronisation automatique une fois la connexion rétablie
  * Mais reconstruction obligatoire :
    + si le serveur secondaire était plus avancé que le serveur promu (« divergence »)
  * Reconstruire les serveurs secondaires à partir du nouveau principal :
    + `rsync`, restauration PITR, plutôt que `pg_basebackup`
    + `pg_rewind`
  * Reconstruction : manuelle !
  * Tablespaces !

</div>

<div class="notes">

Un secondaire qui a bien « accroché » son primaire se synchronise automatiquement
avec lui, que ce soit par _streaming_ ou _log shipping_. C'est notamment le cas si
l'on vient de le construire depuis une sauvegarde ou avec `pg_basebackup`,
et que l'archivage ou le _streaming_ alimentent correctement le secondaire.
Cependant, il y a des cas où un secondaire ne peut être simplement raccroché
à un primaire, notamment si le secondaire se croit plus avancé que le primaire dans
le flux des journaux.

<!-- détaillé parce que pas évident -->
<!-- explication détaillée des timelines dans DBA2/3 , I2 -->
Le cas typique est un ancien primaire que l'on veut
transformer en secondaire d'un ancien secondaire promu. Si la bascule s'était faite
proprement, et que l'ancien primaire avait pu envoyer tous ses journaux avant de s'arrêter
ou d'être arrêté, il n'y a pas de problème. Si le primaire a été arrêté violemment,
sans pouvoir transmettre tous ses journaux, l'ancien secondaire n'a rejoué que ce qu'il a reçu,
puis a ouvert en écriture sa propre _timeline_ depuis un point moins avancé
que là où le primaire était finalement arrivé avant d'être arrêté.
Les deux serveurs ont donc « divergé », même pendant très peu de temps.
Les journaux non envoyés au nouveau primaire doivent être considérés comme perdus.
Quand l'ancien primaire revient en ligne, parfois très longtemps après, il voit que sa _timeline_
est plus avancée que la version qu'en a gardée le nouveau primaire.
Il ne sait donc pas comment appliquer les journaux qu'il reçoit du nouveau primaire.

La principale solution, et la plus simple, reste alors la reconstruction du secondaire à raccrocher.
<!--
Ne pas développer, c'est le sujet du début de W3
-->

L'utilisation de `pg_basebackup` est possible mais déconseillée si la
volumétrie est importante :
cet outil impose une copie de l'ensemble des données du serveur principal, et
ce peut être long.

La durée de reconstruction des secondaires peut être optimisée en utilisant des
outils de synchronisation de fichiers pour réduire le volume
des données à transférer. Les outils de restauration PITR offrent souvent une
restauration en mode delta (notamment l'option `--delta` de pgBackRest)
et c'est ce qui est généralement à
privilégier. Dans un script de sauvegarde PITR, `rsync --whole-file` reste une bonne option.

Le fait de disposer de l'ensemble des fichiers de configuration sur tous les
nœuds permet de gagner un temps précieux lors des phases de reconstruction,
qui peuvent également être scriptées.

Par contre, les opérations de
reconstructions se doivent d'être lancées **manuellement** pour éviter tout
risque de corruption de données dues à des opérations automatiques externes,
comme lors de l'utilisation de solutions de haute disponibilité.

Enfin, on rappelle qu'il ne faut pas oublier de prendre en compte les
_tablespaces_ lors de la reconstruction.

Une alternative à la reconstruction est l'utilisation de l'outil `pg_rewind`
pour « rembobiner » l'ancien primaire, si tous les journaux nécessaires sont disponibles.

</div>


---

## Conclusion

<div class="slide-content">

  * Système de réplication fiable
  * Simple à maîtriser et à configurer
</div>

<div class="notes">

La réplication interne à PostgreSQL est le résultat de
travaux remontant aussi loin que la version 8.0. Elle est fondée sur des bases
solides et saines.

Cette réplication reste fidèle aux principes du moteur de PostgreSQL :

  * simple à maîtriser ;
  * simple à configurer ;
  * fonctionnelle ;
  * stable.

</div>

---


<!--


 ATTENTION : TP communs à W2A (DBA3) et R56 (HAPAT) 
 à ceci près qu'en DBA3 tout est sur 1 machine et 2 instances
 et HAPAT a 3 VMs
 
 Ne pas faire trop diverger 
 
 Si on passe bien en multiVM en DBA3, les 3 modules vraient utiliser le TP de W2A
 https://gitlab.dalibo.info/formation/manuels/-/issues/851
  
-->

\newpage

## Travaux pratiques

<div class="notes">

<div class="box warning">

Ce TP suppose que les instances tournent sur la même machine.
N'oubliez pas qu'il faut un répertoire de données et un numéro de port
par serveur PostgreSQL.

Dans la réalité, il s'agira de deux machines différentes : l'archivage
nécessitera des opérations supplémentaires (montage de partitions réseau,
connexion ssh sans mot de passe…).

</div>

</div>
<!--


 ATTENTION : TP communs à W2A (DBA3) et R56 (HAPAT) 
 à ceci près qu'en DBA3 tout est sur 1 machine et 2 instances
 et HAPAT a 3 VMs
 
 Ne pas faire trop diverger 
 
 Si on passe bien en multiVM en DBA3, les 3 modules vraient utiliser le TP de W2A
 https://gitlab.dalibo.info/formation/manuels/-/issues/851
  
-->

### Sur Rocky Linux 8 ou 9

#### Réplication asynchrone en flux avec un seul secondaire

<div class="notes">

<div class="slide-content">

**But** : Mettre en place une réplication asynchrone en flux.

</div>

> - Créer l'instance principale dans `/var/lib/pgsql/16/instance1`.

> - Mettre en place la configuration de la réplication par _streaming_.
> - L'utilisateur dédié sera nommé **repli**.

> - Créer la première instance secondaire **instance2**, par copie **à chaud**
du répertoire de données avec `pg_basebackup` vers `/var/lib/psql/16/instance2`.
> - Penser à modifier le port de cette nouvelle instance avant de la démarrer.

> - Démarrer **instance2** et s'assurer que la réplication fonctionne bien
avec `ps`.
> - Tenter de se connecter au serveur secondaire.
> - Créer quelques tables pour vérifier que les écritures se propagent
du primaire au secondaire.

</div>

#### Promotion de l'instance secondaire

<div class="notes">

<div class="slide-content">

**But** : Promouvoir un serveur secondaire en primaire.

</div>

> - En respectant les étapes de vérification de l'état des instances, effectuer
une promotion contrôlée de l'instance secondaire.

> - Tenter de se connecter au serveur secondaire fraîchement promu.
> - Les écritures y sont-elles possibles ?

</div>

#### Retour à la normale

<div class="notes">

<div class="slide-content">

**But** : Revenir à l'architecture d'origine.

</div>

> - Reconstruire l'instance initiale (`/var/lib/pgsql/16/instance1`) comme
nouvelle instance secondaire en repartant d'une copie complète de **instance2**
en utilisant `pg_basebackup`.

> - Démarrer cette nouvelle instance.
> - Vérifier que les processus adéquats sont bien présents, et que les données
précédemment insérées dans les tables créées plus haut sont bien présentes
dans l'instance reconstruite.

> - Inverser à nouveau les rôles des deux instances afin que **instance2**
redevienne l'instance secondaire.

</div>

---

<!--


 ATTENTION : TP communs à W2A (DBA3) et R56 (HAPAT) 
 à ceci près qu'en DBA3 tout est sur 1 machine et 2 instances
 et HAPAT a 3 VMs
 
 Ne pas faire trop diverger 
 
 Si on passe bien en multiVM en DBA3, les 3 modules vraient utiliser le TP de W2A
 https://gitlab.dalibo.info/formation/manuels/-/issues/851
  
-->

\newpage

## Travaux pratiques (solutions)

<div class="notes">

<div class="box tip">

La version de PostgreSQL est la version 16.
Adapter au besoin pour une version ultérieure.
Noter que les versions 12 et précédentes utilisent d'autres fichiers.

</div>

</div>
<!--


 ATTENTION : TP communs à W2A (DBA3) et R56 (HAPAT) 
 à ceci près qu'en DBA3 tout est sur 1 machine et 2 instances
 et HAPAT a 3 VMs
 
 Ne pas faire trop diverger 
 
 Si on passe bien en multiVM en DBA3, les 3 modules vraient utiliser le TP de W2A
 https://gitlab.dalibo.info/formation/manuels/-/issues/851
  
-->

### Sur Rocky Linux 8 ou 9

<div class="notes">

<div class="box tip">

Cette solution se base sur un système Rocky Linux 8,
installé à minima depuis les paquets du PGDG, et en anglais.

</div>

<div class="box tip">
Le prompt `#` indique une commande à exécuter avec l'utilisateur `root`.
Le prompt `$` est utilisé pour les commandes de l'utilisateur `postgres`.
</div>

La mise en place d'une ou plusieurs instances sur le même poste est décrite plus haut.

En préalable, nettoyer les instances précédemment créées sur le serveur.
</div>
<!--


 ATTENTION : TP communs à W2A (DBA3) et R56 (HAPAT) 
 à ceci près qu'en DBA3 tout est sur 1 machine et 2 instances
 et HAPAT a 3 VMs
 
 Ne pas faire trop diverger 
 
 Si on passe bien en multiVM en DBA3, les 3 modules vraient utiliser le TP de W2A
 https://gitlab.dalibo.info/formation/manuels/-/issues/851
  
-->

<div class="notes">

Ensuite, afin de réaliser l'ensemble des TP, configurer 4 services PostgreSQL
« instance[1-4] ».

<!-- FIXME
Pour faciliter la vie aux élèves, fournir les fichier .service à copier coller ?
Ce serait plus clair. n'empêche pas de laisser les sed.
-->

<!-- on garde 'text' à cause de la coloration syntaxique malheureuse -->
```default
# cp /lib/systemd/system/postgresql-16.service \
                          /etc/systemd/system/instance1.service

# sed -i "s|/var/lib/pgsql/16/data/|/var/lib/pgsql/16/instance1/|" \
                          /etc/systemd/system/instance1.service
```

```default
# cp /lib/systemd/system/postgresql-16.service \
                          /etc/systemd/system/instance2.service

# sed -i "s|/var/lib/pgsql/16/data/|/var/lib/pgsql/16/instance2/|" \
                          /etc/systemd/system/instance2.service
```

```default
# cp /lib/systemd/system/postgresql-16.service \
                          /etc/systemd/system/instance3.service

# sed -i "s|/var/lib/pgsql/16/data/|/var/lib/pgsql/16/instance3/|" \
                          /etc/systemd/system/instance3.service
```

```default
# cp /lib/systemd/system/postgresql-16.service \
                          /etc/systemd/system/instance4.service

# sed -i "s|/var/lib/pgsql/16/data/|/var/lib/pgsql/16/instance4/|" \
                          /etc/systemd/system/instance4.service
```

</div>
<!--


 ATTENTION : TP communs à W2A (DBA3) et R56 (HAPAT) 
 à ceci près qu'en DBA3 tout est sur 1 machine et 2 instances
 et HAPAT a 3 VMs
 
 Ne pas faire trop diverger 
 
 Si on passe bien en multiVM en DBA3, les 3 modules vraient utiliser le TP de W2A
 https://gitlab.dalibo.info/formation/manuels/-/issues/851
  
-->

<div class="notes">

#### Réplication asynchrone en flux avec un seul secondaire

<!-- NB : peu de volumétrie volontairement pour éviter risque de décrochage -->

> - Créer l'instance principale dans `/var/lib/pgsql/16/instance1`.

```default
# /usr/pgsql-16/bin/postgresql-16-setup initdb instance1
Initializing database ... OK
# systemctl start instance1
```

> - Mettre en place la configuration de la réplication par _streaming_.
> - L'utilisateur dédié sera nommé **repli**.

Depuis la version 10, le comportement de PostgreSQL a changé et la réplication
est activée par défaut en local.

Nous allons cependant modifier le fichier `/var/lib/pgsql/16/instance1/pg_hba.conf`
pour que l'accès en réplication soit autorisé pour l'utilisateur **repli** :

```ini
host  replication  repli  127.0.0.1/32  scram-sha-256
```

Cette configuration indique que l'utilisateur **repli** peut se connecter en mode
réplication à partir de l'adresse IP `127.0.0.1`. L'utilisateur `repli`
n'existant pas, il faut le créer (nous utiliserons le mot de passe **confidentiel**) :

```bash
$ createuser --no-superuser --no-createrole --no-createdb --replication -P repli
Enter password for new role:
Enter it again:
```

Configurer ensuite le fichier `.pgpass` de l'utilisateur système `postgres` :

```bash
$ echo "*:*:*:repli:confidentiel" > ~/.pgpass
$ chmod 600 ~/.pgpass
```

Pour prendre en compte la configuration, la configuration de l'instance
principale doit être rechargée :

```bash
$ psql -c 'SELECT pg_reload_conf()'
```

> - Créer la première instance secondaire **instance2**, par copie **à chaud** du répertoire de données avec `pg_basebackup` vers `/var/lib/psql/16/instance2`.
> - Penser à modifier le port de cette nouvelle instance avant de la démarrer.

Utiliser `pg_basebackup` pour créer l'instance secondaire :

```bash
$ pg_basebackup -D /var/lib/pgsql/16/instance2 -P -R -c fast -h 127.0.0.1 -U repli
```
```default
25314/25314 kB (100%), 1/1 tablespace
```

L'option `-R` ou `--write-recovery-conf` de `pg_basebackup` a préparé la
configuration de la mise en réplication en créant le fichier `standby.signal`
ainsi qu'en configurant `primary_conninfo` dans le fichier `postgresql.auto.conf`
(dans les versions antérieures à la 11, il renseignerait `recovery.conf`) :

```bash
$ cat /var/lib/pgsql/16/instance2/postgresql.auto.conf
```
```default
primary_conninfo = 'user=repli passfile=''/var/lib/pgsql/.pgpass''
					host=127.0.0.1 port=5432 sslmode=prefer sslcompression=0
					gssencmode=prefer krbsrvname=postgres target_session_attrs=any'

$ ls /var/lib/pgsql/16/instance2/standby.signal
```
```default
/var/lib/pgsql/16/instance2/standby.signal
```

Il faut désormais positionner le port d'écoute dans le fichier de configuration,
c'est-à-dire `/var/lib/pgsql/16/instance2/postgresql.conf` :

```ini
port=5433
```

> - Démarrer **instance2** et s'assurer que la réplication fonctionne bien avec `ps`.
> - Tenter de se connecter au serveur secondaire.
> - Créer quelques tables pour vérifier que les écritures se propagent du primaire au secondaire.

Il ne reste désormais plus qu'à démarrer l'instance secondaire :

```default
# systemctl start instance2
```

La commande `ps` suivante permet de voir que les deux serveurs sont lancés :

```bash
$ ps -o pid,cmd fx
```

La première partie concerne le serveur secondaire :

```default
PID CMD
9671 /usr/pgsql-16/bin/postmaster -D /var/lib/pgsql/16/instance2/
9673  \_ postgres: logger
9674  \_ postgres: startup   recovering 000000010000000000000003
9675  \_ postgres: checkpointer
9676  \_ postgres: background writer
9677  \_ postgres: stats collector
9678  \_ postgres: walreceiver   streaming 0/3000148
```

La deuxième partie concerne le serveur principal :

```default
PID CMD
9564 /usr/pgsql-16/bin/postmaster -D /var/lib/pgsql/16/instance1/
9566  \_ postgres: logger
9568  \_ postgres: checkpointer
9569  \_ postgres: background writer
9570  \_ postgres: walwriter
9571  \_ postgres: autovacuum launcher
9572  \_ postgres: stats collector
9573  \_ postgres: logical replication launcher
9679  \_ postgres: walsender repli 127.0.0.1(58420) streaming 0/3000148
```

Pour différencier les deux instances, il est possible d'identifier le
répertoire de données (l'option `-D`), les autres processus sont des fils du
processus postmaster. Il est aussi possible de configurer le paramètre `cluster_name`.

Nous avons bien les deux processus de réplication en flux `wal sender` et `wal receiver`.

Créons quelques données sur le principal et assurons-nous qu'elles soient
transmises au secondaire :

```bash
$ createdb b1
```
```bash
$ psql b1
```
```default
psql (16.1)
Type "help" for help.
```

```sql
b1=# CREATE TABLE t1(id integer);
```
```default
CREATE TABLE
```
```sql
b1=# INSERT INTO t1 SELECT generate_series(1, 1000000);
```
```default
INSERT 0 1000000
```

On constate que le flux a été transmis :

```bash
b1=# \! ps -o pid,cmd fx | egrep "(startup|walsender|walreceiver)"
```
```default
 9674  \_ postgres: startup   recovering 000000010000000000000006
 9678  \_ postgres: walreceiver   streaming 0/6D4CD28
 9679  \_ postgres: walsender repli 127.0.0.1(58420) streaming 0/6D4CD28
[...]
```

Essayons de nous connecter au secondaire et d'exécuter quelques requêtes :

```default
$ psql -p 5433 b1
```
```default
psql (16.1)
Type "help" for help.
```

```sql
b1=# SELECT COUNT(*) FROM t1;
```
```default
  count
---------
 1000000
```
```sql
b1=# CREATE TABLE t2(id integer);
```
```default
ERROR:  cannot execute CREATE TABLE in a read-only transaction
```

On peut se connecter, lire des données, mais pas écrire.

Le comportement est visible dans les logs de l'instance secondaire dans le
répertoire  
`/var/lib/pgsql/16/instance2/log` :

```default
... LOG:  database system is ready to accept read only connections
```

PostgreSQL indique bien qu'il accepte des connexions en lecture seule.

#### Promotion de l'instance secondaire

> - En respectant les étapes de vérification de l'état des instances, effectuer
une promotion contrôlée de l'instance secondaire.

Arrêt de l'instance primaire et vérification de son état :

```default
# systemctl stop instance1
```
```bash
$ /usr/pgsql-16/bin/pg_controldata -D /var/lib/pgsql/16/instance1/ \
| grep -E '(cluster)|(REDO)'
```
```default
Database cluster state:               shut down
Latest checkpoint's REDO location:    0/6D4E5C8
```

Vérification de l'instance secondaire :

```bash
$ psql -p 5433 -c 'CHECKPOINT;'
```
```bash
$ /usr/pgsql-16/bin/pg_controldata -D /var/lib/pgsql/16/instance2/ \
| grep -E '(cluster)|(REDO)'
```
```default
Database cluster state:               in archive recovery
Latest checkpoint's REDO location:    0/6D4E5C8
```

L'instance principale est bien arrêtée, l'instance secondaire est bien en
`archive recovery` et les deux sont bien synchronisées.

Promotion de l'instance secondaire :

```bash
$ /usr/pgsql-16/bin/pg_ctl -D /var/lib/pgsql/16/instance2 promote
```
```default
waiting for server to promote.... done
server promoted
```

> - Tenter de se connecter au serveur secondaire fraîchement promu.
> - Les écritures y sont-elles possibles ?

Connectons-nous à ce nouveau primaire et tentons d'y insérer des données :

```bash
$ psql -p 5433 b1
psql (16.1)
Type "help" for help.
```

```sql
b1=# CREATE TABLE t2(id integer);
```
```default
CREATE TABLE
```
```sql
b1=# INSERT INTO t2 SELECT generate_series(1, 1000000);
```
```default
INSERT 0 1000000
```

Les écritures sont désormais bien possible sur cette instance.

#### Retour à la normale

> - Reconstruire l'instance initiale (`/var/lib/pgsql/16/instance1`) comme
nouvelle instance secondaire en repartant d'une copie complète de **instance2**
en utilisant `pg_basebackup`.

Afin de rétablir la situation, nous pouvons réintégrer l'ancienne instance
primaire en tant que nouveau secondaire. Pour ce faire, nous devons
re-synchroniser les données. Utilisons `pg_basebackup` comme précédemment
après avoir mis de côté les fichiers de l'ancien primaire :

```bash
$ mv /var/lib/pgsql/16/instance1 /var/lib/pgsql/16/instance1.old
```
```bash
$ pg_basebackup -D /var/lib/pgsql/16/instance1 -P -R -c fast \
-h 127.0.0.1 -p 5433 -U repli
```
```default
104385/104385 kB (100%), 1/1 tablespace
```

Créer le fichier `standby.signal` s'il n'existe pas déjà.
Contrôler `postgresql.auto.conf` (qui contient potentiellement deux lignes
`primary_conninfo` !) et adapter le port :

```bash
$ touch /var/lib/pgsql/16/instance1/standby.signal
```
```bash
$ cat /var/lib/pgsql/16/instance1/postgresql.auto.conf
```
```default
primary_conninfo = 'user=repli passfile=''/var/lib/pgsql/.pgpass'' host=127.0.0.1 port=5433 sslmode=prefer sslcompression=0 gssencmode=prefer krbsrvname=postgres target_session_attrs=any'
```

Repositionner le port d'écoute dans le fichier `/var/lib/pgsql/16/instance1/postgresql.conf` :

```ini
port=5432
```

Enfin, démarrer le service :

```default
# systemctl start instance1
```

> - Démarrer cette nouvelle instance.
> - Vérifier que les processus adéquats sont bien présents, et que les données
précédemment insérées dans les tables créées plus haut sont bien présentes
dans l'instance reconstruite.

Les processus adéquats sont bien présents :

```bash
$ ps -o pid,cmd fx | egrep "(startup|walsender|walreceiver)"
```
```default
12520  \_ postgres: startup   recovering 00000002000000000000000A
12524  \_ postgres: walreceiver   streaming 0/A000148
12525  \_ postgres: walsender repli 127.0.0.1(38614) streaming 0/A000148
```
```bash
$ psql -p 5432 b1
```
```default
psql (16.1)
Type "help" for help.
```

En nous connectant
à la nouvelle instance secondaire (port 5432), vérifions que les données
précédemment insérées dans la table `t2` sont bien présentes :

```sql
b1=# SELECT COUNT(*) FROM t2;
```
```default
  count
---------
 1000000
```

> - Inverser à nouveau les rôles des deux instances afin que **instance2**
redevienne l'instance secondaire.

Afin que l'instance 5432 redevienne primaire et celle sur le port 5433
secondaire, on peut ré-appliquer la procédure de promotion vue précédemment
dans l'autre sens.

Arrêt de l'instance primaire et vérification de son état :

```default
# systemctl stop instance2
```
```bash
$ /usr/pgsql-16/bin/pg_controldata -D /var/lib/pgsql/16/instance2/ \
| grep -E '(cluster)|(REDO)'
```
```default
Database cluster state:               shut down
Latest checkpoint's REDO location:    0/C000060
```

Vérification de l'instance secondaire :

```bash
$ psql -p 5432 -c 'CHECKPOINT;'
$ /usr/pgsql-16/bin/pg_controldata -D /var/lib/pgsql/16/instance1/ \
| grep -E '(cluster)|(REDO)'
```
```default
Database cluster state:               in archive recovery
Latest checkpoint's REDO location:    0/C000060
```

L'instance principale est bien arrêtée, l'instance secondaire est bien en
`archive recovery` et les deux sont bien synchronisées.

Promotion de l'instance secondaire :

```bash
$ /usr/pgsql-16/bin/pg_ctl -D /var/lib/pgsql/16/instance1 promote
```
```default
waiting for server to promote.... done
server promoted
```

Afin que **instance2** redevienne l'instance secondaire, créer le fichier
`standby.signal`, démarrer le service et vérifier que les processus adéquats
sont bien présents :

```bash
$ touch /var/lib/pgsql/16/instance2/standby.signal
```
```default
# systemctl start instance2
```
```bash
$  ps -o pid,cmd fx | egrep "(startup|walsender|walreceiver)"
```
```default
 5844  \_ postgres: startup   recovering 00000003000000000000000C
 5848  \_ postgres: walreceiver   streaming 0/C0001F0
 5849  \_ postgres: walsender repli 127.0.0.1(48230) streaming 0/C0001F0
```

</div>
