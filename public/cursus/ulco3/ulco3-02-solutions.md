---
revision: 24.12
date: "Année académique 2024-2025"

licence : "Creative Commons BY-NC-SA"
include_licence : CC-BY-NC-SA-2.0-FR

author: © 2005-2025 DALIBO SARL SCOP

trademarks: |
  Le logo éléphant de PostgreSQL (« Slonik ») est une création sous copyright et
  le nom « PostgreSQL » est une marque déposée par PostgreSQL Community Association
  of Canada.

##
## PDF Options
##

## Limiter la profondeur de la table des matières
toc-depth: 2

## Mettre les lien http en pieds de page
links-as-notes: false

## Police plus petite dans un bloc de code

code-blocks-fontsize: small

## Filtre : pandoc-latex-admonition
## les catégories `important` et `warning` sont synonymes
## même chose pour `tip` et `note`
pandoc-latex-admonition:
  - color: Red
    classes: [warning]
    linewidth: 4
  - color: Red
    classes: [important]
    linewidth: 4
  - color: DarkSeaGreen
    classes: [tip]
    linewidth: 4
  - color: DarkSeaGreen
    classes: [note]
    linewidth: 4
  - color: DodgerBlue
    classes: [slide-content]
    linewidth: 4

##
## Reveal Options
##

## Taille affichage
width: 960
height: 700

## beige/blood/moon/simple/solarized/black/league/night/serif/sky/white
theme: white

## None - Fade - Slide - Convex - Concave - Zoom
transition: None
transitionSpeed: fast

## Barre de progression
progress: true

## Affiche N° de slide
slideNumber: true

## Le numero de slide apparait dans la barre d'adresse
history: true

## Defilement des slides avec la roulette
mouseWheel: true

## Annule la transformation uppercase de certains thèmes
title-transform : none

## Cache l'auteur sur la première slide
## Mettre en commentaire pour désactiver
hide_author_in_slide: true

title : 'Solutions de réplication'
subtitle : 'FCU Calais - M2 I2L'
---

# Solutions de réplication

![PostgreSQL](medias/elephants/Green_Elephants_Garden_Sculptures-LoroParque_Tenerife-epSos.de-flickr_via_Wikicommons-CC-BY-2.0-300dpi.png){ height=600 }
\

<div class="notes">

Source de la photo : [epSos.de](https://www.flickr.com/photos/36495803@N05/3574411866/)
via [Wikimedia](https://commons.wikimedia.org/wiki/File:Green_Elephants_Garden_Sculptures.jpg),
licence CC-BY-2.0.

</div>

---

## Préambule

<div class="slide-content">

  * Attention au vocabulaire !
  * Identifier le besoin
  * Keep It Simple…
</div>


<div class="notes">

La réplication est le processus de partage d'informations permettant de
garantir la sécurité et la disponibilité des données entre plusieurs
serveurs et plusieurs applications. Chaque SGBD dispose de différentes
solutions pour cela et introduit sa propre terminologie. Les expressions telles
que « cluster », « actif/passif » ou « primaire/secondaire » peuvent avoir un
sens différent selon le SGBD choisi. Dès lors, il devient difficile de
comparer et de savoir ce que désignent réellement ces termes. C'est pourquoi
nous débuterons ce module par un rappel théorique et conceptuel. Nous nous
attacherons ensuite à citer les outils de réplication, internes et externes.

</div>

---

### Au menu

<div class="slide-content">

  * Rappels théoriques
  * Réplication interne
    + réplication physique
    + réplication logique
  * Quelques logiciels externes de réplication
  * Alternatives

</div>

<div class="notes">

Dans cette présentation, nous reviendrons rapidement sur la classification des
solutions de réplication, qui sont souvent utilisés dans un but de haute
disponibilité, mais pas uniquement.

PostgreSQL dispose d'une réplication physique basée sur le rejeu des journaux de
transactions par un serveur dit « en _standby_ ». Nous présenterons ainsi les
techniques dites de _Warm Standby_ et de _Hot Standby_.

Depuis la version 10 existe aussi une réplication logique, basée sur le transfert
du résultat des ordres.

Nous détaillerons ensuite les projets de réplication autour de PostgreSQL les
plus en vue actuellement.

</div>

---

### Objectifs

<div class="slide-content">

  * Identifier les différences entre les solutions de réplication proposées
  * Choisir le système le mieux adapté à votre besoin
</div>

<div class="notes">

La communauté PostgreSQL propose plusieurs réponses aux problématiques de
réplication. Le but de cette présentation est de vous apporter les
connaissances nécessaires pour comparer chaque solution et comprendre les
différences fondamentales qui les séparent.

À l'issue de cette présentation, vous serez capable de choisir le système de
réplication qui correspond le mieux à vos besoins et aux contraintes de votre
environnement de production.

</div>

---

## Rappels théoriques

<div class="slide-content">

  * Termes
  * Réplication
    + synchrone / asynchrone
    + symétrique / asymétrique
    + diffusion des modifications
</div>

<div class="notes">

Le domaine de la haute disponibilité est couvert par un bon nombre de termes
qu'il est préférable de définir avant de continuer.

</div>

---

### Cluster, primaire, secondaire, _standby_…

<div class="slide-content">

  * **Cluster** : ambiguïté !
    + groupe de bases de données = 1 instance (PostgreSQL)
    + groupe de serveurs (haute disponibilité et/ou réplication)

  * Pour désigner les membres :
    + Primaire/_primary_
    + Secondaire/_standby_
</div>

<div class="notes">

Toute la documentation (anglophone) de PostgreSQL parle de _cluster_ dans le
contexte d'un serveur PostgreSQL seul. Dans ce contexte, le _cluster_ est un
groupe de bases de données, groupe étant la traduction directe de _cluster_.
En français, on évitera tout ambiguïté en parlant d'« instance ».

Dans le domaine de la haute disponibilité et de la réplication, un _cluster_
désigne un groupe de serveurs. Par exemple, un groupe d'un serveur primaire et
de ses deux serveurs secondaires compose un cluster de réplication.

Le serveur, généralement unique, ouvert en écriture est désigné comme
« **primaire** » (_primary_), parfois « principal ».
Les serveurs connectés dessus sont « **secondaires** » (_secondary_)
ou « _standby_ » (prêts à prendre le relai). Les termes « maître/esclave »
sont à éviter mais encore très courants. On trouvera dans le présent cours
aussi bien « primaire/secondaire » que « principal/_standby_ ».

</div>

---

### Réplication asynchrone asymétrique

<div class="slide-content">

  * **Asymétrique**
    + écritures sur un serveur primaire unique
    + lectures sur le primaire et/ou les secondaires
  * **Asynchrone**
    + les écritures sur les serveurs secondaires sont différées
    + perte de données possible en cas de crash du primaire
  * Exemples :
    + réplication par _streaming_, _log shipping_, trigger

</div>

<div class="notes">

Dans la réplication asymétrique, seul le serveur primaire accepte des écritures, et
les serveurs secondaires ne sont accessibles qu'en lecture.

Dans la réplication asynchrone, les écritures sont faites sur le primaire et
le client reçoit une validation de l'écriture avant même qu'elles ne soient poussées
vers le secondaire. La mise à jour des tables répliquées **est différée**
(asynchrone). Le délai de réplication dépend de la technique et de la charge.

L'inconvénient de ce délai est que certaines données validées sur
le primaire pourraient ne pas être disponibles sur les secondaires si le primaire
est détruit avant que toutes les données, déjà validées auprès des clients, ne
soient poussées sur les secondaires.

Autrement dit, il existe une fenêtre de temps, plus ou moins longue, de perte
de données (qui entre dans le calcul du RPO).

La réplication par _streaming_ de PostgreSQL fonctionne par défaut
dans ce mode. Il concerne aussi la réplication par _log shipping_.
Les outils de réplication par trigger, plus rarement utilisés
de nos jours (Bucardo ou Slony semblent abandonnés), fonctionnent aussi ainsi.

</div>

---

### Réplication asynchrone symétrique

<div class="slide-content">

  * **Symétrique**
    + « multiprimaire »
    + écritures sur les différents primaires
    + besoin d'un gestionnaire de conflits
    + lectures sur les différents primaires
  * **Asynchrone**
    + la réplication des écritures est différées
    + perte de données possible en cas de crash du serveur primaire
    + **risque d'incohérences !**
  * Exemples :
    + BDR (EDB) : réplication logique

</div>

<div class="notes">

Dans la réplication symétrique, tous les serveurs sont accessibles aussi bien
en lecture qu'en écriture. On parle souvent de « multiprimaire ».

La réplication asynchrone, comme indiqué précédemment, envoie des modifications
vers les autres membres du cluster mais n'attend aucune validation de leur part. Il
y a donc toujours un risque de perte de données déjà validées si le serveur tombe
sans avoir eu le temps de les répliquer vers au moins un autre serveur du cluster.

Ce mode de réplication ne respecte généralement pas les propriétés ACID
(Atomicité, Cohérence, Isolation, Durabilité) car si une copie échoue sur
l'autre primaire alors que la transaction a déjà été validée, on peut alors
arriver dans une situation où les données sont incohérentes entre les serveurs.
Généralement, ce type de système doit proposer un gestionnaire de conflits, de
préférence personnalisable, pour gérer ces cas de figure.

PostgreSQL ne supporte pas la réplication symétrique nativement.
Plusieurs projets ont tenté de remplir ce vide.

[BDR](https://www.enterprisedb.com/docs/bdr/latest/),
de 2nd Quadrant (à présent EDB), se base sur la réplication logique et une extension
propre au-dessus de PostgreSQL. BDR assure une réplication symétrique
de toutes les données concernées de plusieurs instances toutes liées aux autres,
et s'adresse notamment au cas de bases dont on a besoin dans des
lieux géographiquement très éloignés. La gestion des conflits
est automatique, mais les verrous ne sont pas propagés pour des raisons
de performance, ce qui peut donc poser des problèmes d'intégrité,
selon les options choisies.
Les données peuvent différer entre les nœuds. L'application doit tenir
compte de tout cela. Par exemple, il vaut mieux privilégier les UUID
pour éviter les conflits.
<!-- ref
https://www.enterprisedb.com/docs/bdr/latest/overview/
https://www.postgresql.org/message-id/flat/CANbhV-HsLrE%2BhpgFJXHG08k5h5HM%2B7g%2B4wMtkk_wR9GnbzMNKg%40mail.gmail.com#a946f363bf49824a83f038ad39e3d916
-->
Les premières versions de BDR étaient sous licence libre, mais ne
sont plus supportées, et la version actuelle (BDR4) est propriétaire et payante.
<!-- reference bienvenue -->

Si l'application le permet, il est possible de se rabattre sur un modèle où
chaque instance est le point d'entrée unique de certaines données (par exemple selon
la géographie), et les autres n'en ont que des copies en lecture, obtenues
d'une manière ou d'une autre, ou doivent accéder au serveur responsable en écriture.
Il s'agit alors plus d'une forme de _sharding_ que de véritable réplication symétrique.

</div>

---

### Réplication synchrone asymétrique

<div class="slide-content">

  * **Asymétrique**
    + écritures sur un serveur primaire unique
    + lectures sur le serveur primaire et/ou les secondaires
  * **Synchrone**
    + les écritures sur les secondaires sont immédiates
    + le client sait si sa commande a réussi sur plusieurs serveurs

</div>

<div class="notes">

Dans la réplication asymétrique, seul le serveur primaire accepte des écritures,
les secondaires ne sont accessibles qu'en lecture.

Dans la réplication synchrone, le client envoie sa requête en écriture sur le
serveur primaire, le serveur primaire l'écrit sur son disque, il envoie les
données au serveur secondaire
attend que ce dernier l'écrive sur son disque. Si tout ce processus
s'est bien passé, le client est averti que l'écriture a été réalisée avec
succès. Concrètement, un ordre `COMMIT` rend la main une fois l'écriture validée
sur plusieurs serveurs, généralement au moins deux (un primaire et un secondaire).
On utilise généralement un mécanisme dit de _Two Phase Commit_ ou
« validation en deux phases », qui assure qu'une transaction est validée sur tous
les nœuds dans la même transaction. Les propriétés ACID sont dans ce cas
respectées.

Le gros avantage de ce système est qu'il n'y a pas de risque de perte de
données quand le serveur primaire s'arrête brutalement et qu'il est nécessaire
de basculer sur un serveur secondaire. L'inconvénient majeur est que cela ralentit
fortement les écritures.

PostgreSQL permet d'ajuster ce ralentissement à la criticité.
Par défaut (paramètre `synchronous_commit` à `on`),
la réplication synchrone garantit que le serveur secondaire a bien écrit la transaction dans
ses journaux et qu'elle a été synchronisée sur son disque (`fsync`), en plus
de celui du primaire bien sûr. En revanche,
elle ne garantit pas forcément que le secondaire a bien rejoué la transaction : il peut se
passer un laps de temps où une lecture sur le secondaire serait
différente du serveur primaire (le temps que le secondaire rejoue la transaction).
PostgreSQL dispose d'un mode (`synchronous_commit` à `remote_apply`) qui permet d'avoir
la garantie que les modifications sont rejouées sur le secondaire, au prix d'une latence
supplémentaire. À l'inverse, on peut estimer qu'il est suffisant que les données
soient juste écrites dans la mémoire cache du serveur secondaire, et pas forcément sur disque,
pour être considérées comme répliquées (`synchronous_commit` à `remote_write`).
La synchronicité peut être désactivée pour les requêtes peu critiques (`synchronous_commit`
à `local`, voire `off`). Ce paramétrage peut être ajusté selon les besoins, requête par requête.
<!-- pas détailler plus, ça empiète déjà sur W2B -->

PostgreSQL permet aussi de disposer de plusieurs secondaires synchrones.

</div>

---

### Réplication synchrone symétrique

<div class="slide-content">

  * **Symétrique**
    + écritures sur les différents serveurs primaires
    + besoin d'un gestionnaire de conflits
    + lectures sur les différents serveurs
  * **Synchrone**
    + les écritures sur les autres serveurs sont immédiates
    + le client sait si sa commande est validée sur plusieurs serveurs
    + risque important de lenteur !

</div>

<div class="notes">

Ce système est le plus intéressant… en théorie. L'utilisateur peut se
connecter à n'importe quel serveur pour des lectures et des écritures. Il n'y
a pas de risque de perte de données, vu que la commande ne réussit que si les
données sont bien enregistrées sur tous les serveurs. Autrement dit, c'est le
meilleur système de réplication et de répartition de charge.

Dans les inconvénients, il faut gérer les éventuels conflits qui peuvent
survenir quand deux transactions concurrentes opèrent sur le même ensemble de
lignes. On résout ces cas particuliers avec des algorithmes plus ou moins
complexes. Il faut aussi accepter la perte de performance en écriture induite
par le côté synchrone du système.

PostgreSQL ne supporte pas la réplication symétrique, donc encore moins la symétrique synchrone.
Certains projets évoqués essaient ou ont essayé d'apporter cette fonctionnalité.

Le besoin d'une architecture « multiprimaire » revient régulièrement, mais il
faut s'assurer qu'il est réel. Avant d'envisager une architecture complexe, et
donc source d'erreurs, optimisez une installation asymétrique simple et
classique, quitte à multiplier les serveurs secondaires, et
testez ses performances : PostgreSQL pourrait bien vous surprendre !

Selon les cas d'utilisation, la réplication logique peut aussi être utile.

</div>

---

### Diffusion des modifications

<div class="slide-content">

  * Par requêtes
    + diffusion de la requête
  * Par triggers
    + diffusion des données résultant de l'opération
  * Par journaux, physique
    + diffusion des blocs disques modifiés
  * Par journaux, logique
    + extraction et diffusion des données résultant de l'opération depuis les journaux
</div>

<div class="notes">

La récupération des données de réplication se fait de différentes façons
suivant l'outil utilisé.

La diffusion de l'opération de mise à jour (donc **le SQL lui-même**) est
très flexible et compatible avec toutes les versions. Cependant, cela pose la
problématique des opérations dites non déterministes. L'insertion de la
valeur `now()` exécutée sur différents serveurs fera que les données seront
différentes, généralement très légèrement différentes, mais différentes
malgré tout. L'outil Pgpool, qui implémente cette méthode de réplication,
est capable de récupérer l'appel à la fonction `now()` pour la remplacer par
la date et l'heure. En effet, il connaît les différentes
fonctions de date et heure proposées en standard par PostgreSQL. Cependant, il
ne connaît pas les fonctions utilisateurs qui pourraient faire de même. Il est
donc préférable de renvoyer les données, plutôt que les requêtes.

Le renvoi du résultat peut se faire de deux façons : soit en récupérant les
nouvelles données avec un trigger, soit en récupérant les nouvelles données
dans les journaux de transactions.

Cette première solution est utilisée par un certain nombre d'outils externes
de réplication, comme autrefois Slony ou Bucardo. Les fonctions triggers étant
écrites en C, cela assure de bonnes performances. Cependant, seules les
modifications des données sont attrapables avec des triggers, les modifications
de la structure ne sont généralement pas gérées.
Autrement dit, l'ajout d'une table, l'ajout
d'une colonne demande une administration plus poussée, non automatisable.

La deuxième solution (par journaux de transactions) est bien plus intéressante
car les journaux contiennent toutes les modifications, données comme
structures. Il suffit au secondaire de réappliquer tous les journaux provenant du primaire
pour être à l'image exacte de celui-ci.
De ce fait, une fois mise en place, cette méthode requiert peu de maintenance.
PostgreSQL l'offre nativement depuis longtemps.

PostgreSQL permet, depuis la version 10, le décodage logique des modifications de
données correspondant aux blocs modifiés dans les journaux de transactions.
L'objectif est de permettre l'extraction logique des données écrites permettant
la mise en place d'une réplication logique des résultats entièrement intégrée,
donc sans triggers. Les modifications de structures doivent encore être gérées à la main.

</div>

---

## Réplication interne physique

<div class="slide-content">

  * Réplication
    + asymétrique
    + asynchrone (défaut) ou synchrone (et selon les transactions)
  * Secondaires
    + non disponibles (_Warm Standby_)
    + disponibles en lecture seule (_Hot Standby_)
    + cascade
    + retard programmé
</div>

<div class="notes">

La réplication physique de PostgreSQL est par défaut **asynchrone** et **asymétrique**.
Il est possible de sélectionner le mode synchrone/asynchrone pour chaque serveur
secondaire individuellement, et séparément pour chaque transaction, en modifiant le
paramètre `synchronous_commit`.

La réplication physique fonctionne par l'envoi des enregistrements des journaux de transactions, soit
par envoi de fichiers complets (on parle de _log shipping_), soit par envoi de
groupes d'enregistrements en flux (on parle là de _streaming replication_),
puisqu'il s'agit d'une réplication par diffusion de journaux.

La différence entre _Warm Standby_ et _Hot Standby_ est très simple :

  * un serveur secondaire en _Warm Standby_ est un serveur de secours sur lequel il n'est
pas possible de se connecter ;
  * un serveur secondaire en _Hot Standby_ accepte les connexions et permet l'exécution
de requêtes en lecture seule.

Un secondaire peut récupérer les informations de
réplication depuis un autre serveur secondaire (fonctionnement en cascade).

Le serveur secondaire peut aussi n'appliquer les informations de réplication qu'après un
délai configurable.

</div>

---

### Log Shipping

<div class="slide-content">

  * But :
    + envoyer les journaux de transactions à un secondaire
  * Première solution disponible
  * Gros inconvénients :
    + perte possible de plusieurs journaux
    + latence à la réplication
    + penser à `archive_timeout` ou `pg_receivewal`
</div>

<div class="notes">

Le _log shipping_ permet d'envoyer les journaux de transactions terminés sur un
autre serveur. Ce dernier peut être un serveur secondaire, en _Warm Standby_ ou en
_Hot Standby_, prêt à les rejouer.

Cependant, son gros inconvénient vient du fait qu'il faut attendre qu'un
journal soit complètement écrit pour qu'il soit propagé vers le secondaire.
Or, un journal de 16 Mo peut contenir plusieurs centaines de transactions !
Si l'archivage a du retard (grosse charge, réseau saturé…), plusieurs journaux
peuvent même être en attente.
Le retard du secondaire par rapport au primaire peut donc devenir
important, ce qui est gênant dans le cas d'un _standby_ utilisé
en lecture seule, par exemple dans le cadre d'une répartition de la charge de
lecture.

<div class="box warning">
En conséquence il est possible de perdre toutes les transactions contenues dans le
journal de transactions en cours, voire tous ceux en retard,
en cas de _failover_ et de destruction physique des journaux sur le primaire.
</div>

On peut cependant moduler le risque de trois façons:

  * sauf avarie très grave sur le serveur primaire, les journaux de
transactions en attente peuvent être récupérés et appliqués sur le serveur
secondaire ;
  * on peut réduire la fenêtre temporelle de la réplication en modifiant la
valeur de la clé de configuration `archive_timeout`. Au delà du délai
exprimé avec cette variable de configuration, le serveur change de journal de
transactions, provoquant l'archivage du précédent. On peut par exemple
envisager un `archive_timeout` à 30 secondes, et ainsi obtenir une réplication
à 30 secondes près. Attention toutefois, les journaux archivés font toujours
16 Mo, qu'ils soient archivés remplis ou non ;
  * on peut utiliser l'outil `pg_receivewal` (nommé `pg_receivexlog`
jusqu'en 9.6) qui crée à distance les journaux de transactions
à partir d'un flux de réplication.

</div>

---

### Streaming replication

<div class="slide-content">

  * But
    + avoir un retard moins important sur le serveur secondaire
  * Rejouer **les enregistrements de transactions** du serveur primaire par **paquets**
    + paquets plus petits qu'un journal de transactions

</div>

<div class="notes">

L'objectif du mécanisme de la _streaming replication_ est d'avoir un secondaire qui
accuse moins de retard. En effet, comme on vient de le voir, le _log shipping_ exige
d'attendre qu'un journal soit complètement rempli avant qu'il ne soit envoyé au
serveur secondaire.

La réplication par _streaming_ diminue ce retard en envoyant les
enregistrements des journaux de transactions par groupe bien inférieur à un
journal complet. Il introduit aussi deux processus gérant le transfert du
contenu des WAL entre le serveur primaire et le serveur secondaire. Ce flux est
totalement indépendant de l'archivage du WAL. Ainsi, en cas de perte du serveur
primaire, sauf retard à cause d'une saturation quelconque, la perte de données
est très faible.

Les délais de réplication entre le serveur primaire et le serveur secondaire sont très courts :
une modification sur le serveur primaire sera en effet très rapidement
répliqué sur un secondaire.

C'est une solution éprouvée et au point depuis des années. Néanmoins, elle a ses
propres inconvénients : réplication de l'instance complète,
architecture matérielle et version majeure de PostgreSQL
forcément identiques entre les serveurs du cluster, etc.

</div>

---

### _Warm Standby_

<div class="slide-content">

  * Serveur de secours
    + prêt à prendre le relai du primaire
    + (presque) identique au primaire
  * Différentes configurations selon les versions
    + asynchrone ou synchrone
    + application immédiate ou retardée
  * En pratique, préférer le _Hot Standby_

</div>

<div class="notes">

Le _Warm Standby_ existe depuis la version 8.2. La
robustesse de ce mécanisme simple est prouvée depuis longtemps.

Les journaux de transactions sont répliqués en _log shipping_ ou _streaming
replication_ selon la version, le besoin et les contraintes d'architecture.
Le serveur secondaire est en mode _recovery_ perpétuel et applique automatiquement
les journaux de transaction reçus.

Un serveur en _Warm Standby_ n'accepte aucune connexion entrante. Il n'est
utile que comme réplicat prêt à être promu en production à la place de l'actuel
primaire en cas d'incident.
Les serveurs secondaires sont donc généralement paramétrés
directement en _Hot Standby_.

</div>

---

### _Hot Standby_

<div class="slide-content">

  * Serveur secondaire
    + accepte les connexions entrantes
    + requêtes en lecture seule et sauvegardes
    + prêt à prendre le relai du primaire
  * Différentes configurations selon les versions
    + asynchrone ou synchrone
    + application immédiate ou retardée
</div>

<div class="notes">

Le _Hot Standby_ est une évolution du _Warm Standby_ :
il accepte les
connexions des utilisateurs et permet d'exécuter des requêtes en
lecture seule.

Ce serveur peut toujours remplir le rôle de serveur de secours,
tout en étant utilisable pour soulager le primaire : sauvegarde,
requêtage en lecture…

</div>

---

### Exemple

![Exemple d'architecture](medias/common/replication-simple.png)
\

<div class="notes">

Cet exemple montre un serveur primaire en _streaming replication_ vers un serveur
secondaire. Ce dernier est configuré en _Hot Standby_. Ainsi, les utilisateurs
peuvent se connecter sur le serveur secondaire pour les requêtes en lecture et sur le
primaire pour des lectures comme des écritures. Cela permet une forme de
répartition de charge des lectures, la répartition étant gérée par le
serveur d'applications ou par un outil spécialisé.

</div>

---

### Réplication interne

![Réplication interne](medias/common/replication-interne.png){ height=660 }
\

<div class="notes">

</div>

---

### Réplication en cascade

![Réplication en cascade](medias/common/replication-cascade.png){ height=660 }
\

<div class="notes">

</div>

---

## Réplication interne logique

<div class="slide-content">

  * Réplique les changements
    + d'une seule base de données
    + d’un ensemble de tables défini
  * Principe Éditeur/Abonnés
</div>

<div class="notes">

Contrairement à la réplication physique, la réplication logique ne réplique pas
les blocs de données. Elle décode le **résultat** des requêtes qui sont transmis
au secondaire. Celui-ci applique les modifications issues du flux de
réplication logique.

La réplication logique utilise un système de publication/abonnement avec un ou
plusieurs « abonnés » qui s'abonnent à une ou plusieurs « publications » d'un nœud
particulier.

Une publication peut être définie sur n'importe quel serveur primaire.
Le nœud sur laquelle la publication est définie est nommé « éditeur ». Le nœud
où un abonnement a été défini est nommé « abonné ».

Une publication est un ensemble de modifications générées par une table ou un
groupe de table. Chaque publication existe au sein d'une seule base de données.

Un abonnement définit la connexion à une autre base de données et un ensemble
de publications (une ou plus) auxquelles l'abonné veut souscrire.

La réplication logique est disponible depuis la version 10 de PostgreSQL.
Contrairement à la réplication physique, elle s'effectue entre instances
primaires, toutes deux ouvertes en écriture avec leurs tables propres.
Rien n'interdit à une instance abonnée pour certaines tables de proposer ses
propres publications, même de tables concernées par un abonnement.

</div>

---

### Réplication logique - Fonctionnement

<div class="slide-content">

  * Création d'une publication sur un serveur
  * Souscription d'un autre serveur à cette publication
  * Limitations :
    + DDL, Large objects, séquences, tables étrangères et vues matérialisées non répliqués
    + peu adaptée pour un _failover_
</div>

<div class="notes">

<!-- Inutile de détailler trop, il y a tout dans W5 -->

Une « publication » est créée sur le serveur éditeur et ne concerne que certaines tables.
L'abonné souscrit à cette publication, c’est un « souscripteur ».
Un processus spécial est lancé : le _logical replication worker_.
Il va se connecter à un slot de réplication sur le serveur éditeur.
Ce dernier va procéder à un décodage logique de ses propres journaux de transaction
pour extraire les résultats des ordres SQL (et non les ordres eux-mêmes).
Le flux logique est transmis à l'abonné qui les applique sur les tables.

La réplication logique possède quelques limitations.
La principale est que seules les données sont répliquées, c'est-à-dire le résultat des ordres
`INSERT`, `DELETE`, `UPDATE`, `TRUNCATE` (sauf en v10 pour ce dernier).
Les tables cible doivent être créés manuellement,
et il faudra dès lors répliquer manuellement les changements de structure.

Il n'est pas obligatoire de conserver strictement la même structure des deux côtés.
Mais, afin de conserver sa cohérence, la réplication s'arrêtera en cas de conflit.
Des clés primaires sur toutes les tables concernées sont fortement conseillées.
Les *large objects* ne sont pas répliqués. Les séquences non plus, y compris
celles utilisées par les colonnes de type `serial`.
Notez que pour éviter des effets de bord sur la cible,
les triggers des tables abonnées ne seront pas déclenchés par les modifications
reçues via la réplication.

En principe, il serait possible d'utiliser la réplication logique en
vue d'un _failover_ vers un serveur de secours en propageant manuellement les mises à jour de
séquences et de schéma. La réplication physique est cependant plus appropriée
et plus efficace pour cela.

La réplication logique vise d'autres objectifs, tels la génération de rapports
sur une instance séparée ou la migration vers une version majeure de PostgreSQL
avec une indisponibilité minimale.

</div>

---

## Réplication externe

<div class="slide-content">

  * Outils les plus connus :
    + Pgpool
    + Slony, Bucardo (abandonnés)
    + pgLogical
  * Niches

</div>

<div class="notes">

Jusqu'en 2010, PostgreSQL ne disposait pas d'un système de réplication évolué,
et plus d'une quinzaine de projets ont tenté de combler ce vide.
L'arrivée de la réplication logique en version 10 met à mal les derniers survivants.
En pratique, ces outils ne comblent plus que des niches.

La liste exhaustive est trop longue pour que l'on puisse évoquer en détail
chacune des solutions, surtout que la plupart sont
obsolètes ou ne semblent plus maintenues. Voici les plus connues :

  * Slony (réplication par trigger, éprouvé et fiable… mais qui n'est plus maintenu) ; <!-- https://www.slony.info/pipermail/slony1-general/2024-March/013559.html -->
  * Bucardo (réplication par trigger, ne semble plus maintenu) ;
  * Pgpool (réplication d'ordres SQL parmi d'autres fonctionnalités ; toujours actif) ;
  * pgLogical (réplication logique, toujours actif) ;
<!-- ci dessous ce qui est vraiment mort -->
  * Londiste ;
  * PGCluster ;
  * Mammoth Replicator/Replicator ;
  * Daffodil Replicator ;
  * RubyRep ;
  * pg_comparator ;
  * Postgres-X2 (ex-Postgres-XC)…

Attention à ne pas confondre ces outils avec d'autres qui utilisent
la réplication interne de PostgreSQL pour gérer un cluster de haute
disponibilité (notamment [repmgr](https://dali.bo/w3_html#repmgr), [PAF](https://dali.bo/w3_html#paf)
ou [Patroni](https://dali.bo/hapat_pdf)), mais ne s'occupent pas de
la réplication par eux-même.

Pour les détails sur ces outils et d'autres,
[voir le wiki](https://wiki.postgresql.org/wiki/Replication,_Clustering,_and_Connection_Pooling)
ou cet article : [Haute disponibilité avec PostgreSQL](https://public.dalibo.com/exports/conferences/_archives/_2011/201102_haute_disponibilite_avec_postgresql/solutionsha.pdf), Guillaume Lelarge, 2009.

</div>

---


## Sharding

<div class="slide-content">

  * Répartition des données sur plusieurs instances
  * Évolution horizontale en ajoutant des serveurs
  * Parallélisation
  * Clé de répartition cruciale
  * Administration complexifiée
  * Sous PostgreSQL :
     + _Foreign Data Wrapper_
     + PL/Proxy
     + Citus (extension), et nombreux _forks_
</div>

<div class="notes">

Le _sharding_ n'est pas de la réplication, ce serait même l'inverse.
Le principe consiste à répartir les données sur plusieurs instances
différentes, chacune étant responsable d'une partie des données, et
ouverte en écriture.

La volumétrie peut augmenter, il suffit de rajouter des serveurs. Plusieurs
serveurs peuvent travailler en parallèle sur la même requête. On contourne
ainsi le principal goulet d'étranglement des performances : les entrées-sorties.

Le problème fondamental est la clé de répartitions des données sur
les différents serveurs. Un cas simple est la répartition des données
de nombreux clients dans plusieurs instances, chaque client n'étant
présent que dans une seule instance. On peut aussi opérer une sorte
de _hash_ de la clé pour répartir équitablement les données sur
les serveurs.  Il faut aviser en fonction de la charge prévue, de la
nécessité d'éviter les conflits lors des mises à jour, du besoin de
croiser les données en masse, des purges à faire de temps à autre,
et de la manière de répartir harmonieusement les écritures et lectures
entre ces instances. C'est au client ou à une couche d'abstraction de
savoir quel(s) serveur(s) interroger.

PostgreSQL n'implémente pas directement le _sharding_. Plusieurs techniques et
outils permettent de le mettre en place.

  * Les _Foreign Data Wrappers_ (et l'extension `postgres_fdw` en particulier)
permettent d'accéder à des données présentes sur d'autres
serveurs. La capacité de `postgres_fdw` à « pousser » filtres et jointures
vers les serveurs distants s'améliore de version en
version. Des tables distantes peuvent être montées en tant que partitions d'une
table mère. Les insertions dans une table partitionnée
peuvent même être redirigées vers une partition distante de manière
transparente. Vous trouverez un exemple dans
[cet article](https://pgdash.io/blog/postgres-11-sharding.html)
ou à la fin du [module de formation V1](https://dali.bo/v1_html#tables-distantes-sharding)
Le parcours simultané des partitions distantes est même possible à partir de
[PostgreSQL 14](https://dali.bo/workshop14_html#lecture-asynchrone-des-tables-distantes).
La réplication logique peut synchroniser des tables non distribuées sur les instances.
<!-- tant pis pour les limitations -->

  * PL/Proxy est une extension qui permet d'appeler plusieurs hôtes
distants à la fois avec un seul appel de fonctions. Elle existe depuis des
années. Son inconvénient majeur est la nécessité de réécrire tous les appels
à distribuer par des fonctions, on ne peut pas se reposer sur le SQL de manière
transparente.

  * [Citus](https://www.citusdata.com/product)
est une extension libre dont le but est
de rendre le _sharding_ transparent, permettant de garder la compatibilité avec
le SQL habituel. Des nœuds sont déclarés auprès du serveur principal (où les
clients se connectent), et quelques fonctions permettent de déclarer les tables
comme distribuées selon une clé (et découpées entre les serveurs) ou tables de
références (dupliquées partout pour faciliter les jointures). Les requêtes sont
redirigées vers le bon serveur selon la clé, ou réellement parallélisées
sur les différents serveurs concernés. Le projet est vivant, bien documenté,
et a bonne réputation. Depuis son rachat par Microsoft en 2019, Citus assure ses 
revenus grâce à une offre disponible sur le cloud Azure. Ceci a permis en 2022 
de libérer les fonctionnalités payantes et de publier l'intégralité projet en 
open-source.

Le _sharding_ permet d'obtenir des performances impressionnantes, mais il a ses
inconvénients :

  * plus de serveurs implique plus de sources de problèmes, de supervision,
de tâches d'administration (chaque serveur a potentiellement son secondaire, sa
réplication…) ;
  * le modèle de données doit être adapté au problème, limitant les interactions
d'un nœud avec les autres ; le choix de la clé est crucial ;
  * les propriétés ACID et la cohérence sont plus difficilement respectées
dans ces environnements.

Historiquement, plusieurs _forks_ de PostgreSQL ont été développés en partie
pour faire du _sharding_, principalement en environnement OLAP/décisionnel,
comme [PostgreSQL-XC/XL](https://www.2ndquadrant.com/fr/resources-old/postgres-xl/), à présent disparu,
ou [Greenplum](https://greenplum.org/), qui existe toujours.<!-- 12/2023 -->
Ces _forks_ ont plus ou moins de difficultés à suivre
la rapidité d'évolution de la version communautaire : les choisir implique de se
passer de certaines nouveautés. D'où le choix de Citus
de la forme d'une extension.

Ce domaine est l'un de ceux où PostgreSQL devrait beaucoup évoluer dans les
années à venir, comme le décrivait
[Bruce Momjian en septembre 2018](https://momjian.us/main/writings/pgsql/sharding.pdf).

</div>
<!--
Résumé de cet include :
  * Répartition des données sur plusieurs instances
  * Évolution horizontale en ajoutant des serveurs
  * Parallélisation
  * Clé de répartition cruciale
  * Administration complexifiée
  * Sous PostgreSQL :
     + _Foreign Data Wrapper_
     + PL/Proxy
     + Citus (extension), et nombreux _forks_

-->

---

## Réplication bas niveau

<div class="slide-content">

  * RAID
  * DRBD
  * SAN Mirroring
  * À prendre évidemment en compte…
</div>

<div class="notes">

Même si cette présentation est destinée à détailler les solutions logicielles de
réplication pour PostgreSQL, on peut tout de même évoquer les
solutions de réplication de « bas niveau », voire matérielles.

De nombreuses techniques matérielles viennent en complément essentiel des
technologies de réplication utilisées dans la haute disponibilité.
Leur utilisation est parfois incontournable, du RAID en passant par les
SAN et autres techniques pour redonder l'alimentation, la
mémoire, les processeurs, etc.

</div>

---

### RAID

<div class="slide-content">

  * Obligatoire
  * Fiabilité d'un serveur
  * RAID 1 ou RAID 10
  * RAID 5 déconseillé (performances)
  * Lectures plus rapides
    + dépend du nombre de disques impliqués
</div>

<div class="notes">

Un système RAID 1 ou RAID 10 permet d'écrire les mêmes données sur plusieurs
disques en même temps. Si un disque meurt, il est possible d'utiliser l'autre
disque pour continuer. C'est de la réplication bas niveau. Le disque
défectueux peut être remplacé sans interruption de service. Ce n'est pas une
réplication entre serveurs mais cela contribue à la haute-disponibilité du
système.

<!-- est-ce toujours aussi vrai pour le RAID5/6 avec le matériel actuel ? -->
Le RAID 5 offre les mêmes avantages en gaspillant moins d'espace qu'un RAID 1,
mais il est déconseillé pour les bases de données (PostgreSQL comme
ses concurrents) à cause des performances en écriture, au quotidien comme lors
de la reconstruction d'une grappe après remplacement d'un disque.

Le système RAID 10 est plus intéressant pour les fichiers de données alors
qu'un système RAID 1 est suffisant pour les journaux de transactions.

Le RAID 0 (répartition des écritures sur plusieurs disques sans redondance) est
évidemment à proscrire.

</div>

---

### DRBD

<div class="slide-content">

  * Simple / synchrone / Bien documenté
  * Lent / Secondaire inaccessible / Linux uniquement
</div>

<div class="notes">

DRBD est un outil capable de répliquer le contenu d'un périphérique blocs. En
ce sens, ce n'est pas un outil spécialisé pour PostgreSQL contrairement aux
autres outils vus dans ce module. Il peut très bien servir à répliquer des
serveurs de fichiers ou de mails. Il réplique les données en temps réel et de
façon transparente, pendant que les applications modifient leur fichiers sur un
périphérique. Il peut fonctionner de façon synchrone ou asynchrone. Tout ça
en fait donc un outil intéressant pour répliquer le répertoire des données
de PostgreSQL.

Pour plus de détails, consulter
[cet article](https://public.dalibo.com/archives/publications/hs45_drbd_la_replication_des_blocs_disques.pdf)
de Guillaume Lelarge dans Linux Magazine Hors Série 45.

DRBD est un
système simple à mettre en place. Son gros avantage est la possibilité
d'avoir une réplication synchrone. Ses inconvénients sont sa lenteur, la
non-disponibilité des secondaires et un volume de données plus important à
répliquer (WAL + tables + index + vues matérialisées…).

</div>

---

### SAN Mirroring

<div class="slide-content">

  * Comparable à DRBD
  * Solution intégrée
  * Manque de transparence
</div>

<div class="notes">

La plupart des constructeurs de baie de stockage proposent des systèmes de
réplication automatisés avec des mécanismes de _failover_/_failback_ parfois
sophistiqués. Ces solutions présentent peu ou prou les mêmes
caractéristiques, avantages et inconvéniants que DRBD. Ces technologies ont en revanche le défaut d'être
opaques et de nécessiter une main d’œuvre hautement qualifiée.

</div>

---

## Conclusion

<div class="slide-content">

Quelle que soit la solution envisagée :

  * Bien définir son besoin
  * Identifier tous les _SPOF_
  * Superviser son _cluster_
  * Tester régulièrement les procédures de _failover_ (Loi de Murphy…)
</div>

<div class="notes">

**Bibliographie :**

  * « [Haute disponibilité, répartition de charge et réplication](https://docs.postgresql.fr/current/high-availability.html) » (documentation officielle)


</div>

---

### Questions

<div class="slide-content">

N'hésitez pas, c'est le moment !
</div>

<div class="notes">

</div>

---

