---
revision: 24.12
date: "Année académique 2024-2025"

licence : "Creative Commons BY-NC-SA"
include_licence : CC-BY-NC-SA-2.0-FR

author: © 2005-2025 DALIBO SARL SCOP

trademarks: |
  Le logo éléphant de PostgreSQL (« Slonik ») est une création sous copyright et
  le nom « PostgreSQL » est une marque déposée par PostgreSQL Community Association
  of Canada.

##
## PDF Options
##

## Limiter la profondeur de la table des matières
toc-depth: 2

## Mettre les lien http en pieds de page
links-as-notes: false

## Police plus petite dans un bloc de code

code-blocks-fontsize: small

## Filtre : pandoc-latex-admonition
## les catégories `important` et `warning` sont synonymes
## même chose pour `tip` et `note`
pandoc-latex-admonition:
  - color: Red
    classes: [warning]
    linewidth: 4
  - color: Red
    classes: [important]
    linewidth: 4
  - color: DarkSeaGreen
    classes: [tip]
    linewidth: 4
  - color: DarkSeaGreen
    classes: [note]
    linewidth: 4
  - color: DodgerBlue
    classes: [slide-content]
    linewidth: 4

##
## Reveal Options
##

## Taille affichage
width: 960
height: 700

## beige/blood/moon/simple/solarized/black/league/night/serif/sky/white
theme: white

## None - Fade - Slide - Convex - Concave - Zoom
transition: None
transitionSpeed: fast

## Barre de progression
progress: true

## Affiche N° de slide
slideNumber: true

## Le numero de slide apparait dans la barre d'adresse
history: true

## Defilement des slides avec la roulette
mouseWheel: true

## Annule la transformation uppercase de certains thèmes
title-transform : none

## Cache l'auteur sur la première slide
## Mettre en commentaire pour désactiver
hide_author_in_slide: true

title : 'Haute-Disponibilité avec PostgreSQL'
subtitle : 'FCU Calais - M2 I2L'
---

# Licence

<div class="slide-content">
Le contenu pédagogique est issu des supports de formation de la société [Dalibo],
distribués sous licence _Creative Commons [CC-BY-NC-SA]_.

![cc](src/img/cc_blue.png)
![by](src/img/attribution_icon_blue.png)
![nc](src/img/nc_blue.png)
![sa](src/img/sa_blue.png)

[Dalibo]: https://www.dalibo.com/formations
[CC-BY-NC-SA]: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
</div>

<div class="notes">
Vous devez citer le nom de l'auteur original de la manière indiquée par l'auteur
de l'œuvre ou le titulaire des droits qui vous confère cette autorisation (mais
pas d'une manière qui suggérerait qu'ils vous soutiennent ou approuvent votre 
utilisation de l'œuvre).

Vous n'avez pas le droit d'utiliser cette création à des fins commerciales.

Si vous modifiez, transformez ou adaptez cette création, vous n'avez le droit de 
distribuer la création qui en résulte que sous un contrat identique à celui-ci.

À chaque réutilisation ou distribution de cette création, vous devez faire 
apparaître clairement au public les conditions contractuelles de sa mise à 
disposition. La meilleure manière de les indiquer est un lien vers cette page web.

Chacune de ces conditions peut être levée si vous obtenez l'autorisation du 
titulaire des droits sur cette œuvre.

Rien dans ce contrat ne diminue ou ne restreint le droit moral de l'auteur ou des 
auteurs.

Le texte complet de la licence est disponible à cette adresse: 
<https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.fr>
</div>

-----

# Formateur

<div class="slide-content">
* Nom : Florent Jardin 
* Profession : Consultant PostgreSQL chez Dalibo
* Hobbies : Course à pied, jeux vidéo, musique

![](https://fljd.in/img/avatar.jpg)
</div>

<div class="notes">
Pour me contacter :

* Site internet <https://fljd.in/a-propos>
* Mastodon <https://fosstodon.org/@fljdin>
* Github <https://github.com/fljdin>
* LinkedIn <https://www.linkedin.com/in/florent-jardin>
</div>

-----

# Présentation du cours

<div class="slide-content">

- Formation : Haute-Disponibilité avec PostgreSQL
- Durée : 2 jours (15h)
- Cursus M2 I2L de l'Université du Littoral Côte d'Opale (_ULCO_)

</div>

---

## Objectifs

<div class="slide-content">
* Mettre à jour PostgreSQL
* Découvrir les mécanismes de réplication
* Comprendre les concepts de la haute disponibilité en général
* Savoir mettre en place la réplication physique
* Administrer un cluster de serveurs répliqués
* Connaître les outils tiers de Haute-Disponibilité
</div>

<div class="notes">
Le présent cursus s'adresse aux étudiants ayant des notions en administration de
PostgreSQL sous Linux (distributions RedHat ou affiliés).

<!-- B (extrait) Mise à jour de PostgreSQL -->
1. Mettre à jour PostgreSQL

La mise à jour majeure est une opération délicate provoquant une interruption
de service. Elle est nécessaire pour bénéficier des améliorations, des corrections
et des innovations apportées par la nouvelle version. Ce module explique les
différentes approches possibles pour mettre à jour PostgreSQL, afin de maîtriser
les risques et la durée totale d’interruption.

<!-- R50 - Généralités sur la Haute-Disponibilité -->
2. Comprendre les concepts de la haute disponibilité en général

La haute disponibilité est un sujet complexe. Plusieurs outils libres coexistent
au sein de l’écosystème PostgreSQL, chacun abordant le sujet d’une façon différente.
Cette partie clarifie la définition de « haute disponibilité », des méthodes
existantes et des contraintes à considérer. L’objectif est d’aider à la prise de
décision et le choix de la solution.

<!-- W1 - Architecture de Haute-Disponibilité -->
3. Découvrir les mécanismes de réplication

La réplication est le processus de partage d’informations permettant de garantir
la sécurité et la disponibilité des données entre plusieurs serveurs et plusieurs
applications. Les expressions telles que « cluster », « actif/passif » ou
« primaire/secondaire » peuvent avoir un sens différent selon le SGBD choisi. Dès
lors, il devient difficile de comparer et de savoir ce que désignent réellement
ces termes. C’est pourquoi nous débuterons ce module par un rappel théorique et
conceptuel. Nous nous attacherons ensuite à citer les outils de réplication,
internes et externes.

<!-- W2A - Réplication physique -->
4. Savoir mettre en place la réplication physique

La réplication physique de PostgreSQL existe depuis la version 8.2 avec la
réplication par fichier. Au fil des versions, de nouvelles fonctionnalités sont
apparues. Avec la version 10, PostgreSQL dispose d’une mécanique de réplication
en flux très complète. Ce module permet de comprendre les principes derrière ce
type de réplication, sa mise en place et son administration.

<!-- W2B - Réplication physique avancée -->
5. Administrer un cluster de serveurs répliqués

En complément du module précédent, il est important de bien superviser un cluster
en réplication, d’en connaître les limites mais aussi d’appréhender toutes les
possibilités offertes par la réplication physique.

<!-- W3 - Outils de réplication -->
6. Connaître les outils tiers de Haute-Disponibilité

Nous aborderons dans ce module différents outils externes qui peuvent nous aider
à administrer notre cluster de réplication.
</div>

-----

## Matériel pédagogique

<div class="slide-content">
* Oracle VirtualBox
* Alma Linux 9
* Terminal SSH
* DBeaver (optionnel)
</div>

<div class="notes">

Le module nécessite un système GNU/Linux pour couvrir l'ensemble des travaux
pratiques. Pour les postes Windows, il est encouragé d'installer [Oracle
VirtualBox][00-1] et de configurer une machine virtuelle avant le début des
cours. Pour des soucis d'utilisation, **il est vivement conseillé d'activer un
transfert de port entre la machine virtuelle et le poste hôte**.

[00-1]: https://www.virtualbox.org/wiki/Downloads

Les corrections des exercices sont adaptées pour le système Alma Linux 9 ou
équivalent. L'image de la distribution `x86_64` est disponible au
[téléchargement][00-2]. Pour celles et ceux plus familiers au format _Vagrant_,
un exemple de fichier de démarrage est proposé dans la [documentation][00-3].

[00-2]: https://almalinux.org/fr/get-almalinux/
[00-3]: https://app.vagrantup.com/almalinux/boxes/9

Les principaux outils requis pour progresser dans ce module sont un terminal SSH
et un éditeur de requêtes SQL. Le logiciel [DBeaver][00-4] Communautaire permet
d'ouvrir une connexion à une instance PostgreSQL sur le port par défaut 5432 de
la machine virtuelle et de naviguer dans les schémas tout en exécutant des
requêtes.

[00-4]: https://dbeaver.io/download/
</div>
