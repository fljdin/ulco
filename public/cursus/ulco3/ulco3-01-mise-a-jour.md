---
revision: 24.12
date: "Année académique 2024-2025"

licence : "Creative Commons BY-NC-SA"
include_licence : CC-BY-NC-SA-2.0-FR

author: © 2005-2025 DALIBO SARL SCOP

trademarks: |
  Le logo éléphant de PostgreSQL (« Slonik ») est une création sous copyright et
  le nom « PostgreSQL » est une marque déposée par PostgreSQL Community Association
  of Canada.

##
## PDF Options
##

## Limiter la profondeur de la table des matières
toc-depth: 2

## Mettre les lien http en pieds de page
links-as-notes: false

## Police plus petite dans un bloc de code

code-blocks-fontsize: small

## Filtre : pandoc-latex-admonition
## les catégories `important` et `warning` sont synonymes
## même chose pour `tip` et `note`
pandoc-latex-admonition:
  - color: Red
    classes: [warning]
    linewidth: 4
  - color: Red
    classes: [important]
    linewidth: 4
  - color: DarkSeaGreen
    classes: [tip]
    linewidth: 4
  - color: DarkSeaGreen
    classes: [note]
    linewidth: 4
  - color: DodgerBlue
    classes: [slide-content]
    linewidth: 4

##
## Reveal Options
##

## Taille affichage
width: 960
height: 700

## beige/blood/moon/simple/solarized/black/league/night/serif/sky/white
theme: white

## None - Fade - Slide - Convex - Concave - Zoom
transition: None
transitionSpeed: fast

## Barre de progression
progress: true

## Affiche N° de slide
slideNumber: true

## Le numero de slide apparait dans la barre d'adresse
history: true

## Defilement des slides avec la roulette
mouseWheel: true

## Annule la transformation uppercase de certains thèmes
title-transform : none

## Cache l'auteur sur la première slide
## Mettre en commentaire pour désactiver
hide_author_in_slide: true

title : 'Mise à jour de PostgreSQL'
subtitle : 'FCU Calais - M2 I2L'
---

# Mise à jour de PostgreSQL

![PostgreSQL](medias/logos/slonik.png){ height=500 }
\

<div class="notes">
</div>

---

## Les versions de PostgreSQL

<div class="slide-content">

  * Historique
  * Numérotation
  * Versions mineures et majeures

</div>

<div class="notes">
</div>

---

### Historique

<!-- TODO pour la mise à jour périodique de l'image :
   récupérer l'image de Wikipédia sur https://en.wikipedia.org/wiki/Template:Timeline_PostgreSQL
-->

![ ](medias/wikipedia/postgresql_release_timeline.png){ height=450 }

<div class="notes">

Sources : [page Wikipédia de PostgreSQL](https://en.wikipedia.org/wiki/PostgreSQL)
et [PostgreSQL Versioning Policy](https://www.postgresql.org/support/versioning/)

</div>

---

### Numérotation

<div class="slide-content">

  * Version récentes (10+)
    + X : version majeure (10, 11, … 16)
    + X.Y : version mineure (14.8, 15.3)
  * Avant la version 10 (toutes périmées !)
    + X.Y : version majeure (8.4, 9.6)
    + X.Y.Z : version mineure (9.6.24)

</div>

<div class="notes">

Une version majeure apporte de nouvelles fonctionnalités, des changements de
comportement, etc. Une version majeure sort généralement tous les ans
à l'automne. Une migration majeure peut se faire directement depuis n'importe quelle
version précédente.
Le numéro est incrémenté chaque année (version 12 en 2019, version 16 en 2023).

<!-- Une partie du texte correspond au slide suivant -->
Une version mineure ne comporte que des corrections de bugs ou de failles de
sécurité. Les publications de versions mineures sont plus fréquentes que
celles de versions majeures, avec un rythme de sortie trimestriel, sauf bug
majeur ou faille de sécurité. Chaque bug est corrigé dans toutes les versions
stables actuellement maintenues par le projet. Le numéro d'une version mineure
porte deux chiffres. Par exemple, en mai 2023 sont sorties les versions
15.3, 14.8, 13.11, 12.15 et 11.20.

<div class="box warning">
Avant la version 10, les versions majeures annuelles portaient deux
chiffres : 9.0 en 2010, 9.6 en 2016.
Les mineures avaient un numéro de plus (par exemple 9.6.24).
Cela a entraîné quelques
confusions, d'où le changement de numérotation.
Il va sans dire que ces versions sont totalement périmées et
ne sont plus supportées, mais beaucoup continuent de fonctionner.
</div>

</div>

---

### Versions mineures

<div class="slide-content">

De M.m  à  M.m+n :

  * En général chaque trimestre
  * Et sans souci
    + *Release notes*
    + tests
    + mise à jour des binaires
    + redémarrage

</div>

<div class="notes">
<!-- Une partie du slide est dans le texte ci-dessus -->
Une mise à jour mineure consiste à mettre à jour vers une nouvelle
version de la même branche majeure, par exemple de 14.8 à 14.9,
ou de 16.0 à 16.1 (mais pas d'une version 14.x à une version 16.x).
Les mises à jour des versions mineures sont cumulatives :
vous pouvez mettre à jour une instance 15.0 en version 15.5 sans
passer par les versions 15.1 à 15.4 intermédiaires.

En général, les mises à jour mineures se font sans souci et ne nécessitent
que le remplacement des binaires et un
redémarrage (et donc une courte interruption).
Les fichiers de données conservent le même format.
Des opérations supplémentaires sont possibles mais rarissimes.
Mais comme pour toute mise à jour, il convient d'être prudent sur
d'éventuels effets de bord. En particulier, il faudra lire les *Release Notes* et, si
possible, effectuer les tests ailleurs qu'en production.

</div>

---

### Versions majeures

<div class="slide-content">

  * 1 version majeure par an
    + maintenue 5 ans
  * [Dernières mises à jour mineures](https://www.postgresql.org/support/versioning/) (au 9/11/2023) :
    - version [11.22](https://www.postgresql.org/docs/current/release-11-22.html) (dernière !)
    - version [12.17](https://www.postgresql.org/docs/current/release-12-17.html)
    - version [13.13](https://www.postgresql.org/docs/current/release-13-13.html)
    - version [14.10](https://www.postgresql.org/docs/current/release-14-10.html)
    - version [15.5](https://www.postgresql.org/docs/current/release-15-5.html)
    - version [16.1](https://www.postgresql.org/docs/current/release-16-1.html)
  * Prochaine sortie de versions mineures prévue : 8 février 2024

</div>

<div class="notes">

La philosophie générale des développeurs de PostgreSQL peut se résumer ainsi :

<div class="box tip">

« Notre politique se base sur la qualité, pas sur les dates de sortie. »

</div>

Toutefois, même si cette philosophie reste très présente parmi les
développeurs, en pratique
une version stable majeure paraît tous les ans, habituellement à l'automne.
Pour ne pas sacrifier la qualité des versions, toute fonctionnalité
supposée insuffisamment stable est repoussée à la version suivante.
Il est déjà arrivé que la sortie de la version majeure soit repoussée
à cause de bugs inacceptables.

La tendance actuelle est de garantir un support pour chaque version majeure
pendant une durée minimale de 5 ans. Ainsi ne sont plus supportées
les versions 10 depuis novembre 2022 et 11 depuis novembre 2023.
Il n'y aura pour elles plus aucune mise à jour mineure, donc
plus de correction de bug ou de faille de sécurité.
Le support de la dernière version majeure, la 16, devrait durer jusqu'en 2028.

<div class="box tip">

Pour plus de détails :

  * [Politique de versionnement](https://www.postgresql.org/support/versioning/) ;
  * [Dates prévues des futures versions](https://www.postgresql.org/developer/roadmap/).

</div>

</div>

---

## Recommandations

<div class="slide-content">

  * Les *Release Notes*
  * Intégrité des données
  * Bien redémarrer le serveur !

</div>

<div class="notes">

Chaque nouvelle version de PostgreSQL est accompagnée d'une note expliquant les
améliorations, les corrections et les innovations apportées par cette version,
qu'elle soit majeure ou mineure. Ces notes contiennent toujours une section
dédiée aux mises à jour dans laquelle se trouvent des conseils essentiels.

Les _Releases Notes_ sont présentes dans
[l'annexe E de la documentation officielle](https://docs.postgresql.fr/current/release.html).

Les données de votre instance PostgreSQL sont toujours compatibles d'une
version mineure à l'autre. Ainsi, les mises à jour vers une version mineure
supérieure peuvent se faire sans migration de données, sauf cas exceptionnel
qui serait alors précisé dans les notes de version. Par exemple, de la 15.3
à la 15.4, il a été
[recommandé de reconstruire les index de type BRIN](https://www.postgresql.org/docs/release/15.4/)
pour prendre en compte une correction de bug les concernant.
Autre exemple : à partir de la [10.3](https://www.postgresql.org/docs/release/10.3/),
`pg_dump` a
imposé des noms d'objets qualifiés pour des raisons de sécurité, ce qui a
posé problème pour certains réimports.

Pensez éventuellement à faire une sauvegarde préalable par
sécurité.

À contrario, si la mise à jour consiste en un changement de version majeure
(par exemple, de la 9.6 à la 16), il est nécessaire de s'assurer que
les données seront transférées correctement sans incompatibilité. Là
encore, il est important de lire les *Releases Notes* **avant** la mise à
jour.

Le site <https://why-upgrade.depesz.com/>, basé sur les release notes,
permet de compiler les différences entre plusieurs versions
de PostgreSQL.

Dans tous les cas, pensez à bien redémarrer le serveur. Mettre à jour les
binaires ne suffit pas.

</div>

---

## Mise à jour mineure

<div class="slide-content">

  * Méthode
    + arrêter PostgreSQL
    + mettre à jour les binaires
    + redémarrer PostgreSQL
  * Pas besoin de s'occuper des données, sauf cas exceptionnel
    + bien lire les *Release Notes* pour s'en assurer

</div>

<div class="notes">

Faire une mise à jour mineure est simple et rapide.

La première action est de lire les *Release Notes* pour s'assurer qu'il n'y
a pas à se préoccuper des données. C'est généralement le cas mais il est
préférable de s'en assurer avant qu'il ne soit trop tard.

La deuxième action est de faire la mise à jour. Tout dépend de la façon dont
PostgreSQL a été installé :

  * par compilation, il suffit de remplacer les anciens binaires par les nouveaux ;
  * par paquets précompilés, il suffit d'utiliser le système de paquets
 (`apt` sur Debian et affiliés, `yum` ou `dnf` sur Red Hat et affiliés) ;
  * par l'installeur graphique, en le ré-exécutant.

Ceci fait, un redémarrage du serveur est nécessaire. Il est intéressant de
noter que les paquets Debian s'occupent directement de cette opération.
Il n'est donc pas nécessaire de le refaire.

</div>

---

## Mise à jour majeure

<div class="slide-content">

  * Bien lire les *Release Notes*
  * Bien tester l'application avec la nouvelle version
    + rechercher les régressions en terme de fonctionnalités et de performances
    + penser aux extensions et aux outils
  * Pour mettre à jour
    + mise à jour des binaires
    + et mise à jour/traitement des fichiers de données
  * 3 méthodes
    + dump/restore
    + réplication logique, externe (Slony) ou interne
    + `pg_upgrade`

</div>

<div class="notes">

Faire une mise à jour majeure est une opération complexe à préparer
prudemment.

La première action là-aussi est de lire les *Release Notes* pour bien
prendre en compte les régressions potentielles en terme de fonctionnalités
et/ou de performances. Cela n'arrive presque jamais mais c'est possible malgré
toutes les précautions mises en place.

La deuxième action est de mettre en place un serveur de tests où se trouve la
nouvelle version de PostgreSQL avec les données de production. Ce serveur sert
à tester PostgreSQL mais aussi, et même surtout, l'application. Le but est de
vérifier encore une fois les régressions possibles.

N'oubliez pas de tester les extensions non officielles, voire développées en
interne, que vous avez installées. Elles sont souvent moins bien testées.

N'oubliez pas non plus de tester les outils d'administration, de monitoring,
de modélisation. Ils nécessitent souvent une mise à jour pour être compatibles
avec la nouvelle version installée.

Une fois que les tests sont concluants, arrive le moment de la mise en production. C'est
une étape qui peut être longue car les fichiers de données doivent être
traités. Il existe plusieurs méthodes que nous détaillerons après.

</div>

---

## Mise à jour majeure par dump/restore

<div class="slide-content">

  * Méthode historique
  * Simple et sans risque
    + mais d'autant plus longue que le volume de données est important
  * Outils :
    + `pg_dumpall -g` puis `pg_dump`
    + `psql` puis `pg_restore`

</div>

<div class="notes">

Il s'agit de la méthode la plus ancienne et la plus sûre. L'idée est de
sauvegarder l'ancienne version avec l'outil de sauvegarde de la nouvelle
version. `pg_dumpall` peut suffire, mais `pg_dump` est malgré tout recommandé.
Le problème de lenteur vient surtout de la restauration. `pg_restore` est un outil assez
lent pour des volumétries importantes. Il convient donc de sélectionner cette
solution si le volume de données n'est pas conséquent (pas plus d'une centaine
de Go) ou si les autres méthodes ne sont pas possibles. Cependant, il est
possible d'accélérer la restauration en utilisant la parallélisation
(option `--jobs`). Ceci
n'est possible que si la sauvegarde a été faite avec `pg_dump -Fd` ou `-Fc`.
Il est à noter que cette sauvegarde peut elle aussi être parallélisée
(option `--jobs` là encore).

</div>

---

### Mise à jour majeure par Slony

<div class="slide-content">

  * Nécessite d'utiliser l'outil de réplication Slony
  * Permet un retour en arrière immédiat sans perte de données

</div>

<div class="notes">

La méthode Slony est certainement la méthode la plus compliquée. C'est aussi
une méthode qui permet un retour arrière vers l'ancienne version sans perte de
données.

L'idée est d'installer la nouvelle version de PostgreSQL normalement, sur le
même serveur ou sur un autre serveur. Il faut installer Slony sur l'ancienne et
la nouvelle instance, et déclarer la réplication de l'ancienne instance vers
la nouvelle. Les utilisateurs peuvent continuer à travailler pendant le
transfert initial des données. Ils n'auront pas de blocages, tout au plus une
perte de performances dues à la lecture et à l'envoi des données vers le
nouveau serveur. Une fois le transfert initial réalisé, les données
modifiées entre temps sont transférées vers le nouveau serveur.

Une fois arrivé à la synchronisation des deux serveurs, il ne reste plus qu'à
déclencher un _switchover_. La réplication aura lieu ensuite entre le nouveau
serveur et l'ancien serveur, ce qui permet un retour en arrière sans perte de
données. Une fois acté que le nouveau serveur donne pleine
satisfaction, il suffit de désinstaller Slony des deux côtés.

</div>

---

## Mise à jour majeure par réplication logique

<div class="slide-content">

  * Possible entre versions 10 et supérieures
  * Remplace Slony, Bucardo...
  * Bascule très rapide
  * Et retour possible

</div>

<div class="notes">

La réplication logique rend possible une migration entre deux
instances de version majeure différente avec une indisponibilité très courte.

La réplication logique n'est disponible en natif qu'à partir de la version 10,
la base à migrer doit donc être en version 10 ou supérieure.

Le même principe que les outils de réplication par trigger
comme Slony ou Bucardo est utilisé, mais plus simplement et avec les outils du
moteur. Le principe est de répliquer une base à l'identique alors
que la production tourne.  Des clés primaires sur chaque table sont
souhaitables mais pas forcément obligatoires.

Lors de la bascule, il suffit d'attendre que les dernières données
soient répliquées, ce qui peut être très rapide, et de connecter
les applications au nouveau serveur. La réplication peut alors être
inversée pour garder l'ancienne production synchrone, permettant de
rebasculer dessus en cas de problème sans perdre les données modifiées
depuis la bascule.

</div>

---

## Mise à jour majeure par pg_upgrade

<div class="slide-content">

  * `pg_upgrade` : fourni avec PostgreSQL
  * Prérequis : pas de changement de format des fichiers entre versions
  * Nécessite les deux versions sur le même serveur
  * Support des serveurs PostgreSQL à migrer :
    + version minimale 9.2 pour pg_upgrade v15
    + version minimale 8.4 sinon

</div>

<div class="notes">

`pg_upgrade` est certainement l'outil le plus rapide pour une mise à jour
majeure.

Il profite du fait que les formats des fichiers de données n'évolue pas,
ou de manière rétrocompatible, entre deux versions majeures. Il n'est donc
pas nécessaire de tout réécrire.

Grossièrement, son fonctionnement est le suivant. Il récupère la
déclaration des objets sur l'ancienne instance avec un `pg_dump` du schéma de
chaque base et de chaque objet global. Il intègre la déclaration des objets
dans la nouvelle instance. Il fait un ensemble de traitement sur les
identifiants d'objets et de transactions. Puis, il copie les fichiers de
données de l'ancienne instance vers la nouvelle instance. La copie est
l'opération la plus longue, mais comme il n'est pas nécessaire de reconstruire
les index et de vérifier les contraintes, cette opération est bien plus rapide
que la restauration d'une sauvegarde style `pg_dump`. Pour aller encore plus
rapidement, il est possible de créer des liens physiques à la place de
la copie des fichiers. Ceci fait, la migration est terminée.

En 2010, Stefan Kaltenbrunner et Bruce Momjian avaient mesuré qu'une base de
150 Go mettait 5 heures à être mise à jour avec la méthode historique
(sauvegarde/restauration). Elle mettait 44 minutes en mode copie et 42 secondes
en mode lien lors de l'utilisation de `pg_upgrade`.

Vu ses performances, ce serait certainement l'outil à privilégier. Cependant,
c'est un outil très complexe et quelques bugs particulièrement méchants ont
terni sa réputation. Notre recommandation est de bien tester la mise à jour
avant de le faire en production, et uniquement sur des bases suffisamment
volumineuses permettant de justifier l'utilisation de cet outil.

Lors du développement de la version 15, les développeurs ont supprimé certaines
vieilles parties du code, ce qui le rend à présent incompatible avec des versions très
anciennes (de la 8.4 à la 9.1).

</div>
\newpage

## Travaux pratiques

<div class="notes">

### Mise à jour majeure avec pg_dump

**But** : Réaliser la mise à jour de PostgreSQL avec l'outil `pg_dump`.

> Installer les paquets de la version la plus récente de PostgreSQL.

> Initialiser le répertoire de données avec `initdb` et démarrer le service sur un port
  différent.

> Recopier les utilisateurs et les bases de données avec `pg_dumpall`.

> Transférer les données avec `pg_dump`.

> Collecter les statistiques avec `ANALYZE`.

> Stopper le service de l'ancienne version et redémarrer la nouvelle instance avec
  le port d'écoute par défaut.

### Mise à jour majeure avec pg_upgrade

**But** : Réaliser la mise à jour de PostgreSQL avec l'outil `pg_upgrade`.

> Initialiser le répertoire de données avec `initdb`.

> S'assurer que le service PostgreSQL de la version à mettre à jour.

> Lancer `pg_upgrade`.

> Démarrer le service PostgreSQL de la nouvelle version.

> Collecter les statistiques avec le script proposé.

</div>

\newpage

## Travaux pratiques (solutions)

<div class="notes">

### Mise à jour majeure avec pg_dump

**Les commandes suivantes sont à exécuter en tant qu'utilisateur `root`.**

> Installer les paquets de la version la plus récente de PostgreSQL.

dnf -y update

```bash
dnf install -y postgresql17-server postgresql17-contrib
```

> Initialiser le répertoire de données avec `initdb` et démarrer le service sur un port
  différent.

```bash
export PGSETUP_INITDB_OPTIONS="--data-checksums"
/usr/pgsql-17/bin/postgresql-17-setup initdb
```

Le changement de port se fait dans le fichier `/var/lib/pgsql/17/data/postgresql.conf`.
Éditer la ligne `port = 5432` pour changer le port par exemple en `5433` et démarrer le service PostgreSQL.

```bash
sed -i 's/#port = 5432/port = 5433/' /var/lib/pgsql/17/data/postgresql.conf
systemctl start postgresql-17
```

> Recopier les données globales avec `pg_dumpall` vers la nouvelle instance.

```bash
sudo -iu postgres
pg_dumpall --globals-only -U postgres > /tmp/pg_dumpall.sql
psql -p 5433 -f /tmp/pg_dumpall.sql
```

> Transférer les données avec `pg_dump`.

L'utilitaire `pg_dump` permet d'extraire au format texte les données d'une base de données.
Il est possible d'exporter toutes les bases de données avec `pg_dumpall` ou de réaliser
une boucle pour les exporter une par une.

```bash
sudo -iu postgres
for db in $(psql -U postgres -t -c "SELECT datname FROM pg_database WHERE datname NOT IN ('template0', 'template1');"); do
  pg_dump -d $db --create | psql -p 5433
done
```

> Collecter les statistiques avec `ANALYZE`.

```bash
vacuumdb -U postgres --analyze-only -p 5433 --all
```

> Stopper le service de l'ancienne version et redémarrer la nouvelle instance avec
  le port d'écoute par défaut.

```bash
systemctl stop postgresql-16
sed -i 's/port = 5433/port = 5432/' /var/lib/pgsql/17/data/postgresql.conf
systemctl restart postgresql-17
```

### Mise à jour majeure avec pg_upgrade

**Les commandes suivantes sont à exécuter en tant qu'utilisateur `root`.**

> Initialiser le répertoire de données avec `initdb`.

Si l'instance de la version cible est déjà en cours d'exécution, il est nécessaire de l'arrêter
et de supprimer le répertoire de données.

```bash
systemctl stop postgresql-17
rm -rf /var/lib/pgsql/17/data
export PGSETUP_INITDB_OPTIONS="--data-checksums"
/usr/pgsql-17/bin/postgresql-17-setup initdb
```

> S'assurer que le service PostgreSQL de la version à mettre à jour.

```bash
systemctl stop postgresql-16
```

> Lancer `pg_upgrade`.

L'utilitaire `pg_upgrade` doit être exécuté en tant qu'utilisateur `postgres`.

```bash
sudo -iu postgres
export PGDATAOLD=/var/lib/pgsql/16/data
export PGDATANEW=/var/lib/pgsql/17/data
export PGBINOLD=/usr/pgsql-16/bin
export PGBINNEW=/usr/pgsql-17/bin
/usr/pgsql-17/bin/pg_upgrade
```

Si un message d'erreur stipule que le répertoire de données source ne dispose pas
du contrôle de somme de contrôle, il est nécessaire de l'activer à froid à l'aide
de l'utilitaire `pg_checksums`.

```console
old cluster does not use data checksums but the new one does
Failure, exiting
```

Exécuter la commande suivante avec l'utilisateur `postgres` et retenter l'opération
avec `pg_upgrade`.

```bash
/usr/pgsql-16/bin/pg_checksums -D /var/lib/pgsql/16/data --enable
/usr/pgsql-17/bin/pg_upgrade
```

> Démarrer le service PostgreSQL de la nouvelle version.

Avec l'utilisateur `root`.

```bash
systemctl start postgresql-17
```

> Collecter les statistiques avec le script proposé.

```bash
sudo -iu postgres
/usr/pgsql-17/bin/vacuumdb --all --analyze-in-stages
```

</div>
