---
revision: 24.12
date: "Année académique 2024-2025"

licence : "Creative Commons BY-NC-SA"
include_licence : CC-BY-NC-SA-2.0-FR

author: © 2005-2025 DALIBO SARL SCOP

trademarks: |
  Le logo éléphant de PostgreSQL (« Slonik ») est une création sous copyright et
  le nom « PostgreSQL » est une marque déposée par PostgreSQL Community Association
  of Canada.

##
## PDF Options
##

## Limiter la profondeur de la table des matières
toc-depth: 2

## Mettre les lien http en pieds de page
links-as-notes: false

## Police plus petite dans un bloc de code

code-blocks-fontsize: small

## Filtre : pandoc-latex-admonition
## les catégories `important` et `warning` sont synonymes
## même chose pour `tip` et `note`
pandoc-latex-admonition:
  - color: Red
    classes: [warning]
    linewidth: 4
  - color: Red
    classes: [important]
    linewidth: 4
  - color: DarkSeaGreen
    classes: [tip]
    linewidth: 4
  - color: DarkSeaGreen
    classes: [note]
    linewidth: 4
  - color: DodgerBlue
    classes: [slide-content]
    linewidth: 4

##
## Reveal Options
##

## Taille affichage
width: 960
height: 700

## beige/blood/moon/simple/solarized/black/league/night/serif/sky/white
theme: white

## None - Fade - Slide - Convex - Concave - Zoom
transition: None
transitionSpeed: fast

## Barre de progression
progress: true

## Affiche N° de slide
slideNumber: true

## Le numero de slide apparait dans la barre d'adresse
history: true

## Defilement des slides avec la roulette
mouseWheel: true

## Annule la transformation uppercase de certains thèmes
title-transform : none

## Cache l'auteur sur la première slide
## Mettre en commentaire pour désactiver
hide_author_in_slide: true

title : 'Fonctionnement de PostgreSQL'
subtitle : 'FCU Calais - M1 I2L'
---

# Licence

<div class="slide-content">
Le contenu pédagogique est issu des supports de formation de la société [Dalibo],
distribués sous licence _Creative Commons [CC-BY-NC-SA]_.

![cc](src/img/cc_blue.png)
![by](src/img/attribution_icon_blue.png)
![nc](src/img/nc_blue.png)
![sa](src/img/sa_blue.png)

[Dalibo]: https://www.dalibo.com/formations
[CC-BY-NC-SA]: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
</div>

<div class="notes">
Vous devez citer le nom de l'auteur original de la manière indiquée par l'auteur
de l'œuvre ou le titulaire des droits qui vous confère cette autorisation (mais
pas d'une manière qui suggérerait qu'ils vous soutiennent ou approuvent votre 
utilisation de l'œuvre).

Vous n'avez pas le droit d'utiliser cette création à des fins commerciales.

Si vous modifiez, transformez ou adaptez cette création, vous n'avez le droit de 
distribuer la création qui en résulte que sous un contrat identique à celui-ci.

À chaque réutilisation ou distribution de cette création, vous devez faire 
apparaître clairement au public les conditions contractuelles de sa mise à 
disposition. La meilleure manière de les indiquer est un lien vers cette page web.

Chacune de ces conditions peut être levée si vous obtenez l'autorisation du 
titulaire des droits sur cette œuvre.

Rien dans ce contrat ne diminue ou ne restreint le droit moral de l'auteur ou des 
auteurs.

Le texte complet de la licence est disponible à cette adresse: 
<https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.fr>
</div>

-----

# Formateur

<div class="slide-content">
* Nom : Florent Jardin 
* Profession : Consultant PostgreSQL chez Dalibo
* Hobbies : Course à pied, jeux vidéo, musique

![](https://fljd.in/img/avatar.jpg)
</div>

<div class="notes">
Pour me contacter :

* Site internet <https://fljd.in/a-propos>
* Mastodon <https://fosstodon.org/@fljdin>
* Github <https://github.com/fljdin>
* LinkedIn <https://www.linkedin.com/in/florent-jardin>
</div>

-----

# Présentation du cours

<div class="slide-content">

- Formation : Fonctionnement de PostgreSQL
- Durée : 2 jours (15h)
- Cursus M1 I2L de l'Université du Littoral Côte d'Opale (_ULCO_)

</div>

-----

## Objectifs

<div class="slide-content">

* Savoir installer PostgreSQL
* Reconnaître les processus et les fichiers d'une instance
* Comprendre les mécanismes de cache et de journalisation
* Appréhender le modèle transactionnel MVCC
* Connaître et configuration l'autovacuum

</div>

<div class="notes">
Ce cursus s'adresse aux étudiants ayant des notions en administration système
sous Linux (distributions RedHat ou affiliés).

<!-- B - Installation de PostgreSQL -->
1. Savoir installer PostgreSQL

Ce module aborde les différentes façons d'installer le logiciel PostgreSQL sur
les différentes plateformes (Linux, Windows). Il présente également les premières
commandes pour appréhender l'administration d'une instance (arrêt, démarrage)
ainsi que les principaux paramètres de configuration qu'il est recommandé de
connaître.

<!-- M1 - Architecture & fichiers de PostgreSQL -->
2. Reconnaître les processus et les fichiers d'une instance

Une fois l'installation d'une instance réalisée, il est demandé de comprendre
comment s'execute PostgreSQL à l'aide de plusieurs processus et de zones en
mémoire. Le module présente ensuite les différents fichiers présents sur le
disque nécessaires au bon fonctionnement de l'instance et de ses bases de
données.

<!-- M3 - Mémoire et journalisation dans PostgreSQL -->
3. Comprendre les mécanismes de cache et de journalisation

Ce module détaille le concept de mémoire partagée pour la gestion des résultats
en cache afin de maintenir de bonne performance générale. Ce fonctionnement
s'appuie sur plusieurs mécanismes dont une zone tampon particulière, couplée aux
fichiers de journalisation.

<!-- M4 - Mécanique du moteur transactionnel & MVCC -->
4. Appréhender le modèle transactionnel MVCC

PostgreSQL s’appuie sur un modèle de gestion de transactions appelé MVCC. Nous
allons expliquer cet acronyme, puis étudier en profondeur son implémentation dans
le moteur. Cette technologie a en effet un impact sur le fonctionnement et
l’administration de PostgreSQL.

<!-- M5 - VACUUM et autovacuum -->
5. Connaître et configuration l'autovacuum

VACUUM est la contrepartie de la flexibilité du modèle MVCC. Derrière les
différentes options de VACUUM se cachent plusieurs tâches très différentes.
Malheureusement, la confusion est facile. Il est capital de les connaître et de
comprendre leur fonctionnement. Autovacuum permet d’automatiser le VACUUM et
allège considérablement le travail de l’administrateur. Il fonctionne
généralement bien, mais il faut savoir le surveiller et l’optimiser.

</div>

-----

## Matériel pédagogique

<div class="slide-content">
* Oracle VirtualBox
* Alma Linux 9
* Terminal SSH
* DBeaver (optionnel)
</div>

<div class="notes">

Le module nécessite un système GNU/Linux pour couvrir l'ensemble des travaux
pratiques. Pour les postes Windows, il est encouragé d'installer [Oracle
VirtualBox][00-1] et de configurer une machine virtuelle avant le début des
cours. Pour des soucis d'utilisation, **il est vivement conseillé d'activer un
transfert de port entre la machine virtuelle et le poste hôte**.

[00-1]: https://www.virtualbox.org/wiki/Downloads

Les corrections des exercices sont adaptées pour le système Alma Linux 9 ou
équivalent. L'image de la distribution `x86_64` est disponible au
[téléchargement][00-2]. Pour celles et ceux plus familiers au format _Vagrant_,
un exemple de fichier de démarrage est proposé dans la [documentation][00-3].

[00-2]: https://almalinux.org/fr/get-almalinux/
[00-3]: https://app.vagrantup.com/almalinux/boxes/9

Les principaux outils requis pour progresser dans ce module sont un terminal SSH
et un éditeur de requêtes SQL. Le logiciel [DBeaver][00-4] Communautaire permet
d'ouvrir une connexion à une instance PostgreSQL sur le port par défaut 5432 de
la machine virtuelle et de naviguer dans les schémas tout en exécutant des
requêtes.

[00-4]: https://dbeaver.io/download/
</div>
