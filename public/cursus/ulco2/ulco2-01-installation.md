---
revision: 24.12
date: "Année académique 2024-2025"

licence : "Creative Commons BY-NC-SA"
include_licence : CC-BY-NC-SA-2.0-FR

author: © 2005-2025 DALIBO SARL SCOP

trademarks: |
  Le logo éléphant de PostgreSQL (« Slonik ») est une création sous copyright et
  le nom « PostgreSQL » est une marque déposée par PostgreSQL Community Association
  of Canada.

##
## PDF Options
##

## Limiter la profondeur de la table des matières
toc-depth: 2

## Mettre les lien http en pieds de page
links-as-notes: false

## Police plus petite dans un bloc de code

code-blocks-fontsize: small

## Filtre : pandoc-latex-admonition
## les catégories `important` et `warning` sont synonymes
## même chose pour `tip` et `note`
pandoc-latex-admonition:
  - color: Red
    classes: [warning]
    linewidth: 4
  - color: Red
    classes: [important]
    linewidth: 4
  - color: DarkSeaGreen
    classes: [tip]
    linewidth: 4
  - color: DarkSeaGreen
    classes: [note]
    linewidth: 4
  - color: DodgerBlue
    classes: [slide-content]
    linewidth: 4

##
## Reveal Options
##

## Taille affichage
width: 960
height: 700

## beige/blood/moon/simple/solarized/black/league/night/serif/sky/white
theme: white

## None - Fade - Slide - Convex - Concave - Zoom
transition: None
transitionSpeed: fast

## Barre de progression
progress: true

## Affiche N° de slide
slideNumber: true

## Le numero de slide apparait dans la barre d'adresse
history: true

## Defilement des slides avec la roulette
mouseWheel: true

## Annule la transformation uppercase de certains thèmes
title-transform : none

## Cache l'auteur sur la première slide
## Mettre en commentaire pour désactiver
hide_author_in_slide: true

title : 'Installation de PostgreSQL'
subtitle : 'FCU Calais - M1 I2L'
---
<!--
 L'esssentiel du cours a été déporté vers les includes 
-->

# Installation de PostgreSQL

![PostgreSQL](medias/logos/slonik.png){ height=500 }
\

<div class="notes">
</div>

---

## Introduction

<div class="slide-content">

  * Pré-requis
  * Installation depuis les sources
  * Installation depuis les binaires
    + installation à partir des paquets
    + installation sous Windows
  * Installation dans les conteneurs
  * Premiers réglages
  * Mises à jours

</div>

<div class="notes">

Il existe trois façons d'installer PostgreSQL :

  * Les installeurs graphiques :
    + uniquement Windows et macOS ;
    + avantages : installation facile, idéale pour les nouveaux venus ;
    + inconvénients : pas d'intégration avec le système de paquets du système d'exploitation.
  * les paquets du système :
    + avantages : meilleure intégration avec les autres logiciels, idéal pour un
serveur en production ;
    + inconvénients : aucun ?

  * Le code source :
    + avantages : configuration très fine, ajout de patchs, intéressant pour les
utilisateurs expérimentés et les testeurs, ou pour embarquer PostgreSQL au sein
d'un ensemble de logiciels ;
    + inconvénients : nécessite un environnement de compilation, ainsi que de
configurer utilisateurs et script de démarrage.

Nous allons maintenant détailler chaque façon d'installer PostgreSQL.

</div>

---

<!-- tout le cours est là -->


## Pré-requis minimaux pour une instance PostgreSQL

---

### Pré-requis minimaux

<!-- Condensé de 2 slides dans J1 -->

<div class="slide-content">

  * À peu près n’importe quel OS actuel
    + Linux (conseillé)
    + Unix propriétaires (dont macOS), FreeBSD
    + Windows
  * N’importe quelle machine
    + …selon les besoins
    + 64 bits conseillé
  * Stockage fiable
  * Pas d'antivirus !

</div>

<div class="notes">

Il n'existe pas de configuration minimale pour une installation
de PostgreSQL.
La configuration de base est très conservatrice (128 Mo de cache).
PostgreSQL peut fonctionner sur n'importe quelle machine actuelle,
sur x86, 32 ou 64 bits.
La configuration dépend plutôt des performances et volumétries
attendues.

Les
[plate-formes officiellement supportées](https://www.postgresql.org/docs/current/supported-platforms.html) 
incluent les principaux dérivés d'Unix, en premier lieu Linux,
mais aussi les FreeBSD, OpenBSD, macOS, Solaris/illumos ou AIX ; ainsi que Windows.
<div class="box tip">
Linux 64 bits est de loin la plate-forme privilégiée par les développeurs,
celle disposant du plus d'outils annexes,
et est donc recommandée pour faire tourner PostgreSQL.
</div>
Les versions 32 bits fonctionnent mais sont de plus en plus
[ignorées par les développeurs d'extensions ou les mainteneurs](https://www.postgresql.org/message-id/Zr3_tXWr0VQol-VH%40msg.df7cb.de), et ne se voient plus vraiment
en production.

Debian et Red Hat et leurs dérivés sont les principales distributions
vues en production (à côté d'Alpine pour les images docker).

<div class="box caution">
Si vous devez absolument installer un antivirus ou un outil anti-intrusion,
il faut impérativement exclure de son analyse
tous les répertoires, fichiers et processus de PostgreSQL.
L'interaction avec des antivirus a régulièrement mené
à des problèmes de performance, voire de corruption.
<!--
mcafee et altima : https://labosupport.dalibo.com/view.php?id=14903#c128545
impact CPU sur des traces massives chez harmonie : https://support.dalibo.com/view.php?id=15919
-->
</div>

</div>

---


## Installation à partir des sources

---

### Étapes d'une installation à partir des sources

<div class="slide-content">

  * Téléchargement
  * Vérification des prérequis
  * Compilation
  * Installation

</div>

<div class="notes">

Les utilisateurs compilent rarement PostgreSQL, et nous
recommandons d'utiliser les paquets précompilés du PGDG.
Certaines utilisations le nécessitent toutefois.
C'est aussi l'occasion
de voir quelques concepts techniques importants.

Nous allons aborder ici les différentes étapes à réaliser pour installer
PostgreSQL à partir des sources :

  * trouver les fichiers sources ;
  * préparer le serveur pour accueillir PostgreSQL ;
  * compiler le serveur ;
  * vérifier le résultat de la compilation ;
  * installer les fichiers compilés.

</div>

---

### Téléchargement

<div class="slide-content">

  * Disponible depuis <https://www.postgresql.org/download/>
    + `postgresql-<version>.tar.bz2`
  * Git : <https://git.postgresql.org/git/postgresql.git>

</div>

<div class="notes">

Les fichiers sources et les instructions de compilation sont disponibles sur le
[site officiel du projet](https://www.postgresql.org/download/)
(ou plus directement
<https://www.postgresql.org/ftp/source/>
ou <https://ftp.postgresql.org/pub/source>).
Le nom du fichier à télécharger se présente
toujours sous la forme `postgresql-<version>.tar.bz2` où `<version>`
représente la version de PostgreSQL (par exemple :
<https://ftp.postgresql.org/pub/source/v17.0/postgresql-17.0.tar.bz2>)

Lorsque la future version de PostgreSQL paraît en phase de test (versions bêta),
souvent à partir de mai, les sources sont accessibles à l'adresse suivante :
<https://www.postgresql.org/developer/beta>.

Il existe bien sûr un [dépôt git](https://git.postgresql.org/gitweb/?p=postgresql.git;a=summary)
officiel, avec un miroir sur [Github](https://github.com/postgres/postgres).

</div>

---

### Phases de compilation/installation

<div class="slide-content">

```bash
# au choix
tar xvfj postgresql-17.0.tar.bz2
git clone --branch REL_17_0 https://git.postgresql.org/git/postgresql.git
```
```bash
./configure --prefix=/usr/local/pgsql   # beaucoup d'options !
make -j8
cd contrib/ && make -j8        # ne pas oublier les contrib
make check

sudo make install && cd contrib/ && sudo make install
```
```bash
pg_config --configure
```

</div>

<div class="notes">

<!--
  Cette partie n'est plus qu'un guide. L'exemple complet est le TP complet.
--> 

La compilation de PostgreSQL sous Linux/Unix suit un processus classique.

Une fois l'archive ou le dépôt Git récupéré, si les librairies habituelles
de développement sont présentes, la compilation est extrêmement classique.
Le répertoire `contrib` contient des modules et extensions gérés par le projet,
dont l'installation est chaudement conseillée. La compilation s'effectue de la même manière.

```bash
./configure
```

Des options courantes pour une machine de production sont :

* `--prefix=`__répertoire__ : répertoire d'installation personnalisé
(par défaut, il s'agit de `/usr/local/pgsql`) ;
* `--with-pgport`=__port__ : port par défaut (si différent de 5432) ;
* `--with-openssl` : support d'OpenSSL pour bénéficier de connexions chiffrées ;
* `--enable-nls` : le support de la langue utilisateur pour les messages
provenant du serveur et des applications ;
* `--with-perl`, `--with-python` : installe les langages PL correspondants.

<div class="box tip">
On voudra généralement activer ces trois dernières options…
et beaucoup d'autres.
</div>

```bash
make --jobs=8 all
(cd contrib/ && make --jobs=8 all)
make --jobs=8 check
sudo make install && cd contrib/ && sudo make install
```

(Le `make` de GNU se nomme `gmake` sur certaines systèmes différents de Linux).
Cette phase est la plus longue,
mais ne dure que quelques minutes sur du matériel récent en parallélisant.

`̀make check` lance des tests de non régression pour vérifier que
PostgreSQL fonctionne correctement sur la machine cible.

Pour les mises à jour, il
est important de connaître les options utilisées lors de la précédente
compilation. L'outil `pg_config` le permet. Un PostgreSQL 17.0
sur Debian 12 affichera ce qui suit (les distributions ont
tendance à intégrer toutes les options) :

```bash
$ /usr/lib/postgresql/17/bin/pg_config --configure
 '--build=x86_64-linux-gnu' '--prefix=/usr' '--includedir=${prefix}/include'
 '--mandir=${prefix}/share/man' '--infodir=${prefix}/share/info'
 '--sysconfdir=/etc' '--localstatedir=/var' '--disable-option-checking'
 '--disable-silent-rules' '--libdir=${prefix}/lib/x86_64-linux-gnu'
 '--runstatedir=/run'
 '--disable-maintainer-mode' '--disable-dependency-tracking'
 '--with-tcl' '--with-perl' '--with-python' '--with-pam' '--with-openssl'
 '--with-libxml' '--with-libxslt' '--mandir=/usr/share/postgresql/17/man'
 '--docdir=/usr/share/doc/postgresql-doc-17'
 '--sysconfdir=/etc/postgresql-common' '--datarootdir=/usr/share/'
 '--datadir=/usr/share/postgresql/17'
 '--bindir=/usr/lib/postgresql/17/bin'
 '--libdir=/usr/lib/x86_64-linux-gnu/'
 '--libexecdir=/usr/lib/postgresql/'
 '--includedir=/usr/include/postgresql/'
 '--with-extra-version= (Debian 17.0-1.pgdg120+1)'
 '--enable-nls' '--enable-thread-safety' '--enable-debug' '--disable-rpath'
 '--with-uuid=e2fs' '--with-gnu-ld' '--with-gssapi' '--with-ldap'
 '--with-pgport=5432' '--with-system-tzdata=/usr/share/zoneinfo'
 'AWK=mawk' 'MKDIR_P=/bin/mkdir -p' 'PROVE=/usr/bin/prove'
 'PYTHON=/usr/bin/python3' 'TAR=/bin/tar' 'XSLTPROC=xsltproc --nonet'
 'CFLAGS=-g -O2 -fstack-protector-strong -Wformat -Werror=format-security -fno-omit-frame-pointer' 'LDFLAGS=-Wl,-z,relro -Wl,-z,now'
 '--enable-tap-tests' '--with-icu'
 '--with-llvm' 'LLVM_CONFIG=/usr/bin/llvm-config-16' 'CLANG=/usr/bin/clang-16'
 '--with-lz4' '--with-zstd'
 '--with-systemd' '--with-selinux' '--enable-dtrace'
 'build_alias=x86_64-linux-gnu'
 'CPPFLAGS=-Wdate-time -D_FORTIFY_SOURCE=2'
 'CXXFLAGS=-g -O2 -fstack-protector-strong -Wformat -Werror=format-security'
```

Il existe aussi une vue `pg_config` accessible sur un serveur démarré :

```sql
 TABLE pg_config ;
       name        |  setting
-------------------+-----------------------------------------------------------
 BINDIR            | /usr/lib/postgresql/17/bin
 DOCDIR            | /usr/share/doc/postgresql-doc-17
 HTMLDIR           | /usr/share/doc/postgresql-doc-17
 INCLUDEDIR        | /usr/include/postgresql
 PKGINCLUDEDIR     | /usr/include/postgresql
 INCLUDEDIR-SERVER | /usr/include/postgresql/17/server
 LIBDIR            | /usr/lib/x86_64-linux-gnu
 PKGLIBDIR         | /usr/lib/postgresql/17/lib
 LOCALEDIR         | /usr/share/locale
 MANDIR            | /usr/share/postgresql/17/man
 SHAREDIR          | /usr/share/postgresql/17
 SYSCONFDIR        | /etc/postgresql-common
 PGXS              | /usr/lib/postgresql/17/lib/pgxs/src/makefiles/pgxs.mk
 CONFIGURE         |  '--build=x86_64-linux-gnu' '--prefix=/usr' '--includedir=${prefix}/include' '--mandir=${prefix}/share/man' '--infodir=${prefix}/share/info' '--sysconfdir=/etc' '--localstatedir=/var' '--disable-option-checking' '--disable-silent-rules' '--libdir=${prefix}/lib/x86_64-linux-gnu' '--runstatedir=/run' '--disable-maintainer-mode' '--disable-dependency-tracking' '--with-tcl' '--with-perl' '--with-python' '--with-pam' '--with-openssl' '--with-libxml' '--with-libxslt' '--mandir=/usr/share/postgresql/17/man' '--docdir=/usr/share/doc/postgresql-doc-17' '--sysconfdir=/etc/postgresql-common' '--datarootdir=/usr/share/' '--datadir=/usr/share/postgresql/17' '--bindir=/usr/lib/postgresql/17/bin' '--libdir=/usr/lib/x86_64-linux-gnu/' '--libexecdir=/usr/lib/postgresql/' '--includedir=/usr/include/postgresql/' '--with-extra-version= (Debian 17.0-1.pgdg120+1)' '--enable-nls' '--enable-thread-safety' '--enable-debug' '--disable-rpath' '--with-uuid=e2fs' '--with-gnu-ld' '--with-gssapi' '--with-ldap' '--with-pgport=5432' '--with-system-tzdata=/usr/share/zoneinfo' 'AWK=mawk' 'MKDIR_P=/bin/mkdir -p' 'PROVE=/usr/bin/prove' 'PYTHON=/usr/bin/python3' 'TAR=/bin/tar' 'XSLTPROC=xsltproc --nonet' 'CFLAGS=-g -O2 -fstack-protector-strong -Wformat -Werror=format-security -fno-omit-frame-pointer' 'LDFLAGS=-Wl,-z,relro -Wl,-z,now' '--enable-tap-tests' '--with-icu' '--with-llvm' 'LLVM_CONFIG=/usr/bin/llvm-config-16' 'CLANG=/usr/bin/clang-16' '--with-lz4' '--with-zstd' '--with-systemd' '--with-selinux' '--enable-dtrace' 'build_alias=x86_64-linux-gnu' 'CPPFLAGS=-Wdate-time -D_FORTIFY_SOURCE=2' 'CXXFLAGS=-g -O2 -fstack-protector-strong -Wformat -Werror=format-security'
 CC                | gcc
 CPPFLAGS          | -Wdate-time -D_FORTIFY_SOURCE=2 -D_GNU_SOURCE -I/usr/include/libxml2
 CFLAGS            | -Wall -Wmissing-prototypes -Wpointer-arith -Wdeclaration-after-statement -Werror=vla -Wendif-labels -Wmissing-format-attribute -Wimplicit-fallthrough=3 -Wcast-function-type -Wshadow=compatible-local -Wformat-security -fno-strict-aliasing -fwrapv -fexcess-precision=standard -Wno-format-truncation -Wno-stringop-truncation -g -g -O2 -fstack-protector-strong -Wformat -Werror=format-security -fno-omit-frame-pointer
 CFLAGS_SL         | -fPIC
 LDFLAGS           | -Wl,-z,relro -Wl,-z,now -L/usr/lib/llvm-16/lib -Wl,--as-needed
 LDFLAGS_EX        | 
 LDFLAGS_SL        | 
 LIBS              | -lpgcommon -lpgport -lselinux -lzstd -llz4 -lxslt -lxml2 -lpam -lssl -lcrypto -lgssapi_krb5 -lz -lreadline -lm 
 VERSION           | PostgreSQL 17.0 (Debian 17.0-1.pgdg120+1)
(23 lignes)
```

</div>

---

### Utilisateur dédié de PostgreSQL

<div class="slide-content">

  * Jamais **root**
  * Utilisateur dédié
    + propriétaire des répertoires et fichiers
    + peut lancer PostgreSQL
    + traditionnellement : **postgres**
  * Variables d'environnement (défaut) :

```bash
export PATH=/usr/local/pgsql/bin:$PATH
export LD_LIBRARY_PATH=/usr/local/pgsql/lib:$LD_LIBRARY_PATH
export MANPATH=$MANPATH:/usr/local/pgsql/share/man
export PGDATA=/usr/local/pgsql/data    # Données
```

</div>

<div class="notes">

<div class="box warning">
Le serveur PostgreSQL ne peut pas être exécuté par l'utilisateur **root**,
pour des raisons de sécurité. Un utilisateur système sans droits particuliers suffit.
Il sera le **seul** propriétaire des répertoires
et fichiers gérés par le serveur PostgreSQL. Il sera aussi le compte qui
permettra de lancer PostgreSQL (directement ou indirectement via les outils système
comme systemd).
</div>

Cet utilisateur système est habituellement appelé **postgres** (et les paquets
d'installation utilisent ce nom),
mais ce n'est absolument pas une obligation.

Il est nécessaire de positionner un certain nombre de
variables d'environnement dans `${HOME}/.profile` ou
dans `/etc/profile`. Avec le chemin par défaut de la compilation,
elles valent :

```bash
export PATH=/usr/local/pgsql/bin:$PATH
export LD_LIBRARY_PATH=/usr/local/pgsql/lib:$LD_LIBRARY_PATH
export MANPATH=$MANPATH:/usr/local/pgsql/share/man
export PGDATA=/usr/local/pgsql/data
```

**Chemins des binaires** :

  * `/usr/local/pgsql/bin` est le chemin par défaut (ou le chemin
indiqué à l'option `--prefix` lors de l'étape `configure`). L'ajouter
à `PATH` permet de rendre l'exécution de PostgreSQL possible depuis
n'importe quel répertoire ;
  * `LD_LIBRARY_PATH` indique au système où trouver les
différentes bibliothèques nécessaires à l'exécution des programmes ;
  * `MANPATH` indique le chemin de recherche des pages de manuel.

**Emplacement des données** :

<!-- exemples slide d'après -->
`PDGATA` est une spécificité de PostgreSQL : cette variable indique
le répertoire des fichiers de données de PostgreSQL, c'est-à-dire
les données de l'utilisateur. (Plus rigoureusement :
elle pointe l'emplacement du fichier `postgresql.conf`, généralement
dans ce répertoire de données.
Les paquets pour Debian/Ubuntu représentent une grosse exception,
avec un `postgresql.conf` dans `/etc`, qui contient le paramètre
`data_directory`, lequel pointe vers le vrai répertoire de données.)
PostgreSQL n'impose de lui-même aucune contrainte sur le chemin.
<!-- conseils + bas -->

</div>

---

### Création du répertoire de données de l'instance

<div class="slide-content">

```bash
$ initdb -D /usr/local/pgsql/data
```

  * Une seule instance sur le répertoire !
  * Pas à la racine d'un point de montage
  * Options d'emplacement :
      + `--data` pour les fichiers de données
      + `--waldir` pour les journaux de transactions
  * Autres options :
    + `--data-checksums` : sommes de contrôle (conseillé !)
    + et : chemin des journaux, mot de passe, encodage…

</div>

<div class="notes">

La commande `initdb` doit être exécutée sous le compte
de l'utilisateur système PostgreSQL décrit dans la section précédente
(généralement **postgres**). (Les paquets d'installation fournissent
d'autres outils, mais tous utilisent `initdb`.)

**Répertoire** :

`initdb` crée les fichiers d'une nouvelle instance avec une première base de données
dans le répertoire indiqué.
Si le répertoire n'existe pas, `initdb` tentera de le créer, s'il a les droits pour le faire.
S'il existe, il doit être vide.

Typiquement, les chemins par défaut sont ceux-ci :
```sh
# chemin par défaut d'une version compilée sans --prefix
/usr/local/pgsql/data
# chemin par défaut des paquets Debian/Ubuntu (première instance v17)
/var/lib/postgresql/17/main
# chemin par défaut des paquets RPM du PGDG (première instance v17)
/var/lib/pgsql/17/data
```

Vous pouvez décider de placer le `PGDATA` n'importe où pourvu que
l'utilisateur sous lequel tourne PostgreSQL puisse y accéder ;
il y a cependant quelques conseils à connaître.

<div class="box important">

Attention : pour des raisons de sécurité et de fiabilité, les répertoires choisis pour
les données de votre instance __ne doivent pas__ être à la racine d'un point de montage. Que
ce soit le répertoire PGDATA, le répertoire `pg_wal` ou les éventuels _tablespaces_.
Si un ou plusieurs
points de montage sont dédiés à l'utilisation de PostgreSQL, positionnez toujours les
données dans un sous-répertoire, voire deux niveaux en-dessous du point de montage,
couramment : point de montage/version majeure/nom instance. Exemples :

```sh
# Si /mnt/donnees est le point de montage
/mnt/donnees/17/dbprod
# Si /var/lib/postgresql est une partition
/var/lib/postgresql/17/main
# Si /var/lib/pgsql est une partition
/var/lib/pgsql/17/data
```
</div>

À ce propos, voir :

* chapitre _[Use of Secondary File Systems](https://www.postgresql.org/docs/current/creating-cluster.html)_ ;
* le détail [des raisons techniques](https://bugzilla.redhat.com/show_bug.cgi?id=1247477#c1).

Enfin :

<div class="box warning">
__Un répertoire de données ne doit être utilisé que par une seule instance (processus) à la fois !__

PostgreSQL vérifie au démarrage qu'aucune autre instance du même serveur
n'utilise les fichiers indiqués, mais cette protection n'est pas absolue,
notamment avec des accès depuis des systèmes différents.
Faites donc bien attention à ne lancer PostgreSQL qu'une seule fois sur un
répertoire de données.
<!-- Sinon ça finit comme chez un client qui nous a appelé en urgence :
https://listes.dalibo.com/hyperkitty/list/dlb-support@listes.dalibo.com/message/VY6ZYIP6AFABR4XGPOEHOOHGA2EADIYN/ -->
</div>

Si plusieurs instances cohabitent sur le serveur, elles devront avoir chacune leur répertoire.

**Lancement de initdb** :

Voici ce qu'affiche cette commande :

<!-- mise à jour : v 17.0 -->
```default
$ initdb --data /usr/local/pgsql/data --data-checksums

The files belonging to this database system will be owned by user "postgres".
This user must also own the server process.

The database cluster will be initialized with locale "fr_FR.UTF-8".
The default database encoding has accordingly been set to "UTF8".
The default text search configuration will be set to "french".

Data page checksums are enabled.

fixing permissions on existing directory /usr/local/pgsql/data ... ok
creating subdirectories ... ok
selecting dynamic shared memory implementation ... posix
selecting default "max_connections" ... 100
selecting default "shared_buffers" ... 128MB
selecting default time zone ... Europe/Paris
creating configuration files ... ok
running bootstrap script ... ok
performing post-bootstrap initialization ... ok
syncing data to disk ... ok

initdb: warning: enabling "trust" authentication for local connections
initdb: hint: You can change this by editing pg_hba.conf or using the option -A, or --auth-local and --auth-host, the next time you run initdb.

Success. You can now start the database server using:

    pg_ctl -D /usr/local/pgsql/data -l logfile start
```

Nous pouvons ainsi suivre les actions de `initdb` :

  * détection de l'utilisateur, de l'encodage et de la locale ;
  * prise en compte des sommes de contrôle ;
  * création ou vérification des droits du répertoire PGDATA (`/usr/local/pgsql/data` ici) ;
  * création des sous-répertoires ;
  * création et modification des fichiers de configuration ;
  * exécution du script bootstrap ;
  * synchronisation sur disque ;
  * affichage de quelques informations supplémentaires
(noter les droits par défaut très laxistes d'une version compilée).

**Authentification par défaut** :

Il est possible de changer la méthode d'authentification par défaut
avec les paramètres en ligne de commande `--auth`, `--auth-host` et
`--auth-local`. Les options `--pwprompt` ou `--pwfile` permettent d'assigner un
mot de passe à l'utilisateur **postgres**.

**Sommes de contrôle** :

Il est conseillé d'activer les sommes de contrôle des fichiers de données
avec l'option `--data-checksums`.
Ces sommes de contrôle sont vérifiées à chaque lecture d'un bloc. Leur activation
est chaudement recommandée pour détecter une corruption physique le plus tôt
possible. Tous les processeurs Intel/AMD pas trop anciens supportent le jeu d'instruction SSE 4.2
(voir dans `/proc/cpuinfo`), il ne devrait pas y avoir d'impact notable sur les
performances. Les journaux générés seront cependant plus nombreux.
<!--
Stricto censu c'est plus dû au wal_log_hints qui affectent le checksum donc forcent un full page write
Réf : https://www.postgresql.org/message-id/flat/8b4e75f8275918fcfdfdf1710a14e0672798e865.camel%40credativ.de#1cc070e736c783f28c3951ee6bd809da
cd src/backend/storage/page/README , ligne 36
-->
Il est possible de vérifier, activer ou désactiver toutes les sommes de contrôle
grâce à l'outil `pg_checksums`.
Cependant, l'opération d'activation sur une instance existante impose un arrêt
et une réécriture complète des fichiers.
(L'opération était même impossible avant PostgreSQL 12.)  <!-- historique -->
Pensez-y donc dès l'installation, y compris si vous installez par les paquets.

**Emplacement des journaux** :

L'option `--waldir` indique l'emplacement des journaux de transaction.
Par défaut, ils sont dans le sous-répertoire `pg_wal/` dans le PGDATA.
Si vous désignez un autre répertoire, `pg_wal` sera un lien symbolique vers
le répertoire désigné.

**Taille des segments** :

Les fichiers des journaux de transaction ont une taille par défaut de 16 Mo.
Augmenter leur taille avec `--wal-segsize`
n'a d'intérêt que pour les très grosses installations
générant énormément de journaux pour optimiser leur archivage.

</div>

---

### Lancement et arrêt

<div class="slide-content">

  * Avec le script de l'OS (recommandé) ou `pg_ctl` :

```bash
systemctl [action] postgresql     # systemd
/etc/init.d/postgresql [action]   # SysV Init
service postgresql [action]       # idem
…
```
```bash
$ pg_ctl --pgdata /usr/local/pgsql/data --log logfile [action]
         --mode [smart|fast|immediate]
```

  * `[action]` dépend du besoin :
    + `start` / `stop` / `restart`
    + `reload` pour recharger la configuration
    + `status`
    + `promote`, `logrotate`, `kill`…

</div>

<div class="notes">

**(Re)démarrage et arrêt** :

La méthode recommandée est d'utiliser un script de démarrage adapté à l'OS,
(voir plus bas les outils les commandes `systemd` ou celles propres à Debian) ,
surtout si l'on a installé PostgreSQL par les paquets.
Au besoin, des scripts d'exemple existent dans le répertoire des sources
(`contrib/start-scripts/`[](https://github.com/postgres/postgres/tree/master/contrib/start-scripts))
pour Linux, FreeBSD et macOS. Ces scripts sont à
exécuter en tant qu'utilisateur **root**.
Sinon, il est possible d'exécuter `pg_ctl` avec l'utilisateur créé précédemment.

Les deux méthodes partagent certaines des actions présentées ci-dessus :
`start`, `stop`, `restart` (aux sens évidents), ou  `reload` (pour recharger la configuration
sans redémarrer PostgreSQL ni couper les connexions).

L'option `--mode` permet de préciser le mode d'arrêt parmi
les trois disponibles :

  * `smart` : pour vider le cache de PostgreSQL sur disque,
interdire de nouvelles connexions et attendre la déconnexion
des clients et d'éventuelles sauvegardes ;
  * `fast` (par défaut) : pour vider le cache sur disque et déconnecter
les clients sans attendre (les transactions en cours sont annulées) ;
  * `immediate` : équivalent à un arrêt brutal : tous les processus serveur sont
tués et donc, au redémarrage, le serveur devra rejouer ses journaux de
transactions.

**Rechargement de la configuration** :

Pour recharger la configuration après changement du paramétrage, la commande :
```bash
pg_ctl reload -D /repertoire_pgdata
```
est équivalente à cet ordre SQL :
```sql
SELECT pg_reload_conf() ;
```

Il faut aussi savoir
que quelques paramètres nécessitent un redémarrage de PostgreSQL et
non un simple rechargement, ils sont indiqués dans les commentaires de
`postgresql.conf`.

</div>

---


## Installation à partir des paquets Linux

---

### Paquets

<div class="slide-content">

  * Packages Debian
  * Packages RPM

</div>

<div class="notes">

Pour une utilisation en environnement de production, il est généralement
préférable d'installer les paquets binaires préparés spécialement pour la
distribution utilisée. Les paquets sont préparés par des personnes
différentes, suivant les recommendations officielles de la distribution. Il y a
donc des différences, parfois importantes, entre les paquets.

</div>

---

### Paquets Debian officiels

<div class="slide-content">

  * Nombreux paquets disponibles :
    + serveur, client, contrib, docs
    + extensions, outils
  * `apt install postgresql-<version majeure>`
    + installe les binaires
    + crée l'utilisateur **postgres**
    + exécute `initdb`
    + démarre le serveur
</div>

<div class="notes">

Sur Debian et les versions dérivées (Ubuntu notamment), l'installation de
PostgreSQL a été découpée en plusieurs paquets (ici pour la version majeure 17) :

  * le serveur : `postgresql-17` ;
  * les clients : `postgresql-client-17` ;
  * la documentation : `postgresql-doc-17`.
<!-- postgresql-contrib-<version majeure> : jusque PostgreSQL 9.6 ; inclus ensuite -->

Ce paquet correspond toujours à la dernière version mineure trimestrielle (par exemple 17.1).

La version majeure dans le nom des paquets (`9.6`,`10`,`13`,`17`…)
permet d'installer plusieurs versions majeures sur le même
serveur physique ou virtuel.
<div class="box tip">
Par défaut, sans autre dépôt, une seule version majeure sera disponible
dans une version de distribution. Par exemple, `apt install postgresql`
sur Debian 12 installera en fait `postgresql-15` (il est en dépendance).
</div>

Il existe aussi des paquets pour les outils, les extensions, etc. Certains
langages de procédures stockées sont disponibles dans des paquets séparés :

  * PL/python dans `postgresql-plpython3-17`
  * PL/perl dans `postgresql-plperl-17`
  * PL/Tcl dans `postgresql-pltcl-17`
  * etc.

Pour compiler des outils liés à PostgreSQL, il est recommandé d'installer
également les bibliothèques de développement qui font partie du paquet
`postgresql-server-dev-17`.

Quand le paquet `postgresql-17` est installé, plusieurs opérations sont immédiatement exécutées :

  * téléchargement du paquet (dans la dernière version mineure) ;
  * installation des binaires contenus dans le paquet ;
  * création de l'utilisateur **postgres** (s'il n'existe pas déjà) ;
  * paramétrage d'une première instance nommée `main` ;
  * création du répertoire des données, lancement de l'instance.

Les exécutables sont installés dans :
```default
/usr/lib/postgresql/17/bin
```

Chaque instance porte un nom, qui se retrouve dans le paramètre
`cluster_name`, et permet d'identifier les processus dans un `ps` ou un
`top`. Le nom de la première instance de chaque version majeure est
par défaut `main`. Pour cette instance :

  * les données sont dans :
```default
/var/lib/postgresql/17/main
```
  * les fichiers de configuration (pas tous ! certains restent dans le
répertoire des données) sont dans :
```default
/etc/postgresql/17/main
```
  * les traces sont gérées par l'OS sous ce nom :
```default
/var/log/postgresql/postgresql-17-main.log
```
  * un fichier PID, la socket d'accès local, et l'espace de travail temporaire
des statistiques d'activité figurent dans `/var/run/postgresql`.

Tout ceci vise à respecter le plus possible la norme
[FHS](https://fr.wikipedia.org/wiki/Filesystem_Hierarchy_Standard)
(_Filesystem Hierarchy Standard_).

En cas de mise à
jour d'un paquet, le serveur PostgreSQL est redémarré après mise à
jour des binaires.

</div>

---

### Paquets Debian : spécificités

<div class="slide-content">

  * Plusieurs versions majeures installables
  * Wrappers/scripts pour la gestion des différentes instances :
    + `pg_lsclusters`
    + `pg_ctlcluster`
      + ou : `systemctl stop|start postgresql-15@main`
    + `pg_createcluster`
    + etc.
  * Respect de la FHS
  * Configuration dans `/etc/postgresql/`

</div>

<div class="notes">

Numéroter les paquets permet d'installer plusieurs versions majeures de PostgreSQL
(mais chacune seulement dans sa dernière version mineure) au besoin sur le même système,
si les dépôts les contiennent.

Les mainteneurs des paquets Debian ont écrit des scripts pour
faciliter la création, la suppression et la gestion de différentes instances
sur le même serveur. Les principaux sont :

  * `pg_lsclusters` liste les instances ;
  * `pg_createcluster <version majeure> <nom instance>` crée une instance
de la version majeure et du nom voulu ;
  * `pg_dropcluster <version majeure> <nom instance>` détruit l'instance désignée ;
  * `/etc/postgresql-common/createcluster.conf` permet de centraliser
les paramètres par défaut des instances ;
  * la gestion d'une instance est réalisée avec la commande `pg_ctlcluster` :
```bash
pg_ctlcluster <version majeure> <nom instance> start|stop|reload|status|promote
```

Ce dernier script interagit avec systemd, qui peut être utilisé pour arrêter
ou démarrer séparément chaque instance.
Ces deux commandes sont équivalentes :

```bash
sudo pg_ctlcluster 17 main start
sudo systemctl start postgresql@17-main
```

</div>

---

### Paquets Debian communautaires

<div class="slide-content">

  * La communauté met des paquets Debian à disposition :
    + <https://apt.postgresql.org>
  * Synchrone avec le projet PostgreSQL
  * Ajout du dépôt dans `/etc/apt/sources.list.d/pgdg.list`
  * Utilisation chaudement conseillée

</div>

<div class="notes">

La distribution Debian préfère des paquets testés et validés, y compris sur
des versions assez anciennes, que d'adopter la dernière version dès qu'elle
est disponible.
Par exemple, Debian 11 ne contiendra jamais que PostgreSQL 13 et ses versions mineures,
et Debian 12 ne contiendra que PostgreSQL 15.

Pour faciliter les mises à jour, la communauté PostgreSQL met à
disposition son propre dépôt de paquets Debian. Elle en assure le maintien et
le support.
Les paquets de la communauté ont la même provenance et le même contenu
que les paquets officiels Debian, avec d'ailleurs les mêmes mainteneurs.
La seule différence est que `apt.postgresql.org` est mis à jour plus fréquemment,
en liaison directe avec la communauté PostgreSQL,
et contient beaucoup plus d'outils et extensions.

Le [wiki](https://wiki.postgresql.org/wiki/Apt) indique quelle est la procédure,
qui peut se résumer à :

```bash
sudo apt install -y postgresql-common
sudo /usr/share/postgresql-common/pgdg/apt.postgresql.org.sh
```
```default
…
Setting up postgresql-common (248) ...
Creating config file /etc/postgresql-common/createcluster.conf with new version
Building PostgreSQL dictionaries from installed myspell/hunspell packages...
Removing obsolete dictionary files:
Created symlink /etc/systemd/system/multi-user.target.wants/postgresql.service → /lib/systemd/system/postgresql.service.
Processing triggers for man-db (2.11.2-2) ...
This script will enable the PostgreSQL APT repository on apt.postgresql.org on
your system. The distribution codename used will be bookworm-pgdg.
Press Enter to continue, or Ctrl-C to abort.
```

Si l'on continue, ce dépôt sera ajouté au système (ici sous Debian 12) :
```default
$ cat /etc/apt/sources.list.d/pgdg.sources
Types: deb
Architectures: amd64
URIs: https://apt.postgresql.org/pub/repos/apt
Suites: bookworm-pgdg
Components: main
Signed-By: /usr/share/postgresql-common/pgdg/apt.postgresql.org.gpg
```

et la version choisie de PostgreSQL sera immédiatement installable :
```bash
sudo apt install postgresql-17
```
</div>

---

### Paquets Red Hat communautaires : yum.postgresql.org

<!-- on parle tout de suite des paquets communautaires
car ceux par défaut ne sont pas une option, contrairement à Debian
-->

<div class="slide-content">

  * Préférer les paquets distribués par la communauté :
    + <https://yum.postgresql.org/>
    + <https://yum.postgresql.org/howto/>
    + plus complets que les Appstream
  * Ajout du dépôt comme paquet RPM

</div>

<div class="notes">

Les versions majeures de Red Hat et de ses dérivés
(Rocky Linux, AlmaLinux, Fedora, CentOS, Scientific Linux…) sont très espacées,
et la version par défaut de PostgreSQL n'est parfois plus supportée.
Même les versions disponibles en AppStream
(avec `dnf module`) sont parfois en retard ou ne contiennent que certaines
versions majeures.
<!-- 2022/05 : Rocky8 par défaut v10 , v13 dispo ;
 2023/08 : il y a la v15 aussi mais pas la 14
 https://blog.crowncloud.net/post/installing-postgresql-in-rocky-linux-8/
 Rocky 9 : 13 par défaut, 15 dispo
 FIXME : à mettre à jour
-->
Les dépôts de la communauté sont donc fortement conseillés.
Ils contiennent aussi beaucoup plus d'utilitaires, toutes les versions majeures
supportées de PostgreSQL simultanément, et collent au plus près des versions publiées
par la communauté.

Ce dépôt convient pour les dérivés de Red Hat comme Fedora, CentOS, Rocky Linux…

</div>

---

### Paquets Red Hat communautaires : installation

<!-- exemple avant la théorie , il y a le TP de toute façon -->

<div class="slide-content">

```bash
sudo dnf install -y \
     https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
sudo dnf -qy module disable postgresql

sudo dnf install -y postgresql17-server
# dont : utilisateur postgres

sudo /usr/pgsql-17/bin/postgresql-17-setup initdb
sudo systemctl enable postgresql-17
sudo systemctl start postgresql-17
```

</div>

<div class="notes">

L'installation de la configuration du dépôt de la communauté est très simple.
Les commandes peuvent même être générées en fonction des versions sur
[https://www.postgresql.org/download/linux/redhat/](https://www.postgresql.org/download/linux/redhat/).
L'exemple ci-dessus installe PostgreSQL 17 sur Rocky 9.

</div>

---

### Paquets Red Hat communautaires : spécificités

<div class="slide-content">

  * Paquets séparés serveur, client, contrib
  * `/usr/pgsql-XX/bin`: binaires
  * Initialisation manuelle (`postgresql-17-setup initdb`)
    + vers : `/var/lib/pgsql/XX/data`
  * Gestion par systemd
  * Particularités :
    + plusieurs versions majeures installables
    + configuration : dans le répertoire de données

</div>

<div class="notes">

Sous Red Hat et les versions dérivées,
les dépôts communautaires ont été découpés en plusieurs paquets,
disponibles pour chacune des versions majeures supportées :

  * le serveur : `postgresqlXX-server` ;
  * les clients : `postgresqlXX` ;
  * les modules contrib : `postgresqlXX-contrib` ;
  * la documentation : `postgresqlXX-docs`.

où XX est la version majeure (par exemple `13` ou `17`).

Il existe aussi des paquets pour les outils, les extensions, etc. Certains
langages de procédures stockées sont disponibles dans des paquets séparés :

  * PL/python dans `postgresqlXX-plpython3` ;
  * PL/perl dans `postgresqlXX-plperl` ;
  * PL/Tcl dans `postgresqlXX-pltcl` ;
  * etc.

Pour compiler des outils liés à PostgreSQL, il est recommandé d'installer
également les bibliothèques de développement qui font partie du paquet
`postgresqlXX-devel`.

Ce nommage sous-entend qu'il est possible d'installer plusieurs versions
majeures sur le même serveur physique ou virtuel. Les exécutables sont
installés dans le répertoire `/usr/pgsql-XX/bin`, les traces dans
`/var/lib/pgsql/XX/data/log` (utilisation du `logger process` de PostgreSQL), les
données dans `/var/lib/pgsql/XX/data`. Ce dernier est le répertoire par défaut
des données, mais il est possible de le surcharger.

<div class="box tip">
Sur un système type Red Hat sans dépôt communautaire, les
noms des paquets ne comportent pas le numéro de version et installent
tous les binaires cités ici dans `/usr/bin`.
</div>

Quand le paquet serveur est installé, plusieurs opérations sont exécutées :
téléchargement du paquet, installation des binaires contenus dans le paquet,
et création de l'utilisateur **postgres** (s'il n'existe pas déjà).

Le
répertoire des données n'est pas créé. Cela reste une opération à
réaliser par la personne qui a installé PostgreSQL sur le serveur.
Lancer le script `/usr/psql-XX/bin/postgresqlXX-setup` en tant que **root** :

```shell
PGSETUP_INITDB_OPTIONS="--data-checksums" \
 /usr/pgsql-17/bin/postgresql-17-setup initdb
```

<div class="box tip">
Plutôt que de respecter la
norme FHS (_Filesystem Hierarchy Standard_), les mainteneurs ont fait
le choix de respecter l'emplacement des fichiers utilisé par défaut
par les développeurs PostgreSQL. La configuration de l'instance
(`postgresql.conf` entre autres) est donc directement dans le PGDATA.
</div>

Pour installer plusieurs instances, il faudra créer manuellement des services
systemd différents. <!-- détail dans annexe d'installation -->

En cas de mise à jour d'un paquet, le serveur PostgreSQL n'est pas redémarré
après mise à jour des binaires. <!-- FIXME : à vérifier ? -->

</div>

---


## Industrialisation avec pglift

---

### pglift : présentation rapide

<!-- 
3 slides de pub d'un produit Dalibo. Se limiter aux exemples et teasing.
-->
<div class="slide-content">
  * Déploiement et _infrastructure as code_ :
    + ligne de commande
    + collections Ansible

  * Couverture fonctionnelle : 
    + Sauvegardes avec pgBackRest
    + Supervision avec Prometheus
    + Administration avec temBoard
    + Analyse avec PoWA
    + Haute disponibilité avec Patroni
    + Intégration système avec `systemd` ou `rsyslog`

</div>

<div class="notes">

[pglift] est un outil permettant de
déployer et d'exploiter PostgreSQL à grande échelle. Le projet fournit à la fois
une interface en ligne de commande pour gérer le cycle de vie des instance et
une collection de modules Ansible pour piloter une _infrastructure-as-code_ dans
un contexte de production.

L'élément fondamental de pglift est l'instance. Celle-ci est constituée
d'une instance PostgreSQL et inclut des composants satellites, facultatifs,
permettant d'exploiter PostgreSQL à grande échelle. Dans sa version 1.0,
pglift supporte les composants suivants : 

**[pgBackRest]** permet de prendre en charge les
sauvegardes physiques PITR (_Point In Time Recovery_) de PostgreSQL.

**[postgres_exporter]**
est un service de supervision permettant de remonter des informations à l'outil
de surveillance [Prometheus].

**[temBoard]** est une console web de
supervision et d'administration dédiée aux instances PostgreSQL.

**[PoWA]** est une console web
permettant d'analyser l'activité des instances PostgreSQL en direct.

**[Patroni]** est un outil
permettant de construire un agrégat d'instances PostgreSQL résilient offrant un
service de haute disponibilité.

Tous les composants satellites supportés par pglift sont des logiciels libres.
Le projet pglift est lui aussi nativement open source, sous licence GPLv3. Son
développement se passe en public sur <https://gitlab.com/dalibo/pglift/> pour
l'API Python et l'interface en ligne de commande et sur
<https://gitlab.com/dalibo/pglift-ansible/> pour la collection Ansible
`dalibo.pglift`.


**Références** :

  * [Présentation sur le blog Dalibo](https://blog.dalibo.com/2023/10/17/pglift-intro.html) (Denis Laxalde)
  * [Documentation](https://pglift.readthedocs.io/)

</div>

---

### pglift : fichier de configuration


```yaml
prefix: /srv # fichier /etc/pglift/settings.yaml
postgresql:
  auth:
    host: scram-sha-256
prometheus:
  execpath: /usr/bin/prometheus-postgres-exporter
pgbackrest:
  repository:
    mode: path
    path: /srv/pgsql-backups
powa: {}
systemd: {}
rsyslog: {}
```

<div class="notes">

À coté de PostgreSQL, l’instance inclut un ensemble d'outils nécessaires à son
utilisation. L'intégration de ces outils satellites est configurée
localement via un fichier YAML `settings.yaml`.

Ce fichier définit comment les différents composants de l'instance sont
configurés, installés et exécutés. Il permet de aussi de définir quels
composants satellites facultatifs supportés par pgLift sont inclus dans
l'instance. Si un élément est listé dans ce fichier sans paramètre associé, il
sera exploité dans sa configuration par défaut. 

Dans l'exemple ci-dessus, temBoard et Patroni ne sont pas installés, et PoWA
est laissé à sa configuration par défaut.

</div>

---

### pglift : exemples de commandes

<div class="slide-content">

  * Initialisation
```bash
pglift instance create main --pgbackrest-stanza=main
```
  * Modification de configuration
```bash
pglift pgconf -i main set log_connections=on
```
  * Sauvegarde physique
```bash
pglift instance backup main
```
  * Utilisation des outils de l'instance
```bash
pglift instance exec main -- psql
pglift instance exec main -- pgbackrest info
```

</div>

<div class="notes">

**Interface impérative en ligne de commande** :

La commande suivante permet la création d'une instance pglift :

```bash
$ pglift instance create main --pgbackrest-stanza=main
INFO     initializing PostgreSQL
INFO     configuring PostgreSQL authentication
INFO     configuring PostgreSQL
INFO     starting PostgreSQL 16-main
INFO     creating role 'powa'
INFO     creating role 'prometheus'
INFO     creating role 'backup'
INFO     altering role 'backup'
INFO     creating 'powa' database in 16/main
INFO     creating extension 'btree_gist' in database powa
INFO     creating extension 'pg_qualstats' in database powa
INFO     creating extension 'pg_stat_statements' in database powa
INFO     creating extension 'pg_stat_kcache' in database powa
INFO     creating extension 'powa' in database powa
INFO     configuring Prometheus postgres_exporter 16-main
INFO     configuring pgBackRest stanza 'main' for
         pg1-path=/srv/pgsql/16/main/data
INFO     creating pgBackRest stanza main
INFO     starting Prometheus postgres_exporter 16-main
```

L'instance pglift inclut l'instance PostgreSQL ainsi que l'ensemble des
modules définis dans le fichier de configuration `settings.yaml`.
pglift gère aussi l'intégration au système avec `systemd` ou
`rsyslog` comme dans notre exemple. Tout ceci fonctionne sans privilège **root**
pour une meilleure séparation des responsabilités et une meilleure sécurité.

pglift permet de récupérer l'état d'une instance à un moment donné :

```bash
$ pglift instance get main -o json
{
  "name": "main",
  "version": "16",
  "port": 5432,
  "settings": {
    "unix_socket_directories": "/run/user/1000/pglift/postgresql",
    "shared_buffers": "1 GB",
    "wal_level": "replica",
    "archive_mode": true,
    "archive_command": "/usr/bin/pgbackrest --config-path=/etc/pgbackrest \
    --stanza=main --pg1-path=/srv/pgsql/16/main/data archive-push %p",
    "effective_cache_size": "4 GB",
    "log_destination": "syslog",
    "logging_collector": true,
    "log_directory": "/var/log/postgresql",
    "log_filename": "16-main-%Y-%m-%d_%H%M%S.log",
    "syslog_ident": "postgresql-16-main",
    "cluster_name": "main",
    "lc_messages": "C",
    "lc_monetary": "C",
    "lc_numeric": "C",
    "lc_time": "C",
    "shared_preload_libraries": "pg_qualstats, pg_stat_statements, pg_stat_kcache"
  },
  "data_checksums": false,
  "locale": "C",
  "encoding": "UTF8",
  "standby": null,
  "state": "started",
  "pending_restart": false,
  "wal_directory": "/srv/pgsql/16/main/wal",
  "prometheus": {
    "port": 9187
  },
  "data_directory": "/srv/pgsql/16/main/data",
  "powa": {},
  "pgbackrest": {
    "stanza": "main"
  }
}
```

ou de modifier l'instance :

```bash
# activation du paramètres log_connections
$ pglift pgconf -i main set log_connections=on
INFO     configuring PostgreSQL
INFO     instance 16/main needs reload due to parameter changes: log_connections
INFO     reloading PostgreSQL configuration for 16-main
log_connections: None -> True
# changement du port prometheus
$ pglift instance alter main --prometheus-port 8188
INFO     configuring PostgreSQL
INFO     reconfiguring Prometheus postgres_exporter 16-main
INFO     instance 16/main needs reload due to parameter changes: log_connections
INFO     reloading PostgreSQL configuration for 16-main
INFO     starting Prometheus postgres_exporter 16-main
$ pglift instance get main # vérification
 name  version  port  data_checksums  locale  encoding  pending_restart  prometheus  pgbackrest
 main  16       5432  False           C       UTF8      False            port: 8188  stanza: main
```

Les instances et objets PostgreSQL peuvent être manipulés à l'aide des outils
*natifs* de PostgreSQL depuis la ligne de commande :

```bash
$ pglift instance exec main -- pgbench -i bench
creating tables...
generating data (client-side)...
100000 of 100000 tuples (100%) done (elapsed 0.06 s, remaining 0.00 s)
vacuuming...
creating primary keys...
done in 0.18 s (drop tables 0.00 s, create tables 0.01 s, ... vacuum 0.04 s, primary keys 0.05 s).
$ pglift instance exec main -- pgbench bench
pgbench (16.0 (Debian 16.0-1.pgdg120+1))
starting vacuum...end.
transaction type: <builtin: TPC-B (sort of)>
scaling factor: 1
query mode: simple
number of clients: 1
number of threads: 1
maximum number of tries: 1
number of transactions per client: 10
number of transactions actually processed: 10/10
number of failed transactions: 0 (0.000%)
latency average = 1.669 ms
initial connection time = 4.544 ms
tps = 599.125277 (without initial connection time)
```

Ceci s'applique aussi à des outils tiers, par exemple avec pgBackRest :

```bash
$ pglift instance exec main -- pgbackrest info
stanza: main
    status: ok
    cipher: none

    db (current)
        wal archive min/max (16): 000000010000000000000001/000000010000000000000007

        full backup: 20231016-092726F
            timestamp start/stop: 2023-10-16 09:27:26+02 / 2023-10-16 09:27:31+02
            wal start/stop: 000000010000000000000004 / 000000010000000000000004
            database size: 32.0MB, database backup size: 32.0MB
            repo1: backup set size: 4.2MB, backup size: 4.2MB

        diff backup: 20231016-092726F_20231016-092821D
            timestamp start/stop: 2023-10-16 09:28:21+02 / 2023-10-16 09:28:24+02
            wal start/stop: 000000010000000000000007 / 000000010000000000000007
            database size: 54.5MB, database backup size: 22.6MB
            repo1: backup set size: 6MB, backup size: 1.8MB
            backup reference list: 20231016-092726F
```

Le [tutoriel de la ligne de commande](https://pglift.readthedocs.io/en/latest/tutorials/cli.html) 
de la documentation recense tous les exemples d'utilisation des commandes
usuelles de _pglift_.

**Interface déclarative avec Ansible** :

pglift comporte une collection de modules Ansible, sous l’espace de noms
`dalibo.pglift`.Voici un exemple de playbook illustrant ses capacités :

```ini
- name: Set up database instances
hosts: dbserver
tasks:
  - name: main instance
    dalibo.pglift.instance:
      name: main
      state: started
      port: 5444
      settings:
        max_connections: 100
        shared_buffers: 1GB
        shared_preload_libraries: 'pg_stat_statements, passwordcheck'
      surole_password: '{{ postgresql_surole_password }}'
      pgbackrest:
        stanza: main
        password: '{{ backup_role_password }}'
      prometheus:
        password: '{{ prometheus_role_password }}'
        port: 9186
      roles:
        - name: admin
          login: true
          password: '{{ admin_password }}'
          connection_limit: 10
          validity: '2025-01-01T00:00'
          in_roles:
            - pg_read_all_stats
            - pg_signal_backend
      databases:
        - name: main
          owner: admin
          settings:
            work_mem: 3MB
          extensions:
            - name: unaccent
              schema: public
```

Le module `dalibo.pglift.instance` permet de gérer l’instance, et les objets
reliés comme des rôles ou des bases de données. Les données sensibles peuvent
être prises en charge par 
[Ansible vault](https://docs.ansible.com/ansible/latest/vault_guide/index.html). 
Les modules Ansible permettent un contrôle plus important que la ligne de commandes,
grâce aux champs imbriqués, pouvant inclure la configuration de l’instance, des
bases de données, des extensions, etc.

Comme tout module Ansible en général, cette interface est complètement
déclarative, idempotente et sans état. Ces modules fonctionnent avec d’autres
modules Ansible, tels que `community.postgresql`. Le 
[tutoriel Ansible](https://pglift.readthedocs.io/en/latest/tutorials/ansible.html) 
de la documentation recense davantage d'exemples d'utilisation.

</div>

---


## Utiliser PostgreSQL dans un conteneur

---

### Docker

<div class="slide-content">

```bash
docker run --name pg17 -e POSTGRES_PASSWORD=mdpsuperfort -d postgres
```
```bash
docker exec -it pg17 psql -U postgres
```

  * Image officielle Docker Inc (pas PGDG !) : <https://hub.docker.com/_/postgres>
  * Très pratique pour le développement
  * Beaucoup de possibilités avancées avec `docker-compose`
  * Ne jamais lancer 2 conteneurs Docker sur un même PGDATA
  * Une base de données est-elle faite pour du docker ? <!--non, mais...-->
    + supervision système délicate

</div>

<div class="notes">

Les conteneurs comme docker et assimilés (podman, containerd, etc.) permettent
d'isoler l'installation de PostgreSQL sur un poste sans avoir à gérer de machine
virtuelle. Une fois configuré, le conteneur permet d'avoir un contexte d'exécution
de PostgreSQL identique peu importe son support. C'est idéal dans le cas de machines
« jetables », par exemple les chaînes de CI/CD.

**Exemple** :

À titre d'exemple, voici les étapes nécessaires à l'installation
d'une instance PostgreSQL sous docker.

D'abord, récupérer l'[image](https://hub.docker.com/_/postgres) de la dernière version de
PostgreSQL maintenue par la communauté _PostgreSQL docker_ :

```bash
docker pull postgres
```

La commande permet de créer et de lancer un nouveau conteneur à partir d'une
image donnée, ici `postgres:17.0`. Certaines options sont passées en paramètres
à `initdb` :

```bash
docker run \
--network net17 \
--name pg17 \
-p 127.0.0.1:16501:5432 \
--env-file password.env \
-e POSTGRES_INITDB_ARGS='--data-checksums --wal-segsize=1' \
-v '/var/lib/postgresql/docker/17/pg17':'/var/lib/postgresql/data' \
-d postgres:17.0 \
-c 'work_mem=10MB' \
-c 'shared_buffers=256MB' \
-c 'cluster_name=pg17'
```

Cette commande permet au conteneur d'être attaché à un réseau dédié. Celui-ci
doit avoir été créé au préalable avec la commande :

```bash
docker network create --subnet=192.168.122.0/24 net17
```
L'option `--name` permet de nommer le conteneur pour le rendre plus
facilement accessible.

L'option `-p` permet de faire suivre le port 5432 ouvert par le container sur le
port 16501 du serveur.

L'option `-d` permet de faire de l'image un démon en arrière-plan.

Les options `-e` définissent des variables d'environnement, moyen systématique
de passer des informations au conteneur. Ici l'option est utilisée pour le mot
de passe et certaines des options d'`initdb`. On préférera utiliser un fichier
`.env` pour `docker compose` ou `docker run --env-file` pour éviter de définir un
secret dans la ligne de commande.

L'option `--env-file` permet de passer en paramètre un fichier contenant des
variables d'environnement. Ici nous passons un fichier contenant le mot de passe
dont le contenu est le suivant :

```bash
# fichier password.env
POSTGRES_PASSWORD=mdpsuperfort
```

L'option `-v '/var/lib/postgresql/docker/17/pg17':'/var/lib/postgresql/data'`
lie un répertoire du disque sur le `PGDATA` du container : ainsi les
fichiers de la base survivront à la disparition du conteneur.

Les paramètres de PostgreSQL dans les `-c` sont transmis directement
à la ligne de commande du postmaster.

Une fois l'image téléchargée et le conteneur instancié, il est possible
d'accéder directement à psql via la commande suivante :

```bash
docker exec -it pg17 psql -U postgres
```

Ou directement via le serveur hôte s'il dispose de psql :

```bash
psql -h 127.0.0.1 -p 16501 -U postgres
```

En production, il est recommandé de dédier un serveur à chaque instance
PostgreSQL. De ce fait, docker permet de reproduire cette isolation pour des cas
d'usage de développement et prototypage où dédier une machine à PostgreSQL est
soit trop coûteux, soit trop complexe.

**Exemple avec docker compose** :

Il est évidement possible de passer par l'outil `docker compose` pour déployer
une instance PostgreSQL conteneurisée.
Voici la commande `docker run` précédente portée sous `docker compose` dans un
fichier YAML.

Fichier `docker-compose.yml`
```yaml
version: '3.3'

networks:
  net16:
    ipam:
      driver: default
      config:
        - subnet: 192.168.122.0/24

services:
    pg16:
      networks:
          - net17
      container_name: pg17
      ports:
          - '127.0.0.1:16501:5432'
      env_file:
          - password.env
      environment:
          - POSTGRES_INITDB_ARGS=--data-checksums --wal-segsize=1
      volumes:
          - /var/lib/postgresql/docker/17/pg17:/var/lib/postgresql/data'
      image: postgres:17.0
      command: [
      -c, work_mem=10MB,
      -c, shared_buffers=256MB,
      ]
```

La commande `docker compose --file docker-compose.yml up -d` permet la création,
ou le lancement, des objets définis dans le fichier `docker-compose.yml`.
L'option `-d` permet de lancer les conteneurs en arrière-plan.

```bash
# Au premier lancement
docker compose --file docker-compose.yml up -d
Creating network "postgresql_net17" with the default driver
Creating pg17 ... done 

# Aux lancements suivants
docker compose --file docker-compose.yml up -d
Starting pg17 ... done
```

Pour arrêter, il faut utiliser l'option `stop` :

```bash
docker compose --file docker-compose.yml stop
Stopping pg17 ... done
```

**Limites de l'utilisation de PostgreSQL sous docker** :

<div class="box caution">
**Ne lancez jamais 2 instances de PostgreSQL sous docker
sur le même PGDATA !**
Les sécurités habituelles de PostgreSQL ne fonctionnent pas
et les deux instances écriront toutes les deux dans les fichiers.
La corruption est garantie.  <!--  histoire vraie, gleu a dû dépanner en urgence -->
</div>

En production, si l'utilisation de PostgreSQL sous docker est de nos jours très
fréquente, ce n'est pas toujours une bonne idée.
Une base de données est l'inverse total d'une machine sans état
et jetable, et pour les performances,
il vaut mieux en première intention gonfler la
base (_scale up_) que multiplier les instances (_scale out_),
qui sont de toute façon toutes dépendantes de l'instance primaire.
Si le choix est fait de fonctionner sous docker, voire Kubernetes,
pour des raisons architecturales,
la base de données est sans doute le dernier composant à migrer.

De plus, pour les performances, la supervision au niveau système
devient très compliquée, et
le DBA est encore plus aveugle qu'avec une virtualisation classique.
L'ajout d'outillage ou d'extensions non fournies avec PostgreSQL
devient un peu plus compliquée (`docker stats` n'est qu'un bon début).

</div>

---

### Kubernetes

<!-- Slide qui est là parce que beaucoup de gens en parlent
et que ça se répand, et à usage internes pour débutants sur le sujet.
En attendant une éventuelle formation sur l'opérateur Cloud Native PG ?
Ce qui suit résume énormément ceci : https://gitlab.dalibo.info/clients/00330-dgfip/cr-projet-k8s
-->

<div class="slide-content">

* Dans les très grands environnements
  + mise à l'échelle automatique
  + complexité à assumer
* Réclame de l'expérience sur PG **et** K8s
* Réservez un _worker_ à PostgreSQL
* Quel opérateur ?
  + CrunchyData, CloudNativePG, Percona…
  + nombreux outils, HA…

</div>

<div class="notes">

La taille de certains SI, ou le besoin très important d'automatisation,
peuvent justifier le déploiement d'un outil comme Kubernetes.
Il permet entre autres l'_autoscaling_ lors de variations de charges importantes.

Comme déjà dit ci-dessus : les bases de données sont tout l'inverse
de machines jetables. Il est toutefois possible de les déployer sur Kubernetes,
mais vos bases PostgreSQL sont sans doute les derniers éléments de votre
infrastructure à migrer vers Kubernetes, si même leur criticité le permet.
En effet, PostgreSQL est plutôt adapté à une scalabilité verticale
(ajout de mémoire ou processeurs)
qu'horizontale, même si la multiplication de secondaires en lecture seule
ou du _sharding_ peut être envisageable. Par contre, des bases de données non critiques
et/ou à courte durée de vie (par exemple pour du développement), et surtout standardisées,
ont intérêt à être gérées par Kubernetes.

L'utilisation de Kubernetes apporte une couche d'abstraction supplémentaire
et demande nécessairement des connaissances à avoir sur son fonctionnement.
La manière de travailler avec PostgreSQL changera aussi. On peut par exemple
citer la modification des paramètres de configuration qui pourra, selon la
façon dont est déployée l'instance dans Kubernetes, être très différente
de la manière « traditionnelle ».
Kubernetes est une plateforme de déploiement d'applications
et ne répondra pas à votre place aux
considérations liées à PostgreSQL :

* les problèmes de performance dus au SQL ou aux disques trop lents, existeront toujours ;
* les montées de versions seront toujours à faire régulièrement ;
* comprendre les concepts essentiels de PostgreSQL (instances, bases, schéma, etc) reste obligatoire ;
* le besoin d'arbitrage entre peu de grosses instances multibases, et de nombreuses petites instances séparée, reste présent ; même si la facilité de déploiement et la légèreté de PostgreSQL favorise les instances monobases.

Même si en théorie on peut s'en passer, il est fortement conseillé
de passer par un opérateur Kubernetes dédié à PostgreSQL, un composant qui facilite énormément
l'administration de PostgreSQL dans Kubernetes. Plusieurs fournisseurs en proposent un,
chacun avec ses avantages, inconvénients et limites. Ils proposent notamment :

* des images docker toutes prêtes, basées sur les paquets du PGDG, ou des images maison,
avec ou sans outils annexes (comme le pooler pgBouncer), et plus ou moins d'extensions ;
* la mise en place de sauvegarde PITR avec des outils spécifiques ;
* une mise en place automatique de haute disponibilité, avec la construction automatique
d'instances en réplication (lecture seule) et des bascules automatiques, le tout étant
basé sur Patroni ou sur les mécanismes de Kubernetes ;
* des mécanismes de surveillance à destination de Kubernetes pour la connaissance
de la santé des instances ;
* des outils de supervision ;
* des mécanismes de mises à jour mineure et majeure ;
* et bien d'autres choses encore.

Citons notamment les opérateurs suivants : <!-- voir document DGFIP K8s , 08/2024 -->

+ postgres-operator, de Zalando (avec WAL-E et Patroni, image docker propre) ;
+ postgres-operator, de Crunchy Data (avec pgBackRest et Patroni, dépôts propres) ;
+ CloudNativePG, initié à l'origine par EDB, le plus récent et
peut-être le plus « à la mode » et  « cloud native »  (avec barman) ;
+ Percona Operator for PostgreSQL (avec pgBackRest, dépôts propres) ;
+ StackGres (avec Patroni)

Pour choisir un opérateur, les points d'attention sont :

+ la licence (opérateur et images docker), car certains fournisseurs poussent bien sûr à la version payante ;
+ la disponibilité et la transparence du code source ;
+ la pérennité de l'opérateur ;
+ la documentation ;
+ la rapidité de mise à jour des versions de PostgreSQL et,
à l'inverse, le support de vieilles versions de PostgreSQL que vous seriez obligés de conserver ;
+ le [niveau de capacité](https://operatorframework.io/operator-capabilities/) de l'opérateur (Level III, IV, V) ;
+ le comportement lors des mises à jour pour limiter les interruptions de service ;
+ la facilité d'adaptation de l'image docker sous-jacente (par exemple avec une extension supplémentaire) ;
+ les outils intégrés (sauvegarde, supervision…), leur facilité d'emploi, leur intégration avec l'existant
(chaque éditeur a tendance à pousser ses outils propres) ;
+ la présence de télémétrie, rarement bienvenue.

Le sujet Kubernetes est trop vaste et évolue trop vite pour être développé ici.
Ajoutons juste qu'un conseil fréquent est de réserver un nœud
(une machine physique ou virtuelle) à chaque instance PostgreSQL,
notamment pour s'assurer que l'instance aura toujours à disposition des capacités RAM et CPU.
Dédier un espace de stockage aux bases est également important, car une base de données
est très sensible aux performances du stockage.

</div>

---


## Installation sous Windows

---

### Comment installer sous Windows ?

<div class="slide-content">

  * Un seul installeur graphique disponible, proposé par EnterpriseDB
  * Ou archive des binaires

</div>

<div class="notes">

Le portage de PostgreSQL sous Windows a justifié à lui seul le passage de la branche 7 à
la branche 8 du projet.
Le système de fichiers NTFS est obligatoire car, contrairement à la VFAT, il gère les
liens symboliques (appelés jonctions sous Windows).

L'installateur n'existe plus qu'en version 64 bits depuis PostgreSQL 11.

Étant donné la quantité de travail
nécessaire pour le développement et la maintenance de l'installeur graphique,
la communauté a abandonné l'installeur graphique qu'elle a proposé un temps.
EntrepriseDB a continué de proposer gratuitement le sien, pour la version
communautaire comme pour leur version payante.
D'autres installateurs ont été proposés par d'autres éditeurs.

Il contient le serveur PostgreSQL avec les modules contrib
ainsi que pgAdmin 4, et aussi un outil appelé StackBuilder permettant
d'installer d'autres outils comme des pilotes JDBC, ODBC, C#, ou PostGIS.

Pour installer PostgreSQL sans installateur ni compilation,
EBD propose aussi une [archive des binaires compilés](https://www.enterprisedb.com/download-postgresql-binaries), même de versions aussi anciennes que la 9.3.

</div>

---

### Installeur graphique

![Installeur graphique - bienvenue](medias/windows/installeur-edb.png)

<div class="notes">

Son utilisation est tout à fait classique. Il y a plusieurs écrans de saisie
d'informations :

  * le répertoire d'installation des binaires ;
  * le choix des outils (copie d'écran ci-dessus), notamment des outils en ligne de commande (à conserver impérativement), des pilotes et de pgAdmin 4 ;
  * le répertoire des données de la première instance ;
  * le mot de passe de l'utilisateur **postgres** ;
  * le numéro de port ;
  * la locale par défaut.

Le répertoire d'installation a une valeur
par défaut généralement convenable car il n'existe pas vraiment de raison
d'installer les binaires PostgreSQL en dehors du répertoire _Program Files_.

Par contre, le répertoire des données de l'instance PostgreSQL.
n'a pas à être dans ce même répertoire _Program Files_ !
Il est souvent modifié pour un autre disque que le disque système.

Le numéro de port est par défaut le 5432, sauf si d'autres instances sont
déjà installées. Dans ce cas, l'installeur propose un numéro de port non utilisé.

Le mot de passe est celui de l'utilisateur **postgres**
au sein de PostgreSQL. En cas de mise à jour, il faut saisir l'ancien mot de passe.
Le service lui-même et tous ses processus tourneront avec le compte système générique
**NETWORK SERVICE** (Compte de service réseau).

La commande `initdb` est exécutée pour créer le répertoire des données.
Un service est ajouté pour lancer automatiquement le serveur au démarrage
de Windows.

Un sous-menu du menu _Démarrage_ contient le nécessaire pour
interagir avec le serveur PostgreSQL, comme une icône pour recharger la configuration
et surtout pgAdmin 4, qui se lancera dans un navigateur.

L'outil StackBuilder, lancé dans la foulée, permet de télécharger
et d'installer d'autres outils :
pilotes pour différents langages (Npgsql pour C#, pgJDBC, psqlODBC),
PostGIS… installés dans le répertoire de l'utilisateur en cours.

L'installateur peut être utilisé uniquement en ligne de commande
(voir les options avec `--help`).

Cet installeur existe aussi sous macOS.
Autant il est
préférable de passer par les paquets de sa distribution Linux, autant il est
recommandé d'utiliser cet installeur sous macOS.

</div>

---


## Premiers réglages

---

### Au menu

<div class="slide-content">

  * Sécurité
  * Configuration
    + accès
    + connexions
    + mémoire
    + journaux
    + traces
    + tâches de fond
</div>

<div class="notes">

</div>

---

### Sécurité

<div class="slide-content">

  * Politique d'accès :
    + pour l'utilisateur **postgres** système
    + pour le rôle **postgres**
  * Règles d'accès à l'instance dans `pg_hba.conf`

</div>

<div class="notes">

Selon l'environnement et la politique de sécurité interne à l'entreprise, il
faut potentiellement initialiser un mot de passe pour l'utilisateur système
**postgres** :

```bash
$ passwd postgres
```

Sans mot de passe, il faudra passer par un système comme `sudo` pour
pouvoir exécuter des commandes en tant qu'utilisateur **postgres**, ce qui sera
nécessaire au moins au début.

Le fait de savoir qu'un utilisateur existe sur un serveur permet à un
utilisateur hostile de tenter de forcer une connexion par force brute. Par
exemple, ce [billet de
blog](https://blog.sucuri.net/2013/07/ssh-brute-force-the-10-year-old-attack-that-still-persists.html),
montre que l'utilisateur **postgres** est dans le top 10 des logins attaqués.

La meilleure solution pour éviter ce type d'attaque est de ne pas définir de
mot de passe pour l'utilisateur OS **postgres** et de se connecter uniquement par
des échanges de clés SSH.

Il est conseillé de ne fixer aucun mot de passe pour l'utilisateur système. Il
en va de même pour le rôle **postgres** dans l'instance. Une fois connecté au
système, nous pourrons utiliser le mode d'authentification local `peer` pour
nous connecter au rôle **postgres**. Ce mode permet de limiter la surface
d'attaque sur son instance.

En cas de besoin d'accès distant en mode superutilisateur, il sera possible de
créer des rôles supplémentaires avec des droits superutilisateur. Ces noms ne
doivent pas être facile à deviner par de potentiels attaquants. Il faut donc
éviter les rôles **admin** ou **root**.

Si vous avez besoin de créer des mots de passe, ils doivent bien sûr être longs
et complexes (par exemple en les générant avec les utilitaires `pwgen` ou
`apg`).

<div class="box warning">

Si vous avez utilisé l'installeur proposé par
EnterpriseDB, l'utilisateur système et le rôle PostgreSQL ont déjà un mot de
passe, celui demandé par l'installeur. Il n'est donc pas nécessaire de leur en
configurer un autre.

</div>

Enfin, il est important de vérifier les règles d'accès au serveur contenues
dans le fichier `pg_hba.conf`. Ces règles définissent les accès à l'instance en
se basant sur plusieurs paramètres : utilisation du réseau ou du socket
fichier, en SSL ou non, depuis quel réseau, en utilisant quel
rôle, pour quelle base de données et avec quelle méthode d'authentification.

</div>

---

### Configuration minimale

<div class="slide-content">

  * Fichier `postgresql.conf`
  * Configuration du moteur
  * Plus de 300 paramètres
  * Quelques paramètres essentiels

</div>

<div class="notes">

La configuration du moteur se fait via un seul fichier, le fichier
`postgresql.conf`. Il se trouve généralement dans le répertoire des données
du serveur PostgreSQL. Sous certaines distributions (Debian et affiliés
principalement), il est déplacé dans `/etc/postgresql/`.

Ce fichier contient beaucoup de paramètres, plus de 300, mais seuls
quelques-uns sont essentiels à connaître pour avoir une instance fiable et
performante.

</div>

---

### Précédence des paramètres

![Ordre de précédence des paramètres](medias/common/precedence-parametres.png)
\

<div class="notes">

PostgreSQL offre une certaine granularité dans sa configuration, ainsi certains
paramètres peuvent être surchargés par rapport au fichier `postgresql.conf`.
Il est utile de connaître l'ordre de précédence. Par exemple, un utilisateur
peut spécifier un paramètre dans sa session avec l’ordre `SET`, celui-ci
sera prioritaire par rapport à la configuration présente dans le fichier
`postgresql.conf`.

</div>

---

### Configuration des connexions : accès au serveur

<div class="slide-content">

  * `listen_addresses = '*'` (systématique)
  * `port = 5432`
  * `password_encryption` = `scram-sha-256` (v10+)

</div>

<div class="notes">

**Ouvrir les accès** :

Par défaut, une instance PostgreSQL n'écoute que sur l'interface de boucle locale (`localhost`) et pas sur les autres interfaces réseaux.
Pour autoriser les connexions externes à
PostgreSQL, il
faut modifier le paramètre `listen_addresses`, en général ainsi :

```ini
listen_addresses = '*'
```

La valeur `*` est un joker indiquant que PostgreSQL doit écouter sur toutes les
interfaces réseaux disponibles au moment où il est lancé. Il est aussi possible
d'indiquer les interfaces, une à une, en les séparant avec des virgules. Cette
méthode est intéressante lorsqu'on veut éviter que l'instance écoute sur une
interface donnée. Par prudence il est possible de se limiter aux interfaces destinées à
être utilisées :

```ini
listen_addresses = 'localhost, 10.1.123.123'
```

La restriction par `listen_addresses` est un premier niveau de sécurité.
Elle est complémentaire de la méthode plus fine par `pg_hba.conf`,
par les IP clientes, utilisateur et base, qu'il faudra
de toute façon déployer.
De plus, modifier `listen_addresses` impose de redémarrer l'instance.

**Port** :

Le port par défaut des connexions TCP/IP est le `5432`.
C'est la valeur traditionnelle et connue de tous les outils courants.

La modifier n'a d'intérêt que si vous voulez exécuter
plusieurs instances PostgreSQL sur le même serveur (physique ou virtuel). En
effet, plusieurs instances sur une même machine ne peuvent pas écouter sur le
même couple adresse IP et port.

Une instance PostgreSQL n'écoute jamais que sur ce seul port, et tous les
clients se connectent dessus. Il n'existe pas de notion de _listener_
ou d'outil de redirection comme sur d'autres bases de données concurrentes, du
moins sans outil supplémentaire (par exemple le pooler PgBouncer).

S'il y a plusieurs instances dans une même machine, elles devront posséder
chacune un couple adresse IP/port unique.
En pratique, il vaut mieux attribuer un port par instance.
Bien sûr, PostgreSQL refusera de démarrer s'il voit que le
port est déjà occupé.

<div class="box warning">
Ne confondez pas la connexion à `localhost` (soit `::1` en IPv6 ou `127.0.0.1` en IPv4),
qui utilise les ports TCP/IP,
et la connexion dite `local`, passant par les _sockets_ de l'OS (par défaut
`/var/run/postgresql/.s.PGSQL.5432` sur les distributions les plus courantes).
La distinction est importante dans `pg_hba.conf` notamment.
</div>

**Chiffrement des mots de passe** :

À partir de la version 10 et avant la version 14, le paramètre
`password_encryption` est à modifier dès l'installation. Il définit
l'algorithme de chiffrement utilisé pour le stockage des mots de passe.  La
valeur `scram-sha-256` permettra d'utiliser la nouvelle norme, plus sécurisée
que l'ancien `md5`. Ce n'est plus nécessaire à partir de la version 14 car
c'est la valeur par défaut. Avant toute modification, vérifiez quand même que
vos outils clients sont compatibles. Au besoin, vous pouvez revenir à `md5`
pour un utilisateur donné.
<!--  plus de détail dans module F -->

</div>

---

### Configuration du nombre de connexions

<div class="slide-content">

  * `max_connections = 100`
  * 1 connexion = 1 processus serveur
  * Compromis entre
    + CPU / nombre requêtes actives / RAM / complexité
  * Danger si trop haut !
    + performances (même avec des connexions inactives)
    + risque de saturation
  * Possibilité de réserver quelques connexions pour l'administration
  <!-- trop long pour le slide, cf texte -->

</div>

<div class="notes">

Le nombre de connexions simultanées est limité par le paramètre
`max_connections`. Dès que ce nombre est atteint, les connexions suivantes sont
refusées avec un message d'erreur, et ce jusqu'à ce qu'un utilisateur connecté
se déconnecte.

<div class="box tip">
`max_connections` vaut par défaut 100, et c'est généralement suffisant
en première intention.
</div>

Noter qu'il existe un paramètre `superuser_reserved_connections`
(à 3 par défaut) qui réserve quelques connexions au superutilisateur
pour qu'il puisse se connecter malgré une saturation.
Depuis PostgreSQL 16, il existe un autre paramètre nommé `reserved_connections`,
(à 0 par défaut) pour réserver quelques connexions aux utilisateurs
à qui l'on aura attribué un rôle spécifique, nommé `pg_use_reserved_connections`.
Ce peut être utile pour des utilisateurs non applicatifs
(supervision et sauvegarde notamment) à qui l'on ne veut ou peut pas donner le rôle
`SUPERUSER`.

Il peut être intéressant de diminuer `max_connections`
pour interdire d'avoir trop de connexions actives.
Cela permet de soulager les entrées-sorties, ou de monter `work_mem`
(la mémoire de tri).
À l'inverse, il est possible d'augmenter `max_connections`
pour qu'un plus grand nombre d'utilisateurs ou d'applications
puisse se connecter en même temps.

Au niveau mémoire, un processus consomme par défaut 2 Mo de mémoire vive.
Cette consommation peut augmenter suivant son activité.

Il faut surtout savoir qu'à chaque connexion se voit associée un processus sur
le serveur, processus qui n'est vraiment actif qu'à l'exécution d'une requête.
Il s'agit donc d'arbitrer entre :

  * le nombre de requêtes à exécuter à un instant T ;
  * le nombre de CPU disponibles ;
  * la complexité et la longueur des requêtes ;
  * et même le nombre de processus que peut gérer l'OS.

L'établissement a un certain coût également. Il faut éviter qu'une application
se connecte et se déconnecte sans cesse.

Il ne sert à rien d'autoriser des milliers de connexions s'il n'y a que
quelques processeurs, ou si les requêtes sont lourdes. Si le nombre
de requêtes réellement actives augmente fortement, le serveur peut
s'effondrer. Restreindre les connexions permet de préserver le serveur,
même si certaines connexions sont refusées.

Le paramétrage est compliqué par le fait qu'une même requête peut mobiliser
plusieurs processeurs si elle est parallélisée. Certaines requêtes
seront limitées par le CPU, d'autres par la bande passante des disques.

Enfin, même si une connexion inactive ne consomme pas de CPU et peu de RAM,
elle a tout de même un impact. En effet, une connexion active va
générer assez fréquemment ce qu'on appelle un snapshot (ou une image) de
l'état des transactions de la base.
La durée de création de ce snapshot dépend principalement du nombre de
connexions, actives ou non, sur le serveur. Donc une connexion active
consommera plus de CPU s'il y a 399 autres connexions, actives ou non, que
s'il y a 9 connexions, actives ou non. Ce comportement est
partiellement corrigé avec la version 14. Mais il vaut mieux éviter
d'avoir des milliers de connexions ouvertes « au cas où ».

Intercaler un « pooler » comme PgBouncer entre les clients et l'instance
peut se justifier dans certains cas :

  * connexions/déconnexions très fréquentes ;
  * centaines, voire milliers, de connexions généralement inactives ;
  * limitation du nombre de connexions actives avec mise en attente au niveau du pooler (sans erreur).

</div>

---

### Configuration de la mémoire partagée

<div class="slide-content">

  * `shared_buffers = (?)GB `
    + 25 % de la RAM généralement
    + max 40 % <!-- cf include -->
    + complémentaire du cache OS

</div>

<div class="notes">

**Shared buffers** :

Chaque fois que PostgreSQL a besoin de lire ou d'écrire des données, il les
charge d'abord dans son cache interne. Ce cache ne sert qu'à ça : stocker des
blocs disques qui sont accessibles à tous les processus PostgreSQL, ce qui
permet d'éviter de trop fréquents accès disques car ces accès sont lents. La
taille de ce cache dépend d'un paramètre appelé `shared_buffers`.

<!-- le texte qui suit est mutualisé avec M3 et J1 -->

</div>

---

### Configuration : mémoire des processus

<div class="slide-content">

  * `work_mem`
    + par processus, voire nœud
    + valeur très dépendante de la charge et des requêtes
    + fichiers temporaires vs saturation RAM

  * × `hash_mem_multiplier`

  * `maintenance_work_mem`

  * Pas de limite stricte à la consommation mémoire des sessions
    + Augmenter prudemment & superviser

</div>

<div class="notes">

<!-- commun B, J1, M3 -->

<!--
temp_buffers : on passe, c'est dans M3
-->
Il existe d'autres paramètres influant sur les besoins en mémoires,
moins importants pour une première approche.

</div>

---

### Configuration des journaux de transactions 1/2

<div class="slide-content">

  `fsync` = `on` (si vous tenez à vos données)

</div>

<div class="notes">

À chaque fois qu'une transaction est validée (`COMMIT`), PostgreSQL écrit les
modifications qu'elle a générées dans les journaux de transactions.

Afin de garantir la durabilité, PostgreSQL effectue des écritures synchrones
des journaux de transaction, donc une écriture physique des données sur
le disque.  Cela a un coût
important sur les performances en écritures s'il y a de nombreuses transactions
mais c'est le prix de la sécurité.

Le paramètre `fsync` permet de désactiver l'envoi
de l'ordre de synchronisation au système d'exploitation.
Ce paramètre **doit** rester à `on` en production. Dans le cas contraire, un arrêt
brutal de la machine peut mener à la perte des journaux non encore enregistrés
et à la corruption de l'instance. D'autres paramètres et techniques existent
pour gagner en performance (et notamment si certaines données peuvent être perdues)
sans pour autant risquer de corrompre l'instance.

</div>

---

\newpage <!-- à cause graphique de grande taille en-dessous -->

### Configuration des journaux de transactions 2/2

![Niveaux de cache et fsync](medias/common/caches-fsync.png){ height=660 }

<div class="notes">

Une écriture peut être soit synchrone soit asynchrone. Pour comprendre ce
mécanisme, nous allons simplifier le cheminement de l'écriture d'un bloc :

  * Dans le cas d'une écriture **asynchrone** : Un processus qui modifie un
fichier écrit en fait d'abord dans le cache
du système de fichiers du système d'exploitation (OS), cache situé en RAM
(mémoire volatile). L'OS confirme tout de suite au processus que l'écriture a
été réalisée pour lui rendre la main au plus vite : il y a donc un gain en
performance important. Cependant, le bloc ne sera écrit sur disque que plus tard
afin notamment de grouper les demandes d'écritures des autres processus,
et de réduire les déplacements des
têtes de lecture/écriture des disques, qui sont des opérations coûteuses en
temps. Entre la confirmation de l'écriture et l'écriture réelle sur les disques,
il peut se passer un certain délai : si une panne survient durant celui-ci,
les données soi-disant écrites seront perdues, car pas encore physiquement sur
le disque.

  * Dans le cas d'une écriture **synchrone** : Un processus écrit dans le cache
du système d'exploitation, puis demande explicitement à l'OS d'effectuer la
synchronisation (écriture physique) sur disque. Les blocs sont donc écrits
sur les disques immédiatement et le processus n'a la confirmation de l'écriture
qu'une fois cela fait. Il attendra donc pendant la durée de cette
opération, mais il aura la garantie que la donnée est bien présente
physiquement sur les disques. Cette synchronisation
est très coûteuse et lente (encore plus avec un disque dur classique et ses
têtes de disques à déplacer).

Un phénomène équivalent peut se produire à nouveau au niveau matériel (hors du contrôle
de l'OS) : pour gagner en performance, les constructeurs ont rajouté un
système de cache au sein des cartes RAID. L'OS (et donc le processus qui écrit)
a donc confirmation de l'écriture dès que la donnée est présente dans
ce cache, alors qu'elle n'est pas encore écrite sur
disque. Afin d'éviter la perte de donnée en cas de panne électrique, ce cache
est secouru par une batterie qui laissera le temps d'écrire le contenu du cache.
Vérifiez qu'elle est bien présente sur vos disques et vos cartes contrôleur RAID.

</div>

---

### Configuration des traces

<div class="slide-content">

  * Selon système/distribution :
    + `log_destination`
    + `logging_collector`
    + emplacement et nom différent pour `postgresql-????.log`
  * `log_line_prefix` à compléter :
    + `log_line_prefix = '%t [%p]: user=%u,db=%d,app=%a,client=%h '`
  * `lc_messages` = `C` (anglais)

</div>

<div class="notes">

PostgreSQL dispose de plusieurs moyens pour enregistrer les traces : soit il les
envoie sur la sortie des erreurs (`stderr`, `csvlog` et `jsonlog`), soit il les envoie à
syslog (`syslog`, seulement sous Unix), soit il les envoie au journal des
événements (`eventlog`, sous Windows uniquement). Dans le cas où les traces
sont envoyées sur la sortie des erreurs, il peut récupérer les messages via
un démon appelé _logger process_ qui va enregistrer les messages dans des
fichiers. Ce démon s'active en configurant le paramètre `logging_collector` à
`on`.

Tout cela est configuré par défaut différemment selon le système et la
distribution. Red Hat active `logging_collector` et PostgreSQL dépose ses
traces dans des fichiers journaliers `$PGDATA/log/postgresql-<jour de la semaine>.log`. Debian
utilise `stderr` sans autre paramétrage et c'est le système qui dépose les
traces dans `/var/log/postgresql/postgresql-VERSION-nominstance.log`.
Les deux variantes fonctionnent. En fonction des habitudes et contraintes
locales, il est possible de préférer et d'activer l'une ou l'autre.

L'entête de chaque ligne des traces doit contenir au moins la date et l'heure
exacte (`%t` ou `%m` suivant la précision désirée) : des traces sans date et
heure ne servent à rien. Des entêtes complets sont suggérés par la documentation
de l'analyseur de log pgBadger :
```ini
log_line_prefix = '%t [%p]: [%l-1] db=%d,user=%u,app=%a,client=%h '
```

Beaucoup d'utilisateurs français récupèrent les traces de PostgreSQL en
français. Bien que cela semble une bonne idée au départ, cela se révèle
être souvent un problème. Non pas à cause de la qualité de la traduction,
mais plutôt parce que les outils de traitement des traces fonctionnent
uniquement avec des traces en anglais. Même un outil comme pgBadger,
pourtant écrit par un Français, ne sait pas interpréter des traces en
français. De plus, la moindre recherche sur Internet ramènera plus de liens
si le message est en anglais. Positionnez donc `lc_messages` à `C`.

</div>

---

### Configuration des tâches de fond

<div class="slide-content">

Laisser ces deux paramètres à `on` :

  * `autovacuum`
  * `track_counts`

</div>

<div class="notes">

En dehors du _logger process_, PostgreSQL dispose d'autres tâches de fond.

Les processus autovacuum jouent un rôle important pour disposer de bonnes
performances : ils empêchent une fragmentation excessive des tables et index, et
mettent à jour les statistiques sur les données (statistiques servant à
l'optimiseur de requêtes).

La récupération des statistiques sur l'activité permet le bon fonctionnement de
l'autovacuum et donne de nombreuses informations importantes à l'administrateur
de bases de données.

Ces deux tâches de fond devraient toujours être activés.

</div>

---

### Se faciliter la vie

<div class="slide-content">

  * Création automatique de configuration
    + pgtune et <https://pgtune.leopard.in.ua/>
    + <http://pgconfigurator.cybertec.at/>
  * Documentation et analyse de configuration
    + <https://postgresqlco.nf>

</div>

<div class="notes">

pgtune existe en plusieurs versions. La version en ligne de commande va détecter
automatiquement le nombre de CPU et la quantité de RAM, alors que la version web
nécessitera que ces informations soient saisies. Suivant le type d'utilisation,
pgtune proposera une configuration adaptée. Cette configuration n'est évidemment
pas forcément optimale par rapport à vos applications, tout
simplement parce qu'il ne connaît que les ressources et le type d'utilisation,
mais c'est généralement un bon point de départ.

pgconfigurator est un outil plus récent, un peu plus graphique, mais il remplit
exactement le même but que pgtune.

Enfin, le site [postgresql.co.nf](https://postgresqlco.nf) est un peu particulier. C'est en quelque
sorte une encyclopédie sur les paramètres de PostgreSQL, mais il est aussi
possible de lui faire analyser une configuration. Après analyse, des
informations supplémentaires seront affichées pour améliorer cette
configuration, que ce soit pour la stabilité du serveur comme pour ses
performances.

</div>

---


## Mise à jour

---

### Mise à jour

<div class="slide-content">

  * Recommandations
  * Mise à jour mineure
  * Mise à jour majeure
  * Mise à jour de l'OS

</div>

<div class="notes">

</div>

---

### Recommandations

<div class="slide-content">

  * Les *Release Notes*
  * Intégrité des données
  * Bien redémarrer le serveur !

</div>

<div class="notes">

Chaque nouvelle version de PostgreSQL est accompagnée d'une note expliquant les
améliorations, les corrections et les innovations apportées par cette version,
qu'elle soit majeure ou mineure. Ces notes contiennent toujours une section
dédiée aux mises à jour dans laquelle se trouvent des conseils essentiels.

Les _Releases Notes_ sont présentes dans
[l'annexe E de la documentation officielle](https://docs.postgresql.fr/current/release.html).

Les données de votre instance PostgreSQL sont toujours compatibles d'une
version mineure à l'autre. Ainsi, les mises à jour vers une version mineure
supérieure peuvent se faire sans migration de données, sauf cas exceptionnel
qui serait alors précisé dans les notes de version. Par exemple, de la 15.3
à la 15.4, il a été
[recommandé de reconstruire les index de type BRIN](https://www.postgresql.org/docs/release/15.4/)
pour prendre en compte une correction de bug les concernant.
Autre exemple : à partir de la [10.3](https://www.postgresql.org/docs/release/10.3/),
`pg_dump` a
imposé des noms d'objets qualifiés pour des raisons de sécurité, ce qui a
posé problème pour certains réimports.

Pensez éventuellement à faire une sauvegarde préalable par
sécurité.

À contrario, si la mise à jour consiste en un changement de version majeure
(par exemple, de la 11 à la 17), il est nécessaire de s'assurer que
les données seront transférées correctement sans incompatibilité. Là
encore, il est important de lire les *Releases Notes* **avant** la mise à
jour.

Le site [why-upgrade], basé sur les _release notes_,
permet de compiler les différences entre plusieurs versions
de PostgreSQL.

Dans tous les cas, pensez à bien redémarrer le serveur. Mettre à jour les
binaires ne suffit pas.

</div>

---

### Mise à jour mineure

<div class="slide-content">

  * Méthode
    + arrêter PostgreSQL
    + mettre à jour les binaires
    + redémarrer PostgreSQL
  * Pas besoin de s'occuper des données, sauf cas exceptionnel
    + bien lire les *Release Notes* pour s'en assurer

</div>

<div class="notes">

Faire une mise à jour mineure est simple et rapide.

La première action est de lire les *Release Notes* pour s'assurer qu'il n'y
a pas à se préoccuper des données. C'est généralement le cas mais il est
préférable de s'en assurer avant qu'il ne soit trop tard.

La deuxième action est de faire la mise à jour. Tout dépend de la façon dont
PostgreSQL a été installé :

  * par compilation, il suffit de remplacer les anciens binaires par les nouveaux ;
  * par paquets précompilés, il suffit d'utiliser le système de paquets
 (`apt` sur Debian et affiliés, `yum` ou `dnf` sur Red Hat et affiliés) ;
  * par l'installeur graphique, en le ré-exécutant.

Ceci fait, un redémarrage du serveur est nécessaire. Il est intéressant de
noter que les paquets Debian s'occupent directement de cette opération.
Il n'est donc pas nécessaire de le refaire.

</div>

---

### Mise à jour majeure

<div class="slide-content">

  * Bien lire les *Release Notes*
  * Bien tester l'application avec la nouvelle version
    + rechercher les régressions en terme de fonctionnalités et de performances
    + penser aux extensions et aux outils
  * Pour mettre à jour
    + mise à jour des binaires
    + et mise à jour/traitement des fichiers de données
  * 3 méthodes
    + dump/restore
    + réplication logique
    + `pg_upgrade`

</div>

<div class="notes">

Faire une mise à jour majeure est une opération complexe à préparer
prudemment.

La première action là-aussi est de lire les *Release Notes* pour bien
prendre en compte les régressions potentielles en terme de fonctionnalités
et/ou de performances. Cela n'arrive presque jamais mais c'est possible malgré
toutes les précautions mises en place.

La deuxième action est de mettre en place un serveur de tests où se trouve la
nouvelle version de PostgreSQL avec les données de production. Ce serveur sert
à tester PostgreSQL mais aussi, et même surtout, l'application. Le but est de
vérifier encore une fois les régressions possibles.

N'oubliez pas de tester les extensions non officielles, voire développées en
interne, que vous avez installées. Elles sont souvent moins bien testées.

N'oubliez pas non plus de tester les outils d'administration, de monitoring,
de modélisation. Ils nécessitent souvent une mise à jour pour être compatibles
avec la nouvelle version installée.

Une fois que les tests sont concluants, arrive le moment de la mise en production. C'est
une étape qui peut être longue car les fichiers de données doivent être
traités. Il existe plusieurs méthodes que nous détaillerons après.

</div>

---

### Mise à jour majeure par dump/restore

<div class="slide-content">

  * Méthode historique
  * Simple et sans risque
    + mais d'autant plus longue que le volume de données est important
  * Outils :
    + `pg_dumpall -g` puis `pg_dump`
    + `psql` puis `pg_restore`

</div>

<div class="notes">

Il s'agit de la méthode la plus ancienne et la plus sûre. L'idée est de
sauvegarder l'ancienne version avec l'outil de sauvegarde de la nouvelle
version. `pg_dumpall` peut suffire, mais `pg_dump` est malgré tout recommandé.

Le problème de lenteur vient surtout de la restauration.
`pg_restore` est un outil assez lent pour des volumétries importantes.
Il convient pour des volumes de données peu conséquents (au plus
une centaine de Go), au cas où l'on est patient,
ou si les autres méthodes ne sont pas possibles.

Il est cependant possible d'accélérer la restauration grâce à la parallélisation
(option `--jobs`). Ce
n'est possible que si la sauvegarde a été faite avec `pg_dump -Fd` ou `-Fc`.
Il est à noter que cette sauvegarde peut elle aussi être parallélisée
(option `--jobs` là encore).

<div class="box caution">
<!--  pas dans le slide, trop de risque de questions -->
</div>

</div>

---

### Mise à jour majeure par réplication logique

<div class="slide-content">

  * Possible entre versions 10 et supérieures
  * Bascule très rapide
  * Et retour possible

</div>

<div class="notes">

La réplication logique rend possible une migration entre deux
instances de version majeure différente avec une indisponibilité très courte.
Le principe est de répliquer une base à l'identique
vers une instance de version plus récente, alors
que la production tourne.

La réplication logique n'est disponible en natif qu'à partir de PostgreSQL version 10,
la base à migrer doit donc être en version 10 ou supérieure.
(Autrefois, on utilisait des outils de réplication par triggers, plus complexes à manier.)
Des clés primaires sur chaque table sont très fortement conseillées,
mais pas forcément obligatoires.

L'idée est d'installer la nouvelle version de PostgreSQL normalement, sur le
même serveur ou sur un autre serveur.
On déclare la réplication de l'ancienne instance vers
la nouvelle. Les utilisateurs peuvent continuer à travailler pendant le
transfert initial des données. Ils verront au pire une
baisse de performances, due à la lecture et à l'envoi des données vers le
nouveau serveur.
Une fois le transfert initial réalisé, les données
modifiées entre-temps sont transférées vers le nouveau serveur.
Toute cette opération peut s'étaler sur plusieurs jours.

Une fois les deux serveurs synchronisés, il ne reste plus qu'à
déclencher un _switchover_ (bascule) ;
puis d'attendre que les dernières données
soient répliquées, ce qui peut être très rapide ; et enfin de connecter
les applications au nouveau serveur. La réplication peut alors être
inversée pour garder l'ancienne production synchrone, permettant de
rebasculer dessus en cas de problème sans perdre les données modifiées
depuis la bascule.
Une fois acté que le nouveau serveur donne pleine
satisfaction, il suffit de débrancher la réplication logique des deux côtés.

Pour les grosses bases, il existe le même danger d'un gel brutal des lignes
comme avec `pg_restore`.

</div>

---

### Mise à jour majeure par pg_upgrade

<div class="slide-content">

  * `pg_upgrade` : fourni avec PostgreSQL
  * Prérequis : pas de changement de format des fichiers entre versions
  * Nécessite les deux versions sur le même serveur
  * Support des serveurs PostgreSQL à migrer :
    + version minimale 9.2 pour pg_upgrade v15
    + version minimale 8.4 sinon

</div>

<div class="notes">

`pg_upgrade` est certainement l'outil le plus rapide pour une mise à jour
majeure.

Il profite du fait que les formats des fichiers de données n'évolue pas,
ou de manière rétrocompatible, entre deux versions majeures. Il n'est donc
pas nécessaire de tout réécrire.

Grossièrement, son fonctionnement est le suivant. Il récupère la
déclaration des objets sur l'ancienne instance avec un `pg_dump` du schéma de
chaque base et de chaque objet global. Il intègre la déclaration des objets
dans la nouvelle instance. Il fait un ensemble de traitement sur les
identifiants d'objets et de transactions. Puis, il copie les fichiers de
données de l'ancienne instance vers la nouvelle instance. La copie est
l'opération la plus longue, mais comme il n'est pas nécessaire de reconstruire
les index et de vérifier les contraintes, cette opération est bien plus rapide
que la restauration d'une sauvegarde style `pg_dump`. Pour aller encore plus
rapidement, il est possible de créer des liens physiques à la place de
la copie des fichiers. Ceci fait, la migration est terminée.

En 2010, Stefan Kaltenbrunner et Bruce Momjian avaient mesuré qu'une base de
150 Go mettait 5 heures à être mise à jour avec la méthode historique
(sauvegarde/restauration). Elle mettait 44 minutes en mode copie et 42 secondes
en mode lien lors de l'utilisation de `pg_upgrade`.

C'est une migration physique : le problème du gel ultérieur des lignes
comme avec `pg_restore` ne se pose pas.

Vu ses performances, ce serait certainement l'outil à privilégier. Cependant,
c'est un outil très complexe et quelques bugs particulièrement méchants ont
terni sa réputation. Notre recommandation est de bien tester la mise à jour
avant de le faire en production, et uniquement sur des bases suffisamment
volumineuses dont le court délai de migration justifie l'utilisation de cet outil.

Lors du développement de la version 15, les développeurs ont supprimé certaines
vieilles parties du code, ce qui le rend à présent incompatible avec des versions très
anciennes (de la 8.4 à la 9.1, cette dernière remontant tout de même à 2012).

</div>

---

### Mise à jour de l'OS

<div class="slide-content">

Si vous migrez aussi l'OS ou déplacez les fichiers d'une instance :

  * compatibilité architecture
  * compatibilité librairies
    + réindexation parfois nécessaire
    + ex : Debian 10 et glibc 2.28

</div>

<div class="notes">

Un projet de migration PostgreSQL est souvent l'occasion de mettre à jour le
système d'exploitation. Vous pouvez également en profiter pour déplacer l'instance sur
un autre serveur à l'OS plus récent en copiant (à froid) le PGDATA.

Il faut bien sûr que l'architecture physique (32/64 bits, _big/little indian_)
reste la même. Cependant, même entre deux versions de la même distribution,
certains composants du système d'exploitation peuvent avoir
une influence, à commencer par la `glibc`. Cette librairie fondamentale de Linux définit l'ordre des
caractères, ordre utilisé dans les index de champs textes. Une incompatibilité entre deux
versions sur ce point oblige donc à reconstruire les index, sous peine
d'incohérence avec les fonctions de comparaison sur le nouveau système et de
corruption à l'écriture.

[Daniel Vérité détaille sur son blog](https://blog-postgresql.verite.pro/2018/08/30/glibc-upgrade.html)
le problème pour les mises à jour entre Debian 9 et 10, à cause de la mise à
jour de la `glibc` en version 2.28.
(Voir aussi le [wiki PostgreSQL](https://wiki.postgresql.org/wiki/Locale_data_changes).)
L'utilisation des
[collations ICU](https://blog-postgresql.verite.pro/2018/07/27/icu_ext.html)
dans les index contourne le problème, mais elles sont encore peu répandues.
Dans beaucoup de cas, la collation `C.UTF-8` propre à PostgreSQL (version 17 au moins)
peut éliminer facilement tout souci de ce genre.

Ce problème ne touche bien sûr pas les migrations ou les restaurations avec
`pg_dump`/`pg_restore` : les données sont alors transmises de manière logique,
indépendamment des caractéristiques physiques des instances source et cible,
et les index sont systématiquement reconstruits sur la machine cible.

</div>

---

<!-- fin des includes -->


## Conclusion

<div class="slide-content">

  * L'installation est simple…
  * …mais elle doit être soigneusement préparée
  * Préférer les paquets officiels
  * Attention aux données lors d'une mise à jour !
</div>

<div class="notes">

</div>

---

### Pour aller plus loin

<div class="slide-content">

  * Documentation officielle, chapitre `Installation`
  * Documentation Dalibo, pour l'installation sur Windows

</div>

<div class="notes">

Vous pouvez retrouver la documentation en ligne sur
<https://docs.postgresql.fr/current/installation.html>.

La documentation de Dalibo pour l'installation de PostgreSQL sur Windows est
disponible sur
<https://public.dalibo.com/archives/etudes/installer_postgresql_9.0_sous_windows.pdf>.

<!-- FIXME à rafraîchir ! -->

</div>

---

### Questions

<div class="slide-content">

N'hésitez pas, c'est le moment !

</div>

<div class="notes">

</div>

---

\newpage

## Travaux pratiques

<div class="box tip">
Tout ce qui suit suppose :

  * une installation sous Rocky Linux avec les paquets RPM du PDGD (yum.postgresql.org) ;
  * une instance installée avec les options par défaut ;
  * un administrateur dont le compte habituel sur la machine, non privilégié, est nommé **dalibo**.

</div>

<div class="notes">

<!-- appelé de tp-enonce.md -->

### Autorisation d'accès distant

<div class="notes">

<div class="slide-content">

  **But** : Mettre en place les accès dans `pg_hba.conf`.

</div>

> Autoriser tous les membres du réseau local à se connecter avec un mot de passe
> (autorisation en IP sans SSL) avec les utilisateurs **boulier** ou **tina**.
> Tester avec l'IP du serveur avant de demander aux voisins de tester.

</div>
<!-- appelé de tp-enonce.md   -->

### Traces maximales

<div class="notes">

<div class="slide-content">

  **But** : suivre toutes les requêtes dans les traces

</div>

> À titre pédagogique et pour alimenter un rapport pgBadger plus tard, toutes les requêtes vont être tracées.
> 
> Dans `postgresql.conf`, positionner ceci :
>
>```ini
>log_min_duration_statement = 0
>log_temp_files = 0
>log_autovacuum_min_duration = 0
>lc_messages = 'C'
>log_line_prefix = '%t [%p]: db=%d,user=%u,app=%a,client=%h '
>```

> Puis passer à `on` les paramètres suivants s'ils ne le sont pas déjà :
>
>```ini
>log_checkpoints
>log_connections
>log_disconnections
>log_lock_waits
>```
> Recharger la configuration.
> 
> Laisser une fenêtre ouverte pour voir défiler le contenu des traces.

</div>
<!-- appelé de tp-enonce.md -->

### Traces

<div class="notes">

<div class="slide-content">

  **But** : Gérer les fichiers de traces

</div>

> Quelle est la méthode de gestion des traces utilisée par défaut ?

> Paramétrer le programme interne de rotation des journaux :
>
> - modifier le fichier `postgresql.conf` pour utiliser le _logging collector_ ;
> - les traces doivent désormais être sauvegardés dans le répertoire
> `/var/lib/pgsql/traces` ;
> - la rotation des journaux doit être automatisée pour générer
> un nouveau fichier de logs toutes les 30 minutes,
> quelle que soit la quantité de logs archivés ;
> le nom du fichier devra donc comporter les minutes.
> - Tester en forçant des rotations avec la fonction `pg_rotate_logfile`.
> - Augmenter la trace (niveau `info`).

> Comment éviter l'accumulation des fichiers ?

</div>

</div>

\newpage

## Travaux pratiques (solutions)

<div class="notes">

<div class="box tip">
Tout ce qui suit suppose :

  * une installation sous Rocky Linux avec les paquets RPM du PDGD (yum.postgresql.org) ;
  * une instance installée avec les options par défaut ;
  * un administrateur dont le compte habituel sur la machine, non privilégié, est nommé **dalibo**.

</div>
<!-- appelé de tp-solution.md -->

### Autorisation d'accès distant

<div class="slide-content">

  **But** : Mettre en place les accès dans `pg_hba.conf`.

</div>

> Autoriser tous les membres du réseau local à se connecter avec un mot de passe
> (autorisation en IP sans SSL) avec les utilisateurs **boulier** ou **tina**.
> Tester avec l'IP du serveur avant de demander aux voisins de tester.

Pour tester, repérer l'adresse IP du serveur avec `ip a`, par exemple `192.168.28.1`,
avec un réseau local en `192.168.28.*`.

Ensuite, lors des appels à `psql`, utiliser `-h 192.168.28.1` au lieu de la connexion
locale ou de **localhost** :

```Bash
$ psql -h 192.168.123.180 -d entreprise -U tina
```

Ajouter les lignes suivantes dans le fichier `pg_hba.conf` :

```file
host       entreprise    tina,boulier    192.168.28.0/24  scram-sha-256
```

Il ne faut pas oublier d'ouvrir PostgreSQL aux connexions extérieures dans `postgresql.conf` :

```ini
listen_addresses = '*'
```
Cette modification néces¨site un rédémarrage

Plus prudemment, on peut juste ajouter l'adresse publique de l'instance PostgreSQL :
```ini
listen_addresses = 'localhost,192.168.28.1'
```
Il y a peut-être un _firewall_ à désactiver :

```bash
$ sudo systemctl status firewalld
$ sudo systemctl stop firewalld
```
<!-- appelé de tp-solution.md -->

### Traces maximales

<div class="slide content">

**But** : suivre toutes les requêtes dans les traces

</div>

> À titre pédagogique et pour alimenter un rapport pgBadger plus tard, toutes les requêtes vont être tracées.
> 
> Dans `postgresql.conf`, positionner ceci :
>
>```ini
>log_min_duration_statement = 0
>log_temp_files = 0
>log_autovacuum_min_duration = 0
>lc_messages = 'C'
>log_line_prefix = '%t [%p]: db=%d,user=%u,app=%a,client=%h '
>```

<div class="box warning">
Éviter de faire cela en production, surtout `log_min_duration_statement = 0` !
Sur une base très active, les traces peuvent rapidement monter à plusieurs dizaines de gigaoctets !

Dans le présent TP, il faut surveiller l'espace disque pour cette raison.
</div>

> Puis passer à `on` les paramètres suivants s'ils ne le sont pas déjà :
>
>```ini
>log_checkpoints
>log_connections
>log_disconnections
>log_lock_waits
>```
> Recharger la configuration.
> 
> Laisser une fenêtre ouverte pour voir défiler le contenu des traces.

Dans `postgresql.conf` :

```ini
log_min_duration_statement = 0
log_temp_files = 0
log_autovacuum_min_duration = 0
lc_messages='C'
log_line_prefix = '%t [%p]: db=%d,user=%u,app=%a,client=%h '
log_checkpoints = on
log_connections = on
log_disconnections = on
log_lock_waits = on
```

```sql
SELECT pg_reload_conf() ;
```
```default
 pg_reload_conf
----------------
 t
```
```sql
SHOW log_min_duration_statement ;
```
```default
 log_min_duration_statement
----------------------------
 0
```

Laisser une fenêtre ouverte avec le fichier, par exemple avec :
```bash
tail -f /var/lib/pgsql/15/data/log/postgresql-Wed.log
```
La moindre requête doit y apparaître, y compris quand
l'utilisateur effectue un simple `\d`.
<!-- appelé de tp-solution.md -->

### Traces

<div class="slide-content">

  **But** : Gérer les fichiers de traces

</div>

> Quelle est la méthode de gestion des traces utilisée par défaut ?

Par défaut, le mode de journalisation est **stderr** :

```sql
postgres@pgbench=# SHOW log_destination ;
```
```default
 log_destination
-----------------
 stderr
```

> Paramétrer le programme interne de rotation des journaux :
>
> - modifier le fichier `postgresql.conf` pour utiliser le _logging collector_ ;
> - les traces doivent désormais être sauvegardés dans le répertoire
> `/var/lib/pgsql/traces` ;
> - la rotation des journaux doit être automatisée pour générer
> un nouveau fichier de logs toutes les 30 minutes,
> quelle que soit la quantité de logs archivés ;
> le nom du fichier devra donc comporter les minutes.
> - Tester en forçant des rotations avec la fonction `pg_rotate_logfile`.
> - Augmenter la trace (niveau `info`).

Sur Red Hat/CentOS/Rocky Linux, le collecteur des traces (_logging collector_)
est activé par défaut dans `postgresql.conf`
(mais ce ne sera pas le cas sur un environnement Debian ou avec une installation compilée,
et il faudra redémarrer pour l'activer) :

```ini
logging_collector = on
```

On crée le répertoire, où **postgres** doit pouvoir écrire :

```bash
$ sudo mkdir -m700 /var/lib/pgsql/traces
$ sudo chown postgres:  /var/lib/pgsql/traces
```

Puis paramétrer le comportement du récupérateur :

```ini
log_directory = '/var/lib/pgsql/traces'
log_filename = 'postgresql-%Y-%m-%d_%H-%M.log'
log_rotation_age = 30min
log_rotation_size = 0
log_min_messages = info
```

Recharger la configuration et voir ce qui se passe dans `/var/lib/pgsql/traces` :

```bash
$ sudo systemctl reload postgresql-12
$ sudo watch -n 5 ls -lh /var/lib/pgsql/traces
```

Dans une autre fenêtre, générer un peu d'activité, avec `pgbench` ou tout simplement avec :
```sql
postgres@pgbench=# SELECT 1 ;
postgres@pgbench=# \watch 1
```

Les fichiers générés doivent porter un nom ressemblant à `postgresql-2019-08-02_16-55.log`.

Pour forcer le passage à un nouveau fichier de traces :

```sql
postgres@pgbench=# SELECT pg_rotate_logfile() ;
```

> Comment éviter l'accumulation des fichiers ?

  * La première méthode consiste à avoir un `log_filename` cyclique.
C'est le cas par défaut sur Red Hat/CentOS/Rocky Linux
avec `postgresql-%a`, qui reprend les jours de la semaine.
Noter qu'il n'est pas forcément garanti qu'un `postgresql-%H-%M.log` planifié toutes les 5 minutes
écrase les fichiers de la journée précédente. En pratique, on descend rarement en-dessous de l'heure.

  * Utiliser l'utilitaire `logrotate`, fourni avec toute distribution Linux, dont le travail est de gérer
rotation, compression et purge. Il est activé par défaut sur Debian.

  * Enfin, on peut rediriger les traces vers un système externe.

</div>
